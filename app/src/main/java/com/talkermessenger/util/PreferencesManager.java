/*
 * Copyright 2016 OpenMarket Ltd
 * Copyright 2017 Vector Creations Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.talkermessenger.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.text.TextUtils;

import com.talkermessenger.R;

import org.matrix.androidsdk.rest.model.Event;
import org.matrix.androidsdk.util.Log;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class PreferencesManager {
    public static final String SETTINGS_IS_INITIAL_SYNC_COMPLETE = "SETTINGS_IS_INITIAL_SYNC_COMPLETE";
    public static final String SETTINGS_MESSAGES_SENT_BY_BOT_PREFERENCE_KEY = "SETTINGS_MESSAGES_SENT_BY_BOT_PREFERENCE_KEY_2";
    public static final String SETTINGS_CHANGE_PASSWORD_PREFERENCE_KEY = "SETTINGS_CHANGE_PASSWORD_PREFERENCE_KEY";
    public static final String SETTINGS_VERSION_PREFERENCE_KEY = "SETTINGS_VERSION_PREFERENCE_KEY";
    public static final String SETTINGS_OLM_VERSION_PREFERENCE_KEY = "SETTINGS_OLM_VERSION_PREFERENCE_KEY";
    public static final String SETTINGS_LOGGED_IN_PREFERENCE_KEY = "SETTINGS_LOGGED_IN_PREFERENCE_KEY";
    public static final String SETTINGS_HOME_SERVER_PREFERENCE_KEY = "SETTINGS_HOME_SERVER_PREFERENCE_KEY";
    public static final String SETTINGS_IDENTITY_SERVER_PREFERENCE_KEY = "SETTINGS_IDENTITY_SERVER_PREFERENCE_KEY";
    public static final String SETTINGS_APP_TERM_CONDITIONS_PREFERENCE_KEY = "SETTINGS_APP_TERM_CONDITIONS_PREFERENCE_KEY";
    public static final String SETTINGS_PRIVACY_POLICY_PREFERENCE_KEY = "SETTINGS_PRIVACY_POLICY_PREFERENCE_KEY";
    public static final String SETTINGS_THIRD_PARTY_NOTICES_PREFERENCE_KEY = "SETTINGS_THIRD_PARTY_NOTICES_PREFERENCE_KEY";
    public static final String SETTINGS_COPYRIGHT_PREFERENCE_KEY = "SETTINGS_COPYRIGHT_PREFERENCE_KEY";
    public static final String SETTINGS_CLEAR_CACHE_PREFERENCE_KEY = "SETTINGS_CLEAR_CACHE_PREFERENCE_KEY";
    public static final String SETTINGS_CLEAR_MEDIA_CACHE_PREFERENCE_KEY = "SETTINGS_CLEAR_MEDIA_CACHE_PREFERENCE_KEY";
    public static final String SETTINGS_ENABLE_BACKGROUND_SYNC_PREFERENCE_KEY = "SETTINGS_ENABLE_BACKGROUND_SYNC_PREFERENCE_KEY";
    public static final String SETTINGS_OTHERS_PREFERENCE_KEY = "SETTINGS_OTHERS_PREFERENCE_KEY";
    public static final String SETTINGS_USER_SETTINGS_PREFERENCE_KEY = "SETTINGS_USER_SETTINGS_PREFERENCE_KEY";
    public static final String SETTINGS_CONTACT_PREFERENCE_KEYS = "SETTINGS_CONTACT_PREFERENCE_KEYS";
    public static final String SETTINGS_NOTIFICATIONS_TARGETS_PREFERENCE_KEY = "SETTINGS_NOTIFICATIONS_TARGETS_PREFERENCE_KEY";
    public static final String SETTINGS_NOTIFICATIONS_TARGET_DIVIDER_PREFERENCE_KEY = "SETTINGS_NOTIFICATIONS_TARGET_DIVIDER_PREFERENCE_KEY";
    public static final String SETTINGS_IGNORED_USERS_PREFERENCE_KEY = "SETTINGS_IGNORED_USERS_PREFERENCE_KEY";
    public static final String SETTINGS_IGNORE_USERS_DIVIDER_PREFERENCE_KEY = "SETTINGS_IGNORE_USERS_DIVIDER_PREFERENCE_KEY";
    public static final String SETTINGS_DEVICES_LIST_PREFERENCE_KEY = "SETTINGS_DEVICES_LIST_PREFERENCE_KEY";
    public static final String SETTINGS_DEVICES_DIVIDER_PREFERENCE_KEY = "SETTINGS_DEVICES_DIVIDER_PREFERENCE_KEY";
    public static final String SETTINGS_CRYPTOGRAPHY_PREFERENCE_KEY = "SETTINGS_CRYPTOGRAPHY_PREFERENCE_KEY";
    public static final String SETTINGS_CRYPTOGRAPHY_DIVIDER_PREFERENCE_KEY = "SETTINGS_CRYPTOGRAPHY_DIVIDER_PREFERENCE_KEY";
    public static final String SETTINGS_LABS_PREFERENCE_KEY = "SETTINGS_LABS_PREFERENCE_KEY";
    public static final String SETTINGS_SET_SYNC_TIMEOUT_PREFERENCE_KEY = "SETTINGS_SET_SYNC_TIMEOUT_PREFERENCE_KEY";
    public static final String SETTINGS_SET_SYNC_DELAY_PREFERENCE_KEY = "SETTINGS_SET_SYNC_DELAY_PREFERENCE_KEY";
    public static final String SETTINGS_ROOM_SETTINGS_LABS_END_TO_END_PREFERENCE_KEY = "SETTINGS_ROOM_SETTINGS_LABS_END_TO_END_PREFERENCE_KEY";
    public static final String SETTINGS_ROOM_SETTINGS_LABS_END_TO_END_IS_ACTIVE_PREFERENCE_KEY = "SETTINGS_ROOM_SETTINGS_LABS_END_TO_END_IS_ACTIVE_PREFERENCE_KEY";
    public static final String SETTINGS_PROFILE_PICTURE_PREFERENCE_KEY = "SETTINGS_PROFILE_PICTURE_PREFERENCE_KEY";
    public static final String SETTINGS_CONTACTS_PHONEBOOK_COUNTRY_PREFERENCE_KEY = "SETTINGS_CONTACTS_PHONEBOOK_COUNTRY_PREFERENCE_KEY";
    public static final String SETTINGS_USER_LANGUAGES_PREFERENCE_KEY = "SETTINGS_USER_LANGUAGES_PREFERENCE_KEY";
    public static final String SETTINGS_USER_NUMBER_PREFERENCE_KEY = "SETTINGS_USER_NUMBER_PREFERENCE_KEY";
    public static final String SETTINGS_INTERFACE_LANGUAGE_PREFERENCE_KEY = "SETTINGS_INTERFACE_LANGUAGE_PREFERENCE_KEY";
    public static final String SETTINGS_BACKGROUND_SYNC_PREFERENCE_KEY = "SETTINGS_BACKGROUND_SYNC_PREFERENCE_KEY";
    public static final String SETTINGS_ENCRYPTION_INFORMATION_DEVICE_NAME_PREFERENCE_KEY = "SETTINGS_ENCRYPTION_INFORMATION_DEVICE_NAME_PREFERENCE_KEY";
    public static final String SETTINGS_ENCRYPTION_INFORMATION_DEVICE_ID_PREFERENCE_KEY = "SETTINGS_ENCRYPTION_INFORMATION_DEVICE_ID_PREFERENCE_KEY";
    public static final String SETTINGS_ENCRYPTION_EXPORT_E2E_ROOM_KEYS_PREFERENCE_KEY = "SETTINGS_ENCRYPTION_EXPORT_E2E_ROOM_KEYS_PREFERENCE_KEY";
    public static final String SETTINGS_ENCRYPTION_IMPORT_E2E_ROOM_KEYS_PREFERENCE_KEY = "SETTINGS_ENCRYPTION_IMPORT_E2E_ROOM_KEYS_PREFERENCE_KEY";
    public static final String SETTINGS_ENCRYPTION_NEVER_SENT_TO_PREFERENCE_KEY = "SETTINGS_ENCRYPTION_NEVER_SENT_TO_PREFERENCE_KEY";
    public static final String SETTINGS_ENCRYPTION_INFORMATION_DEVICE_KEY_PREFERENCE_KEY = "SETTINGS_ENCRYPTION_INFORMATION_DEVICE_KEY_PREFERENCE_KEY";
    public static final String SETTINGS_DISPLAY_NAME_PREFERENCE_KEY = "SETTINGS_DISPLAY_NAME_PREFERENCE_KEY";
    public static final String SETTINGS_ENABLE_ALL_NOTIF_PREFERENCE_KEY = "SETTINGS_ENABLE_ALL_NOTIF_PREFERENCE_KEY";
    public static final String SETTINGS_ENABLE_THIS_DEVICE_PREFERENCE_KEY = "SETTINGS_ENABLE_THIS_DEVICE_PREFERENCE_KEY";
    public static final String SETTINGS_TURN_SCREEN_ON_PREFERENCE_KEY = "SETTINGS_TURN_SCREEN_ON_PREFERENCE_KEY";
    public static final String SETTINGS_CONTAINING_MY_DISPLAY_NAME_PREFERENCE_KEY = "SETTINGS_CONTAINING_MY_DISPLAY_NAME_PREFERENCE_KEY_2";
    public static final String SETTINGS_CONTAINING_MY_USER_NAME_PREFERENCE_KEY = "SETTINGS_CONTAINING_MY_USER_NAME_PREFERENCE_KEY_2";
    public static final String SETTINGS_MESSAGES_IN_ONE_TO_ONE_PREFERENCE_KEY = "SETTINGS_MESSAGES_IN_ONE_TO_ONE_PREFERENCE_KEY_2";
    public static final String SETTINGS_MESSAGES_IN_GROUP_CHAT_PREFERENCE_KEY = "SETTINGS_MESSAGES_IN_GROUP_CHAT_PREFERENCE_KEY_2";
    public static final String SETTINGS_INVITED_TO_ROOM_PREFERENCE_KEY = "SETTINGS_INVITED_TO_ROOM_PREFERENCE_KEY_2";
    public static final String SETTINGS_CALL_INVITATIONS_PREFERENCE_KEY = "SETTINGS_CALL_INVITATIONS_PREFERENCE_KEY_2";
    public static final String SETTINGS_MEDIA_SAVING_PERIOD_KEY = "SETTINGS_MEDIA_SAVING_PERIOD_KEY";
    public static final String SETTINGS_DATA_SAVE_MODE_PREFERENCE_KEY = "SETTINGS_DATA_SAVE_MODE_PREFERENCE_KEY";
    public static final String SETTINGS_START_ON_BOOT_PREFERENCE_KEY = "SETTINGS_START_ON_BOOT_PREFERENCE_KEY";
    public static final String SETTINGS_INTERFACE_TEXT_SIZE_KEY = "SETTINGS_INTERFACE_TEXT_SIZE_KEY";
    //not used by new settings
    public static final String SETTINGS_NOTIFICATION_RINGTONE_SELECTION_PREFERENCE_KEY = "SETTINGS_NOTIFICATION_RINGTONE_SELECTION_PREFERENCE_KEY";
    private static final String LOG_TAG = PreferencesManager.class.getSimpleName();
    private static final String SETTINGS_HIDE_READ_RECEIPTS_KEY = "SETTINGS_HIDE_READ_RECEIPTS_KEY";
    private static final String SETTINGS_ALWAYS_SHOW_TIMESTAMPS_KEY = "SETTINGS_ALWAYS_SHOW_TIMESTAMPS_KEY";
    private static final String SETTINGS_12_24_TIMESTAMPS_KEY = "SETTINGS_12_24_TIMESTAMPS_KEY";
    private static final String SETTINGS_DISABLE_MARKDOWN_KEY = "SETTINGS_DISABLE_MARKDOWN_KEY";
    private static final String SETTINGS_DONT_SEND_TYPING_NOTIF_KEY = "SETTINGS_DONT_SEND_TYPING_NOTIF_KEY";
    private static final String SETTINGS_HIDE_JOIN_LEAVE_MESSAGES_KEY = "SETTINGS_HIDE_JOIN_LEAVE_MESSAGES_KEY";
    private static final String SETTINGS_HIDE_AVATAR_DISPLAY_NAME_CHANGES_MESSAGES_KEY = "SETTINGS_HIDE_AVATAR_DISPLAY_NAME_CHANGES_MESSAGES_KEY";
    private static final String SETTINGS_MEDIA_SAVING_PERIOD_SELECTED_KEY = "SETTINGS_MEDIA_SAVING_PERIOD_SELECTED_KEY";
    private static final String SETTINGS_PIN_UNREAD_MESSAGES_PREFERENCE_KEY = "SETTINGS_PIN_UNREAD_MESSAGES_PREFERENCE_KEY";
    private static final String SETTINGS_PIN_MISSED_NOTIFICATIONS_PREFERENCE_KEY = "SETTINGS_PIN_MISSED_NOTIFICATIONS_PREFERENCE_KEY";
    private static final String SETTINGS_DISABLE_PIWIK_SETTINGS_PREFERENCE_KEY = "SETTINGS_DISABLE_PIWIK_SETTINGS_PREFERENCE_KEY";
    private static final String SETTINGS_USE_JITSI_CONF_PREFERENCE_KEY = "SETTINGS_USE_JITSI_CONF_PREFERENCE_KEY";
    private static final String SETTINGS_USE_MATRIX_APPS_PREFERENCE_KEY = "SETTINGS_USE_MATRIX_APPS_PREFERENCE_KEY";
    //message notification ringtone
    private static final String SETTINGS_NOTIFICATION_RINGTONE_PREFERENCE_KEY = "SETTINGS_NOTIFICATION_RINGTONE_PREFERENCE_KEY";
    //group notification ringtone
    private static final String SETTINGS_GROUP_NOTIFICATION_RINGTONE_PREFERENCE_KEY = "SETTINGS_GROUP_NOTIFICATION_RINGTONE_PREFERENCE_KEY";
    //show preview (message)
    private static final String SETTINGS_SHOW_PREVIEW_PREFERENCE_KEY = "SETTINGS_SHOW_PREVIEW_PREFERENCE_KEY";

    //Chat Design
    private static final String SETTINGS_BACKGROUND_CHAT_PICTURE_PREFERENCE_KEY = "SETTINGS_BACKGROUND_CHAT_PICTURE_PREFERENCE_KEY";
    private static final String SETTINGS_IN_CHAT_COLOR_PREFERENCE_KEY = "SETTINGS_IN_CHAT_COLOR_PREFERENCE_KEY";
    private static final String SETTINGS_OUT_CHAT_COLOR_PREFERENCE_KEY = "SETTINGS_OUT_CHAT_COLOR_PREFERENCE_KEY";
    private static final String SETTINGS_IN_BG_COLOR_PREFERENCE_KEY = "SETTINGS_IN_BG_COLOR_PREFERENCE_KEY";
    private static final String SETTINGS_OUT_BG_COLOR_PREFERENCE_KEY = "SETTINGS_OUT_BG_COLOR_PREFERENCE_KEY";

    //Translations
    private static final String SETTINGS_TWO_STEP_TRANSLATION_PREFERENCE_KEY = "SETTINGS_TWO_STEP_TRANSLATION_PREFERENCE_KEY";
    private static final String SETTINGS_AUTO_TRANSLATION_INCOMING_PREFERENCE_KEY = "SETTINGS_AUTO_TRANSLATION_INCOMING_PREFERENCE_KEY";
    private static final String SETTINGS_TRANSLATION_TIMER_PREFERENCE_KEY = "SETTINGS_TRANSLATION_TIMER_PREFERENCE_KEY";

    //Media
    private static final String SETTINGS_PHOTO_AUTO_DOWNLOAD_PREFERENCE_KEY = "SETTINGS_PHOTO_AUTO_DOWNLOAD_PREFERENCE_KEY";
    private static final String SETTINGS_AUDIO_AUTO_DOWNLOAD_PREFERENCE_KEY = "SETTINGS_AUDIO_AUTO_DOWNLOAD_PREFERENCE_KEY";
    private static final String SETTINGS_VIDEO_AUTO_DOWNLOAD_PREFERENCE_KEY = "SETTINGS_VIDEO_AUTO_DOWNLOAD_PREFERENCE_KEY";
    private static final String SETTINGS_DOCUMENT_AUTO_DOWNLOAD_PREFERENCE_KEY = "SETTINGS_DOCUMENT_AUTO_DOWNLOAD_PREFERENCE_KEY";
    private static final String SETTINGS_SAVE_FOR_PERSONAL_CHATS_PREFERENCE_KEY = "SETTINGS_SAVE_FOR_PERSONAL_CHATS_PREFERENCE_KEY";
    private static final String SETTINGS_SAVE_FOR_GROUP_CHATS_PREFERENCE_KEY = "SETTINGS_SAVE_FOR_GROUP_CHATS_PREFERENCE_KEY";
    private static final String SETTINGS_SAVE_FOR_ROOM_PREFERENCE_KEY = "SETTINGS_SAVE_FOR_ROOM_PREFERENCE_KEY";

    //Privacy
    private static final String SETTINGS_LAST_SEEN_PRIVACY_PREFERENCE_KEY = "SETTINGS_LAST_SEEN_PRIVACY_PREFERENCE_KEY";
    private static final String SETTINGS_PROFILE_PHOTO_PRIVACY_PREFERENCE_KEY = "SETTINGS_PROFILE_PHOTO_PRIVACY_PREFERENCE_KEY";
    private static final String SETTINGS_READ_RECEIPTS_PRIVACY_PREFERENCE_KEY = "SETTINGS_READ_RECEIPTS_PRIVACY_PREFERENCE_KEY";
    private static final String SETTINGS_CALLS_PRIVACY_PREFERENCE_KEY = "SETTINGS_LOCATION_PRIVACY_PREFERENCE_KEY";

    private static final String SETTINGS_USE_NATIVE_CAMERA_PREFERENCE_KEY = "SETTINGS_USE_NATIVE_CAMERA_PREFERENCE_KEY";

    private static final String SETTINGS_SOFT_KEYBOARD_SIZE = "SETTINGS_SOFT_KEYBOARD_SIZE";
    private static final String SETTINGS_IS_FIRST_RUN = "SETTINGS_IS_FIRST_RUN";
    private static final String SETTINGS_TRANSLATOR_STRING = "SETTINGS_TRANSLATOR_STRING";

    public static final String SETTINGS_PASS_CODE = "SETTINGS_PASS_CODE";
    public static final String SETTINGS_PAYMENTS_TIMEOUT = "SETTINGS_PAYMENTS_TIMEOUT";
    private static final String SETTINGS_FLEXIBLE_TRANSLATION = "SETTINGS_FLEXIBLE_TRANSLATION";
    private static final String SETTINGS_SOURCE_LANGUAGE_CODE = "SETTINGS_SOURCE_LANGUAGE_CODE";
    private static final String SETTINGS_TARGET_LANGUAGE_CODE = "SETTINGS_TARGET_LANGUAGE_CODE";
    private static final String SETTINGS_CALL_LOG_CLEAR_TIME = "SETTINGS_CALL_LOG_CLEAR_TIME";
    private static final int MEDIA_SAVING_3_DAYS = 0;
    private static final int MEDIA_SAVING_1_WEEK = 1;
    private static final int MEDIA_SAVING_1_MONTH = 2;
    private static final int MEDIA_SAVING_FOREVER = 3;
    //message notification custom ringtone
    private static final String SETTINGS_NOTIFICATION_CUSTOM_RINGTONE_PREFERENCE_KEY = "SETTINGS_NOTIFICATION_RINGTONE_PREFERENCE_KEY";
    //group notification ringtone
    private static final String SETTINGS_GROUP_NOTIFICATION_CUSTOM_RINGTONE_PREFERENCE_KEY = "SETTINGS_GROUP_NOTIFICATION_CUSTOM_RINGTONE_PREFERENCE_KEY";
    // some preferences keys must be kept after a logout
    private static final List<String> mKeysToKeepAfterLogout = Arrays.asList(
            SETTINGS_HIDE_READ_RECEIPTS_KEY,
            SETTINGS_ALWAYS_SHOW_TIMESTAMPS_KEY,
            SETTINGS_12_24_TIMESTAMPS_KEY,
            SETTINGS_DONT_SEND_TYPING_NOTIF_KEY,
            SETTINGS_HIDE_JOIN_LEAVE_MESSAGES_KEY,
            SETTINGS_HIDE_AVATAR_DISPLAY_NAME_CHANGES_MESSAGES_KEY,
            SETTINGS_MEDIA_SAVING_PERIOD_KEY,
            SETTINGS_MEDIA_SAVING_PERIOD_SELECTED_KEY,

            SETTINGS_PIN_UNREAD_MESSAGES_PREFERENCE_KEY,
            SETTINGS_PIN_MISSED_NOTIFICATIONS_PREFERENCE_KEY,
            SETTINGS_DATA_SAVE_MODE_PREFERENCE_KEY,
            SETTINGS_START_ON_BOOT_PREFERENCE_KEY,
            SETTINGS_INTERFACE_TEXT_SIZE_KEY,
            SETTINGS_USE_JITSI_CONF_PREFERENCE_KEY,
            SETTINGS_USE_MATRIX_APPS_PREFERENCE_KEY,
            SETTINGS_NOTIFICATION_RINGTONE_PREFERENCE_KEY,
            SETTINGS_NOTIFICATION_RINGTONE_SELECTION_PREFERENCE_KEY,
            SETTINGS_GROUP_NOTIFICATION_RINGTONE_PREFERENCE_KEY,

            SETTINGS_SET_SYNC_TIMEOUT_PREFERENCE_KEY,
            SETTINGS_SET_SYNC_DELAY_PREFERENCE_KEY,
            SETTINGS_ROOM_SETTINGS_LABS_END_TO_END_PREFERENCE_KEY,
            SETTINGS_CONTACTS_PHONEBOOK_COUNTRY_PREFERENCE_KEY,
            SETTINGS_INTERFACE_LANGUAGE_PREFERENCE_KEY,
            SETTINGS_BACKGROUND_SYNC_PREFERENCE_KEY,
            SETTINGS_ENABLE_BACKGROUND_SYNC_PREFERENCE_KEY,
            SETTINGS_SET_SYNC_TIMEOUT_PREFERENCE_KEY,
            SETTINGS_SET_SYNC_DELAY_PREFERENCE_KEY
    );


    /**
     * Clear the preferences.
     *
     * @param context the context
     */
    public static void clearPreferences(Context context) {
        Set<String> keysToKeep = new HashSet<>(mKeysToKeepAfterLogout);

        // theme
        keysToKeep.add(ThemeUtils.APPLICATION_THEME_KEY);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();

        // get all the existing keys
        Set<String> keys = preferences.getAll().keySet();
        // remove the one to keep

        keys.removeAll(keysToKeep);

        for (String key : keys) {
            editor.remove(key);
        }

        editor.commit();
    }

    /**
     * Tells if a background service can be started.
     *
     * @param context the context
     * @return true if a background service can be started.
     */
    @SuppressLint("NewApi")
    public static boolean canStartBackgroundService(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            return ((PowerManager) context.getSystemService(Context.POWER_SERVICE)).isIgnoringBatteryOptimizations(context.getPackageName());
        }

        return true;
    }

    /**
     * Tells if the timestamp must be displayed in 12h format
     *
     * @param context the context
     * @return true if the time must be displayed in 12h format
     */
    public static boolean displayTimeIn12hFormat(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SETTINGS_12_24_TIMESTAMPS_KEY, false);
    }

    /**
     * Tells if the join / leave membership events must be hidden in the messages list.
     *
     * @param context the context
     * @return true if the join / leave membership events must be hidden in the messages list
     */
    public static boolean hideJoinLeaveMessages(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SETTINGS_HIDE_JOIN_LEAVE_MESSAGES_KEY, false);
    }

    /**
     * Tells if the avatar / display name events must be hidden in the messages list.
     *
     * @param context the context
     * @return true true if the avatar / display name events must be hidden in the messages list.
     */
    public static boolean hideAvatarDisplayNameChangeMessages(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SETTINGS_HIDE_AVATAR_DISPLAY_NAME_CHANGES_MESSAGES_KEY, false);
    }

    /**
     * Tells the native camera to take a photo or record a video.
     *
     * @param context the context
     * @return true to use the native camera app to record video or take photo.
     */
    public static boolean useNativeCamera(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SETTINGS_USE_NATIVE_CAMERA_PREFERENCE_KEY, false);
    }

    /**
     * Update incoming chat color
     *
     * @param context the context
     * @param color   the new color
     */
    public static void setIntIncomingChatColor(Context context, int color) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putInt(SETTINGS_IN_CHAT_COLOR_PREFERENCE_KEY, color);
        editor.commit();
    }

    public static int getIntIncomingChatColor(Context context) { // Example: #ffffffff
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(SETTINGS_IN_CHAT_COLOR_PREFERENCE_KEY, -16777216);
    }

    /**
     * Update outgoing chat color
     *
     * @param context the context
     * @param color   the new color
     */
    public static void setIntOutgoingChatColor(Context context, int color) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putInt(SETTINGS_OUT_CHAT_COLOR_PREFERENCE_KEY, color);
        editor.commit();
    }

    public static int getIntOutgoingChatColor(Context context) { // Example: #ffffffff
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(SETTINGS_OUT_CHAT_COLOR_PREFERENCE_KEY, -16777216);
    }

    public static void setIntIncomingBgColor(Context context, int color) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putInt(SETTINGS_IN_BG_COLOR_PREFERENCE_KEY, color);
        editor.commit();
    }

    public static int getIntIncomingBgColor(Context context) { // Example: #ffffffff
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(SETTINGS_IN_BG_COLOR_PREFERENCE_KEY, context.getResources().getColor(R.color.default_income));
    }

    public static void setIntOutgoingBgColor(Context context, int color) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putInt(SETTINGS_OUT_BG_COLOR_PREFERENCE_KEY, color);
        editor.commit();
    }

    public static int getIntOutgoingBgColor(Context context) { // Example: #ffffffff
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(SETTINGS_OUT_BG_COLOR_PREFERENCE_KEY, context.getResources().getColor(R.color.default_outgoing));
    }

    /**
     * Update the background chat picture
     *
     * @param context the context
     * @param string  the new picture
     */
    public static void setBackgroundChatPicture(Context context, String string) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(SETTINGS_BACKGROUND_CHAT_PICTURE_PREFERENCE_KEY, string);
        editor.commit();
    }

    public static Uri getBackgroundChatPicture(Context context) {
        String uri = PreferenceManager.getDefaultSharedPreferences(context).getString(SETTINGS_BACKGROUND_CHAT_PICTURE_PREFERENCE_KEY, null);
        if (uri != null) return Uri.parse(uri);
        else return null;
    }

    /**
     * Update the notification ringtone
     *
     * @param context the context
     * @param uri     the new notification ringtone
     */
    public static void setMessageNotificationRingTone(Context context, Uri uri) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();

        String value = "";

        if (null != uri) {
            value = uri.toString();

            if (value.startsWith("file://")) {
                // it should never happen
                // else android.os.FileUriExposedException will be triggered.
                // see https://github.com/vector-im/riot-android/issues/1725
                return;
            }
        }

        editor.putString(SETTINGS_NOTIFICATION_RINGTONE_PREFERENCE_KEY, value);
        editor.commit();
    }

    public static void setGroupNotificationRingTone(Context context, Uri uri) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();

        String value = "";

        if (null != uri) {
            value = uri.toString();

            if (value.startsWith("file://")) {
                // it should never happen
                // else android.os.FileUriExposedException will be triggered.
                // see https://github.com/vector-im/riot-android/issues/1725
                return;
            }
        }

        editor.putString(SETTINGS_GROUP_NOTIFICATION_RINGTONE_PREFERENCE_KEY, value);
        editor.commit();
    }

    /**
     * Provides the selected notification ring tone
     *
     * @return the selected ring tone
     */
    public static Uri getNotificationRingTone(String url) {
//        String url = PreferenceManager.getDefaultSharedPreferences(context).getString(SETTINGS_NOTIFICATION_RINGTONE_PREFERENCE_KEY, null);

        // the user selects "None"
        if (TextUtils.equals(url, "")) {
            return null;
        }

        Uri uri = null;

        // https://github.com/vector-im/riot-android/issues/1725
        if ((null != url) && !url.startsWith("file://")) {
            try {
                uri = Uri.parse(url);
            } catch (Exception e) {
                Log.e(LOG_TAG, "## getNotificationRingTone() : Uri.parse failed");
            }
        }

        if (null == uri) {
            uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        }

        Log.d(LOG_TAG, "## getNotificationRingTone() returns " + uri);
        return uri;
    }

    public static Uri getMessageNotificationRingTone(Context context) {
        String url = PreferenceManager.getDefaultSharedPreferences(context).getString(SETTINGS_NOTIFICATION_RINGTONE_PREFERENCE_KEY, null);
        return getNotificationRingTone(url);
    }

    public static Uri getGroupNotificationRingTone(Context context) {
        String url = PreferenceManager.getDefaultSharedPreferences(context).getString(SETTINGS_GROUP_NOTIFICATION_RINGTONE_PREFERENCE_KEY, null);
        return getNotificationRingTone(url);
    }

    /**
     * Provide the notification ringtone filename
     *
     * @param context the context
     * @return the filename
     */
    public static String getNotificationRingToneName(Context context, Uri toneUri) {
//        Uri toneUri = getNotificationRingTone(context);

        if (null == toneUri) {
            return null;
        }

        String name = null;

        Cursor cursor = null;

        try {
            String[] proj = {MediaStore.Audio.Media.DATA};
            cursor = context.getContentResolver().query(toneUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
            cursor.moveToFirst();

            File file = new File(cursor.getString(column_index));
            name = file.getName();

            if (name.contains(".")) {
                name = name.substring(0, name.lastIndexOf("."));
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, "## getNotificationRingToneName() failed() : " + e.getMessage());
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return name;
    }

    public static String getMessageNotificationRingToneName(Context context) {
        return getNotificationRingToneName(context, getMessageNotificationRingTone(context));
    }

    public static String getGroupNotificationRingToneName(Context context) {
        return getNotificationRingToneName(context, getGroupNotificationRingTone(context));
    }

    /**
     * Tells if the data save mode is enabled
     *
     * @param context the context
     * @return true if the data save mode is enabled
     */
    public static boolean useDataSaveMode(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SETTINGS_DATA_SAVE_MODE_PREFERENCE_KEY, false);
    }

    /**
     * Tells if the conf calls must be done with Jitsi.
     *
     * @param context the context
     * @return true if the conference call must be done with jitsi.
     */
    public static boolean useJitsiConfCall(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SETTINGS_USE_JITSI_CONF_PREFERENCE_KEY, true);
    }

    /**
     * Tells if the matrix apps are supported.
     *
     * @param context the context
     * @return true if the matrix apps are supported.
     */
    public static boolean useMatrixApps(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SETTINGS_USE_MATRIX_APPS_PREFERENCE_KEY, false);
    }

    /**
     * Tells if the application is started on boot
     *
     * @param context the context
     * @return true if the application must be started on boot
     */
    public static boolean autoStartOnBoot(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SETTINGS_START_ON_BOOT_PREFERENCE_KEY, false);
    }

    /**
     * Tells if the application is started on boot
     *
     * @param context the context
     * @param value   true to start the application on boot
     */
    public static void setAutoStartOnBoot(Context context, boolean value) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(SETTINGS_START_ON_BOOT_PREFERENCE_KEY, value);
        editor.commit();
    }

    /**
     * Provides the medias saving choice list.
     *
     * @param context the context
     * @return the list
     */
    public static CharSequence[] getMediasSavingItemsChoicesList(Context context) {
        return new CharSequence[]{
                context.getString(R.string.media_saving_period_3_days),
                context.getString(R.string.media_saving_period_1_week),
                context.getString(R.string.media_saving_period_1_month),
                context.getString(R.string.media_saving_period_forever)};
    }

    /**
     * Provides the selected saving period.
     *
     * @param context the context
     * @return the selected period
     */
    public static int getSelectedMediasSavingPeriod(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(SETTINGS_MEDIA_SAVING_PERIOD_SELECTED_KEY, MEDIA_SAVING_1_WEEK);
    }

    /**
     * Updates the selected saving period.
     *
     * @param context the context
     * @param index   the selected period index
     */
    public static void setSelectedMediasSavingPeriod(Context context, int index) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(SETTINGS_MEDIA_SAVING_PERIOD_SELECTED_KEY, index);
        editor.commit();
    }

    /**
     * Provides the minimum last access time to keep a media file.
     *
     * @param context the context
     * @return the min last access time (in seconds)
     */
    public static long getMinMediasLastAccessTime(Context context) {
        int selection = getSelectedMediasSavingPeriod(context);

        switch (selection) {
            case MEDIA_SAVING_3_DAYS:
                return (System.currentTimeMillis() / 1000) - (3 * 24 * 60 * 60);
            case MEDIA_SAVING_1_WEEK:
                return (System.currentTimeMillis() / 1000) - (7 * 24 * 60 * 60);
            case MEDIA_SAVING_1_MONTH:
                return (System.currentTimeMillis() / 1000) - (30 * 24 * 60 * 60);
            case MEDIA_SAVING_FOREVER:
                return 0;
        }

        return 0;
    }

    /**
     * Provides the selected saving period.
     *
     * @param context the context
     * @return the selected period
     */
    public static String getSelectedMediasSavingPeriodString(Context context) {
        int selection = getSelectedMediasSavingPeriod(context);

        switch (selection) {
            case MEDIA_SAVING_3_DAYS:
                return context.getString(R.string.media_saving_period_3_days);
            case MEDIA_SAVING_1_WEEK:
                return context.getString(R.string.media_saving_period_1_week);
            case MEDIA_SAVING_1_MONTH:
                return context.getString(R.string.media_saving_period_1_month);
            case MEDIA_SAVING_FOREVER:
                return context.getString(R.string.media_saving_period_forever);
        }
        return "?";
    }

    /**
     * Fix some migration issues
     */
    public static void fixMigrationIssues(Context context) {
        // some key names have been updated to supported language switch
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        if (preferences.contains(context.getString(R.string.settings_pin_missed_notifications))) {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean(SETTINGS_PIN_MISSED_NOTIFICATIONS_PREFERENCE_KEY, preferences.getBoolean(context.getString(R.string.settings_pin_missed_notifications), false));
            editor.remove(context.getString(R.string.settings_pin_missed_notifications));
            editor.commit();
        }

        if (preferences.contains(context.getString(R.string.settings_pin_unread_messages))) {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean(SETTINGS_PIN_UNREAD_MESSAGES_PREFERENCE_KEY, preferences.getBoolean(context.getString(R.string.settings_pin_unread_messages), false));
            editor.remove(context.getString(R.string.settings_pin_unread_messages));
            editor.commit();
        }

        if (preferences.contains("MARKDOWN_PREFERENCE_KEY")) {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean(SETTINGS_DISABLE_MARKDOWN_KEY, !preferences.getBoolean("MARKDOWN_PREFERENCE_KEY", false));
            editor.remove("MARKDOWN_PREFERENCE_KEY");
            editor.commit();
        }

        if (!preferences.contains(SETTINGS_USE_JITSI_CONF_PREFERENCE_KEY)) {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean(SETTINGS_USE_JITSI_CONF_PREFERENCE_KEY, true);
            editor.commit();
        }

        if (!preferences.contains(SETTINGS_PIN_MISSED_NOTIFICATIONS_PREFERENCE_KEY)) {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean(SETTINGS_PIN_MISSED_NOTIFICATIONS_PREFERENCE_KEY, true);
            editor.commit();
        }

        if (!preferences.contains(SETTINGS_PIN_UNREAD_MESSAGES_PREFERENCE_KEY)) {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean(SETTINGS_PIN_UNREAD_MESSAGES_PREFERENCE_KEY, true);
            editor.commit();
        }
    }

    /**
     * Tells if the markdown is enabled
     *
     * @param context the context
     * @return true if the markdown is enabled
     */
    public static boolean isMarkdownEnabled(Context context) {
        return !PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SETTINGS_DISABLE_MARKDOWN_KEY, false);
    }

    /**
     * Update the markdown enable status.
     *
     * @param context   the context
     * @param isEnabled true to enable the markdown
     */
    public static void setMarkdownEnabled(Context context, boolean isEnabled) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(SETTINGS_DISABLE_MARKDOWN_KEY, !isEnabled);
        editor.commit();
    }

    /**
     * Tells if the read receipts must be hidden
     *
     * @param context the context
     * @return true if the read receipts must be hidden
     */
    public static boolean hideReadReceipts(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SETTINGS_HIDE_READ_RECEIPTS_KEY, false);
    }

    public static void setHideReadReceipts(Context context, boolean hide) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(SETTINGS_HIDE_READ_RECEIPTS_KEY, hide);
        editor.commit();
    }

    /**
     * Update two-step translation
     *
     * @param context the context
     * @return true if the read receipts must be hidden
     */
    public static boolean isTwoStepTranslationAllow(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(SETTINGS_TWO_STEP_TRANSLATION_PREFERENCE_KEY, false);
    }

    public static void setTwoStepTranslationAllow(Context context, boolean allow) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(SETTINGS_TWO_STEP_TRANSLATION_PREFERENCE_KEY, allow);
        editor.commit();
    }

    /**
     * Update auto translation incoming
     *
     * @param context the context
     * @return true if the read receipts must be hidden
     */
    public static boolean isAutoTranslationIncomingAllow(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(SETTINGS_AUTO_TRANSLATION_INCOMING_PREFERENCE_KEY, false);
    }

    public static void setAutoTranslationIncomingAllow(Context context, boolean allow) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(SETTINGS_AUTO_TRANSLATION_INCOMING_PREFERENCE_KEY, allow);
        editor.commit();
    }

    /**
     * Update translation timer
     *
     * @param context the context
     * @return true if the read receipts must be hidden
     */
    public static int getTranslationTimer(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getInt(SETTINGS_TRANSLATION_TIMER_PREFERENCE_KEY, 1);
    }

    public static void setTranslationTimer(Context context, int seconds) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(SETTINGS_TRANSLATION_TIMER_PREFERENCE_KEY, seconds);
        editor.commit();
    }

    /**
     * Update photo auto download method
     *
     * @param context the context
     * @return true if the read receipts must be hidden
     */
    public static DownloadMethod getPhotoAutoDownloadMethod(Context context) {
        return DownloadMethod.valueOf(PreferenceManager.getDefaultSharedPreferences(context)
                .getString(SETTINGS_PHOTO_AUTO_DOWNLOAD_PREFERENCE_KEY, DownloadMethod.WIFI_AND_CELLULAR.name()));
    }

    public static void setPhotoAutoDownloadMethod(Context context, DownloadMethod method) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(SETTINGS_PHOTO_AUTO_DOWNLOAD_PREFERENCE_KEY, method.name());
        editor.commit();
    }

    /**
     * Update audio auto download method
     *
     * @param context the context
     * @return true if the read receipts must be hidden
     */
    public static DownloadMethod getAudioAutoDownloadMethod(Context context) {
        return DownloadMethod.valueOf(PreferenceManager.getDefaultSharedPreferences(context)
                .getString(SETTINGS_AUDIO_AUTO_DOWNLOAD_PREFERENCE_KEY, DownloadMethod.WIFI_AND_CELLULAR.name()));
    }

    public static void setAudioAutoDownloadMethod(Context context, DownloadMethod method) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(SETTINGS_AUDIO_AUTO_DOWNLOAD_PREFERENCE_KEY, method.name());
        editor.commit();
    }

    /**
     * Update video auto download method
     *
     * @param context the context
     * @return true if the read receipts must be hidden
     */
    public static DownloadMethod getVideoAutoDownloadMethod(Context context) {
        return DownloadMethod.valueOf(PreferenceManager.getDefaultSharedPreferences(context)
                .getString(SETTINGS_VIDEO_AUTO_DOWNLOAD_PREFERENCE_KEY, DownloadMethod.WIFI_AND_CELLULAR.name()));
    }

    public static void setVideoAutoDownloadMethod(Context context, DownloadMethod method) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(SETTINGS_VIDEO_AUTO_DOWNLOAD_PREFERENCE_KEY, method.name());
        editor.commit();
    }

    /**
     * Update document auto download method
     *
     * @param context the context
     * @return true if the read receipts must be hidden
     */
    public static DownloadMethod getDocAutoDownloadMethod(Context context) {
        return DownloadMethod.valueOf(PreferenceManager.getDefaultSharedPreferences(context)
                .getString(SETTINGS_DOCUMENT_AUTO_DOWNLOAD_PREFERENCE_KEY, DownloadMethod.WIFI_AND_CELLULAR.name()));
    }

    public static void setDocAutoDownloadMethod(Context context, DownloadMethod method) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(SETTINGS_DOCUMENT_AUTO_DOWNLOAD_PREFERENCE_KEY, method.name());
        editor.commit();
    }

    /**
     * Allow save media for personal chats
     *
     * @param context the context
     * @return true if the read receipts must be hidden
     */
    public static boolean isSaveMediaForPersonalChatAllow(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(SETTINGS_SAVE_FOR_PERSONAL_CHATS_PREFERENCE_KEY, false);
    }

    public static void setSaveMediaForPersonalChatAllow(Context context, boolean allow) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(SETTINGS_SAVE_FOR_PERSONAL_CHATS_PREFERENCE_KEY, allow);
        editor.commit();
    }

    /**
     * Allow save media for group chats
     *
     * @param context the context
     * @return true if the read receipts must be hidden
     */
    public static boolean isSaveMediaForGroupChatAllow(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(SETTINGS_SAVE_FOR_GROUP_CHATS_PREFERENCE_KEY, false);
    }

    public static void setSaveMediaForGroupChatAllow(Context context, boolean allow) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(SETTINGS_SAVE_FOR_GROUP_CHATS_PREFERENCE_KEY, allow);
        editor.commit();
    }

    /**
     * Allow save media for room
     *
     * @param context the context
     * @return true if the read receipts must be hidden
     */
    public static boolean isSaveMediaForRoomAllow(Context context, String roomId) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(SETTINGS_SAVE_FOR_ROOM_PREFERENCE_KEY + "_" + roomId, false);
    }

    public static void setSaveMediaForRoomAllow(Context context, boolean allow, String roomId) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(SETTINGS_SAVE_FOR_ROOM_PREFERENCE_KEY + "_" + roomId, allow);
        editor.commit();
    }

    /**
     * Show notification message
     *
     * @param context the context
     * @return true if the read receipts must be hidden
     */
    public static boolean isShowPreviewAllow(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(SETTINGS_SHOW_PREVIEW_PREFERENCE_KEY, false);
    }

    public static void setShowPreviewAllow(Context context, boolean allow) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(SETTINGS_SHOW_PREVIEW_PREFERENCE_KEY, allow);
        editor.commit();
    }

    /**
     * Update last seen privacy method
     *
     * @param context the context
     * @return true if the read receipts must be hidden
     */
    public static PrivacyMethod getLastSeenPrivacyMethod(Context context) {
        return PrivacyMethod.valueOf(PreferenceManager.getDefaultSharedPreferences(context)
                .getString(SETTINGS_LAST_SEEN_PRIVACY_PREFERENCE_KEY, PrivacyMethod.EVERYONE.name()));
    }

    public static void setLastSeenPrivacyMethod(Context context, PrivacyMethod method) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(SETTINGS_LAST_SEEN_PRIVACY_PREFERENCE_KEY, method.name());
        editor.commit();
    }

    /**
     * Get/set user number
     *
     * @param context the context
     * @return true if the read receipts must be hidden
     */
    public static String getUserNumber(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(SETTINGS_USER_NUMBER_PREFERENCE_KEY, context.getResources().getString(R.string.no_user_phone_number));
    }

    public static void setUserNumber(Context context, String userNumber) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(SETTINGS_USER_NUMBER_PREFERENCE_KEY, userNumber);
        editor.commit();
    }

    /**
     * Update profile photo privacy method
     *
     * @param context the context
     * @return true if the read receipts must be hidden
     */
    public static PrivacyMethod getProfilePhotoPrivacyMethod(Context context) {
        return PrivacyMethod.valueOf(PreferenceManager.getDefaultSharedPreferences(context)
                .getString(SETTINGS_PROFILE_PHOTO_PRIVACY_PREFERENCE_KEY, PrivacyMethod.EVERYONE.name()));
    }

    public static void setProfilePhotoPrivacyMethod(Context context, PrivacyMethod method) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(SETTINGS_PROFILE_PHOTO_PRIVACY_PREFERENCE_KEY, method.name());
        editor.commit();
    }

    /**
     * Update read receipts privacy method
     *
     * @param context the context
     * @return true if the read receipts must be hidden
     */
    public static PrivacyMethod getReadReceiptsPrivacyMethod(Context context) {
        return PrivacyMethod.valueOf(PreferenceManager.getDefaultSharedPreferences(context)
                .getString(SETTINGS_READ_RECEIPTS_PRIVACY_PREFERENCE_KEY, PrivacyMethod.EVERYONE.name()));
    }

    public static void setReadReceiptsPrivacyMethod(Context context, PrivacyMethod method) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(SETTINGS_READ_RECEIPTS_PRIVACY_PREFERENCE_KEY, method.name());
        editor.commit();
    }

    /**
     * Update read calls method
     *
     * @param context the context
     * @return true if the read receipts must be hidden
     */
    public static PrivacyMethod getCallsPrivacyMethod(Context context) {
        return PrivacyMethod.valueOf(PreferenceManager.getDefaultSharedPreferences(context)
                .getString(SETTINGS_CALLS_PRIVACY_PREFERENCE_KEY, PrivacyMethod.EVERYONE.name()));
    }

    public static void setCallsPrivacyMethod(Context context, PrivacyMethod method) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(SETTINGS_CALLS_PRIVACY_PREFERENCE_KEY, method.name());
        editor.commit();
    }

    /**
     * Tells if the message timestamps must be always shown.
     *
     * @param context the context
     * @return true if the message timestamps must be always shown.
     */
    public static boolean alwaysShowTimeStamps(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SETTINGS_ALWAYS_SHOW_TIMESTAMPS_KEY, false);
    }

    /**
     * Tells if the typing notifications must NOT be sent
     *
     * @param context the context
     * @return true to do NOT send the typing notifs
     */
    public static boolean dontSendTypingNotifs(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SETTINGS_DONT_SEND_TYPING_NOTIF_KEY, false);
    }

    /**
     * Tells of the missing notifications rooms must be displayed at left (home screen)
     *
     * @param context the context
     * @return true to move the missed notifications to the left side
     */
    public static boolean pinMissedNotifications(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SETTINGS_PIN_MISSED_NOTIFICATIONS_PREFERENCE_KEY, false);
    }

    /**
     * Tells of the unread rooms must be displayed at left (home screen)
     *
     * @param context the context
     * @return true to move the unread room to the left side
     */
    public static boolean pinUnreadMessages(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SETTINGS_PIN_UNREAD_MESSAGES_PREFERENCE_KEY, false);
    }

    /**
     * Tells if Piwik can be used
     *
     * @param context the context
     * @return null if not defined, true / false when defined
     */
    public static Boolean trackWithPiwik(Context context) {
        return !PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SETTINGS_DISABLE_PIWIK_SETTINGS_PREFERENCE_KEY, false);
    }

    public static void saveDNDtime(long start, long end, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong("dnd_start", start);
        editor.putLong("dnd_end", end);
        editor.commit();
    }

    public static long getStartDNDTime(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getLong("dnd_start", 0);
    }

    public static long getEndDNDTime(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getLong("dnd_end", 0);
    }

    public static void stopDNDMode(Context context) {
        saveDNDtime(0, 0, context);
    }

    public static boolean isInDNDMode(Context context) {
        long start = getStartDNDTime(context);
        long end = getEndDNDTime(context);
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int min = calendar.get(Calendar.MINUTE);
        long current = TimeUnit.HOURS.toMillis(hour) + TimeUnit.MINUTES.toMillis(min);
        return current > start && current < end;
    }

    public static void saveDNDTimeForUser(long start, long end, Context context, String userId) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong("dnd_start_" + userId, start);
        editor.putLong("dnd_end_" + userId, end);
        editor.commit();
    }

    public static void saveStartDNDTimeForUser(long start, Context context, String userId) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong("dnd_start_" + userId, start);
        editor.commit();
    }

    public static void saveEndDNDTimeForUser(long end, Context context, String userId) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong("dnd_end_" + userId, end);
        editor.commit();
    }

    public static long getStartDNDTimeForUser(Context context, String userId) {
        return PreferenceManager.getDefaultSharedPreferences(context).getLong("dnd_start_" + userId, 0);
    }

    public static long getEndDNDTimeForUser(Context context, String userId) {
        return PreferenceManager.getDefaultSharedPreferences(context).getLong("dnd_end_" + userId, 0);
    }

    public static void stopDNDModeForUser(Context context, String userId) {
        saveDNDTimeForUser(0, 0, context, userId);
    }

    public static boolean isInDNDModeForUser(Context context, String userId) {
        long start = getStartDNDTimeForUser(context, userId);
        long end = getEndDNDTimeForUser(context, userId);
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int min = calendar.get(Calendar.MINUTE);
        long current = TimeUnit.HOURS.toMillis(hour) + TimeUnit.MINUTES.toMillis(min);
        return current > start && current < end;
    }

    public static void statAddIncomeCall(Context context, Long length) {
        addIncomeMessages(context);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong("in_call", getIncomeCall(context) + length);
        editor.commit();
    }

    public static void statAddOutCall(Context context, Long length) {
        addOutMessages(context);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong("out_call", getOutCall(context) + length);
        editor.commit();
    }

    public static void statAddIncomeTextMessage(Context context) {
        addIncomeMessages(context);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("in_text", getIncomeTextMessage(context) + 1);
        editor.commit();
    }

    public static void statAddOutTextMessage(Context context) {
        addOutMessages(context);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("out_text", getOutTextMessage(context) + 1);
        editor.commit();
    }

    public static void statAddIncomeVideo(Context context) {
        addIncomeMessages(context);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("in_video", getIncomeVideo(context) + 1);
        editor.commit();
    }

    public static void statAddOutVideo(Context context) {
        addOutMessages(context);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("out_video", getOutVideo(context) + 1);
        editor.commit();
    }

    public static void statAddIncomeAudio(Context context) {
        addIncomeMessages(context);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("in_audio", getIncomeAudio(context) + 1);
        editor.commit();
    }

    public static void statAddOutAudio(Context context) {
        addOutMessages(context);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("out_audio", getOutAudio(context) + 1);
        editor.commit();
    }

    public static void statAddIncomeImage(Context context) {
        addIncomeMessages(context);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("in_image", getIncomeImage(context) + 1);
        editor.commit();
    }

    public static void statAddOutImage(Context context) {
        addOutMessages(context);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("out_image", getOutImage(context) + 1);
        editor.commit();
    }

    private static void addIncomeMessages(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("in_all", getAllIncomeMessages(context) + 1);
        editor.commit();
    }

    private static void addOutMessages(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("out_all", getAllOutMessages(context) + 1);
        editor.commit();
    }

    public static void statAddIncomeCallForUser(Context context, Long length, String userId) {
        addIncomeMessagesForUser(context, userId);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong("in_call_" + userId, getIncomeCallForUser(context, userId) + length);
        editor.commit();
    }

    public static void statAddOutCallForUser(Context context, Long length, String userId) {
        addOutMessagesForUser(context, userId);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong("out_call_" + userId, getOutCallForUser(context, userId) + length);
        editor.commit();
    }

    public static void statAddIncomeTextMessageForUser(Context context, String userId) {
        addIncomeMessagesForUser(context, userId);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("in_text_" + userId, getIncomeTextMessageForUser(context, userId) + 1);
        editor.commit();
    }

    public static void statAddOutTextMessageForUser(Context context, String userId) {
        addOutMessagesForUser(context, userId);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("out_text_" + userId, getOutTextMessageForUser(context, userId) + 1);
        editor.commit();
    }

    public static void statAddIncomeVideoForUser(Context context, String userId) {
        addIncomeMessagesForUser(context, userId);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("in_video_" + userId, getIncomeVideoForUser(context, userId) + 1);
        editor.commit();
    }

    public static void statAddOutVideoForUser(Context context, String userId) {
        addOutMessagesForUser(context, userId);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("out_video_" + userId, getOutVideoForUser(context, userId) + 1);
        editor.commit();
    }

    public static void statAddIncomeAudioForUser(Context context, String userId) {
        addIncomeMessagesForUser(context, userId);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("in_audio_" + userId, getIncomeAudioForUser(context, userId) + 1);
        editor.commit();
    }

    public static void statAddOutAudioForUser(Context context, String userId) {
        addOutMessagesForUser(context, userId);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("out_audio_" + userId, getOutAudioForUser(context, userId) + 1);
        editor.commit();
    }

    public static void statAddIncomeImageForUser(Context context, String userId) {
        addIncomeMessagesForUser(context, userId);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("in_image_" + userId, getIncomeImageForUser(context, userId) + 1);
        editor.commit();
    }

    public static void statAddOutImageForUser(Context context, String userId) {
        addOutMessagesForUser(context, userId);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("out_image_" + userId, getOutImageForUser(context, userId) + 1);
        editor.commit();
    }

    private static void addIncomeMessagesForUser(Context context, String userId) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("in_all_" + userId, getAllIncomeMessagesForUser(context, userId) + 1);
        editor.commit();
    }

    private static void addOutMessagesForUser(Context context, String userId) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("out_all_" + userId, getAllOutMessagesForUser(context, userId) + 1);
        editor.commit();
    }

    public static void statAddIncomeCallForGroup(Context context, Long length, String roomId) {
        addIncomeMessagesForGroup(context, roomId);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong("in_call_group_" + roomId, getIncomeCallForGroup(context, roomId) + length);
        editor.commit();
    }

    public static void statAddOutCallForGroup(Context context, Long length, String roomId) {
        addOutMessagesForGroup(context, roomId);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong("out_call_group_" + roomId, getOutCallForGroup(context, roomId) + length);
        editor.commit();
    }

    public static void statAddIncomeTextMessageForGroup(Context context, String roomId) {
        addIncomeMessagesForGroup(context, roomId);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("in_text_group_" + roomId, getIncomeTextMessageForGroup(context, roomId) + 1);
        editor.commit();
    }

    public static void statAddOutTextMessageForGroup(Context context, String roomId) {
        addOutMessagesForGroup(context, roomId);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("out_text_group_" + roomId, getOutTextMessageForGroup(context, roomId) + 1);
        editor.commit();
    }

    public static void statAddIncomeVideoForGroup(Context context, String roomId) {
        addIncomeMessagesForGroup(context, roomId);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("in_video_group_" + roomId, getIncomeVideoForGroup(context, roomId) + 1);
        editor.commit();
    }

    public static void statAddOutVideoForGroup(Context context, String roomId) {
        addOutMessagesForGroup(context, roomId);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("out_video_group_" + roomId, getOutVideoForGroup(context, roomId) + 1);
        editor.commit();
    }

    public static void statAddIncomeAudioForGroup(Context context, String roomId) {
        addIncomeMessagesForGroup(context, roomId);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("in_audio_group_" + roomId, getIncomeAudioForGroup(context, roomId) + 1);
        editor.commit();
    }

    public static void statAddOutAudioForGroup(Context context, String roomId) {
        addOutMessagesForGroup(context, roomId);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("out_audio_group_" + roomId, getOutAudioForGroup(context, roomId) + 1);
        editor.commit();
    }

    public static void statAddIncomeImageForGroup(Context context, String roomId) {
        addIncomeMessagesForGroup(context, roomId);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("in_image_group_" + roomId, getIncomeImageForGroup(context, roomId) + 1);
        editor.commit();
    }

    public static void statAddOutImageForGroup(Context context, String roomId) {
        addOutMessagesForGroup(context, roomId);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("out_image_group_" + roomId, getOutImageForGroup(context, roomId) + 1);
        editor.commit();
    }

    private static void addIncomeMessagesForGroup(Context context, String roomId) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("in_all_group_" + roomId, getAllIncomeMessagesForGroup(context, roomId) + 1);
        editor.commit();
    }

    private static void addOutMessagesForGroup(Context context, String roomId) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("out_all_group_" + roomId, getAllOutMessagesForGroup(context, roomId) + 1);
        editor.commit();
    }


    public static int getAllIncomeMessages(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("in_all", 0);
    }

    public static int getAllOutMessages(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("out_all", 0);
    }

    public static long getIncomeCall(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getLong("in_call", 0L);
    }

    public static long getOutCall(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getLong("out_call", 0L);
    }

    public static int getIncomeTextMessage(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("in_text", 0);
    }

    public static int getOutTextMessage(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("out_text", 0);
    }

    public static int getIncomeVideo(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("in_video", 0);
    }

    public static int getOutVideo(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("out_video", 0);
    }

    public static int getIncomeImage(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("in_image", 0);
    }

    public static int getOutImage(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("out_image", 0);
    }

    public static int getIncomeAudio(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("in_audio", 0);
    }

    public static int getOutAudio(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("out_audio", 0);
    }

    public static int getAllIncomeMessagesForUser(Context context, String userId) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("in_all_" + userId, 0);
    }

    public static int getAllOutMessagesForUser(Context context, String userId) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("out_all_" + userId, 0);
    }

    public static long getIncomeCallForUser(Context context, String userId) {
        return PreferenceManager.getDefaultSharedPreferences(context).getLong("in_call_" + userId, 0L);
    }

    public static long getOutCallForUser(Context context, String userId) {
        return PreferenceManager.getDefaultSharedPreferences(context).getLong("out_call_" + userId, 0L);
    }

    public static int getIncomeTextMessageForUser(Context context, String userId) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("in_text_" + userId, 0);
    }

    public static int getOutTextMessageForUser(Context context, String userId) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("out_text_" + userId, 0);
    }

    public static int getIncomeVideoForUser(Context context, String userId) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("in_video_" + userId, 0);
    }

    public static int getOutVideoForUser(Context context, String userId) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("out_video_" + userId, 0);
    }

    public static int getIncomeImageForUser(Context context, String userId) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("in_image_" + userId, 0);
    }

    public static int getOutImageForUser(Context context, String userId) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("out_image_" + userId, 0);
    }

    public static int getIncomeAudioForUser(Context context, String userId) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("in_audio_" + userId, 0);
    }

    public static int getOutAudioForUser(Context context, String userId) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("out_audio_" + userId, 0);
    }

    public static int getAllIncomeMessagesForGroup(Context context, String roomId) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("in_all_group_" + roomId, 0);
    }

    public static int getAllOutMessagesForGroup(Context context, String roomId) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("out_all_group_" + roomId, 0);
    }

    public static long getIncomeCallForGroup(Context context, String roomId) {
        return PreferenceManager.getDefaultSharedPreferences(context).getLong("in_call_group_" + roomId, 0L);
    }

    public static long getOutCallForGroup(Context context, String roomId) {
        return PreferenceManager.getDefaultSharedPreferences(context).getLong("out_call_group_" + roomId, 0L);
    }

    public static int getIncomeTextMessageForGroup(Context context, String roomId) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("in_text_group_" + roomId, 0);
    }

    public static int getOutTextMessageForGroup(Context context, String roomId) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("out_text_group_" + roomId, 0);
    }

    public static int getIncomeVideoForGroup(Context context, String roomId) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("in_video_group_" + roomId, 0);
    }

    public static int getOutVideoForGroup(Context context, String roomId) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("out_video_group_" + roomId, 0);
    }

    public static int getIncomeImageForGroup(Context context, String roomId) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("in_image_group_" + roomId, 0);
    }

    public static int getOutImageForGroup(Context context, String roomId) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("out_image_group_" + roomId, 0);
    }

    public static int getIncomeAudioForGroup(Context context, String roomId) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("in_audio_group_" + roomId, 0);
    }

    public static int getOutAudioForGroup(Context context, String roomId) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("out_audio_group_" + roomId, 0);
    }


    public static boolean isInitialSyncComplete(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SETTINGS_IS_INITIAL_SYNC_COMPLETE, false);
    }

    public static void setInitialSyncComplete(Context context) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(SETTINGS_IS_INITIAL_SYNC_COMPLETE, true).commit();
    }

    public static HashSet<Event> getFavouriteMsgsFromUser(Context context, String userId) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        HashSet<Event> msgList = new HashSet<>();
        try {
            msgList = (HashSet<Event>) ObjectSerializer.deserialize(preferences.getString("favourite_msgs_set_new_" + userId, ObjectSerializer.serialize(new HashSet<Event>())));
            return msgList;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void addFavouriteMsgFromUser(Context context, String userId, Event event) {
        HashSet<Event> favouriteMsgsFromUser = getFavouriteMsgsFromUser(context, userId);
        if (favouriteMsgsFromUser != null) {
            for (Iterator<Event> i = favouriteMsgsFromUser.iterator(); i.hasNext(); ) {
                Event element = i.next();
                if (element.eventId.equals(event.eventId)) {
                    return;
                }
            }
            favouriteMsgsFromUser.add(event);
            addFavouriteMsg(context, event);
        }
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        try {
            editor.putString("favourite_msgs_set_new_" + userId, ObjectSerializer.serialize(favouriteMsgsFromUser));
        } catch (IOException e) {
            e.printStackTrace();
        }
        editor.commit();
    }

    public static void deleteFavouriteMsgFromUser(Context context, String userId, Event event) {
        deleteFavouriteMsg(context, event);
        HashSet<Event> favouriteMsgsFromUser = getFavouriteMsgsFromUser(context, userId);
        if (favouriteMsgsFromUser != null) {
            for (Iterator<Event> i = favouriteMsgsFromUser.iterator(); i.hasNext(); ) {
                Event element = i.next();
                if (element.eventId.equals(event.eventId)) {
                    i.remove();
                    break;
                }
            }
        }
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        try {
            editor.putString("favourite_msgs_set_new_" + userId, ObjectSerializer.serialize(favouriteMsgsFromUser));
        } catch (IOException e) {
            e.printStackTrace();
        }
        editor.commit();
    }

    public static HashSet<Event> getFavouriteMsgs(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        HashSet<Event> msgList = new HashSet<Event>();
        try {
            msgList = (HashSet<Event>) ObjectSerializer.deserialize(preferences.getString("favourite_msgs_set_new", ObjectSerializer.serialize(new HashSet<Event>())));
            return msgList;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void addFavouriteMsgCommonRoom(Context context, String roomId) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("favourite_msgs_common_room", roomId);
        editor.commit();
    }

    public static String getFavouriteMsgCommonRoom(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString("favourite_msgs_common_room", null);
    }

    public static void addFavouriteMsg(Context context, Event event) {
        HashSet<Event> favouriteMsgs = getFavouriteMsgs(context);
        if (favouriteMsgs != null) {
            for (Iterator<Event> i = favouriteMsgs.iterator(); i.hasNext(); ) {
                Event element = i.next();
                if (element.eventId.equals(event.eventId)) {
                    return;
                }
            }
            favouriteMsgs.add(event);
        }
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        try {
            editor.putString("favourite_msgs_set_new", ObjectSerializer.serialize(favouriteMsgs));
        } catch (IOException e) {
            e.printStackTrace();
        }
        editor.commit();
    }

    public static void deleteFavouriteMsg(Context context, Event event) {
        HashSet<Event> favouriteMsgsFromUser = getFavouriteMsgs(context);
        if (favouriteMsgsFromUser != null) {
            for (Iterator<Event> i = favouriteMsgsFromUser.iterator(); i.hasNext(); ) {
                Event element = i.next();
                if (element.eventId.equals(event.eventId)) {
                    i.remove();
                    break;
                }
            }
        }

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        try {
            editor.putString("favourite_msgs_set_new", ObjectSerializer.serialize(favouriteMsgsFromUser));
        } catch (IOException e) {
            e.printStackTrace();
        }
        editor.commit();
    }

    public static HashSet<Event> getFavouriteMsgsFromGroup(Context context, String roomId) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        HashSet<Event> msgList = new HashSet<>();
        try {
            msgList = (HashSet<Event>) ObjectSerializer.deserialize(preferences.getString("favourite_msgs_set_new_group_" + roomId, ObjectSerializer.serialize(new HashSet<Event>())));
            return msgList;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void addFavouriteMsgFromGroup(Context context, Event event, String roomId) {
        HashSet<Event> favouriteMsgsFromGroup = getFavouriteMsgsFromGroup(context, roomId);
        if (favouriteMsgsFromGroup != null) {
            for (Iterator<Event> i = favouriteMsgsFromGroup.iterator(); i.hasNext(); ) {
                Event element = i.next();
                if (element.eventId.equals(event.eventId)) {
                    return;
                }
            }
            favouriteMsgsFromGroup.add(event);
            addFavouriteMsg(context, event);
        }
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        try {
            editor.putString("favourite_msgs_set_new_group_" + roomId, ObjectSerializer.serialize(favouriteMsgsFromGroup));
        } catch (IOException e) {
            e.printStackTrace();
        }
        editor.commit();
    }

    public static void deleteFavouriteMsgFromGroup(Context context, Event event, String roomId) {
        deleteFavouriteMsg(context, event);

        HashSet<Event> favouriteMsgsFromGroup = getFavouriteMsgsFromGroup(context, roomId);
        if (favouriteMsgsFromGroup != null) {
            for (Iterator<Event> i = favouriteMsgsFromGroup.iterator(); i.hasNext(); ) {
                Event element = i.next();
                if (element.eventId.equals(event.eventId)) {
                    i.remove();
                    break;
                }
            }
        }
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        try {
            editor.putString("favourite_msgs_set_new_group_" + roomId, ObjectSerializer.serialize(favouriteMsgsFromGroup));
        } catch (IOException e) {
            e.printStackTrace();
        }
        editor.commit();
    }

    public static int getKeyboardLayoutSize(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(SETTINGS_SOFT_KEYBOARD_SIZE, 290);
    }

    public static void setKeyoardLayoutSize(Context context, int size) {
        Log.e("setKeyoardLayoutSize", size + "");
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(SETTINGS_SOFT_KEYBOARD_SIZE, size);
        editor.commit();
    }

    public static boolean isFirstRun(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SETTINGS_IS_FIRST_RUN, true);
    }

    public static void setFirstRun(Context context, boolean isFirst) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(SETTINGS_IS_FIRST_RUN, isFirst);
        editor.commit();
    }

    public static void setTranslatorString(Context context, String s) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(SETTINGS_TRANSLATOR_STRING, s);
        editor.commit();
    }

    public static String getTranslatorString(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(SETTINGS_TRANSLATOR_STRING, "");
    }

    public static void setPassword(Context context, String pass) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(SETTINGS_PASS_CODE, pass);
        editor.commit();
    }

    public static String getPassword(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(SETTINGS_PASS_CODE, "");
    }

    public static void setPaymentTimeout(Context context, long time) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(SETTINGS_PAYMENTS_TIMEOUT, time);
        editor.commit();
    }

    public static boolean isPaymentsTimeoutExpired(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return (System.currentTimeMillis() - preferences.getLong(SETTINGS_PAYMENTS_TIMEOUT, 0l) > 3600000);
    }

    public static Uri getGroupNotificationCustomRingTone(Context context, String id) {
        String url = PreferenceManager.getDefaultSharedPreferences(context).getString(SETTINGS_GROUP_NOTIFICATION_CUSTOM_RINGTONE_PREFERENCE_KEY + id, null);
        return getNotificationRingTone(url);
    }

    public static String getGroupNotificationCustomRingToneName(Context context, String id) {
        return getNotificationRingToneName(context, getGroupNotificationCustomRingTone(context, id));
    }

    public static Uri getMessageNotificationCustomRingTone(Context context, String id) {
        String url = PreferenceManager.getDefaultSharedPreferences(context).getString(SETTINGS_NOTIFICATION_CUSTOM_RINGTONE_PREFERENCE_KEY + id, null);
        return getNotificationRingTone(url);
    }

    public static void setGroupNotificationCustomRingTone(Context context, Uri uri, String id) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();

        String value = "";

        if (null != uri) {
            value = uri.toString();

            if (value.startsWith("file://")) {
                // it should never happen
                // else android.os.FileUriExposedException will be triggered.
                // see https://github.com/vector-im/riot-android/issues/1725
                return;
            }
        }

        editor.putString(SETTINGS_GROUP_NOTIFICATION_CUSTOM_RINGTONE_PREFERENCE_KEY + id, value);
        editor.commit();
    }

    public static void setMessageNotificationCustomRingTone(Context context, Uri uri, String id) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();

        String value = "";

        if (null != uri) {
            value = uri.toString();

            if (value.startsWith("file://")) {
                // it should never happen
                // else android.os.FileUriExposedException will be triggered.
                // see https://github.com/vector-im/riot-android/issues/1725
                return;
            }
        }

        editor.putString(SETTINGS_NOTIFICATION_CUSTOM_RINGTONE_PREFERENCE_KEY + id, value);
        editor.commit();
    }

    public static void setFlexibleTranslation(Context context, boolean b) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(SETTINGS_FLEXIBLE_TRANSLATION, b);
        editor.commit();
    }

    public static boolean isFlexibleTranslationEnabled(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(SETTINGS_FLEXIBLE_TRANSLATION, true);
    }

    public static void setLanguageCodes(Context context, String source, String target) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(SETTINGS_SOURCE_LANGUAGE_CODE, source);
        editor.putString(SETTINGS_TARGET_LANGUAGE_CODE, target);
        editor.commit();
    }

    public static String getSourceLanguageCode(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(SETTINGS_SOURCE_LANGUAGE_CODE, "ru");
    }

    public static String getTargetLanguageCode(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(SETTINGS_TARGET_LANGUAGE_CODE, "en");
    }

    public static void setCallLogClearTime(Context context, long time) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(SETTINGS_CALL_LOG_CLEAR_TIME, time);
        editor.commit();
    }

    public static long getCallLogClearTime(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getLong(SETTINGS_CALL_LOG_CLEAR_TIME, 0);
    }
}
