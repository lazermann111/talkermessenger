package com.talkermessenger.util;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;

public class PreImEditText extends android.support.v7.widget.AppCompatEditText {
    public PreImEditText(Context context) {
        super(context);
    }

    public PreImEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PreImEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    OnKeyPreImeListener onKeyPreImeListener;

    public void setOnKeyPreImeListener(OnKeyPreImeListener onKeyPreImeListener) {
        this.onKeyPreImeListener = onKeyPreImeListener;
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
            if (onKeyPreImeListener != null)
                onKeyPreImeListener.onBackPressed();
            return false;
        } return false;
    }

    public interface OnKeyPreImeListener {
        void onBackPressed();
    }
}
