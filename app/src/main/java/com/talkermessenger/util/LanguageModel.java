package com.talkermessenger.util;

import android.graphics.drawable.Drawable;

/**
 * Created by dev_serhii on 18.01.2018.
 */

public class LanguageModel {
    private String languageName;
    private Drawable languageIcon;

    public LanguageModel(String languageName, Drawable languageIcon) {
        this.languageName = languageName;
        this.languageIcon = languageIcon;
    }

    public String getLanguageName() {
        return languageName;
    }

    public Drawable getLanguageIcon() {
        return languageIcon;
    }
}
