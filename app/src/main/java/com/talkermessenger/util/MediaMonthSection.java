package com.talkermessenger.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.talkermessenger.Matrix;
import com.talkermessenger.R;
import com.talkermessenger.activity.CommonActivityUtils;
import com.talkermessenger.activity.VectorMediasViewerActivity;
import com.talkermessenger.db.VectorContentProvider;
import com.talkermessenger.fragments.VectorMessageListFragment;

import org.matrix.androidsdk.MXSession;
import org.matrix.androidsdk.db.MXMediasCache;
import org.matrix.androidsdk.listeners.MXMediaDownloadListener;
import org.matrix.androidsdk.rest.callback.SimpleApiCallback;
import org.matrix.androidsdk.rest.model.EncryptedFileInfo;
import org.matrix.androidsdk.rest.model.Event;
import org.matrix.androidsdk.rest.model.FileMessage;
import org.matrix.androidsdk.rest.model.ImageMessage;
import org.matrix.androidsdk.rest.model.MatrixError;
import org.matrix.androidsdk.rest.model.Message;
import org.matrix.androidsdk.rest.model.VideoMessage;
import org.matrix.androidsdk.util.JsonUtils;
import org.matrix.androidsdk.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import io.github.luizgrp.sectionedrecyclerviewadapter.SectionParameters;
import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection;
import nl.changer.audiowife.AudioWife;


public class MediaMonthSection extends StatelessSection {

    String mTitle;
    List<Event> mData;
    private MXSession mSession;
    private Activity mActivity;
    private String LOG_TAG = getClass().getName();
    private ArrayList<String> previousLinks = new ArrayList<>();

    public MediaMonthSection(String title, List<Event> list, Activity activity, MXSession session) {
        super(SectionParameters.builder()
                .itemResourceId(R.layout.item_vector_member_media)
                .headerResourceId(R.layout.item_header_vector_member_media)
                .build());

        this.mTitle = title;
        this.mData = list;
        this.mActivity = activity;
        this.mSession = session;
        this.mActivity = activity;
    }

    @Override
    public int getContentItemsTotal() {
        return mData.size();
    }

    @Override
    public RecyclerView.ViewHolder getItemViewHolder(View view) {
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindItemViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ItemViewHolder itemViewHolder = (ItemViewHolder) holder;

        Event event = mData.get(position);
        Message message = JsonUtils.toMessage(event.getContent());

        String thumbUrl = null;
        int avatarId = R.drawable.filetype_attachment;
        EncryptedFileInfo encryptedFileInfo = null;

        itemViewHolder.mPlay.setVisibility(View.GONE);
        itemViewHolder.mSeekBar.setVisibility(View.GONE);

        if (Message.MSGTYPE_IMAGE.equals(message.msgtype)) {
            ImageMessage imageMessage = JsonUtils.toImageMessage(event.getContent());
            thumbUrl = imageMessage.getThumbnailUrl();

            if (null == thumbUrl) {
                thumbUrl = imageMessage.getUrl();
            }

            if ("image/gif".equals(imageMessage.getMimeType())) {
                avatarId = R.drawable.filetype_gif;
            } else {
                avatarId = R.drawable.filetype_image;
            }

            if (null != imageMessage.info) {
                encryptedFileInfo = imageMessage.info.thumbnail_file;
            }
        } else if (Message.MSGTYPE_VIDEO.equals(message.msgtype)) {
            VideoMessage videoMessage = JsonUtils.toVideoMessage(event.getContent());

            thumbUrl = videoMessage.getThumbnailUrl();

            avatarId = R.drawable.filetype_video;

            if (null != videoMessage.info) {
                encryptedFileInfo = videoMessage.info.thumbnail_file;
            }

        } else if (Message.MSGTYPE_FILE.equals(message.msgtype) || Message.MSGTYPE_AUDIO.equals(message.msgtype)) {

            if (Message.MSGTYPE_AUDIO.equals(message.msgtype)) {
                avatarId = R.drawable.filetype_audio;
                itemViewHolder.mPlay.setVisibility(View.VISIBLE);
                itemViewHolder.mSeekBar.setVisibility(View.VISIBLE);
                downloadFile(mSession.getMediasCache().getDownloadableUrl(JsonUtils.toFileMessage(event.getContent()).getUrl()), itemViewHolder);
            } else avatarId = R.drawable.filetype_attachment;
        } else if (Message.MSGTYPE_TEXT.equals(message.msgtype)) {
            itemViewHolder.mMediaView.setVisibility(View.GONE);

            String[] words = message.body.split(" ");
            if (words.length > 0) {
                outerloop:
                for (String word : words) {
                    if (word.contains("http") || word.contains("https")) {
                        if (previousLinks.size() > 0) {
                            boolean isLinkNew = true;
                            for (String link : previousLinks) {
                                if (link.equals(word)) {
                                    isLinkNew = false;
                                }
                            }
                            if (isLinkNew) {
                                String link = word;
                                if (word.length() > 25)
                                    link = word.substring(0, 25) + "...";
                                itemViewHolder.mLink.setText(link);
                                itemViewHolder.mLink.setVisibility(View.VISIBLE);
                                previousLinks.add(word);
                                onLinkClick(itemViewHolder, word);
                                break outerloop;
                            }
                        } else {
                            String link = word;
                            if (word.length() > 25)
                                link = word.substring(0, 25) + "...";
                            itemViewHolder.mLink.setText(link);
                            itemViewHolder.mLink.setVisibility(View.VISIBLE);
                            previousLinks.add(word);
                            onLinkClick(itemViewHolder, word);
                            break outerloop;
                        }
                    }
                }
            }

            avatarId = R.drawable.filetype_link;
        }

        // thumbnail
        ImageView thumbnailView = itemViewHolder.mMediaView;

        // default avatar
        thumbnailView.setImageResource(avatarId);

        if (null != thumbUrl) {
            // detect if the media is encrypted
            if (null == encryptedFileInfo) {
                int size = mActivity.getResources().getDimensionPixelSize(R.dimen.member_list_avatar_size);
                mSession.getMediasCache().loadAvatarThumbnail(mSession.getHomeServerConfig(), thumbnailView, thumbUrl, size);
            } else {
                mSession.getMediasCache().loadBitmap(mSession.getHomeServerConfig(), thumbnailView, thumbUrl, 0, ExifInterface.ORIENTATION_UNDEFINED, null, encryptedFileInfo);
            }
        }

        itemViewHolder.mMediaView.setOnClickListener(view -> {
            onItemClick(itemViewHolder, event);
        });
    }

    private void onLinkClick(ItemViewHolder itemViewHolder, String word) {
        itemViewHolder.mLink.setOnClickListener(view -> {
            try {
                Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(word));
                mActivity.startActivity(myIntent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(mActivity, "No application can handle this request.", Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        });
    }

    @Override
    public RecyclerView.ViewHolder getHeaderViewHolder(View view) {
        return new HeaderViewHolder(view);
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder) {
        HeaderViewHolder headerHolder = (HeaderViewHolder) holder;
        headerHolder.mTitle.setText(mTitle);
    }


    private class HeaderViewHolder extends RecyclerView.ViewHolder {

        private TextView mTitle;

        HeaderViewHolder(View view) {
            super(view);
            mTitle = (TextView) view.findViewById(R.id.media_month_title);
        }
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        ImageView mMediaView;
        ImageView mPlay;
        ImageView mPause;
        SeekBar mSeekBar;
        TextView mLink;

        ItemViewHolder(View itemView) {
            super(itemView);
            mMediaView = (ImageView) itemView.findViewById(R.id.media_view);
            mPlay = (ImageView) itemView.findViewById(R.id.play);
            mPause = (ImageView) itemView.findViewById(R.id.pause);
            mSeekBar = (SeekBar) itemView.findViewById(R.id.media_seekbar);
            mLink = (TextView) itemView.findViewById(R.id.link);
            mSeekBar.getThumb().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
            mPlay.setColorFilter(Color.WHITE);
            mPause.setColorFilter(Color.WHITE);
        }
    }


    public void onItemClick(ItemViewHolder itemViewHolder, Event event) {
        Message message = JsonUtils.toMessage(event.getContent());

        // video and images are displayed inside a medias slider.
        if (Message.MSGTYPE_IMAGE.equals(message.msgtype) || (Message.MSGTYPE_VIDEO.equals(message.msgtype))) {
            ArrayList<SlidableMediaInfo> mediaMessagesList = listSlidableMessages(event);
            int listPosition = getMediaMessagePosition(mediaMessagesList, message);

            if (listPosition >= 0) {
                Intent viewImageIntent = new Intent(mActivity, VectorMediasViewerActivity.class);

                viewImageIntent.putExtra(VectorMediasViewerActivity.EXTRA_MATRIX_ID, mSession.getCredentials().userId);
                viewImageIntent.putExtra(VectorMediasViewerActivity.KEY_INFO_LIST, mediaMessagesList);
                viewImageIntent.putExtra(VectorMediasViewerActivity.KEY_INFO_LIST_INDEX, listPosition);

                mActivity.startActivity(viewImageIntent);
            }
        } else if (Message.MSGTYPE_FILE.equals(message.msgtype)) {
            FileMessage fileMessage = JsonUtils.toFileMessage(event.getContent());

            if (null != fileMessage.getUrl()) {
                onMediaAction(VectorMessageListFragment.ACTION_VECTOR_OPEN, fileMessage.getUrl(), fileMessage.getMimeType(), fileMessage.body, fileMessage.file);
            }
        }
//        else if (Message.MSGTYPE_AUDIO.equals(message.msgtype)) {
//            downloadFile(mSession.getMediasCache().getDownloadableUrl(JsonUtils.toFileMessage(event.getContent()).getUrl()), itemViewHolder);
//        }
    }

    int getMediaMessagePosition(ArrayList<SlidableMediaInfo> mediaMessagesList, Message mediaMessage) {
        String url = null;

        if (mediaMessage instanceof ImageMessage) {
            url = ((ImageMessage) mediaMessage).getUrl();
        } else if (mediaMessage instanceof VideoMessage) {
            url = ((VideoMessage) mediaMessage).getUrl();
        }

        // sanity check
        if (null == url) {
            return -1;
        }

        for (int index = 0; index < mediaMessagesList.size(); index++) {
            if (mediaMessagesList.get(index).mMediaUrl.equals(url)) {
                return index;
            }
        }

        return -1;
    }

    ArrayList<SlidableMediaInfo> listSlidableMessages(Event event) {
        ArrayList<SlidableMediaInfo> res = new ArrayList<>();

        Message message = JsonUtils.toMessage(event.getContent());

        if (Message.MSGTYPE_IMAGE.equals(message.msgtype)) {
            ImageMessage imageMessage = (ImageMessage) message;

            SlidableMediaInfo info = new SlidableMediaInfo();
            info.mMessageType = Message.MSGTYPE_IMAGE;
            info.mFileName = imageMessage.body;
            info.mMediaUrl = imageMessage.getUrl();
            info.mRotationAngle = imageMessage.getRotation();
            info.mOrientation = imageMessage.getOrientation();
            info.mMimeType = imageMessage.getMimeType();
            info.mEncryptedFileInfo = imageMessage.file;
            res.add(info);
        } else if (Message.MSGTYPE_VIDEO.equals(message.msgtype)) {
            SlidableMediaInfo info = new SlidableMediaInfo();
            VideoMessage videoMessage = (VideoMessage) message;
            info.mMessageType = Message.MSGTYPE_VIDEO;
            info.mFileName = videoMessage.body;
            info.mMediaUrl = videoMessage.getUrl();
            info.mThumbnailUrl = (null != videoMessage.info) ? videoMessage.info.thumbnail_url : null;
            info.mMimeType = videoMessage.getMimeType();
            info.mEncryptedFileInfo = videoMessage.file;
            res.add(info);
        }

        return res;
    }

    void onMediaAction(final int menuAction, final String mediaUrl, final String mediaMimeType, final String filename, final EncryptedFileInfo encryptedFileInfo) {
        MXMediasCache mediasCache = Matrix.getInstance(mActivity).getMediasCache();
        File file = mediasCache.mediaCacheFile(mediaUrl, mediaMimeType);

        // check if the media has already been downloaded
        if (null != file) {
            // download
            if ((menuAction == VectorMessageListFragment.ACTION_VECTOR_SAVE) || (menuAction == VectorMessageListFragment.ACTION_VECTOR_OPEN)) {
                CommonActivityUtils.saveMediaIntoDownloads(mActivity, file, filename, mediaMimeType, new SimpleApiCallback<String>() {
                    @Override
                    public void onSuccess(String savedMediaPath) {
                        if (null != savedMediaPath) {
                            if (menuAction == VectorMessageListFragment.ACTION_VECTOR_SAVE) {
                                Toast.makeText(mActivity, mActivity.getText(R.string.media_slider_saved), Toast.LENGTH_LONG).show();
                            } else {
                                CommonActivityUtils.openMedia(mActivity, savedMediaPath, mediaMimeType);
                            }
                        }
                    }
                });
            } else {
                // shared / forward
                Uri mediaUri = null;

                File renamedFile = file;

                if (!TextUtils.isEmpty(filename)) {
                    try {
                        InputStream fin = new FileInputStream(file);
                        String tmpUrl = mediasCache.saveMedia(fin, filename, mediaMimeType);

                        if (null != tmpUrl) {
                            renamedFile = mediasCache.mediaCacheFile(tmpUrl, mediaMimeType);
                        }
                    } catch (Exception e) {
                        Log.e(LOG_TAG, "onMediaAction shared / forward failed : " + e.getLocalizedMessage());
                    }
                }

                if (null != renamedFile) {
                    try {
                        mediaUri = VectorContentProvider.absolutePathToUri(mActivity, renamedFile.getAbsolutePath());
                    } catch (Exception e) {
                        Log.e(LOG_TAG, "onMediaAction VectorContentProvider.absolutePathToUri: " + e.getLocalizedMessage());
                    }
                }

                if (null != mediaUri) {
                    final Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.setType(mediaMimeType);
                    sendIntent.putExtra(Intent.EXTRA_STREAM, mediaUri);

                    if (menuAction == VectorMessageListFragment.ACTION_VECTOR_FORWARD) {
                        CommonActivityUtils.sendFilesTo(mActivity, sendIntent);
                    } else {
                        mActivity.startActivity(sendIntent);
                    }
                }
            }
        } else {
            // else download it
            final String downloadId = mediasCache.downloadMedia(mActivity.getApplicationContext(), mSession.getHomeServerConfig(), mediaUrl, mediaMimeType, encryptedFileInfo);

            if (null != downloadId) {
                mediasCache.addDownloadListener(downloadId, new MXMediaDownloadListener() {
                    @Override
                    public void onDownloadError(String downloadId, JsonElement jsonElement) {
                        MatrixError error = JsonUtils.toMatrixError(jsonElement);

                        if ((null != error) && error.isSupportedErrorCode()) {
                            Toast.makeText(mActivity, error.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onDownloadComplete(String aDownloadId) {
                        if (aDownloadId.equals(downloadId)) {

                            mActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    onMediaAction(menuAction, mediaUrl, mediaMimeType, filename, encryptedFileInfo);
                                }
                            });
                        }
                    }
                });
            }
        }

    }

    @SuppressLint("StaticFieldLeak")
    private void downloadFile(String url, ItemViewHolder itemViewHolder) {
//        File file = new File(mActivity.getCacheDir(), getFileName(url));
        File mediaBaseFolderFile = new File(mActivity.getApplicationContext().getFilesDir(), "MXMediaStore");
        File audioFolder = new File(mediaBaseFolderFile, "Audio");
        if (!audioFolder.exists())
            audioFolder.mkdirs();
        File file = new File(audioFolder, getFileName(url));
        if (file.exists()) {
            if (itemViewHolder.mPlay != null) {
                AudioWife audioWife = new AudioWife();
                audioWife.init(mActivity, Uri.fromFile(file))
                        .setPlayView(itemViewHolder.mPlay)
                        .setPauseView(itemViewHolder.mPause)
                        .setSeekBar(itemViewHolder.mSeekBar);
            }
        } else {
            new AsyncTask<String, String, String>() {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                }

                @Override
                protected String doInBackground(String... strings) {
                    InputStream input = null;
                    OutputStream output = null;
                    HttpURLConnection connection = null;
                    try {
                        URL url = new URL(strings[0]);
                        connection = (HttpURLConnection) url.openConnection();
                        connection.connect();

                        if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                            return "Server returned HTTP " + connection.getResponseCode()
                                    + " " + connection.getResponseMessage();
                        }

                        // download the file
                        input = connection.getInputStream();
//                        output = new FileOutputStream(mActivity.getCacheDir() + getFileName(strings[0]));
                        String name = audioFolder + getFileName(strings[0]);
                        output = new FileOutputStream(name);

                        byte data[] = new byte[4096];
                        int count;
                        while ((count = input.read(data)) != -1) {
                            // allow canceling with back button
                            if (isCancelled()) {
                                input.close();
                                return null;
                            }
                            output.write(data, 0, count);
                        }
                    } catch (Exception e) {
                        return e.toString();
                    } finally {
                        try {
                            if (output != null)
                                output.close();
                            if (input != null)
                                input.close();
                        } catch (IOException ignored) {
                        }

                        if (connection != null)
                            connection.disconnect();
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(String result) {
                    if (itemViewHolder.mPlay != null) {
                        AudioWife audioWife = new AudioWife();
                        audioWife.init(mActivity, Uri.fromFile(file))
                                .setPlayView(itemViewHolder.mPlay)
                                .setPauseView(itemViewHolder.mPause)
                                .setSeekBar(itemViewHolder.mSeekBar);
                    }
                }
            }.execute(url);
        }
    }

    private String getFileName(String url) {
        Log.e("filename", url.substring(url.lastIndexOf('/')));
        return url.substring(url.lastIndexOf('/'));
    }
}
