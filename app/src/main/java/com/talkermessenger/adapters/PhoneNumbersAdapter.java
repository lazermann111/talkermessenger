package com.talkermessenger.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.talkermessenger.R;

import java.util.List;

public class PhoneNumbersAdapter extends RecyclerView.Adapter<PhoneNumbersAdapter.ViewHolder> {
    private List<String> mDataset;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mTextView;

        public ViewHolder(View v) {
            super(v);
            mTextView = v.findViewById(R.id.additional_phone_number_section_text);
        }
    }

    public PhoneNumbersAdapter(List<String> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public PhoneNumbersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                             int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_item_phone_number, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            PhoneNumberUtil pnu = PhoneNumberUtil.getInstance();
            Phonenumber.PhoneNumber pn = pnu.parse(mDataset.get(position), null);
            String phoneNumber = PhoneNumberUtil.getInstance().format(pn,
                    PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL);
            holder.mTextView.setText(phoneNumber);
        } catch (Exception e) {
            e.printStackTrace();
            holder.mTextView.setText(mDataset.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
