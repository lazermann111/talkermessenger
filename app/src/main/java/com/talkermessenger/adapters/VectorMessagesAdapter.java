/*
 * Copyright 2017 Vector Creations Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.talkermessenger.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.provider.ContactsContract;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.text.style.BackgroundColorSpan;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.iskar.talkersdk.LanguagesHandler;
import com.iskar.talkersdk.TranslationHandler;
import com.iskar.talkersdk.callback.TranslationInterface;
import com.iskar.talkersdk.model.Translation;
import com.talkermessenger.GlideApp;
import com.talkermessenger.R;
import com.talkermessenger.VectorApp;
import com.talkermessenger.activity.CommonActivityUtils;
import com.talkermessenger.activity.VectorRoomActivity;
import com.talkermessenger.listeners.IMessagesAdapterActionsListener;
import com.talkermessenger.util.EventGroup;
import com.talkermessenger.util.MatrixLinkMovementMethod;
import com.talkermessenger.util.MatrixURLSpan;
import com.talkermessenger.util.PreferencesManager;
import com.talkermessenger.util.RiotEventDisplay;
import com.talkermessenger.util.ThemeUtils;
import com.talkermessenger.util.VectorUtils;
import com.talkermessenger.widgets.WidgetsManager;

import org.json.JSONException;
import org.json.JSONObject;
import org.matrix.androidsdk.MXSession;
import org.matrix.androidsdk.adapters.AbstractMessagesAdapter;
import org.matrix.androidsdk.adapters.MessageRow;
import org.matrix.androidsdk.crypto.data.MXDeviceInfo;
import org.matrix.androidsdk.data.Room;
import org.matrix.androidsdk.data.RoomState;
import org.matrix.androidsdk.data.RoomSummary;
import org.matrix.androidsdk.data.store.IMXStore;
import org.matrix.androidsdk.db.MXMediasCache;
import org.matrix.androidsdk.rest.callback.ApiCallback;
import org.matrix.androidsdk.rest.model.ContactMessage;
import org.matrix.androidsdk.rest.model.EncryptedEventContent;
import org.matrix.androidsdk.rest.model.Event;
import org.matrix.androidsdk.rest.model.EventContent;
import org.matrix.androidsdk.rest.model.FileMessage;
import org.matrix.androidsdk.rest.model.ImageMessage;
import org.matrix.androidsdk.rest.model.LocationMessage;
import org.matrix.androidsdk.rest.model.MatrixError;
import org.matrix.androidsdk.rest.model.Message;
import org.matrix.androidsdk.rest.model.PowerLevels;
import org.matrix.androidsdk.rest.model.ReceiptData;
import org.matrix.androidsdk.rest.model.RoomMember;
import org.matrix.androidsdk.rest.model.TranslationMessage;
import org.matrix.androidsdk.util.EventDisplay;
import org.matrix.androidsdk.util.JsonUtils;
import org.matrix.androidsdk.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Formatter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;
import nl.changer.audiowife.AudioWife;

import static android.content.pm.PackageManager.MATCH_ALL;
import static android.graphics.Color.LTGRAY;
import static android.graphics.Color.TRANSPARENT;
import static android.graphics.Color.WHITE;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.talkermessenger.activity.VectorMemberDetailsActivity.EXTRA_ROOM_ID;

/**
 * An adapter which can display room information.
 */
public class VectorMessagesAdapter extends AbstractMessagesAdapter {
    static final int ROW_TYPE_TEXT = 0;
    static final int ROW_TYPE_TEXT_OUT = 1;
    static final int ROW_TYPE_IMAGE = 2;
    static final int ROW_TYPE_IMAGE_OUT = 3;
    static final int ROW_TYPE_NOTICE = 4;
    static final int ROW_TYPE_ROOM_MEMBER = 5;
    static final int ROW_TYPE_EMOTE = 6;
    static final int ROW_TYPE_EMOTE_OUT = 7;
    static final int ROW_TYPE_FILE = 8;
    static final int ROW_TYPE_FILE_OUT = 9;
    static final int ROW_TYPE_VIDEO = 10;
    static final int ROW_TYPE_VIDEO_OUT = 11;
    static final int ROW_TYPE_MERGE = 12;
    static final int ROW_TYPE_HIDDEN = 13;
    static final int ROW_TYPE_EMOJI = 14;
    static final int ROW_TYPE_EMOJI_OUT = 15;
    static final int ROW_TYPE_AUDIO = 16;
    static final int ROW_TYPE_AUDIO_OUT = 17;
    static final int ROW_TYPE_CONTACT = 18;
    static final int ROW_TYPE_LOCATION = 19;
    static final int ROW_TYPE_CONTACT_OUT = 20;
    static final int ROW_TYPE_LOCATION_OUT = 21;
    static final int ROW_TYPE_TRANSLATION = 22;
    static final int ROW_TYPE_TRANSLATION_OUT = 23;
    static final int NUM_ROW_TYPES = 24;
    private static final String LOG_TAG = VectorMessagesAdapter.class.getSimpleName();
    private static final Pattern mEmojisPattern = Pattern.compile("((?:[\uD83C\uDF00-\uD83D\uDDFF]|[\uD83E\uDD00-\uD83E\uDDFF]|[\uD83D\uDE00-\uD83D\uDE4F]|[\uD83D\uDE80-\uD83D\uDEFF]|[\u2600-\u26FF]\uFE0F?|[\u2700-\u27BF]\uFE0F?|\u24C2\uFE0F?|[\uD83C\uDDE6-\uD83C\uDDFF]{1,2}|[\uD83C\uDD70\uD83C\uDD71\uD83C\uDD7E\uD83C\uDD7F\uD83C\uDD8E\uD83C\uDD91-\uD83C\uDD9A]\uFE0F?|[\u0023\u002A\u0030-\u0039]\uFE0F?\u20E3|[\u2194-\u2199\u21A9-\u21AA]\uFE0F?|[\u2B05-\u2B07\u2B1B\u2B1C\u2B50\u2B55]\uFE0F?|[\u2934\u2935]\uFE0F?|[\u3030\u303D]\uFE0F?|[\u3297\u3299]\uFE0F?|[\uD83C\uDE01\uD83C\uDE02\uD83C\uDE1A\uD83C\uDE2F\uD83C\uDE32-\uD83C\uDE3A\uD83C\uDE50\uD83C\uDE51]\uFE0F?|[\u203C\u2049]\uFE0F?|[\u25AA\u25AB\u25B6\u25C0\u25FB-\u25FE]\uFE0F?|[\u00A9\u00AE]\uFE0F?|[\u2122\u2139]\uFE0F?|\uD83C\uDC04\uFE0F?|\uD83C\uDCCF\uFE0F?|[\u231A\u231B\u2328\u23CF\u23E9-\u23F3\u23F8-\u23FA]\uFE0F?))");
    final Activity mContext;
    final LayoutInflater mLayoutInflater;
    // session
    final MXSession mSession;
    final VectorMessagesAdapterHelper mHelper;
    private TranslationHandler translationHandler;
    // formatted time by event id
    // it avoids computing them several times
    private final HashMap<String, String> mEventFormattedTsMap = new HashMap<>();
    private final HashMap<Integer, Integer> mRowTypeToLayoutId = new HashMap<>();
    // To keep track of events and avoid duplicates. For instance, we add a message event
    // when the current user sends one but it will also come down the event stream
    private final HashMap<String, MessageRow> mEventRowMap = new HashMap<>();
    private final HashMap<String, Integer> mEventType = new HashMap<>();
    // the message text colors
    private final int mDefaultMessageTextColor;
    private final int mNotSentMessageTextColor;
    private final int mSendingMessageTextColor;
    private final int mEncryptingMessageTextColor;
    private final int mHighlightMessageTextColor;
    private final int mMaxImageWidth;
    private final int mMaxImageHeight;
    // media cache
    private final MXMediasCache mMediasCache;
    private final VectorMessagesAdapterMediasHelper mMediasHelper;
    private final Set<String> mHiddenEventIds = new HashSet<>();
    private final Set<String> mFavouriteEventIds = new HashSet<>();
    private final Locale mLocale;
    // custom settings
    private final boolean mAlwaysShowTimeStamps;
    private final boolean mHideReadReceipts;
    // the color depends in the theme
    private final Drawable mPadlockDrawable;
    private final List<EventGroup> mEventGroups = new ArrayList<>();
    /**
     * callback for the creation of the direct message room
     **/
    private final ApiCallback<String> mCreateDirectMessageCallBack = new ApiCallback<String>() {
        @Override
        public void onSuccess(String roomId) {
            Log.d(LOG_TAG, "## mCreateDirectMessageCallBack: onSuccess - start goToRoomPage " + roomId);
            openRoomById(roomId);
        }

        @Override
        public void onMatrixError(MatrixError e) {
            Log.d(LOG_TAG, "## mCreateDirectMessageCallBack: onMatrixError Msg=" + e.getLocalizedMessage());
        }

        @Override
        public void onNetworkError(Exception e) {
            Log.d(LOG_TAG, "## mCreateDirectMessageCallBack: onNetworkError Msg=" + e.getLocalizedMessage());
        }

        @Override
        public void onUnexpectedError(Exception e) {
            Log.d(LOG_TAG, "## mCreateDirectMessageCallBack: onUnexpectedError Msg=" + e.getLocalizedMessage());
        }
    };
    // true when the room is encrypted
    public boolean mIsRoomEncrypted;
    //true when group chat
    public boolean mIsGroup;
    // events listeners
    IMessagesAdapterActionsListener mVectorMessagesAdapterEventsListener = null;
    int mSearchHighlightMessageTextColor;
    // an event is selected when the user taps on it
    private String mSelectedEventId;
    // current date : used to compute the day header
    private Date mReferenceDate = new Date();
    // day date of each message
    // the hours, minutes and seconds are removed
    private ArrayList<Date> mMessagesDateList = new ArrayList<>();
    // when the adapter is used in search mode
    // the searched message should be highlighted
    private String mSearchedEventId = null;
    private String mHighlightedEventId = null;
    // define the e2e icon to use for a dedicated eventId
    // can be a drawable or
    private HashMap<String, Object> mE2eIconByEventId = new HashMap<>();
    // device info by device id
    private HashMap<String, MXDeviceInfo> mE2eDeviceByEventId = new HashMap<>();
    private boolean mIsSearchMode = false;
    private boolean mIsPreviewMode = false;
    private boolean mIsUnreadViewMode = false;
    private String mPattern = null;
    private ArrayList<MessageRow> mLiveMessagesRowList = null;
    // id of the read markers event
    private String mReadReceiptEventId;
    private MatrixLinkMovementMethod mLinkMovementMethod;
    private LanguagesHandler languagesHandler;
    private String myLanguage;
    /*
     * *********************************************************************************************
     * Graphical items
     * *********************************************************************************************
     */
    private int currentSearchRow = -1;
    private ArrayList<Integer> searchedEvents = new ArrayList<>();
    private boolean isFavouriteMode;
    private List<MessageRow> rows = new ArrayList<>();
    private String mReadMarkerEventId;
    private boolean mCanShowReadMarker = true;
    private ReadMarkerListener mReadMarkerListener;

    /**
     * Creates a messages adapter with the default layouts.
     */
    public VectorMessagesAdapter(MXSession session, Activity context, MXMediasCache mediasCache) {
        this(session, context,
                R.layout.adapter_item_vector_message_text_emote_notice,
                R.layout.adapter_item_vector_message_text_emote_notice_out,
                R.layout.adapter_item_vector_message_image_video,
                R.layout.adapter_item_vector_message_image_video_out,
                R.layout.adapter_item_vector_message_text_emote_notice,
                R.layout.adapter_item_vector_message_room_member,
                R.layout.adapter_item_vector_message_text_emote_notice,
                R.layout.adapter_item_vector_message_text_emote_notice_out,
                R.layout.adapter_item_vector_message_file,
                R.layout.adapter_item_vector_message_file_out,
                R.layout.adapter_item_vector_message_image_video,
                R.layout.adapter_item_vector_message_image_video_out,
                R.layout.adapter_item_vector_message_merge,
                R.layout.adapter_item_vector_hidden_message,
                R.layout.adapter_item_vector_message_emoji,
                R.layout.adapter_item_vector_message_emoji_out,
                R.layout.adapter_item_vector_message_audio,
                R.layout.adapter_item_vector_message_audio_out,
                R.layout.adapter_item_vector_message_contact,
                R.layout.adapter_item_vector_message_location,
                R.layout.adapter_item_vector_message_contact_out,
                R.layout.adapter_item_vector_message_location_out,
                R.layout.adapter_item_vector_message_text_emote_notice,
                R.layout.adapter_item_vector_message_text_emote_notice_out,
                mediasCache);
    }

    /**
     * Expanded constructor.
     * each message type has its own layout.
     *
     * @param session           the dedicated layout.
     * @param context           the context
     * @param textResLayoutId   the text message layout.
     * @param imageResLayoutId  the image message layout.
     * @param noticeResLayoutId the notice message layout.
     * @param emoteRestLayoutId the emote message layout
     * @param fileResLayoutId   the file message layout
     * @param videoResLayoutId  the video message layout
     * @param mediasCache       the medias cache.
     */
    VectorMessagesAdapter(MXSession session,
                          Activity context,
                          int textResLayoutId,
                          int textOutResLayoutId,
                          int imageResLayoutId,
                          int imageOutResLayoutId,
                          int noticeResLayoutId,
                          int roomMemberResLayoutId,
                          int emoteRestLayoutId,
                          int emoteOutRestLayoutId,
                          int fileResLayoutId,
                          int fileOutResLayoutId,
                          int videoResLayoutId,
                          int videoOutResLayoutId,
                          int mergeResLayoutId,
                          int hiddenResLayoutId,
                          int emojiResLayoutId,
                          int emojiOutResLayoutId,
                          int audioResLayoutId,
                          int audioOutResLayoutId,
                          int contactResLayoutId,
                          int locationResLayoutId,
                          int contactResLayoutIdOut,
                          int locationResLayoutIdOut,
                          int translationResLayoutId,
                          int translationResLayoutIdOut,
                          MXMediasCache mediasCache) {
        super(context, 0);
        mContext = context;
        mRowTypeToLayoutId.put(ROW_TYPE_TEXT, textResLayoutId);
        mRowTypeToLayoutId.put(ROW_TYPE_TEXT_OUT, textOutResLayoutId);
        mRowTypeToLayoutId.put(ROW_TYPE_IMAGE, imageResLayoutId);
        mRowTypeToLayoutId.put(ROW_TYPE_IMAGE_OUT, imageOutResLayoutId);
        mRowTypeToLayoutId.put(ROW_TYPE_NOTICE, noticeResLayoutId);
        mRowTypeToLayoutId.put(ROW_TYPE_ROOM_MEMBER, roomMemberResLayoutId);
        mRowTypeToLayoutId.put(ROW_TYPE_EMOTE, emoteRestLayoutId);
        mRowTypeToLayoutId.put(ROW_TYPE_EMOTE_OUT, emoteOutRestLayoutId);
        mRowTypeToLayoutId.put(ROW_TYPE_FILE, fileResLayoutId);
        mRowTypeToLayoutId.put(ROW_TYPE_FILE_OUT, fileOutResLayoutId);
        mRowTypeToLayoutId.put(ROW_TYPE_VIDEO, videoResLayoutId);
        mRowTypeToLayoutId.put(ROW_TYPE_VIDEO_OUT, videoOutResLayoutId);
        mRowTypeToLayoutId.put(ROW_TYPE_MERGE, mergeResLayoutId);
        mRowTypeToLayoutId.put(ROW_TYPE_HIDDEN, hiddenResLayoutId);
        mRowTypeToLayoutId.put(ROW_TYPE_EMOJI, emojiResLayoutId);
        mRowTypeToLayoutId.put(ROW_TYPE_EMOJI_OUT, emojiOutResLayoutId);
        mRowTypeToLayoutId.put(ROW_TYPE_AUDIO, audioResLayoutId);
        mRowTypeToLayoutId.put(ROW_TYPE_AUDIO_OUT, audioOutResLayoutId);
        mRowTypeToLayoutId.put(ROW_TYPE_CONTACT, contactResLayoutId);
        mRowTypeToLayoutId.put(ROW_TYPE_LOCATION, locationResLayoutId);
        mRowTypeToLayoutId.put(ROW_TYPE_CONTACT_OUT, contactResLayoutIdOut);
        mRowTypeToLayoutId.put(ROW_TYPE_LOCATION_OUT, locationResLayoutIdOut);
        mRowTypeToLayoutId.put(ROW_TYPE_TRANSLATION, translationResLayoutId);
        mRowTypeToLayoutId.put(ROW_TYPE_TRANSLATION_OUT, translationResLayoutIdOut);
        mMediasCache = mediasCache;
        mLayoutInflater = LayoutInflater.from(mContext);
        // the refresh will be triggered only when it is required
        // for example, retrieve the historical messages triggers a refresh for each message
        setNotifyOnChange(false);

        mDefaultMessageTextColor = getDefaultMessageTextColor();
        mNotSentMessageTextColor = getNotSentMessageTextColor();
        mSendingMessageTextColor = getSendingMessageTextColor();
        mEncryptingMessageTextColor = getEncryptingMessageTextColor();
        mHighlightMessageTextColor = getHighlightMessageTextColor();
        mSearchHighlightMessageTextColor = getSearchHighlightMessageTextColor();

        Point size = new Point(0, 0);
        getScreenSize(size);

        int screenWidth = size.x;
        int screenHeight = size.y;

        // landscape / portrait
        if (screenWidth < screenHeight) {
            mMaxImageWidth = Math.round(screenWidth * 0.6f);
            mMaxImageHeight = Math.round(screenHeight * 0.4f);
        } else {
            mMaxImageWidth = Math.round(screenWidth * 0.4f);
            mMaxImageHeight = Math.round(screenHeight * 0.6f);
        }

        mSession = session;

        isFavouriteMode = mContext.getIntent().getBooleanExtra("isFavouriteMode", false);

        // helpers
        mMediasHelper = new VectorMessagesAdapterMediasHelper(context, mSession, mMaxImageWidth, mMaxImageHeight, mNotSentMessageTextColor, mDefaultMessageTextColor);
        mHelper = new VectorMessagesAdapterHelper(context, mSession);

        mLocale = VectorApp.getApplicationLocale();

        //mAlwaysShowTimeStamps = PreferencesManager.alwaysShowTimeStamps(VectorApp.getInstance());
        mAlwaysShowTimeStamps = true;
        mHideReadReceipts = PreferencesManager.hideReadReceipts(VectorApp.getInstance());

        mPadlockDrawable = CommonActivityUtils.tintDrawable(mContext, ContextCompat.getDrawable(mContext, R.drawable.e2e_unencrypted), R.attr.settings_icon_tint_color);
        translationHandler = new TranslationHandler(PreferencesManager.getTranslationTimer(mContext), mContext, "", 2, mSession.getMyUserId(), mSession.getCredentials().accessToken);
        myLanguage = new LanguagesHandler(mContext).getMyLanguages(mSession.getMyUserId()).getLangs().get(0);
    }

    /**
     * Test if a string contains emojis.
     * It seems that the regex [emoji_regex]+ does not work.
     * Some characters like ?, # or digit are accepted.
     *
     * @param body the body to test
     * @return true if the body contains only emojis
     */
    private static boolean containsOnlyEmojis(String body) {
        boolean res = false;

        if (!TextUtils.isEmpty(body)) {
            Matcher matcher = mEmojisPattern.matcher(body);

            int start = -1;
            int end = -1;

            while (matcher.find()) {
                int nextStart = matcher.start();

                // first emoji position
                if (start < 0) {
                    if (nextStart > 0) {
                        return false;
                    }
                } else {
                    // must not have a character between
                    if (nextStart != end) {
                        return false;
                    }
                }
                start = nextStart;
                end = matcher.end();
            }

            res = (-1 != start) && (end == body.length());
        }

        return res;
    }

    /*
     * *********************************************************************************************
     * Items getter / setter
     * *********************************************************************************************
     */

    /**
     * Tells if the event of type 'eventType' can be merged.
     *
     * @param eventType the event type to test
     * @return true if the event can be merged
     */
    private static boolean isMergeableEvent(int eventType) {
        return (ROW_TYPE_NOTICE != eventType) && (ROW_TYPE_ROOM_MEMBER != eventType) && (ROW_TYPE_HIDDEN != eventType);
    }

    /**
     * Return the screen size.
     *
     * @param size the size to set
     */
    @SuppressLint("NewApi")
    @SuppressWarnings("deprecation")
    private void getScreenSize(Point size) {
        WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            display.getSize(size);
        } else {
            size.set(display.getWidth(), display.getHeight());
        }
    }

    /**
     * @return the max thumbnail width
     */
    public int getMaxThumbnailWidth() {
        return mMaxImageWidth;
    }

    /**
     * @return the max thumbnail height
     */
    public int getMaxThumbnailHeight() {
        return mMaxImageHeight;
    }

    // customization methods
    private int getDefaultMessageTextColor() {
        return ThemeUtils.getColor(mContext, R.attr.message_text_color);
    }

    private int getNoticeTextColor() {
        return ThemeUtils.getColor(mContext, R.attr.notice_text_color);
    }

    private int getEncryptingMessageTextColor() {
        return ThemeUtils.getColor(mContext, R.attr.encrypting_message_text_color);
    }

    private int getSendingMessageTextColor() {
        return ThemeUtils.getColor(mContext, R.attr.sending_message_text_color);
    }

    private int getHighlightMessageTextColor() {
        return ThemeUtils.getColor(mContext, R.attr.highlighted_message_text_color);
    }

    private int getSearchHighlightMessageTextColor() {
        return ThemeUtils.getColor(mContext, R.attr.highlighted_searched_message_text_color);
    }

    private int getNotSentMessageTextColor() {
        return ThemeUtils.getColor(mContext, R.attr.unsent_message_text_color);
    }

    /*
     * *********************************************************************************************
     * Display modes
     * *********************************************************************************************
     */

    /**
     * Tests if the row can be inserted in a merge row.
     *
     * @param row the message row to test
     * @return true if the row can be merged
     */
    boolean supportMessageRowMerge(MessageRow row) {
        return EventGroup.isSupported(row);
    }

    @Override
    public void addToFront(MessageRow row) {
        boolean isRightFavouriteRoom = true;
        String roomId;
        String roomIdEventSpecialParameter;
        if (isFavouriteMode) {
            roomId = mContext.getIntent().getStringExtra("roomId");
            if (roomId != null) {
                roomIdEventSpecialParameter = row.getEvent().getFavouriteRoomId();
//            String favouriteRoomId = row.getEvent().getContentAsJsonObject().get("favouriteRoomId").getAsString();
//            if(!roomId.equals(favouriteRoomId)){
//                isRightFavouriteRoom = false;
//            }
                if (!roomId.equals(roomIdEventSpecialParameter)) {
                    isRightFavouriteRoom = false;
                }
            }
        }

        if (isSupportedRow(row) && isRightFavouriteRoom) {
            // ensure that notifyDataSetChanged is not called
            // it seems that setNotifyOnChange is reinitialized to true;
            setNotifyOnChange(false);

            if (mIsSearchMode) {
                mLiveMessagesRowList.add(0, row);
            } else {
                insert(row, (!addToEventGroupToFront(row)) ? 0 : 1);
            }

            if (row.getEvent().eventId != null) {
                mEventRowMap.put(row.getEvent().eventId, row);
            }
        }
    }

    @Override
    public void remove(MessageRow row) {
        if (null != row) {
            if (mIsSearchMode) {
                mLiveMessagesRowList.remove(row);
            } else {
                removeFromEventGroup(row);

                // get the position before removing the item
                int position = getPosition(row);

                // remove it
                super.remove(row);

                // check merge
                checkEventGroupsMerge(row, position);
            }
        }
    }

    @Override
    public void add(MessageRow row) {
        add(row, true);
    }

    @Override
    public void add(MessageRow row, boolean refresh) {
        // ensure that notifyDataSetChanged is not called
        // it seems that setNotifyOnChange is reinitialized to true;
        setNotifyOnChange(false);

        if (isSupportedRow(row)) {
            if (mIsSearchMode) {
                mLiveMessagesRowList.add(row);
            } else {
                addToEventGroup(row);
                super.add(row);
            }

            if (row.getEvent().eventId != null) {
                mEventRowMap.put(row.getEvent().eventId, row);
            }

            if ((!mIsSearchMode) && refresh) {
                this.notifyDataSetChanged();
            } else {
                setNotifyOnChange(true);
            }
        } else {
            setNotifyOnChange(true);
        }
    }

    @Override
    public MessageRow getMessageRow(String eventId) {
        if (null != eventId) {
            return mEventRowMap.get(eventId);
        } else {
            return null;
        }
    }

    @Override
    public MessageRow getClosestRow(Event event) {
        if (event == null) {
            return null;
        } else {
            return getClosestRowFromTs(event.eventId, event.getOriginServerTs());
        }
    }

    @Override
    public MessageRow getClosestRowFromTs(final String eventId, final long eventTs) {
        MessageRow messageRow = getMessageRow(eventId);

        if (messageRow == null) {
            List<MessageRow> rows = new ArrayList<>(mEventRowMap.values());

            // loop because the list is not sorted
            for (MessageRow row : rows) {
                if (!(row.getEvent() instanceof EventGroup)) {
                    long rowTs = row.getEvent().getOriginServerTs();

                    // check if the row event has been received after eventTs (from)
                    if (rowTs > eventTs) {
                        // not yet initialised
                        if (messageRow == null) {
                            messageRow = row;
                        }
                        // keep the closest row
                        else if (rowTs < messageRow.getEvent().getOriginServerTs()) {
                            messageRow = row;
                            Log.d(LOG_TAG, "## getClosestRowFromTs() " + row.getEvent().eventId);
                        }
                    }
                }
            }
        }

        return messageRow;
    }

    @Override
    public MessageRow getClosestRowBeforeTs(final String eventId, final long eventTs) {
        MessageRow messageRow = getMessageRow(eventId);

        if (messageRow == null) {
            List<MessageRow> rows = new ArrayList<>(mEventRowMap.values());

            // loop because the list is not sorted
            for (MessageRow row : rows) {
                if (!(row.getEvent() instanceof EventGroup)) {
                    long rowTs = row.getEvent().getOriginServerTs();

                    // check if the row event has been received before eventTs (from)
                    if (rowTs < eventTs) {
                        // not yet initialised
                        if (messageRow == null) {
                            messageRow = row;
                        }
                        // keep the closest row
                        else if (rowTs > messageRow.getEvent().getOriginServerTs()) {
                            messageRow = row;
                            Log.d(LOG_TAG, "## getClosestRowBeforeTs() " + row.getEvent().eventId);
                        }
                    }
                }
            }
        }

        return messageRow;
    }

    @Override
    public void updateEventById(Event event, String oldEventId) {
        MessageRow row = mEventRowMap.get(event.eventId);

        // the event is not yet defined
        if (null == row) {
            MessageRow oldRow = mEventRowMap.get(oldEventId);

            if (null != oldRow) {
                mEventRowMap.remove(oldEventId);
                mEventRowMap.put(event.eventId, oldRow);
            }
        } else {
            // the eventId already exists
            // remove the old display
            removeEventById(oldEventId);
        }

        notifyDataSetChanged();
    }

    @Override
    public void removeEventById(String eventId) {
        // ensure that notifyDataSetChanged is not called
        // it seems that setNotifyOnChange is reinitialized to true;
        setNotifyOnChange(false);

        MessageRow row = mEventRowMap.get(eventId);

        if (row != null) {
            remove(row);
        }
    }


    /*
     * *********************************************************************************************
     * ArrayAdapter methods
     * *********************************************************************************************
     */

    @Override
    public void setIsPreviewMode(boolean isPreviewMode) {
        mIsPreviewMode = isPreviewMode;
    }

    @Override
    public void setIsUnreadViewMode(boolean isUnreadViewMode) {
        mIsUnreadViewMode = isUnreadViewMode;
    }

    @Override
    public boolean isUnreadViewMode() {
        return mIsUnreadViewMode;
    }

    /*
     * *********************************************************************************************
     * Preview mode
     * *********************************************************************************************
     */
    @Override
    public void setSearchPattern(String pattern) {
        clearSearchMarkers();
        if (!TextUtils.equals(pattern, mPattern)) {
            mPattern = pattern;
            mIsSearchMode = !TextUtils.isEmpty(mPattern);
            // in search mode, the live row are cached.
            if (mIsSearchMode) {
                // backup live events
                mLiveMessagesRowList = new ArrayList<>();
                for (int pos = 0; pos < this.getCount(); pos++) {
                    mLiveMessagesRowList.add(this.getItem(pos));
                    Message message = JsonUtils.toMessage(mLiveMessagesRowList.get(pos).getEvent().getContent());
                    if (message.msgtype.equals(Message.MSGTYPE_TEXT))
                        if (message.body.toLowerCase().contains(pattern.toLowerCase())) {
                            mLiveMessagesRowList.get(pos).setSearched(true);
                            Log.e("body position", message.body + "");
                            Log.e("position ", pos + "");
                        }
                    if (message.msgtype.equals(Message.MSGTYPE_TRANSLATION)) {
                        try {
                            String textTranslated = new JSONObject(message.body).getString("textTranslated");
                            String textOriginal = new JSONObject(message.body).getString("textOriginal");
                            if (textTranslated.toLowerCase().contains(pattern.toLowerCase())
                                    || textOriginal.toLowerCase().contains(pattern.toLowerCase())) {
                                mLiveMessagesRowList.get(pos).setSearched(true);
                                Log.e("body position", message.body + "");
                                Log.e("position ", pos + "");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }
            } else if (null != mLiveMessagesRowList) {
                // clear and restore the cached live events.
                this.clear();
                this.addAll(mLiveMessagesRowList);
                mLiveMessagesRowList = null;
                notifyDataSetChanged();
            }
        }
    }

    private void clearSearchMarkers() {
        searchedEvents.clear();
        if (mLiveMessagesRowList != null)
            for (MessageRow row : this.mLiveMessagesRowList) {
                row.setSearched(false);
            }
    }

    private ArrayList<Integer> getSearchedEventsList() {
        ArrayList<Integer> searchedEvents = new ArrayList<>();
        if (this.mLiveMessagesRowList != null)
            for (MessageRow row : this.mLiveMessagesRowList) {
                if (row.isSearched() && getPosition(row) > currentSearchRow) {
                    if (!searchedEvents.contains(getPosition(row)))
                        searchedEvents.add(getPosition(row));
                }
            }
        return searchedEvents;
    }

    public int getNextSearchEvent() {
        if (searchedEvents.size() == 0) {
            searchedEvents = getSearchedEventsList();
            currentSearchRow = calculateNext();
            return currentSearchRow;
        } else {
            currentSearchRow = calculateNext();
            return currentSearchRow;
        }
    }
    /*
     * *********************************************************************************************
     * Public methods
     * *********************************************************************************************
     */

    public int getPreviousSearchEvent() {
        if (searchedEvents.size() == 0) {
            searchedEvents = getSearchedEventsList();
            currentSearchRow = calculatePrevious();
            return currentSearchRow;
        } else {
            currentSearchRow = calculatePrevious();
            return currentSearchRow;
        }
    }

    private int calculateNext() {
        for (int i = 0; i < searchedEvents.size(); i++) {
            if (searchedEvents.get(i) > currentSearchRow) {
                return searchedEvents.get(i);
            }
        }
        if (searchedEvents.size() > 0)
            return searchedEvents.get(0);
        else return -1;
    }

    private int calculatePrevious() {
        if (currentSearchRow == -1)
            currentSearchRow = 9999;
        for (int i = searchedEvents.size() - 1; i > -1; i--) {
            if (searchedEvents.get(i) < currentSearchRow) {
                return searchedEvents.get(i);
            }
        }
        if (searchedEvents.size() > 0)
            return searchedEvents.get(searchedEvents.size() - 1);
        else return -1;
    }

    @Override
    public int getViewTypeCount() {
        return NUM_ROW_TYPES;
    }

    @Override
    public void clear() {
        super.clear();
        if (!mIsSearchMode) {
            mEventRowMap.clear();
        }
    }

    @Override
    public int getItemViewType(int position) {
        // GA Crash
        if (position >= getCount()) {
            return ROW_TYPE_TEXT;
        }

        MessageRow row = getItem(position);
        return getItemViewType(row.getEvent());
    }

    @Override
    public void notifyDataSetChanged() {
        Log.e("notifydatasetchanged", "called");
//        if (isFavouriteMode) {
//            this.setNotifyOnChange(false);
//            fillFavouritelList();
//            this.setNotifyOnChange(true);
//        } else {
        // the event with invalid timestamp must be pushed at the end of the history
        this.setNotifyOnChange(false);

        List<MessageRow> undeliverableEvents = new ArrayList<>();

        for (int i = 0; i < getCount(); i++) {
            MessageRow row = getItem(i);
            Event event = row.getEvent();

            if ((null != event) && (!event.isValidOriginServerTs() || event.isUnkownDevice())) {
                undeliverableEvents.add(row);
                remove(row);
                i--;
            }
        }

        if (undeliverableEvents.size() > 0) {
            try {
                Collections.sort(undeliverableEvents, new Comparator<MessageRow>() {
                    @Override
                    public int compare(MessageRow m1, MessageRow m2) {
                        long diff = m1.getEvent().getOriginServerTs() - m2.getEvent().getOriginServerTs();
                        return (diff > 0) ? +1 : ((diff < 0) ? -1 : 0);
                    }
                });
            } catch (Exception e) {
                Log.e(LOG_TAG, "## notifyDataSetChanged () : failed to sort undeliverableEvents " + e.getMessage());
            }

            this.addAll(undeliverableEvents);
        }

        this.setNotifyOnChange(true);

        // build event -> date list
        refreshRefreshDateList();

        manageCryptoEvents();
        sortRows();
//        }

        //  do not refresh the room when the application is in background
        // on large rooms, it drains a lot of battery
        if (!VectorApp.isAppInBackground()) {
            super.notifyDataSetChanged();
        }

    }

    /**
     * Notify the fragment that some bing rules could have been updated.
     */
    public void onBingRulesUpdate() {
        this.notifyDataSetChanged();
    }

    /*
     * *********************************************************************************************
     * Item view methods
     * *********************************************************************************************
     */

    /**
     * the parent fragment is paused.
     */
    public void onPause() {
        mEventFormattedTsMap.clear();
    }

    /**
     * Toggle the selection mode.
     *
     * @param eventId the tapped eventID.
     */
    public void onEventTap(String eventId) {
        // the tap to select is only enabled when the adapter is not in search mode.
        if (!mIsSearchMode) {
            if (null == mSelectedEventId) {
                mSelectedEventId = eventId;
            } else {
                mSelectedEventId = null;
            }
            notifyDataSetChanged();
        }
    }

    /**
     * Display a bar to the left of the message
     *
     * @param eventId the event id
     */
    public void setSearchedEventId(String eventId) {
        mSearchedEventId = eventId;
        updateHighlightedEventId();
    }

    /**
     * Cancel the message selection mode
     */
    public void cancelSelectionMode() {
        if (null != mSelectedEventId) {
            mSelectedEventId = null;
            notifyDataSetChanged();
        }
    }

    /**
     * @return true if there is a selected item.
     */
    public boolean isInSelectionMode() {
        return null != mSelectedEventId;
    }

    /**
     * Define the events listener
     *
     * @param listener teh events listener
     */
    public void setVectorMessagesAdapterActionsListener(IMessagesAdapterActionsListener listener) {
        mVectorMessagesAdapterEventsListener = listener;
        mMediasHelper.setVectorMessagesAdapterActionsListener(listener);
        mHelper.setVectorMessagesAdapterActionsListener(listener);

        if (null != mLinkMovementMethod) {
            mLinkMovementMethod.updateListener(listener);
        } else if (null != listener) {
            mLinkMovementMethod = new MatrixLinkMovementMethod(listener);
        }
        mHelper.setLinkMovementMethod(mLinkMovementMethod);
    }

    /**
     * Retrieves the MXDevice info from an event id
     *
     * @param eventId the event id
     * @return the linked device info, null it it does not exist.
     */
    public MXDeviceInfo getDeviceInfo(String eventId) {
        MXDeviceInfo deviceInfo = null;

        if (null != eventId) {
            deviceInfo = mE2eDeviceByEventId.get(eventId);
        }

        return deviceInfo;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // GA Crash : it seems that some invalid indexes are required
        if (position >= getCount()) {
            Log.e(LOG_TAG, "## getView() : invalid index " + position + " >= " + getCount());

            // create dummy one is required
           /* if (null == convertView) {
                convertView = mLayoutInflater.inflate(mRowTypeToLayoutId.get(ROW_TYPE_TEXT), parent, false);
            }*/

            if (null != mVectorMessagesAdapterEventsListener) {
                mVectorMessagesAdapterEventsListener.onInvalidIndexes();
            }

            return convertView;
        }

        final View inflatedView;
        int viewType = getItemViewType(position);

        switch (viewType) {
            case ROW_TYPE_EMOJI:
            case ROW_TYPE_EMOJI_OUT:
            case ROW_TYPE_TEXT:
            case ROW_TYPE_TEXT_OUT:
                inflatedView = getTextView(viewType, position, convertView, parent);
                break;
            case ROW_TYPE_IMAGE:
            case ROW_TYPE_VIDEO:
            case ROW_TYPE_IMAGE_OUT:
            case ROW_TYPE_VIDEO_OUT:
                inflatedView = getImageVideoView(viewType, position, convertView, parent);
                break;
            case ROW_TYPE_NOTICE:
            case ROW_TYPE_ROOM_MEMBER:
                inflatedView = getNoticeRoomMemberView(viewType, position, convertView, parent);
                break;
            case ROW_TYPE_EMOTE:
            case ROW_TYPE_EMOTE_OUT:
                inflatedView = getEmoteView(viewType, position, convertView, parent);
                break;
            case ROW_TYPE_FILE:
            case ROW_TYPE_FILE_OUT:
                inflatedView = getFileView(viewType, position, convertView, parent);
                break;
            case ROW_TYPE_HIDDEN:
                inflatedView = getHiddenView(position, convertView, parent);
                break;
            case ROW_TYPE_MERGE:
                inflatedView = getMergeView(position, convertView, parent);
                break;
            case ROW_TYPE_AUDIO:
            case ROW_TYPE_AUDIO_OUT:
                inflatedView = getAudioView(viewType, position, convertView, parent);
                break;
            case ROW_TYPE_CONTACT:
            case ROW_TYPE_CONTACT_OUT:
                inflatedView = getContactView(viewType, position, convertView, parent);
                break;
            case ROW_TYPE_LOCATION:
            case ROW_TYPE_LOCATION_OUT:
                inflatedView = getLocationView(viewType, position, convertView, parent);
                break;
            case ROW_TYPE_TRANSLATION:
            case ROW_TYPE_TRANSLATION_OUT:
                inflatedView = getTranslationView(viewType, position, convertView, parent);
                break;
            default:
                throw new RuntimeException("Unknown item view type for position " + position);
        }

        if (mReadMarkerListener != null) {
            handleReadMarker(inflatedView, position);
        }

        if (null != inflatedView) {
            inflatedView.setBackgroundColor(TRANSPARENT);
        }

        displayE2eIcon(inflatedView, position);

        if (isFavouriteMode) {
            inflatedView.findViewById(R.id.receipt).setVisibility(View.GONE);
            inflatedView.findViewById(R.id.receipt1).setVisibility(View.GONE);
            inflatedView.findViewById(R.id.receipt).setTag("gone");
            inflatedView.findViewById(R.id.receipt1).setTag("gone");

            ImageView imageView2 = inflatedView.findViewById(R.id.receipt2);
            imageView2.setVisibility(View.VISIBLE);
            Drawable starDrawable = CommonActivityUtils.tintDrawable(mContext, ContextCompat.getDrawable(mContext, R.drawable.star_profile), R.attr.settings_icon_tint_color);
//            starDrawable.setColorFilter(0xff37474f, PorterDuff.Mode.MULTIPLY);
            imageView2.setImageDrawable(starDrawable);
            if (viewType == ROW_TYPE_IMAGE || viewType == ROW_TYPE_IMAGE_OUT || viewType == ROW_TYPE_VIDEO || viewType == ROW_TYPE_VIDEO_OUT)
                imageView2.setColorFilter(WHITE);
            else
                imageView2.setColorFilter(Color.parseColor("#a5a5a5"));
        }

        return inflatedView;
    }

    /**
     * Common view management.
     *
     * @param position    the item position.
     * @param convertView the row view
     * @param subView     the message content view
     * @param msgType     the message type
     */
    private void manageSubView(final int position, View convertView, View subView, int msgType) {
        MessageRow row = getItem(position);

        convertView.setClickable(true);

        // click on the avatar opens the details page
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mVectorMessagesAdapterEventsListener) {
                    mVectorMessagesAdapterEventsListener.onRowClick(position);
                }
            }
        });

        // click on the avatar opens the details page
        convertView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return (null != mVectorMessagesAdapterEventsListener) && mVectorMessagesAdapterEventsListener.onRowLongClick(position);
            }
        });

        Event event = row.getEvent();

        // isMergedView -> the message is going to be merged with the previous one
        // willBeMerged ->tell if a message separator must be displayed
        boolean isMergedView = false;
        boolean willBeMerged = false;
        if (convertView.findViewById(R.id.bubble_layout) != null)
            if (mIsSearchMode)
                if (row.isSearched())
                    convertView.findViewById(R.id.bubble_layout).setAlpha(1);
                else convertView.findViewById(R.id.bubble_layout).setAlpha(0.3f);
            else convertView.findViewById(R.id.bubble_layout).setAlpha(1);
        // the notices are never merged
        if (!mIsSearchMode &&
                isMergeableEvent(msgType)) {
            if (position > 0) {
                Event prevEvent = getItem(position - 1).getEvent();
                isMergedView = isMergeableEvent(getItemViewType(prevEvent)) && TextUtils.equals(prevEvent.getSender(), event.getSender());
            }

            // not the last message
            if ((position + 1) < this.getCount()) {
                Event nextEvent = getItem(position + 1).getEvent();
                willBeMerged = isMergeableEvent(getItemViewType(nextEvent)) && TextUtils.equals(nextEvent.getSender(), event.getSender());
            }
        }

        // inherited class custom behaviour
        isMergedView =
                mergeView(event, position, isMergedView);

        // init senders
        if (isFavouriteMode) {
            String userId = mContext.getIntent().getStringExtra("userId");
            if (null == userId)
                mHelper.setSenderValue(convertView, row, isMergedView, true);
            else
                mHelper.setSenderValue(convertView, row, isMergedView, false);
        } else
            mHelper.setSenderValue(convertView, row, isMergedView, mIsGroup);

        // message timestamp
        TextView tsTextView = VectorMessagesAdapterHelper.setTimestampValue(convertView, getFormattedTimestamp(event));

        if (null != tsTextView)

        {
            tsTextView.setVisibility((((position + 1) == this.getCount()) || mIsSearchMode || mAlwaysShowTimeStamps) ? VISIBLE : GONE);
        }

        // Sender avatar
        // View avatarLayoutView = mHelper.setSenderAvatar(convertView, row, isMergedView);

        // if the messages are merged
        // the thumbnail is hidden
        // and the subview must be moved to be aligned with the previous body
        View bodyLayoutView = convertView.findViewById(R.id.messagesAdapter_body_layout);
        //  VectorMessagesAdapterHelper.alignSubviewToAvatarView(subView, bodyLayoutView, avatarLayoutView, isMergedView);

        // messages separator
        View messageSeparatorView = convertView.findViewById(R.id.messagesAdapter_message_separator);

        if (null != messageSeparatorView)

        {
            messageSeparatorView.setVisibility(VISIBLE);
            //  messageSeparatorView.setVisibility((willBeMerged || ((position + 1) == this.getCount())) ? View.GONE : View.VISIBLE);
        }

        // display the day separator
        VectorMessagesAdapterHelper.setHeader(convertView,

                headerMessage(position), position);

        // read receipts
        if (mHideReadReceipts)

        {
            mHelper.hideReadReceipts(convertView);
        } else

        {
            if (!isFavouriteMode)
                displayReadReceipts(convertView, row, mIsPreviewMode, position);
        }

        // selection mode
        manageSelectionMode(convertView, event);

        // read marker
        // setReadMarker(convertView, row, isMergedView, avatarLayoutView, bodyLayoutView);

        // download / upload progress layout
        if (((ROW_TYPE_IMAGE == msgType) || (ROW_TYPE_FILE == msgType) || (ROW_TYPE_VIDEO == msgType)
                || (ROW_TYPE_IMAGE_OUT == msgType) || (ROW_TYPE_FILE_OUT == msgType) ||
                (ROW_TYPE_VIDEO_OUT == msgType)))

        {
            VectorMessagesAdapterHelper.setMediaProgressLayout(convertView, bodyLayoutView);
        }
    }

    /**
     * Display the read receipts within the dedicated vector layout.
     * Console application displays them on the message side.
     * Vector application displays them in a dedicated line under the message
     *
     * @param convertView   base view
     * @param row           the message row
     * @param isPreviewMode true if preview mode
     */
    private void displayReadReceipts(View convertView, MessageRow row, boolean isPreviewMode, int position) {
       /* View avatarsListView = convertView.findViewById(R.id.messagesAdapter_avatars_list);
        if (null == avatarsListView) {
            return;
        }*/

        if (!mSession.isAlive()) {
            return;
        }
        ImageView imageView = convertView.findViewById(R.id.receipt);
        ImageView imageView1 = convertView.findViewById(R.id.receipt1);
        final String eventId = row.getEvent().eventId;
        RoomState roomState = row.getRoomState();
        IMXStore store = mSession.getDataHandler().getStore();
        // sanity check
        if (null == roomState) {
            convertView.setVisibility(GONE);
            return;
        }
        // hide the read receipts until there is a way to retrieve them
        // without triggering a request per message
        if (isPreviewMode) {
            //  avatarsListView.setVisibility(GONE);
            imageView.setVisibility(GONE);
            imageView1.setVisibility(GONE);
            imageView.setTag("gone");
            imageView1.setTag("gone");
            return;
        }
        if (!row.getEvent().getSender().equals(mSession.getMyUserId())) {
            //  avatarsListView.setVisibility(GONE);
            imageView.setVisibility(GONE);
            imageView1.setVisibility(GONE);
            imageView.setTag("gone");
            imageView1.setTag("gone");
            return;
        }
        /// avatarsListView.setVisibility(GONE);
        imageView.setVisibility(GONE);
        imageView1.setVisibility(GONE);
        List<ReceiptData> receipts = store.getEventReceipts(roomState.roomId, eventId, true, true);
        if (row.getEvent().getOriginServerTs() == rows.get(rows.size() - 1).getEvent().getOriginServerTs()) {
            if (!row.getEvent().isSent()) {
                imageView.setVisibility(VISIBLE);
                imageView1.setVisibility(GONE);
                imageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.circle_grey));
                imageView.setTag("grey");
                imageView1.setTag("gone");
            } else if (row.getEvent().isSent()) {
                imageView.setVisibility(VISIBLE);
                imageView1.setVisibility(VISIBLE);
                imageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.circle_grey));
                imageView.setTag("grey");
                imageView1.setImageDrawable(mContext.getResources().getDrawable(R.drawable.circle_grey));
                imageView1.setTag("grey");
            }
        } else if (position < rows.size() - 1 && (row.getEvent().isSent() && (!rows.get(position + 1).getEvent().isSent()))) {
            imageView.setVisibility(VISIBLE);
            imageView1.setVisibility(VISIBLE);
            imageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.circle_grey));
            imageView.setTag("grey");
            imageView1.setImageDrawable(mContext.getResources().getDrawable(R.drawable.circle_grey));
            imageView1.setTag("grey");
        }
        if (receipts.size() > 1 && position < rows.size() - 1 &&
                receipts.get(receipts.size() - 1).eventId.equals(row.getEvent().eventId)) {
            imageView.setVisibility(VISIBLE);
            imageView1.setVisibility(VISIBLE);
            imageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.circle_green));
            imageView.setTag("green");
            imageView1.setImageDrawable(mContext.getResources().getDrawable(R.drawable.circle_green));
            imageView1.setTag("green");
        } else if (receipts.size() == 1 &&
                receipts.get(0).eventId.equals(row.getEvent().eventId)) {
            imageView.setVisibility(VISIBLE);
            imageView1.setVisibility(VISIBLE);
            imageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.circle_green));
            imageView.setColorFilter(Color.parseColor("#00C900"));
            imageView.setTag("green");
            imageView1.setImageDrawable(mContext.getResources().getDrawable(R.drawable.circle_green));
            imageView1.setColorFilter(Color.parseColor("#00C900"));
            imageView1.setTag("green");
        }
    }

    private void sortRows() {
        RowComparator com = new RowComparator();
        rows = new ArrayList<MessageRow>(mEventRowMap.values());
        Collections.sort(rows, com);
    }

    private List<MessageRow> sortRows(List<MessageRow> rows) {
        RowComparator com = new RowComparator();
        Collections.sort(rows, com);
        return rows;
    }

    /**
     * Convert Event to view type.
     *
     * @param event the event to convert
     * @return the view type.
     */
    private int getItemViewType(Event event) {

        String eventId = event.eventId;
        String eventType = event.getType();

        if ((null != eventId) && mHiddenEventIds.contains(eventId)) {
            return ROW_TYPE_HIDDEN;
        }

        // never cache the view type of the encrypted messages
        if (Event.EVENT_TYPE_MESSAGE_ENCRYPTED.equals(eventType)) {
            return ROW_TYPE_TEXT;
        }

        if (event instanceof EventGroup) {
            return ROW_TYPE_MERGE;
        }

        // never cache the view type of encrypted events
        if (null != eventId) {
            Integer type = mEventType.get(eventId);

            if (null != type) {
                return type;
            }
        }

        int viewType;

        if (Event.EVENT_TYPE_MESSAGE.equals(eventType)) {
            Message message = JsonUtils.toMessage(event.getContent());
            String msgType = message.msgtype;
            if (Message.MSGTYPE_TEXT.equals(msgType)) {
                if (containsOnlyEmojis(message.body)) {
                    if (!event.getSender().equals(mSession.getMyUserId()))
                        viewType = ROW_TYPE_EMOJI;
                    else viewType = ROW_TYPE_EMOJI_OUT;
                } else {
                    if (!event.getSender().equals(mSession.getMyUserId()))
                        viewType = ROW_TYPE_TEXT;
                    else viewType = ROW_TYPE_TEXT_OUT;
                }

            } else if (Message.MSGTYPE_IMAGE.equals(msgType)) {
                if (!event.getSender().equals(mSession.getMyUserId()))
                    viewType = ROW_TYPE_IMAGE;
                else viewType = ROW_TYPE_IMAGE_OUT;
            } else if (Message.MSGTYPE_EMOTE.equals(msgType)) {
                if (!event.getSender().equals(mSession.getMyUserId()))
                    viewType = ROW_TYPE_EMOTE;
                else viewType = ROW_TYPE_EMOTE_OUT;
            } else if (Message.MSGTYPE_NOTICE.equals(msgType)) {
                viewType = ROW_TYPE_NOTICE;
            } else if (Message.MSGTYPE_FILE.equals(msgType)) {
                if (!event.getSender().equals(mSession.getMyUserId()))
                    viewType = ROW_TYPE_FILE;
                else viewType = ROW_TYPE_FILE_OUT;
            } else if (Message.MSGTYPE_AUDIO.equals(msgType)) {
                //TODO audio message view
                if (!event.getSender().equals(mSession.getMyUserId()))
                    viewType = ROW_TYPE_AUDIO;
                else viewType = ROW_TYPE_AUDIO_OUT;
            } else if (Message.MSGTYPE_VIDEO.equals(msgType)) {
                if (!event.getSender().equals(mSession.getMyUserId()))
                    viewType = ROW_TYPE_VIDEO;
                else viewType = ROW_TYPE_VIDEO_OUT;
            } else if (Message.MSGTYPE_CONTACT.equals(msgType)) {
                if (!event.getSender().equals(mSession.getMyUserId()))
                    viewType = ROW_TYPE_CONTACT;
                else viewType = ROW_TYPE_CONTACT_OUT;
            } else if (Message.MSGTYPE_LOCATION.equals(msgType)) {
                if (!event.getSender().equals(mSession.getMyUserId()))
                    viewType = ROW_TYPE_LOCATION;
                else viewType = ROW_TYPE_LOCATION_OUT;
            } else if (Message.MSGTYPE_TRANSLATION.equals(msgType)) {
                if (!event.getSender().equals(mSession.getMyUserId()))
                    viewType = ROW_TYPE_TRANSLATION;
                else viewType = ROW_TYPE_TRANSLATION_OUT;
            } else {
                // Default is to display the body as text
                if (!event.getSender().equals(mSession.getMyUserId()))
                    viewType = ROW_TYPE_TEXT;
                else viewType = ROW_TYPE_TEXT_OUT;
            }
        } else if (
                event.isCallEvent() ||
                        Event.EVENT_TYPE_STATE_HISTORY_VISIBILITY.equals(eventType) ||
                        Event.EVENT_TYPE_STATE_ROOM_TOPIC.equals(eventType) ||
                        Event.EVENT_TYPE_STATE_ROOM_MEMBER.equals(eventType) ||
                        Event.EVENT_TYPE_STATE_ROOM_NAME.equals(eventType) ||
                        Event.EVENT_TYPE_STATE_ROOM_THIRD_PARTY_INVITE.equals(eventType) ||
                        Event.EVENT_TYPE_MESSAGE_ENCRYPTION.equals(eventType)) {
            viewType = ROW_TYPE_ROOM_MEMBER;

        } else if (WidgetsManager.WIDGET_EVENT_TYPE.equals(eventType)) {
            return ROW_TYPE_ROOM_MEMBER;
        } else {
            throw new RuntimeException("Unknown event type: " + eventType);
        }

        if (null != eventId) {
            mEventType.put(eventId, new Integer(viewType));
        }

        return viewType;
    }

    /**
     * Text message management
     *
     * @param viewType    the view type
     * @param position    the message position
     * @param convertView the text message view
     * @param parent      the parent view
     * @return the updated text view.
     */
    private View getTextView(final int viewType, final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(mRowTypeToLayoutId.get(viewType), parent, false);
        }

        try {
            MessageRow row = getItem(position);
            Event event = row.getEvent();
            Message message = JsonUtils.toMessage(event.getContent());
            RoomState roomState = row.getRoomState();

            EventDisplay display = new RiotEventDisplay(mContext, event, roomState);
            CharSequence textualDisplay = display.getTextualDisplay();

            SpannableString body = new SpannableString((null == textualDisplay) ? "" : textualDisplay);
            final TextView bodyTextView = convertView.findViewById(R.id.messagesAdapter_body);

            // cannot refresh it
            if (null == bodyTextView) {
                Log.e(LOG_TAG, "getTextView : invalid layout");
                return convertView;
            }
            bodyTextView.setText(message.body);

            int textColor;

         /*   if (row.getEvent().isEncrypting()) {
                textColor = mEncryptingMessageTextColor;
            } else if (row.getEvent().isSending() || row.getEvent().isUnsent()) {
                textColor = mSendingMessageTextColor;
            } else if (row.getEvent().isUndeliverable() || row.getEvent().isUnkownDevice()) {
                textColor = mNotSentMessageTextColor;
            } else {
                textColor = shouldHighlighted ? mHighlightMessageTextColor : mDefaultMessageTextColor;
            }*/

            // bodyTextView.setTextColor(textColor);

            View textLayout = convertView.findViewById(R.id.messagesAdapter_text_layout);
            this.manageSubView(position, convertView, textLayout, ROW_TYPE_TEXT);
            addContentViewListeners(convertView, bodyTextView, position);
            //TODO bubbles
            if (event.getSender().equals(mSession.getMyUserId())) {
                Drawable tempBackground = mContext.getResources().getDrawable(R.drawable.image_bubble);
                Drawable background = DrawableCompat.wrap(tempBackground);
                DrawableCompat.setTint(background, PreferencesManager.getIntOutgoingBgColor(mContext));
                convertView.findViewById(R.id.bubble_layout).setBackground(background);
                ((TextView) convertView.findViewById(R.id.messagesAdapter_body)).setTextColor(PreferencesManager.getIntOutgoingChatColor(mContext));
                if (convertView.findViewById(R.id.receipt) != null
                        && convertView.findViewById(R.id.receipt1) != null
                        && convertView.findViewById(R.id.receipt).getTag() != null
                        && !convertView.findViewById(R.id.receipt).getTag().equals("green")) {
                    ((ImageView) convertView.findViewById(R.id.receipt)).setColorFilter(Color.parseColor("#a5a5a5"));
                    ((ImageView) convertView.findViewById(R.id.receipt1)).setColorFilter(Color.parseColor("#a5a5a5"));
                }
            } else {
                Drawable tempBackground = mContext.getResources().getDrawable(R.drawable.image_bubble);
                Drawable background = DrawableCompat.wrap(tempBackground);
                DrawableCompat.setTint(background, PreferencesManager.getIntIncomingBgColor(mContext));
                convertView.findViewById(R.id.bubble_layout).setBackground(background);
                ((TextView) convertView.findViewById(R.id.messagesAdapter_body)).setTextColor(PreferencesManager.getIntIncomingChatColor(mContext));
                if (convertView.findViewById(R.id.receipt) != null
                        && convertView.findViewById(R.id.receipt1) != null
                        && convertView.findViewById(R.id.receipt).getTag() != null
                        && !convertView.findViewById(R.id.receipt).getTag().equals("green")) {
                    ((ImageView) convertView.findViewById(R.id.receipt)).setColorFilter(Color.parseColor("#a5a5a5"));
                    ((ImageView) convertView.findViewById(R.id.receipt1)).setColorFilter(Color.parseColor("#a5a5a5"));
                }
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, "## getTextView() failed : " + e.getMessage());
        }

        return convertView;
    }

    /**
     * Translation message management
     *
     * @param viewType    the view type
     * @param position    the message position
     * @param convertView the text message view
     * @param parent      the parent view
     * @return the updated text view.
     */
    private View getTranslationView(final int viewType, final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = mLayoutInflater.inflate(mRowTypeToLayoutId.get(viewType), parent, false);
        }

        try {
            MessageRow row = getItem(position);
            Event event = row.getEvent();
            TranslationMessage messageBody = JsonUtils.toTranslationMessage(event.getContent());
            String textTranslated = "";
            String textOriginal = "";
            Boolean imFlag = false;
            try {
                imFlag = new JSONObject(messageBody.body).getBoolean("fromImGroup");
            } catch (JSONException e) {
            }
            if (!event.getSender().equals(mSession.getMyUserId())) {
                if (imFlag) {
                    textTranslated = new JSONObject(messageBody.body).getString("textOriginal");
                    textOriginal = new JSONObject(messageBody.body).getString("textTranslated");
                } else {
                    textTranslated = new JSONObject(messageBody.body).getString("textTranslated");
                    textOriginal = new JSONObject(messageBody.body).getString("textOriginal");
                }
            } else {
                textTranslated = new JSONObject(messageBody.body).getString("textOriginal");
                textOriginal = new JSONObject(messageBody.body).getString("textTranslated");
            }

            final TextView bodyTextView = convertView.findViewById(R.id.messagesAdapter_body);
            bodyTextView.setText(textTranslated);
            View textLayout = convertView.findViewById(R.id.messagesAdapter_text_layout);
            this.manageSubView(position, convertView, textLayout, ROW_TYPE_TEXT);

            String finalTextOriginal = textOriginal;
            String finalTextTranslated = textTranslated;
            bodyTextView.setOnClickListener(new View.OnClickListener() {
                boolean first = true;

                @Override
                public void onClick(View v) {
                    if (first) {
                        bodyTextView.setText(finalTextOriginal);
                        Log.e(LOG_TAG, finalTextOriginal + "onclick" + first);
                        first = false;
                    } else {
                        bodyTextView.setText(finalTextTranslated);
                        Log.e(LOG_TAG, finalTextTranslated + "onclick" + first);
                        first = true;
                    }
                }
            });

            if (!event.getSender().equals(mSession.getMyUserId()) && imFlag && myLanguage != null) {
                Translation existTranslation = null;
                existTranslation = translationHandler.checkIfAlreadyTranslated(myLanguage, new JSONObject(messageBody.body).getString("textTranslated"));
                if (existTranslation != null) {
                    Log.e(LOG_TAG, "message in im group, translation already exist");
                    try {
                        textTranslated = existTranslation.getTranslation().get(0);
                        textOriginal = new JSONObject(messageBody.body).getString("textOriginal");
                        bodyTextView.setText(textTranslated);
                        String finalTextOriginal1 = textOriginal;
                        String finalTextTranslated1 = textTranslated;
                        bodyTextView.setOnClickListener(new View.OnClickListener() {
                            boolean first = true;

                            @Override
                            public void onClick(View v) {
                                if (first) {
                                    bodyTextView.setText(finalTextOriginal1);
                                    Log.e(LOG_TAG, finalTextOriginal1 + "onclick" + first);
                                    first = false;
                                } else {
                                    bodyTextView.setText(finalTextTranslated1);
                                    Log.e(LOG_TAG, finalTextTranslated1 + "onclick" + first);
                                    first = true;
                                }
                            }
                        });
                    } catch (Exception e) {

                    }
                } else if (PreferencesManager.isAutoTranslationIncomingAllow(mContext)) {
                    Log.e(LOG_TAG, "message in im group, auto-translation allowed");
                    translationHandler.setImGroupIncomingTranslationListener(myLanguage,
                            new JSONObject(messageBody.body).getString("textTranslated"), new TranslationInterface() {
                                @Override
                                public void onTranslate(Translation translationResponse) {
                                    String textTranslated;
                                    String textOriginal;
                                    try {
                                        textTranslated = translationResponse.getTranslation().get(0);
                                        textOriginal = new JSONObject(messageBody.body).getString("textOriginal");
                                        bodyTextView.setText(textTranslated);
                                        bodyTextView.setOnClickListener(new View.OnClickListener() {
                                            boolean first = true;

                                            @Override
                                            public void onClick(View v) {
                                                if (first) {
                                                    bodyTextView.setText(textOriginal);
                                                    Log.e(LOG_TAG, textOriginal + "onclick" + first);
                                                    first = false;
                                                } else {
                                                    bodyTextView.setText(textTranslated);
                                                    Log.e(LOG_TAG, textTranslated + "onclick" + first);
                                                    first = true;
                                                }
                                            }
                                        });
                                    } catch (Exception e) {

                                    }
                                }

                                @Override
                                public void onFailure(String msg) {

                                }
                            });
                } else {
                    Log.e(LOG_TAG, "message in im group, auto-translation not allowed");
                    bodyTextView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                translationHandler.setImGroupIncomingTranslationListener(myLanguage,
                                        new JSONObject(messageBody.body).getString("textTranslated"), new TranslationInterface() {
                                            @Override
                                            public void onTranslate(Translation translationResponse) {
                                                String textTranslated;
                                                String textOriginal;
                                                try {
                                                    textTranslated = translationResponse.getTranslation().get(0);
                                                    textOriginal = new JSONObject(messageBody.body).getString("textOriginal");
                                                    bodyTextView.setText(textTranslated);
                                                    bodyTextView.setOnClickListener(new View.OnClickListener() {
                                                        boolean first = true;

                                                        @Override
                                                        public void onClick(View v) {
                                                            if (first) {
                                                                bodyTextView.setText(textOriginal);
                                                                Log.e(LOG_TAG, textOriginal + "onclick" + first);
                                                                first = false;
                                                            } else {
                                                                bodyTextView.setText(textTranslated);
                                                                Log.e(LOG_TAG, textTranslated + "onclick" + first);
                                                                first = true;
                                                            }
                                                        }
                                                    });
                                                } catch (Exception e) {

                                                }
                                            }

                                            @Override
                                            public void onFailure(String msg) {

                                            }
                                        });
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
            //TODO bubbles
            if (event.getSender().equals(mSession.getMyUserId())) {
                Drawable tempBackground = mContext.getResources().getDrawable(R.drawable.image_bubble);
                Drawable background = DrawableCompat.wrap(tempBackground);
                DrawableCompat.setTint(background, PreferencesManager.getIntOutgoingBgColor(mContext));
                convertView.findViewById(R.id.bubble_layout).setBackground(background);
                ((TextView) convertView.findViewById(R.id.messagesAdapter_body)).setTextColor(PreferencesManager.getIntOutgoingChatColor(mContext));
                if (convertView.findViewById(R.id.receipt) != null
                        && convertView.findViewById(R.id.receipt1) != null
                        && convertView.findViewById(R.id.receipt).getTag() != null
                        && !convertView.findViewById(R.id.receipt).getTag().equals("green")) {
                    ((ImageView) convertView.findViewById(R.id.receipt)).setColorFilter(Color.parseColor("#a5a5a5"));
                    ((ImageView) convertView.findViewById(R.id.receipt1)).setColorFilter(Color.parseColor("#a5a5a5"));
                }
            } else {
                Drawable tempBackground = mContext.getResources().getDrawable(R.drawable.image_bubble);
                Drawable background = DrawableCompat.wrap(tempBackground);
                DrawableCompat.setTint(background, PreferencesManager.getIntIncomingBgColor(mContext));
                convertView.findViewById(R.id.bubble_layout).setBackground(background);
                ((TextView) convertView.findViewById(R.id.messagesAdapter_body)).setTextColor(PreferencesManager.getIntIncomingChatColor(mContext));
                if (convertView.findViewById(R.id.receipt) != null
                        && convertView.findViewById(R.id.receipt1) != null
                        && convertView.findViewById(R.id.receipt).getTag() != null
                        && !convertView.findViewById(R.id.receipt).getTag().equals("green")) {
                    ((ImageView) convertView.findViewById(R.id.receipt)).setColorFilter(Color.parseColor("#a5a5a5"));
                    ((ImageView) convertView.findViewById(R.id.receipt1)).setColorFilter(Color.parseColor("#a5a5a5"));
                }
            }
        } catch (
                Exception e)

        {
            Log.e(LOG_TAG, "## getTranslationView() failed : " + e.getMessage());
        }

        return convertView;
    }

    /**
     * Text message management
     *
     * @param viewType    the view type
     * @param position    the message position
     * @param convertView the text message view
     * @param parent      the parent view
     * @return the updated text view.
     */
    private View getContactView(final int viewType, final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(mRowTypeToLayoutId.get(viewType), parent, false);
        }

        try {
            MessageRow row = getItem(position);
            Event event = row.getEvent();
            ContactMessage messageBody = JsonUtils.toContactMessage(event.getContent());
            String displayName = new JSONObject(messageBody.body).getString("displayName");
            addContentViewListeners(convertView, convertView, position);
            ((TextView) convertView.findViewById(R.id.name)).setText(displayName);
            this.manageSubView(position, convertView, null, ROW_TYPE_TEXT);
            String matrixId;
            try {
                matrixId = new JSONObject(messageBody.body).getString("matrixId");
            } catch (Exception e) {
                matrixId = null;
            }
            if (matrixId != null) {
                VectorUtils.loadContactAvatar(mContext, mSession, convertView.findViewById(R.id.contact_avatar),
                        mSession.getDataHandler().getUser(matrixId));
                convertView.findViewById(R.id.write_button_wrapper).setVisibility(VISIBLE);
                convertView.findViewById(R.id.divider).setVisibility(VISIBLE);
            } else {
                GlideApp.with(mContext).load(mContext.getResources().getDrawable(R.drawable.man2))
                        .into((ImageView) convertView.findViewById(R.id.contact_avatar));
                convertView.findViewById(R.id.write_button_wrapper).setVisibility(GONE);
                convertView.findViewById(R.id.divider).setVisibility(GONE);
            }
            convertView.findViewById(R.id.add_button_wrapper).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String eMail;
                    String phone;
                    try {
                        eMail = new JSONObject(messageBody.body).getString("email");
                    } catch (Exception e) {
                        eMail = null;
                    }
                    try {
                        phone = new JSONObject(messageBody.body).getString("phoneNumber");
                    } catch (Exception e) {
                        phone = null;
                    }
                    Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
                    intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
                    if (displayName != null)
                        intent.putExtra(ContactsContract.Intents.Insert.NAME, displayName);
                    if (eMail != null)
                        intent.putExtra(ContactsContract.Intents.Insert.EMAIL, eMail);
                    intent.putExtra(ContactsContract.Intents.Insert.EMAIL_TYPE, ContactsContract.CommonDataKinds.Email.TYPE_WORK);
                    if (phone != null)
                        intent.putExtra(ContactsContract.Intents.Insert.PHONE, phone);
                    intent.putExtra(ContactsContract.Intents.Insert.PHONE_TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_WORK);
                    mContext.startActivity(intent);
                }
            });
            String finalMatrixId = matrixId;
            convertView.findViewById(R.id.write_button_wrapper).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startRoom(finalMatrixId);
                }
            });

            if (event.getSender().equals(mSession.getMyUserId())) {
                Drawable tempBackground = mContext.getResources().getDrawable(R.drawable.image_bubble);
                Drawable background = DrawableCompat.wrap(tempBackground);
                DrawableCompat.setTint(background, PreferencesManager.getIntOutgoingBgColor(mContext));
                convertView.findViewById(R.id.bubble_layout).setBackground(background);
            } else {
                Drawable tempBackground = mContext.getResources().getDrawable(R.drawable.image_bubble);
                Drawable background = DrawableCompat.wrap(tempBackground);
                DrawableCompat.setTint(background, PreferencesManager.getIntIncomingBgColor(mContext));
                convertView.findViewById(R.id.bubble_layout).setBackground(background);
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, "## getContactView() failed : " + e.getMessage());
        }

        return convertView;
    }

    /**
     * Text message management
     *
     * @param viewType    the view type
     * @param position    the message position
     * @param convertView the text message view
     * @param parent      the parent view
     * @return the updated text view.
     */
    private View getLocationView(final int viewType, final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(mRowTypeToLayoutId.get(viewType), parent, false);
        }
        try {

            MessageRow row = getItem(position);
            Event event = row.getEvent();
            LocationMessage messageBody = JsonUtils.toLocationMessage(event.getContent());
            double latitude = new JSONObject(messageBody.body).getDouble("latitude");
            double longitude = new JSONObject(messageBody.body).getDouble("longitude");
            String place = getAddress(latitude, longitude, false);
            String address = getAddress(latitude, longitude, true);
            if (place != null)
                ((TextView) convertView.findViewById(R.id.location_name)).setText(place);
            else convertView.findViewById(R.id.location_name).setVisibility(GONE);
            if (address != null)
                ((TextView) convertView.findViewById(R.id.place_name)).setText(address);
            else convertView.findViewById(R.id.place_name).setVisibility(GONE);
            GlideApp.with(mContext)
                    .load("https://maps.googleapis.com/maps/api/staticmap?center=" + latitude
                            + "," + longitude + "&zoom=16&size=300x300&markers=color:red%7Clabel:C%7C"
                            + latitude + "," + longitude).centerCrop()
                    .into((ImageView) convertView.findViewById(R.id.mapView));
            addContentViewListeners(convertView, convertView, position);
            convertView.findViewById(R.id.mapview_wrapper).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PackageManager packageManager = mContext.getPackageManager();
                    Uri gmmIntentUri = Uri.parse("geo:" + latitude + "," + longitude + "?z=16&q=" + latitude + "," + longitude);
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    if (packageManager.queryIntentActivities(mapIntent, MATCH_ALL).size() > 0)
                        mContext.startActivity(mapIntent);
                    else {
                        try {
                            String query = URLEncoder.encode(latitude + "," + longitude, "utf-8");
                            String url = "http://www.google.com/search?q=" + query;
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse(url));
                            mContext.startActivity(intent);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            this.manageSubView(position, convertView, null, ROW_TYPE_TEXT);
            //TODO bubbles
            if (event.getSender().equals(mSession.getMyUserId())) {
                Drawable tempBackground = mContext.getResources().getDrawable(R.drawable.image_bubble);
                Drawable background = DrawableCompat.wrap(tempBackground);
                DrawableCompat.setTint(background, PreferencesManager.getIntOutgoingBgColor(mContext));
                convertView.findViewById(R.id.bubble_layout).setBackground(background);
                if (convertView.findViewById(R.id.receipt) != null
                        && convertView.findViewById(R.id.receipt1) != null
                        && convertView.findViewById(R.id.receipt).getTag() != null
                        && !convertView.findViewById(R.id.receipt).getTag().equals("green")) {
                    ((ImageView) convertView.findViewById(R.id.receipt)).setColorFilter(Color.parseColor("#a5a5a5"));
                    ((ImageView) convertView.findViewById(R.id.receipt1)).setColorFilter(Color.parseColor("#a5a5a5"));
                }
            } else {
                Drawable tempBackground = mContext.getResources().getDrawable(R.drawable.image_bubble);
                Drawable background = DrawableCompat.wrap(tempBackground);
                DrawableCompat.setTint(background, PreferencesManager.getIntIncomingBgColor(mContext));
                convertView.findViewById(R.id.bubble_layout).setBackground(background);
                if (convertView.findViewById(R.id.receipt) != null
                        && convertView.findViewById(R.id.receipt1) != null
                        && convertView.findViewById(R.id.receipt).getTag() != null
                        && !convertView.findViewById(R.id.receipt).getTag().equals("green")) {
                    ((ImageView) convertView.findViewById(R.id.receipt)).setColorFilter(LTGRAY);
                    ((ImageView) convertView.findViewById(R.id.receipt1)).setColorFilter(LTGRAY);
                }
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, "## getLocationView() failed : " + e.getMessage());
        }

        return convertView;
    }

    private String getAddress(double lat, double lng, boolean thoroughfare) {
        Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            Address obj = addresses.get(0);
            String add = "";//obj.getAddressLine(0);
            if (obj.getLocality() != null)
                add = add + obj.getLocality();
            if (obj.getSubAdminArea() != null)
                add = add + ", " + obj.getSubAdminArea();
            if (obj.getAdminArea() != null)
                add = add + ", " + obj.getAdminArea();
            add = add + ", " + obj.getCountryName();
            add = add + ", " + obj.getPostalCode();
            if (thoroughfare) return obj.getThoroughfare() + ", " + obj.getSubThoroughfare();
            else return add;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Image / Video  message management
     *
     * @param type        ROW_TYPE_IMAGE or ROW_TYPE_VIDEO
     * @param position    the message position
     * @param convertView the message view
     * @param parent      the parent view
     * @return the updated text view.
     */
    private View getImageVideoView(int type, final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(mRowTypeToLayoutId.get(type), parent, false);
        }

        try {
            MessageRow row = getItem(position);
            Event event = row.getEvent();

            Message message;
            int waterMarkResourceId = -1;

            if (type == ROW_TYPE_IMAGE || type == ROW_TYPE_IMAGE_OUT) {
                ImageMessage imageMessage = JsonUtils.toImageMessage(event.getContent());
                message = imageMessage;

            } else {
                message = JsonUtils.toVideoMessage(event.getContent());
                waterMarkResourceId = R.drawable.filetype_video;
            }

            // display a type watermark
            final ImageView imageTypeView = convertView.findViewById(R.id.messagesAdapter_image_type);

            if (null == imageTypeView) {
                Log.e(LOG_TAG, "getImageVideoView : invalid layout");
                return convertView;
            }

            imageTypeView.setBackgroundColor(TRANSPARENT);

            if (waterMarkResourceId > 0) {
                imageTypeView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.play));
                imageTypeView.setAlpha(0.5f);
                imageTypeView.setVisibility(VISIBLE);
            } else {
                imageTypeView.setVisibility(GONE);
            }

            // download management
            mMediasHelper.managePendingImageVideoDownload(convertView, event, message, position);

            // upload management
            mMediasHelper.managePendingImageVideoUpload(convertView, event, message);

            // dimmed when the message is not sent
            ImageView imageLayout = convertView.findViewById(R.id.image_body);
            imageLayout.setAlpha(event.isSent() ? 1.0f : 0.5f);

            this.manageSubView(position, convertView, imageLayout, type);

            if (JsonUtils.toImageMessage(event.getContent()).getMimeType().equals("image/gif"))
                GlideApp.with(mContext).asGif()
                        .load(mSession.getMediasCache().getDownloadableUrl(JsonUtils.toImageMessage(event.getContent()).getUrl()))
                        .override(mMaxImageWidth, mMaxImageHeight)
                        .centerCrop()
                        .into(imageLayout);

            if (convertView.findViewById(R.id.receipt) != null
                    && convertView.findViewById(R.id.receipt1) != null
                    && convertView.findViewById(R.id.receipt).getTag() != null
                    && !convertView.findViewById(R.id.receipt).getTag().equals("green")) {
                ((ImageView) convertView.findViewById(R.id.receipt)).setColorFilter(WHITE);
                ((ImageView) convertView.findViewById(R.id.receipt1)).setColorFilter(WHITE);
            }
            //TODO bubbles
           /* if (event.getSender().equals(mSession.getMyUserId())) {
                Drawable tempBackground = mContext.getResources().getDrawable(R.drawable.image_bubble);
                Drawable background = DrawableCompat.wrap(tempBackground);
                DrawableCompat.setTint(background, PreferencesManager.getIntOutgoingBgColor(mContext));
                convertView.findViewById(R.id.messagesAdapter_image).setBackground(background);
            } else {
                Drawable tempBackground = mContext.getResources().getDrawable(R.drawable.image_bubble);
                Drawable background = DrawableCompat.wrap(tempBackground);
                DrawableCompat.setTint(background, PreferencesManager.getIntIncomingBgColor(mContext));
                convertView.findViewById(R.id.messagesAdapter_image).setBackground(background);
            }*/
            ((TextView) convertView.findViewById(R.id.messagesAdapter_timestamp)).setTextColor(Color.WHITE);
            ((TextView) convertView.findViewById(R.id.messagesAdapter_sender)).setTextColor(Color.WHITE);
            addContentViewListeners(convertView, imageLayout, position);

        } catch (Exception e) {
            Log.e(LOG_TAG, "## getImageVideoView() failed : " + e.getMessage());
        }
        return convertView;
    }

    /**
     * Notice message management
     *
     * @param position    the message position
     * @param convertView the message view
     * @param parent      the parent view
     * @return the updated text view.
     */
    private View getNoticeRoomMemberView(final int viewType, final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(mRowTypeToLayoutId.get(viewType), parent, false);
        }

        try {
            MessageRow row = getItem(position);
            Event msg = row.getEvent();
            RoomState roomState = row.getRoomState();

            CharSequence notice;

            //   EventDisplay display = new RiotEventDisplay(mContext, msg, roomState);
            notice = getTextualDisplay(msg);

            TextView noticeTextView = convertView.findViewById(R.id.messagesAdapter_body);

            if (null == noticeTextView) {
                Log.e(LOG_TAG, "getNoticeRoomMemberView : invalid layout");
                return convertView;
            }

            if (TextUtils.isEmpty(notice)) {
                noticeTextView.setText("");
            } else {
                SpannableStringBuilder strBuilder = new SpannableStringBuilder(notice);
                MatrixURLSpan.refreshMatrixSpans(strBuilder, mVectorMessagesAdapterEventsListener);
                noticeTextView.setText(strBuilder);
            }

            View textLayout = convertView.findViewById(R.id.messagesAdapter_text_layout);
            this.manageSubView(position, convertView, textLayout, viewType);

            //  addContentViewListeners(convertView, noticeTextView, position);

            // android seems having a big issue when the text is too long and an alpha !=1 is applied:
            // ---> the text is not displayed.
            // It is sometimes partially displayed and/or flickers while scrolling.
            // Apply an alpha != 1, trigger the same issue.
            // It is related to the number of characters not to the number of lines.
            // I don't understand why the render graph fails to do it.
            // the patch apply the alpha to the text color but it does not work for the hyperlinks.
            noticeTextView.setAlpha(1.0f);
            noticeTextView.setTextColor(getNoticeTextColor());
        } catch (Exception e) {
            Log.e(LOG_TAG, "## getNoticeRoomMemberView() failed : " + e.getMessage());
        }

        return convertView;
    }

    /**
     * Stringify the linked event.
     *
     * @param mEvent event.
     * @return The text or null if it isn't possible.
     */
    public CharSequence getTextualDisplay(Event mEvent) {
        CharSequence text = null;

        try {
            JsonObject jsonEventContent = mEvent.getContentAsJsonObject();

            String userDisplayName = VectorMessagesAdapterHelper.getUserDisplayName(mEvent.getSender(), mSession, getContext());
            String eventType = mEvent.getType();

            if (mEvent.isCallEvent()) {
                if (Event.EVENT_TYPE_CALL_INVITE.equals(eventType)) {
                    boolean isVideo = false;
                    // detect call type from the sdp
                    try {
                        JsonObject offer = jsonEventContent.get("offer").getAsJsonObject();
                        JsonElement sdp = offer.get("sdp");
                        String sdpValue = sdp.getAsString();
                        isVideo = sdpValue.contains("m=video");
                    } catch (Exception e) {
                        Log.e(LOG_TAG, "getTextualDisplay : " + e.getMessage());
                    }

                    if (isVideo) {
                        return mContext.getString(org.matrix.androidsdk.R.string.notice_placed_video_call, userDisplayName);
                    } else {
                        return mContext.getString(org.matrix.androidsdk.R.string.notice_placed_voice_call, userDisplayName);
                    }
                } else if (Event.EVENT_TYPE_CALL_ANSWER.equals(eventType)) {
                    return mContext.getString(org.matrix.androidsdk.R.string.notice_answered_call, userDisplayName);
                } else if (Event.EVENT_TYPE_CALL_HANGUP.equals(eventType)) {
                    return mContext.getString(org.matrix.androidsdk.R.string.notice_ended_call, userDisplayName);
                } else {
                    return eventType;
                }
            } else return null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return text;
    }

    /**
     * Emote message management
     *
     * @param position    the message position
     * @param convertView the message view
     * @param parent      the parent view
     * @return the updated text view.
     */
    private View getEmoteView(final int viewType, final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(mRowTypeToLayoutId.get(viewType), parent, false);
        }

        try {
            MessageRow row = getItem(position);
            Event event = row.getEvent();
            RoomState roomState = row.getRoomState();

            TextView emoteTextView = convertView.findViewById(R.id.messagesAdapter_body);

            if (null == emoteTextView) {
                Log.e(LOG_TAG, "getEmoteView : invalid layout");
                return convertView;
            }

            Message message = JsonUtils.toMessage(event.getContent());
            String userDisplayName = (null == roomState) ? event.getSender() : roomState.getMemberName(event.getSender());

            String body = "* " + userDisplayName + " " + message.body;

            String htmlString = null;

            if (TextUtils.equals(Message.FORMAT_MATRIX_HTML, message.format)) {
                htmlString = mHelper.getSanitisedHtml(message.formatted_body);

                if (null != htmlString) {
                    htmlString = "* " + userDisplayName + " " + message.formatted_body;
                }
            }

            highlightPattern(emoteTextView, new SpannableString(body), htmlString, null, false);

            int textColor;

            if (row.getEvent().isEncrypting()) {
                textColor = mEncryptingMessageTextColor;
            } else if (row.getEvent().isSending() || row.getEvent().isUnsent()) {
                textColor = mSendingMessageTextColor;
            } else if (row.getEvent().isUndeliverable() || row.getEvent().isUnkownDevice()) {
                textColor = mNotSentMessageTextColor;
            } else {
                textColor = mDefaultMessageTextColor;
            }

            emoteTextView.setTextColor(textColor);

            View textLayout = convertView.findViewById(R.id.messagesAdapter_text_layout);
            this.manageSubView(position, convertView, textLayout, ROW_TYPE_EMOTE);
            //TODO bubbles
            if (event.getSender().equals(mSession.getMyUserId())) {
                Drawable tempBackground = mContext.getResources().getDrawable(R.drawable.image_bubble);
                Drawable background = DrawableCompat.wrap(tempBackground);
                DrawableCompat.setTint(background, PreferencesManager.getIntOutgoingBgColor(mContext));
                convertView.findViewById(R.id.messagesAdapter_body_layout).setBackground(background);
                ((TextView) convertView.findViewById(R.id.messagesAdapter_body)).setTextColor(PreferencesManager.getIntOutgoingChatColor(mContext));
            } else {
                Drawable tempBackground = mContext.getResources().getDrawable(R.drawable.image_bubble);
                Drawable background = DrawableCompat.wrap(tempBackground);
                DrawableCompat.setTint(background, PreferencesManager.getIntIncomingBgColor(mContext));
                convertView.findViewById(R.id.messagesAdapter_body_layout).setBackground(background);
                ((TextView) convertView.findViewById(R.id.messagesAdapter_body)).setTextColor(PreferencesManager.getIntIncomingChatColor(mContext));
            }
            addContentViewListeners(convertView, emoteTextView, position);
        } catch (Exception e) {
            Log.e(LOG_TAG, "## getEmoteView() failed : " + e.getMessage());
        }

        return convertView;
    }

    /**
     * File message management
     *
     * @param position    the message position
     * @param convertView the message view
     * @param parent      the parent view
     * @return the updated text view.
     */
    private View getFileView(final int viewType, final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(mRowTypeToLayoutId.get(viewType), parent, false);
        }

        try {
            MessageRow row = getItem(position);
            Event event = row.getEvent();

            final FileMessage fileMessage = JsonUtils.toFileMessage(event.getContent());
            final TextView fileTextView = convertView.findViewById(R.id.messagesAdapter_filename);

            if (null == fileTextView) {
                Log.e(LOG_TAG, "getFileView : invalid layout");
                return convertView;
            }

            fileTextView.setPaintFlags(fileTextView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            fileTextView.setText("\n" + fileMessage.body + "\n");

            // display the right message type icon.
            // Audio and File messages are managed by the same method
            final ImageView imageTypeView = convertView.findViewById(R.id.messagesAdapter_image_type);

            if (null != imageTypeView) {
                imageTypeView.setImageResource(Message.MSGTYPE_AUDIO.equals(fileMessage.msgtype) ? R.drawable.filetype_audio : R.drawable.filetype_attachment);
            }
            imageTypeView.setBackgroundColor(TRANSPARENT);

            mMediasHelper.managePendingFileDownload(convertView, event, viewType, fileMessage, position);
            mMediasHelper.managePendingUpload(convertView, event, viewType, fileMessage.url);

            View fileLayout = convertView.findViewById(R.id.messagesAdapter_file_layout);
            this.manageSubView(position, convertView, fileLayout, viewType);

            addContentViewListeners(convertView, fileTextView, position);
        } catch (Exception e) {
            Log.e(LOG_TAG, "## getFileView() failed " + e.getMessage());
        }
        return convertView;
    }

    /**
     * Audio message management
     *
     * @param position    the message position
     * @param convertView the message view
     * @param parent      the parent view
     * @return the updated text view.
     */
    private View getAudioView(final int viewType, final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(mRowTypeToLayoutId.get(viewType), parent, false);
        }

        try {
            MessageRow row = getItem(position);
            Event event = row.getEvent();

            final FileMessage fileMessage = JsonUtils.toFileMessage(event.getContent());

            mMediasHelper.managePendingFileDownload(convertView, event, viewType, fileMessage, position);
            mMediasHelper.managePendingUpload(convertView, event, viewType, fileMessage.url);

            View fileLayout = convertView.findViewById(R.id.messagesAdapter_file_layout);
            this.manageSubView(position, convertView, fileLayout, viewType);
            String downloadableUrl = mMediasCache.getDownloadableUrl(fileMessage.getUrl());

            if (event.getSender().equals(mSession.getMyUserId()))
                downloadFile(downloadableUrl, convertView);
            else {
                File mediaBaseFolderFile = new File(mContext.getApplicationContext().getFilesDir(), "MXMediaStore");
                File audioFolder = new File(mediaBaseFolderFile, "Audio");
                File file = new File(audioFolder, getFileName(downloadableUrl));
                if (file.exists())
                    downloadFile(downloadableUrl, convertView);
                else {
                    switch (PreferencesManager.getAudioAutoDownloadMethod(mContext)) {
                        case WIFI_AND_CELLULAR:
                            downloadFile(downloadableUrl, convertView);
                            break;
                        case WIFI:
                            ConnectivityManager connManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
                            NetworkInfo current = connManager.getActiveNetworkInfo();
                            boolean isWifi = current != null && current.getType() == ConnectivityManager.TYPE_WIFI;
                            if (isWifi)
                                downloadFile(downloadableUrl, convertView);
                            break;
                    }
                    ImageView mDownload = convertView.findViewById(R.id.download);
                    if (!file.exists()) {
                        convertView.findViewById(R.id.play).setVisibility(GONE);
                        mDownload.setVisibility(VISIBLE);
                        View finalConvertView = convertView;
                        mDownload.setOnClickListener(v -> {
                            downloadFile(downloadableUrl, finalConvertView);
                            mDownload.setVisibility(GONE);
                            finalConvertView.findViewById(R.id.upload_event_spinner).setVisibility(VISIBLE);
                        });
                    }
                }
            }


            if (event.getSender().equals(mSession.getMyUserId())) {
                Drawable tempBackground = mContext.getResources().getDrawable(R.drawable.image_bubble);
                Drawable background = DrawableCompat.wrap(tempBackground);
                DrawableCompat.setTint(background, PreferencesManager.getIntOutgoingBgColor(mContext));
                convertView.findViewById(R.id.player_layout).setBackground(background);
                if (convertView.findViewById(R.id.receipt) != null
                        && convertView.findViewById(R.id.receipt1) != null
                        && convertView.findViewById(R.id.receipt).getTag() != null
                        && !convertView.findViewById(R.id.receipt).getTag().equals("green")) {
                    ((ImageView) convertView.findViewById(R.id.receipt)).setColorFilter(Color.parseColor("#a5a5a5"));
                    ((ImageView) convertView.findViewById(R.id.receipt1)).setColorFilter(Color.parseColor("#a5a5a5"));
                }
            } else {
                Drawable tempBackground = mContext.getResources().getDrawable(R.drawable.image_bubble);
                Drawable background = DrawableCompat.wrap(tempBackground);
                DrawableCompat.setTint(background, PreferencesManager.getIntIncomingBgColor(mContext));
                convertView.findViewById(R.id.player_layout).setBackground(background);
                if (convertView.findViewById(R.id.receipt) != null
                        && convertView.findViewById(R.id.receipt1) != null
                        && convertView.findViewById(R.id.receipt).getTag() != null
                        && !convertView.findViewById(R.id.receipt).getTag().equals("green")) {
                    ((ImageView) convertView.findViewById(R.id.receipt)).setColorFilter(Color.parseColor("#a5a5a5"));
                    ((ImageView) convertView.findViewById(R.id.receipt1)).setColorFilter(Color.parseColor("#a5a5a5"));
                }
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, "## getFileView() failed " + e.getMessage());
        }
        return convertView;
    }

    @SuppressLint("StaticFieldLeak")
    private void downloadFile(String url, View convertView) {
        File mediaBaseFolderFile = new File(mContext.getApplicationContext().getFilesDir(), "MXMediaStore");
        File audioFolder = new File(mediaBaseFolderFile, "Audio");
        if (!audioFolder.exists())
            audioFolder.mkdirs();
        File file = new File(audioFolder, getFileName(url));
        if (file.exists()) {
            ImageView mPlayMedia = convertView.findViewById(R.id.play);
            ImageView mPauseMedia = convertView.findViewById(R.id.pause);
            SeekBar mMediaSeekBar = convertView.findViewById(R.id.media_seekbar);
            TextView mRunTime = convertView.findViewById(R.id.playback_time);
            TextView mTotalTime = convertView.findViewById(R.id.total_time);
            convertView.findViewById(R.id.upload_event_spinner).setVisibility(GONE);
            convertView.findViewById(R.id.download).setVisibility(GONE);
            mPlayMedia.setVisibility(VISIBLE);
            if (mPlayMedia != null) {
                AudioWife audioWife = new AudioWife();
                audioWife.init(mContext, Uri.fromFile(file))
                        .setPlayView(mPlayMedia)
                        .setPauseView(mPauseMedia)
                        .setSeekBar(mMediaSeekBar)
                        .setRuntimeView(mRunTime)
                        .setTotalTimeView(mTotalTime);
                mRunTime.setVisibility(GONE);
                mTotalTime.setVisibility(VISIBLE);
                audioWife.addOnCompletionListener(mp -> {
                    mRunTime.setVisibility(GONE);
                    mTotalTime.setVisibility(VISIBLE);
                });

                audioWife.addOnPlayClickListener(v -> {
                    mRunTime.setVisibility(VISIBLE);
                    mTotalTime.setVisibility(GONE);
                });
            }
        } else {
            new AsyncTask<String, String, String>() {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                }

                @Override
                protected String doInBackground(String... strings) {
                    InputStream input = null;
                    OutputStream output = null;
                    HttpURLConnection connection = null;
                    try {
                        URL url = new URL(strings[0]);
                        connection = (HttpURLConnection) url.openConnection();
                        connection.connect();

                        // expect HTTP 200 OK, so we don't mistakenly save error report
                        // instead of the file
                        if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                            return "Server returned HTTP " + connection.getResponseCode()
                                    + " " + connection.getResponseMessage();
                        }

                        // download the file
                        input = connection.getInputStream();
                        String name1 = mContext.getCacheDir() + getFileName(strings[0]);
//                        output = new FileOutputStream(name1);

                        String name = audioFolder + getFileName(strings[0]);
                        output = new FileOutputStream(name);

                        byte data[] = new byte[4096];
                        int count;
                        while ((count = input.read(data)) != -1) {
                            // allow canceling with back button
                            if (isCancelled()) {
                                input.close();
                                return null;
                            }
                            output.write(data, 0, count);
                        }
                    } catch (Exception e) {
                        return e.toString();
                    } finally {
                        try {
                            if (output != null)
                                output.close();
                            if (input != null)
                                input.close();
                        } catch (IOException ignored) {
                        }

                        if (connection != null)
                            connection.disconnect();
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(String result) {
                    ImageView mPlayMedia = convertView.findViewById(R.id.play);
                    ImageView mPauseMedia = convertView.findViewById(R.id.pause);
                    SeekBar mMediaSeekBar = convertView.findViewById(R.id.media_seekbar);
                    TextView mRunTime = convertView.findViewById(R.id.playback_time);
                    TextView mTotalTime = convertView.findViewById(R.id.total_time);
                    convertView.findViewById(R.id.upload_event_spinner).setVisibility(GONE);
                    convertView.findViewById(R.id.download).setVisibility(GONE);
                    mPlayMedia.setVisibility(VISIBLE);
                    if (mPlayMedia != null && file.exists()) {
                        AudioWife audioWife = new AudioWife();
                        audioWife.init(mContext, Uri.fromFile(file))
                                .setPlayView(mPlayMedia)
                                .setPauseView(mPauseMedia)
                                .setSeekBar(mMediaSeekBar)
                                .setRuntimeView(mRunTime)
                                .setTotalTimeView(mTotalTime);
                        mRunTime.setVisibility(GONE);
                        mTotalTime.setVisibility(VISIBLE);

                        audioWife.addOnCompletionListener(mp -> {
                            mRunTime.setVisibility(GONE);
                            mTotalTime.setVisibility(VISIBLE);
                        });

                        audioWife.addOnPlayClickListener(v -> {
                            mRunTime.setVisibility(VISIBLE);
                            mTotalTime.setVisibility(GONE);
                        });
                    }
                }
            }.execute(url);
        }
    }

    private String getFileName(String url) {
        Log.e("filename", url.substring(url.lastIndexOf('/')));
        return url.substring(url.lastIndexOf('/'));
    }

    /**
     * Hidden message management.
     *
     * @param position    the message position
     * @param convertView the message view
     * @param parent      the parent view
     * @return the updated text view.
     */
    private View getHiddenView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(mRowTypeToLayoutId.get(ROW_TYPE_HIDDEN), parent, false);
        }

        // display the day separator
        VectorMessagesAdapterHelper.setHeader(convertView, headerMessage(position), position);

        return convertView;
    }

    /*
     * *********************************************************************************************
     * Private methods
     * *********************************************************************************************
     */

    /**
     * Get a merge view for a position.
     *
     * @param position    the message position
     * @param convertView the message view
     * @param parent      the parent view
     * @return the updated text view.
     */
    private View getMergeView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(mRowTypeToLayoutId.get(ROW_TYPE_MERGE), parent, false);
        }

        try {
            MessageRow row = getItem(position);
            final EventGroup event = (EventGroup) row.getEvent();

            View headerLayout = convertView.findViewById(R.id.messagesAdapter_merge_header_layout);
            TextView headerTextView = convertView.findViewById(R.id.messagesAdapter_merge_header_text_view);
            TextView summaryTextView = convertView.findViewById(R.id.messagesAdapter_merge_summary);
            View separatorLayout = convertView.findViewById(R.id.messagesAdapter_merge_separator);
            View avatarsLayout = convertView.findViewById(R.id.messagesAdapter_merge_avatar_list);

            // test if the layout is still valid
            // reported by a rageshake
            if ((null == headerLayout) || (null == headerTextView) || (null == summaryTextView)
                    || (null == separatorLayout) || (null == avatarsLayout)) {
                Log.e(LOG_TAG, "getMergeView : invalid layout");
                return convertView;
            }

            separatorLayout.setVisibility(event.isExpanded() ? VISIBLE : GONE);
            summaryTextView.setVisibility(event.isExpanded() ? GONE : VISIBLE);
            avatarsLayout.setVisibility(event.isExpanded() ? GONE : VISIBLE);

            headerTextView.setText(event.isExpanded() ? "collapse" : "expand");

            if (!event.isExpanded()) {
                avatarsLayout.setVisibility(VISIBLE);
                List<CircleImageView> avatarView = new ArrayList<>();

                avatarView.add(convertView.findViewById(R.id.mels_list_avatar_1));
                avatarView.add(convertView.findViewById(R.id.mels_list_avatar_2));
                avatarView.add(convertView.findViewById(R.id.mels_list_avatar_3));
                avatarView.add(convertView.findViewById(R.id.mels_list_avatar_4));
                avatarView.add(convertView.findViewById(R.id.mels_list_avatar_5));

                List<MessageRow> messageRows = event.getAvatarRows(avatarView.size());

                for (int i = 0; i < avatarView.size(); i++) {
                    CircleImageView imageView = avatarView.get(i);

                    if (i < messageRows.size()) {
                        mHelper.loadMemberAvatar(imageView, messageRows.get(i));
                        imageView.setVisibility(VISIBLE);
                    } else {
                        imageView.setVisibility(GONE);
                    }
                }


                summaryTextView.setText(event.toString(mContext));
            }

            headerLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    event.setIsExpanded(!event.isExpanded());
                    updateHighlightedEventId();

                    if (event.contains(mSelectedEventId)) {
                        cancelSelectionMode();
                    } else {
                        notifyDataSetChanged();
                    }
                }
            });

            // set the message marker
            convertView.findViewById(R.id.messagesAdapter_highlight_message_marker).setBackgroundColor(ContextCompat.getColor(mContext, TextUtils.equals(mHighlightedEventId, event.eventId) ? R.color.vector_green_color : android.R.color.transparent));

            // display the day separator
            VectorMessagesAdapterHelper.setHeader(convertView, headerMessage(position), position);

            boolean isInSelectionMode = (null != mSelectedEventId);
            boolean isSelected = TextUtils.equals(event.eventId, mSelectedEventId);

            float alpha = (!isInSelectionMode || isSelected) ? 1.0f : 0.2f;

            // the message body is dimmed when not selected
            // convertView.findViewById(R.id.messagesAdapter_body_view).setAlpha(alpha);
        } catch (Exception e) {
            Log.e(LOG_TAG, "## getMergeView() failed " + e.getMessage());
        }

        return convertView;
    }

    /**
     * Highlight a pattern in a text view.
     *
     * @param textView the text view
     * @param text     the text to display
     * @param pattern  the pattern to highlight
     */
    void highlightPattern(TextView textView, Spannable text, String pattern) {
        highlightPattern(textView, text, null, pattern, false);
    }

    /**
     * Highlight a pattern in a text view.
     *
     * @param textView          the text view
     * @param text              the text to display
     * @param htmlFormattedText the text in HTML format
     * @param pattern           the pattern to highlight
     * @param isHighlighted     true when the event is highlighted
     */
    private void highlightPattern(TextView textView, Spannable text, String htmlFormattedText, String pattern, boolean isHighlighted) {
        mHelper.highlightPattern(textView, text, htmlFormattedText, pattern, new BackgroundColorSpan(mSearchHighlightMessageTextColor), isHighlighted);
    }

    /**
     * Check if the row must be added to the list.
     *
     * @param row the row to check.
     * @return true if should be added
     */
    private boolean isSupportedRow(MessageRow row) {
        boolean isSupported = VectorMessagesAdapterHelper.isDisplayableEvent(mContext, row);

        isSupported = isSupported && !row.getEvent().isHidden()
                && row.getEvent().getOriginServerTs() > mSession.getDataHandler().getStore().getSummary(row.getEvent().roomId).getDeletionTime();
        Log.e("time", mSession.getDataHandler().getStore().getSummary(row.getEvent().roomId).getDeletionTime() + "");
        if (isSupported) {
            String eventId = row.getEvent().eventId;

            MessageRow currentRow = mEventRowMap.get(eventId);

            // the row should be added only if the message has not been received
            isSupported = (null == currentRow);

            // check if the message is already received
            if (null != currentRow) {
                // waiting for echo
                // the message is displayed as sent event if the echo has not been received
                // it avoids displaying a pending message whereas the message has been sent
                if (currentRow.getEvent().getAge() == Event.DUMMY_EVENT_AGE) {
                    currentRow.updateEvent(row.getEvent());
                }
            }

            if (TextUtils.equals(row.getEvent().getType(), Event.EVENT_TYPE_STATE_ROOM_MEMBER)) {
                RoomMember roomMember = JsonUtils.toRoomMember(row.getEvent().getContent());
                String membership = roomMember.membership;

                //if (PreferencesManager.hideJoinLeaveMessages(mContext)) {

                isSupported = !TextUtils.equals(membership, RoomMember.MEMBERSHIP_LEAVE)
                        && !TextUtils.equals(membership, RoomMember.MEMBERSHIP_JOIN)
                        && !TextUtils.equals(membership, RoomMember.MEMBERSHIP_INVITE);
                if (isSupported && PreferencesManager.hideAvatarDisplayNameChangeMessages(mContext) && TextUtils.equals(membership, RoomMember.MEMBERSHIP_JOIN)) {
                    EventContent eventContent = JsonUtils.toEventContent(row.getEvent().getContentAsJsonObject());
                    EventContent prevEventContent = row.getEvent().getPrevContent();

                    String senderDisplayName = eventContent.displayname;
                    String prevUserDisplayName = null;
                    String avatar = eventContent.avatar_url;
                    String prevAvatar = null;

                    if ((null != prevEventContent)) {
                        prevUserDisplayName = prevEventContent.displayname;
                        prevAvatar = prevEventContent.avatar_url;
                    }

                    // !Updated display name && same avatar
                    isSupported = TextUtils.equals(prevUserDisplayName, senderDisplayName) && TextUtils.equals(avatar, prevAvatar);
                }
            }
        }

        return isSupported;
    }

    /**
     * Provides the formatted timestamp to display.
     * null means that the timestamp text must be hidden.
     *
     * @param event the event.
     * @return the formatted timestamp to display.
     */
    private String getFormattedTimestamp(Event event) {
        String res = mEventFormattedTsMap.get(event.eventId);

        if (null != res) {
            return res;
        }

        if (event.isValidOriginServerTs()) {
            res = AdapterUtils.tsToString(mContext, event.getOriginServerTs(), true);
        } else {
            res = " ";
        }

        mEventFormattedTsMap.put(event.eventId, res);

        return res;
    }

    /**
     * Refresh the messages date list
     */
    private void refreshRefreshDateList() {
        // build messages timestamps
        ArrayList<Date> dates = new ArrayList<>();

        Date latestDate = AdapterUtils.zeroTimeDate(new Date());

        for (int index = 0; index < this.getCount(); index++) {
            MessageRow row = getItem(index);
            Event event = row.getEvent();

            if (event.isValidOriginServerTs()) {
                latestDate = AdapterUtils.zeroTimeDate(new Date(event.getOriginServerTs()));
            }

            dates.add(latestDate);
        }

        synchronized (this) {
            mMessagesDateList = dates;
            mReferenceDate = new Date();
        }
    }

    /**
     * Converts a difference of days to a string.
     *
     * @param date    the date to display
     * @param nbrDays the number of days between the reference days
     * @return the date text
     */
    private String dateDiff(Date date, long nbrDays) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        int week = calendar.get(Calendar.WEEK_OF_YEAR);
        calendar.setTime(new Date(System.currentTimeMillis()));
        if (nbrDays == 0) {
            return mContext.getResources().getString(R.string.today);
        } else if (nbrDays == 1) {
            return mContext.getResources().getString(R.string.yesterday);
        } else if (week == calendar.get(Calendar.WEEK_OF_YEAR)) {
            return (new SimpleDateFormat("EEEE", mLocale)).format(date);
        } else if (year == calendar.get(Calendar.YEAR)) {
            return (new SimpleDateFormat("dd MMMM", mLocale)).format(date);
        } else {
            int flags = DateUtils.FORMAT_SHOW_DATE |
                    DateUtils.FORMAT_SHOW_YEAR |
                    DateUtils.FORMAT_ABBREV_ALL |
                    DateUtils.FORMAT_SHOW_WEEKDAY;

            Formatter f = new Formatter(new StringBuilder(50), mLocale);
            return DateUtils.formatDateRange(mContext, f, date.getTime(), date.getTime(), flags).toString();
        }
    }

    /**
     * Compute the message header for the item at position.
     * It might be null.
     *
     * @param position the event position
     * @return the header
     */
    String headerMessage(int position) {
        Date prevMessageDate = null;
        Date messageDate = null;

        synchronized (this) {
            if ((position > 0) && (position < mMessagesDateList.size())) {
                prevMessageDate = mMessagesDateList.get(position - 1);
            }
            if (position < mMessagesDateList.size()) {
                messageDate = mMessagesDateList.get(position);
            }
        }

        // sanity check
        if (null == messageDate) {
            return null;
        }

        // same day or get the oldest message
        if ((null != prevMessageDate) && 0 == (prevMessageDate.getTime() - messageDate.getTime())) {
            return null;
        }
        return dateDiff(messageDate, (mReferenceDate.getTime() - messageDate.getTime()) / AdapterUtils.MS_IN_DAY);
    }

    /*
     * *********************************************************************************************
     * E2e management
     * *********************************************************************************************
     */

    /**
     * Manage the select mode i.e highlight an item when the user tap on it
     *
     * @param contentView the cell view.
     * @param event       the linked event
     */
    private void manageSelectionMode(final View contentView, final Event event) {
        final String eventId = event.eventId;

        //  boolean isInSelectionMode = (null != mSelectedEventId);
        //  boolean isSelected = TextUtils.equals(eventId, mSelectedEventId);

        // display the action icon when selected
        //   contentView.findViewById(R.id.messagesAdapter_action_image).setVisibility(isSelected ? View.VISIBLE : View.GONE);

        //float alpha = (!isInSelectionMode || isSelected) ? 1.0f : 0.2f;

        // the message body is dimmed when not selected
        //contentView.findViewById(R.id.messagesAdapter_body_view).setAlpha(alpha);
        //contentView.findViewById(R.id.messagesAdapter_avatars_list).setAlpha(alpha);

       /* TextView tsTextView = contentView.findViewById(R.id.messagesAdapter_timestamp);
        if (isInSelectionMode && isSelected) {
            tsTextView.setVisibility(View.VISIBLE);
        }*/

        if (!(event instanceof EventGroup)) {
            contentView.findViewById(R.id.message_timestamp_layout).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (TextUtils.equals(eventId, mSelectedEventId)) {
                        onMessageClick(event, getEventText(contentView), contentView.findViewById(R.id.messagesAdapter_action_anchor));
                    } else {
                        onEventTap(eventId);
                    }
                }
            });

            contentView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (!mIsSearchMode) {
                        onMessageClick(event, getEventText(contentView), contentView.findViewById(R.id.messagesAdapter_action_anchor));
                        mSelectedEventId = eventId;
                        return true;
                    }

                    return false;
                }
            });
        }
    }

    /**
     * Check an event can be merged with the previous one
     *
     * @param event          the event to merge
     * @param position       the event position in the list
     * @param shouldBeMerged true if the event should be merged
     * @return true to merge the event
     */
    boolean mergeView(Event event, int position, boolean shouldBeMerged) {
        if (shouldBeMerged) {
            shouldBeMerged = null == headerMessage(position);
        }

        return shouldBeMerged && !event.isCallEvent();
    }

    /*
     * *********************************************************************************************
     * Read markers
     * *********************************************************************************************
     */

    /**
     * Return the text displayed in a convertView in the chat history.
     *
     * @param contentView the cell view
     * @return the displayed text.
     */
    private String getEventText(View contentView) {
        String text = null;

        if (null != contentView) {
            TextView bodyTextView = contentView.findViewById(R.id.messagesAdapter_body);

            if (null != bodyTextView) {
                text = bodyTextView.getText().toString();
            }
        }

        return text;
    }

    /**
     * Add click listeners on content view
     *
     * @param convertView the cell view
     * @param contentView the main message view
     * @param position    the item position
     */
    private void addContentViewListeners(final View convertView, final View contentView, final int position) {
        contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mVectorMessagesAdapterEventsListener) {
                    // GA issue
                    if (position < getCount()) {
                        mVectorMessagesAdapterEventsListener.onContentClick(position);
                    }
                }
            }
        });

        contentView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                // GA issue
                if (position < getCount()) {
                    MessageRow row = getItem(position);
                    Event event = row.getEvent();

                    if (!mIsSearchMode) {
                        onMessageClick(event, getEventText(contentView), convertView.findViewById(R.id.messagesAdapter_action_anchor));
                        mSelectedEventId = event.eventId;
                        return true;
                    }
                }

                return true;
            }
        });
    }

    /**
     * Display the e2e icon
     *
     * @param inflatedView the base view
     * @param position     the item position
     */
    private void displayE2eIcon(View inflatedView, int position) {
        ImageView e2eIconView = inflatedView.findViewById(R.id.message_adapter_e2e_icon);

        if (null != e2eIconView) {
            View senderMargin = inflatedView.findViewById(R.id.e2e_sender_margin);
            View senderNameView = inflatedView.findViewById(R.id.messagesAdapter_sender);

            MessageRow row = getItem(position);
            final Event event = row.getEvent();

            if (mE2eIconByEventId.containsKey(event.eventId)) {
                if (null != senderMargin) {
                    senderMargin.setVisibility(senderNameView.getVisibility());
                }
                e2eIconView.setVisibility(VISIBLE);

                Object icon = mE2eIconByEventId.get(event.eventId);

                if (icon instanceof Drawable) {
                    e2eIconView.setImageDrawable((Drawable) icon);
                } else {
                    e2eIconView.setImageResource((int) icon);
                }

                int type = getItemViewType(position);

                if ((type == ROW_TYPE_IMAGE) || (type == ROW_TYPE_VIDEO)) {
                    View bodyLayoutView = inflatedView.findViewById(R.id.messagesAdapter_body_layout);
                    ViewGroup.MarginLayoutParams bodyLayout = (ViewGroup.MarginLayoutParams) bodyLayoutView.getLayoutParams();
                    ViewGroup.MarginLayoutParams e2eIconViewLayout = (ViewGroup.MarginLayoutParams) e2eIconView.getLayoutParams();

                    e2eIconViewLayout.setMargins(bodyLayout.leftMargin, e2eIconViewLayout.topMargin, e2eIconViewLayout.rightMargin, e2eIconViewLayout.bottomMargin);
                    // bodyLayout.setMargins(4, bodyLayout.topMargin, bodyLayout.rightMargin, bodyLayout.bottomMargin);
                    e2eIconView.setLayoutParams(e2eIconViewLayout);
                    bodyLayoutView.setLayoutParams(bodyLayout);
                }

            } else {
                e2eIconView.setVisibility(GONE);
                if (null != senderMargin) {
                    senderMargin.setVisibility(GONE);
                }
            }
        }
    }

    /**
     * Found the dedicated icon to display for each event id
     */
    private void manageCryptoEvents() {
        HashMap<String, Object> e2eIconByEventId = new HashMap<>();
        HashMap<String, MXDeviceInfo> e2eDeviceInfoByEventId = new HashMap<>();

        if (mIsRoomEncrypted && mSession.isCryptoEnabled()) {
            // the key is "userid_deviceid"
            for (int index = 0; index < this.getCount(); index++) {
                MessageRow row = getItem(index);
                Event event = row.getEvent();

                // oneself event
                if (event.mSentState != Event.SentState.SENT) {
                    e2eIconByEventId.put(event.eventId, R.drawable.e2e_verified);
                }
                // not encrypted event
                else if (!event.isEncrypted()) {
                    e2eIconByEventId.put(event.eventId, mPadlockDrawable);
                }
                // in error cases, do not display
                else if (null != event.getCryptoError()) {
                    e2eIconByEventId.put(event.eventId, R.drawable.e2e_blocked);
                } else {
                    EncryptedEventContent encryptedEventContent = JsonUtils.toEncryptedEventContent(event.getWireContent().getAsJsonObject());

                    if (TextUtils.equals(mSession.getCredentials().deviceId, encryptedEventContent.device_id) &&
                            TextUtils.equals(mSession.getMyUserId(), event.getSender())
                            ) {
                        e2eIconByEventId.put(event.eventId, R.drawable.e2e_verified);
                        MXDeviceInfo deviceInfo = mSession.getCrypto().deviceWithIdentityKey(encryptedEventContent.sender_key, event.getSender(), encryptedEventContent.algorithm);

                        if (null != deviceInfo) {
                            e2eDeviceInfoByEventId.put(event.eventId, deviceInfo);
                        }

                    } else {
                        MXDeviceInfo deviceInfo = mSession.getCrypto().deviceWithIdentityKey(encryptedEventContent.sender_key, event.getSender(), encryptedEventContent.algorithm);

                        if (null != deviceInfo) {
                            e2eDeviceInfoByEventId.put(event.eventId, deviceInfo);
                            if (deviceInfo.isVerified()) {
                                e2eIconByEventId.put(event.eventId, R.drawable.e2e_verified);
                            } else if (deviceInfo.isBlocked()) {
                                e2eIconByEventId.put(event.eventId, R.drawable.e2e_blocked);
                            } else {
                                e2eIconByEventId.put(event.eventId, R.drawable.e2e_warning);
                            }
                        } else {
                            e2eIconByEventId.put(event.eventId, R.drawable.e2e_warning);
                        }
                    }
                }
            }
        }

        mE2eDeviceByEventId = e2eDeviceInfoByEventId;
        mE2eIconByEventId = e2eIconByEventId;
    }

    @Override
    public void resetReadMarker() {
        Log.d(LOG_TAG, "resetReadMarker");
        mReadMarkerEventId = null;
    }

    @Override
    public void updateReadMarker(final String readMarkerEventId, final String readReceiptEventId) {
        mReadMarkerEventId = readMarkerEventId;
        mReadReceiptEventId = readReceiptEventId;
        if (readMarkerEventId != null && !readMarkerEventId.equals(mReadMarkerEventId)) {
            Log.d(LOG_TAG, "updateReadMarker read marker id has changed: " + readMarkerEventId);
            mCanShowReadMarker = true;
            notifyDataSetChanged();
        }
    }

    /**
     * Specify a listener for read marker
     *
     * @param listener the read marker listener
     */
    public void setReadMarkerListener(final ReadMarkerListener listener) {
        mReadMarkerListener = listener;
    }

    /**
     * Animate a read marker view
     */
    private void animateReadMarkerView(final Event event, final View readMarkerView) {
        if (readMarkerView != null && mCanShowReadMarker) {
            mCanShowReadMarker = false;
            if (readMarkerView.getAnimation() == null) {
                final Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.unread_marker_anim);
                animation.setStartOffset(500);
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        readMarkerView.setVisibility(GONE);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }
                });
                readMarkerView.setAnimation(animation);
            }

            final Handler uiHandler = new Handler(Looper.getMainLooper());

            uiHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (readMarkerView != null && readMarkerView.getAnimation() != null) {
                        readMarkerView.setVisibility(VISIBLE);
                        readMarkerView.getAnimation().start();

                        // onAnimationEnd does not seem being called when
                        // NotifyDataSetChanged is called during the animation.
                        // This issue is easily reproducable on an Android 7.1 device.
                        // So, we ensure that the listener is always called.
                        uiHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (mReadMarkerListener != null) {
                                    mReadMarkerListener.onReadMarkerDisplayed(event, readMarkerView);
                                }
                            }
                        }, readMarkerView.getAnimation().getDuration() + readMarkerView.getAnimation().getStartOffset());

                    } else {
                        // The animation has been cancelled by a notifyDataSetChanged
                        // With the membership events merge, it will happen more often than before
                        // because many new back paginate will be required to fill the screen.
                        if (mReadMarkerListener != null) {
                            mReadMarkerListener.onReadMarkerDisplayed(event, readMarkerView);
                        }
                    }
                }
            });
        }
    }

    /**
     * Tells if the event is the mReadMarkerEventId one.
     *
     * @param event the event to test
     * @return true if the event is the mReadMarkerEventId one.
     */
    private boolean isReadMarkedEvent(Event event) {
        // if the read marked event is hidden and the event is a merged one
        if ((null != mReadMarkerEventId) && (mHiddenEventIds.contains(mReadMarkerEventId) && (event instanceof EventGroup))) {
            // check it is contains in it
            return ((EventGroup) event).contains(mReadMarkerEventId);
        }

        return event.eventId.equals(mReadMarkerEventId);
    }

    /**
     * Check whether the read marker view should be displayed for the given row
     *
     * @param inflatedView row view
     * @param position     position in adapter
     */
    private void handleReadMarker(final View inflatedView, final int position) {
        final MessageRow row = getItem(position);
        final Event event = row != null ? row.getEvent() : null;
        final View readMarkerView = inflatedView.findViewById(R.id.message_read_marker);
        if (readMarkerView != null) {
            if (event != null && !event.isDummyEvent() && mReadMarkerEventId != null && mCanShowReadMarker
                    && isReadMarkedEvent(event) && !mIsPreviewMode && !mIsSearchMode
                    && (!mReadMarkerEventId.equals(mReadReceiptEventId) || position < getCount() - 1)) {
                Log.d(LOG_TAG, " Display read marker " + event.eventId + " mReadMarkerEventId" + mReadMarkerEventId);
                // Show the read marker
                animateReadMarkerView(event, readMarkerView);
            } else if (GONE != readMarkerView.getVisibility()) {
                Log.v(LOG_TAG, "hide read marker");
                readMarkerView.setVisibility(GONE);
            }
        }
    }

    /**
     * Init the read marker
     *
     * @param convertView      the main view
     * @param row              the message row
     * @param isMergedView     true if the message is merged
     * @param avatarLayoutView the avatar layout
     * @param bodyLayoutView   the body layout
     */
    private void setReadMarker(View convertView, MessageRow row, boolean isMergedView, View avatarLayoutView, View bodyLayoutView) {
        Event event = row.getEvent();

        // search message mode
        View highlightMakerView = convertView.findViewById(R.id.messagesAdapter_highlight_message_marker);
        View readMarkerView = convertView.findViewById(R.id.message_read_marker);

        if (null != highlightMakerView) {
            // align marker view with the message
            ViewGroup.MarginLayoutParams highlightMakerLayout = (ViewGroup.MarginLayoutParams) highlightMakerView.getLayoutParams();
            highlightMakerLayout.setMargins(5, highlightMakerLayout.topMargin, 5, highlightMakerLayout.bottomMargin);

            if (TextUtils.equals(mHighlightedEventId, event.eventId)) {
                if (mIsUnreadViewMode) {
                    highlightMakerView.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.transparent));
                    if (readMarkerView != null) {
                        // Show the read marker
                        animateReadMarkerView(event, readMarkerView);
                    }
                } else {
                    ViewGroup.LayoutParams avatarLayout = avatarLayoutView.getLayoutParams();
                    ViewGroup.MarginLayoutParams bodyLayout = (ViewGroup.MarginLayoutParams) bodyLayoutView.getLayoutParams();

                    if (isMergedView) {
                        highlightMakerLayout.setMargins(avatarLayout.width + 5, highlightMakerLayout.topMargin, 5, highlightMakerLayout.bottomMargin);
                    } else {
                        highlightMakerLayout.setMargins(5, highlightMakerLayout.topMargin, 5, highlightMakerLayout.bottomMargin);
                    }

                    // move left the body
                    bodyLayout.setMargins(4, bodyLayout.topMargin, 4, bodyLayout.bottomMargin);
                    highlightMakerView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.vector_green_color));
                }
            } else {
                highlightMakerView.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.transparent));
            }

            highlightMakerView.setLayoutParams(highlightMakerLayout);
        }
    }

    /*
     * *********************************************************************************************
     * Handle message click events
     * *********************************************************************************************
     */

    /**
     * The user taps on the action icon.
     *
     * @param event      the selected event.
     * @param textMsg    the event text
     * @param anchorView the popup anchor.
     */
    @SuppressLint("NewApi")
    private void onMessageClick(final Event event, final String textMsg, final View anchorView) {
        Log.e(LOG_TAG, "onMessageClick " + event.getType());
        final PopupMenu popup = (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) ? new PopupMenu(mContext, anchorView, Gravity.END) : new PopupMenu(mContext, anchorView);

        popup.getMenuInflater().inflate(R.menu.vector_room_message_settings, popup.getMenu());

        // force to display the icons
        try {
            Field[] fields = popup.getClass().getDeclaredFields();
            for (Field field : fields) {
                if ("mPopup".equals(field.getName())) {
                    field.setAccessible(true);
                    Object menuPopupHelper = field.get(popup);
                    Class<?> classPopupHelper = Class.forName(menuPopupHelper.getClass().getName());
                    Method setForceIcons = classPopupHelper.getMethod("setForceShowIcon", boolean.class);
                    setForceIcons.invoke(menuPopupHelper, true);
                    break;
                }
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, "onMessageClick : force to display the icons failed " + e.getLocalizedMessage());
        }

        Menu menu = popup.getMenu();
        //  CommonActivityUtils.tintMenuIcons(menu, ThemeUtils.getColor(mContext, R.attr.settings_icon_tint_color));

        // hide entries
        for (int i = 0; i < menu.size(); i++) {
            menu.getItem(i).setVisible(false);
        }

        //  menu.findItem(R.id.ic_action_view_source).setVisible(true);
        //  menu.findItem(R.id.ic_action_view_decrypted_source).setVisible(event.isEncrypted() && (null != event.getClearEvent()));
        //  menu.findItem(R.id.ic_action_vector_permalink).setVisible(true);
        if (!isFavouriteMode)
            menu.findItem(R.id.ic_action_vector_add_to_favourite).setVisible(true);

        //   if (!TextUtils.isEmpty(textMsg)) {
        menu.findItem(R.id.ic_action_vector_copy).setVisible(true);
        //       menu.findItem(R.id.ic_action_vector_quote).setVisible(true);
        // }

        if (event.isUploadingMedias(mMediasCache)) {
            menu.findItem(R.id.ic_action_vector_cancel_upload).setVisible(true);
        }

        if (event.isDownloadingMedias(mMediasCache)) {
            menu.findItem(R.id.ic_action_vector_cancel_download).setVisible(true);
        }

        if (event.canBeResent()) {
            menu.findItem(R.id.ic_action_vector_resend_message).setVisible(true);

            if (event.isUndeliverable() || event.isUnkownDevice()) {
                menu.findItem(R.id.ic_action_vector_redact_message).setVisible(true);
            }
        } else if (event.mSentState == Event.SentState.SENT) {

            // test if the event can be redacted
            boolean canBeRedacted = !mIsPreviewMode && !TextUtils.equals(event.getType(), Event.EVENT_TYPE_MESSAGE_ENCRYPTION);

            if (canBeRedacted) {
                // oneself message -> can redact it
                if (TextUtils.equals(event.sender, mSession.getMyUserId())) {
                    canBeRedacted = true;
                } else {
                    // need the minimum power level to redact an event
                    Room room = mSession.getDataHandler().getRoom(event.roomId);

                    if ((null != room) && (null != room.getLiveState().getPowerLevels())) {
                        PowerLevels powerLevels = room.getLiveState().getPowerLevels();
                        canBeRedacted = powerLevels.getUserPowerLevel(mSession.getMyUserId()) >= powerLevels.redact;
//                        canBeRedacted = true;
                    }
                }
            }
            menu.findItem(R.id.ic_action_vector_redact_message).setVisible(canBeRedacted);
            if (Event.EVENT_TYPE_MESSAGE.equals(event.getType())) {
                Message message = JsonUtils.toMessage(event.getContentAsJsonObject());
                // share / forward the message
                menu.findItem(R.id.ic_action_vector_share).setVisible(true);
                menu.findItem(R.id.ic_action_vector_forward).setVisible(true);
                // save the media in the downloads directory
                if (Message.MSGTYPE_IMAGE.equals(message.msgtype) || Message.MSGTYPE_VIDEO.equals(message.msgtype) || Message.MSGTYPE_FILE.equals(message.msgtype)) {
                    menu.findItem(R.id.ic_action_vector_save).setVisible(true);
                }
                // offer to report a message content
                //     menu.findItem(R.id.ic_action_vector_report).setVisible(!mIsPreviewMode && !TextUtils.equals(event.sender, mSession.getMyUserId()));
            }
        }
        // display the menu
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(final MenuItem item) {
                // warn the listener
                if (null != mVectorMessagesAdapterEventsListener) {
                    mVectorMessagesAdapterEventsListener.onEventAction(event, textMsg, item.getItemId());
                }
                // disable the selection
                mSelectedEventId = null;
                notifyDataSetChanged();
                return true;
            }
        });

        // fix an issue reported by GA
        try {
            popup.show();
        } catch (Exception e) {
            Log.e(LOG_TAG, " popup.show failed " + e.getMessage());
        }
    }

    /*
     * *********************************************************************************************
     *  EventGroups events
     * *********************************************************************************************
     */

    /**
     * Insert the MessageRow in an EventGroup to the front.
     *
     * @param row the messageRow
     * @return true if the MessageRow has been inserted
     */
    private boolean addToEventGroupToFront(MessageRow row) {
        MessageRow eventGroupRow = null;

        if (supportMessageRowMerge(row)) {
            if ((getCount() > 0) && (getItem(0).getEvent() instanceof EventGroup) && ((EventGroup) getItem(0).getEvent()).canAddRow(row)) {
                eventGroupRow = getItem(0);
            }

            if (null == eventGroupRow) {
                eventGroupRow = new MessageRow(new EventGroup(mHiddenEventIds), null);
                mEventGroups.add((EventGroup) eventGroupRow.getEvent());
                super.insert(eventGroupRow, 0);
                mEventRowMap.put(eventGroupRow.getEvent().eventId, row);
            }

            ((EventGroup) eventGroupRow.getEvent()).addToFront(row);
            updateHighlightedEventId();
        }

        return (null != eventGroupRow);
    }

    /**
     * Add a MessageRow into an EventGroup (if it is possible)
     *
     * @param row the row to added
     */
    private void addToEventGroup(MessageRow row) {
        if (supportMessageRowMerge(row)) {
            MessageRow eventGroupRow = null;

            // search backward the EventGroup event
            for (int i = getCount() - 1; i >= 0; i--) {
                MessageRow curRow = getItem(i);

                if (curRow.getEvent() instanceof EventGroup) {
                    // the event can be added (same day ?)
                    if (((EventGroup) curRow.getEvent()).canAddRow(row)) {
                        eventGroupRow = curRow;
                    }
                    break;
                } else
                    // there is no more room member events
                    if (!TextUtils.equals(curRow.getEvent().getType(), Event.EVENT_TYPE_STATE_ROOM_MEMBER)) {
                        break;
                    }
            }

            if (null == eventGroupRow) {
                eventGroupRow = new MessageRow(new EventGroup(mHiddenEventIds), null);
                super.add(eventGroupRow);
                mEventGroups.add((EventGroup) eventGroupRow.getEvent());
                mEventRowMap.put(eventGroupRow.getEvent().eventId, eventGroupRow);
            }

            ((EventGroup) eventGroupRow.getEvent()).add(row);
            updateHighlightedEventId();
        }
    }

    /**
     * Remove a message row from the known event groups
     *
     * @param row the message row
     * @return true if the message has been removed
     */
    private void removeFromEventGroup(MessageRow row) {
        if (supportMessageRowMerge(row)) {
            String eventId = row.getEvent().eventId;
            for (EventGroup eventGroup : mEventGroups) {
                if (eventGroup.contains(eventId)) {
                    eventGroup.removeByEventId(eventId);

                    if (eventGroup.isEmpty()) {
                        mEventGroups.remove(eventGroup);
                        super.remove(row);
                        updateHighlightedEventId();
                        return;
                    }
                }
            }
        }
    }

    /**
     * Update the highlighted eventId
     */
    private void updateHighlightedEventId() {
        if (null != mSearchedEventId) {
            if (!mEventGroups.isEmpty() && mHiddenEventIds.contains(mSearchedEventId)) {
                for (EventGroup eventGroup : mEventGroups) {
                    if (eventGroup.contains(mSearchedEventId)) {
                        mHighlightedEventId = eventGroup.eventId;
                        return;
                    }
                }
            }
        }
        mHighlightedEventId = mSearchedEventId;
    }

    /**
     * This method is called after a message deletion at position 'position'.
     * It checks and merges if required two EventGroup around the deleted item.
     *
     * @param deletedRow the deleted row
     * @param position   the deleted item position
     */
    private void checkEventGroupsMerge(MessageRow deletedRow, int position) {
        if ((position > 0) && (position < getCount() - 1) && !EventGroup.isSupported(deletedRow)) {
            Event eventBef = getItem(position - 1).getEvent();
            Event eventAfter = getItem(position).getEvent();

            if (TextUtils.equals(eventBef.getType(), Event.EVENT_TYPE_STATE_ROOM_MEMBER) && eventAfter instanceof EventGroup) {
                EventGroup nextEventGroup = (EventGroup) eventAfter;
                EventGroup eventGroupBefore = null;

                for (int i = position - 1; i >= 0; i--) {
                    if (getItem(i).getEvent() instanceof EventGroup) {
                        eventGroupBefore = (EventGroup) getItem(i).getEvent();
                        break;
                    }
                }

                if (null != eventGroupBefore) {
                    List<MessageRow> nextRows = new ArrayList<>(nextEventGroup.getRows());
                    // check if the next EventGroup can be added in the previous Event group.
                    // it might be impossible if the messages were not sent the same days
                    if (eventGroupBefore.canAddRow(nextRows.get(0))) {
                        for (MessageRow rowToAdd : nextRows) {
                            eventGroupBefore.add(rowToAdd);
                        }
                    }

                    MessageRow row = mEventRowMap.get(nextEventGroup.eventId);
                    mEventGroups.remove(nextEventGroup);
                    super.remove(row);

                    updateHighlightedEventId();
                }
            }
        }
    }

    private void startRoom(String userId) {
        String existingRoomId;
        if (null != (existingRoomId = isDirectChatRoomAlreadyExist(userId))) {
            Log.e(LOG_TAG, "roome exist" + existingRoomId);
            openRoom(mSession.getDataHandler().getRoom(existingRoomId));
        } else {
            mSession.createDirectMessageRoom(userId, mCreateDirectMessageCallBack);
        }
    }

    /**
     * Return the first direct chat room for a given user ID.
     *
     * @param aUserId user ID to search for
     * @return a room ID if search succeed, null otherwise.
     */
    private String isDirectChatRoomAlreadyExist(String aUserId) {
        if (null != mSession) {
            IMXStore store = mSession.getDataHandler().getStore();

            HashMap<String, List<String>> directChatRoomsDict;

            if (null != store.getDirectChatRoomsDict()) {
                directChatRoomsDict = new HashMap<>(store.getDirectChatRoomsDict());

                if (directChatRoomsDict.containsKey(aUserId)) {
                    ArrayList<String> roomIdsList = new ArrayList<>(directChatRoomsDict.get(aUserId));

                    if (0 != roomIdsList.size()) {
                        for (String roomId : roomIdsList) {
                            Room room = mSession.getDataHandler().getRoom(roomId, false);

                            // check if the room is already initialized
                            if ((null != room) && room.isReady() && !room.isInvited() && !room.isLeaving()) {
                                // test if the member did not leave the room
                                Collection<RoomMember> members = room.getActiveMembers();

                                for (RoomMember member : members) {
                                    if (TextUtils.equals(member.getUserId(), aUserId)) {
                                        Log.d(LOG_TAG, "## isDirectChatRoomAlreadyExist(): for user=" + aUserId + " roomFound=" + roomId);
                                        return roomId;
                                    }
                                }

                            }
                        }
                    }
                }
            }
        }
        Log.d(LOG_TAG, "## isDirectChatRoomAlreadyExist(): for user=" + aUserId + " no found room");
        return null;
    }

    /**
     * Open the selected room
     *
     * @param room
     */
    private void openRoom(final Room room) {
        // sanity checks
        // reported by GA
        if ((null == mSession.getDataHandler()) || (null == mSession.getDataHandler().getStore())) {
            return;
        }

        final String roomId;
        // cannot join a leaving room
        if (room == null || room.isLeaving()) {
            roomId = null;
        } else {
            roomId = room.getRoomId();
        }

        if (roomId != null) {
            openRoomById(roomId);
        }
    }

    private void openRoomById(String roomId) {
        Room room = mSession.getDataHandler().getRoom(roomId);
        final RoomSummary roomSummary = mSession.getDataHandler().getStore().getSummary(roomId);
        if (null != roomSummary) {
            room.sendReadReceipt();
        }
        Intent intent = new Intent(mContext, VectorRoomActivity.class);
        // intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra(EXTRA_ROOM_ID, roomId);
        mContext.startActivity(intent);
    }

//    public void fillFavouritelList() {
//        this.clear();
//        List<MessageRow> messages = new ArrayList<>();
//        Collection<Event> events;
//        String userId = mContext.getIntent().getStringExtra("userId");
//        String roomId = mContext.getIntent().getStringExtra("roomId");
//
//        if (roomId != null && !roomId.equals("")) {
//            events = PreferencesManager.getFavouriteMsgsFromGroup(mContext, roomId);
//        } else if (userId != null && !userId.equals("")) {
//            events = PreferencesManager.getFavouriteMsgsFromUser(mContext, userId);
//        } else {
//            events = PreferencesManager.getFavouriteMsgs(mContext);
//        }
//        if (events != null)
//            for (int i = 0; i < events.size(); i++) {
//                Event event = (Event) events.toArray()[i];
//                MessageRow messageRow = new MessageRow(event, mSession.getDataHandler().getRoom(event.roomId).getState());
//                messageRow.setRoomId(event.roomId);
//                messageRow.addCall();
//                messages.add(messageRow);
//            }
//
//        try {
//            Collections.sort(messages, (m1, m2) -> {
//                long diff = m2.getEvent().getOriginServerTs() - m1.getEvent().getOriginServerTs();
//                return (diff < 0) ? +1 : (diff > 0 ? -1 : 0);
//            });
//        } catch (Exception e) {
//            Log.e(LOG_TAG, "## notifyDataSetChanged () : failed to sort events " + e.getMessage());
//        }
//        this.addAll(messages);
//    }

    public interface ReadMarkerListener {
        void onReadMarkerDisplayed(Event event, View view);

    }

    class RowComparator implements Comparator<MessageRow> {

        @Override
        public int compare(MessageRow o1, MessageRow o2) {
            long originServerTs1 = o1.getEvent().getOriginServerTs();
            long originServerTs2 = o2.getEvent().getOriginServerTs();
            return Double.compare(originServerTs1, originServerTs2);
        }
    }
}