package com.talkermessenger.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.talkermessenger.R;
import com.talkermessenger.util.LanguageModel;
import com.talkermessenger.util.VectorUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dev_serhii on 18.01.2018.
 */

public class UserLanguagesAdapter extends RecyclerView.Adapter<UserLanguagesAdapter.ViewHolder> {

    private List<LanguageModel> userLanguages = new ArrayList<>();
    private Context context;

    public UserLanguagesAdapter(Context context, List<LanguageModel> userLanguages) {
        this.context = context;
        this.userLanguages = userLanguages;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()), parent);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.langIcon.setImageDrawable(userLanguages.get(position).getLanguageIcon());
//                VectorUtils.getAvatar(context,
//                        VectorUtils.getAvatarColor(userLanguages.get(position).getLanguageName()),
//                        "@@", true)
        holder.langText.setText(userLanguages.get(position).getLanguageName());

    }

    @Override
    public int getItemCount() {
        return userLanguages == null ? 0 : userLanguages.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.contact_avatar)
        ImageView langIcon;

        @BindView(R.id.section_text_content3)
        TextView langText;

        public ViewHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.adapter_item_account_section_fragment, parent, false));

            ButterKnife.bind(this, itemView);
        }
    }
}
