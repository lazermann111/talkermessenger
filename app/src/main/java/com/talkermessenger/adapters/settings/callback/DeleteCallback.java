package com.talkermessenger.adapters.settings.callback;

import com.iskar.talkersdk.model.TalkerContact;

/**
 * Created by Nikita on 20.04.2018.
 */

public interface DeleteCallback {
    void onDeleted(TalkerContact contact);
}
