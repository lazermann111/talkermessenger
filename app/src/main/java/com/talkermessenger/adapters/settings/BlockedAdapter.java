package com.talkermessenger.adapters.settings;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.iskar.talkersdk.model.TalkerContact;
import com.talkermessenger.R;
import com.talkermessenger.adapters.settings.callback.DeleteCallback;
import com.talkermessenger.adapters.settings.viewHolder.BlockedViewHolder;

import java.util.List;

/**
 * Created by Nikita on 18.04.2018.
 */

public class BlockedAdapter extends RecyclerView.Adapter<BlockedViewHolder> {

    private Context context;
    private List<TalkerContact> contacts;
    private DeleteCallback deleteCallback;

    public BlockedAdapter(Context context, List<TalkerContact> contacts, DeleteCallback deleteCallback) {
        this.context = context;
        this.contacts = contacts;
        this.deleteCallback = deleteCallback;
    }

    @NonNull
    @Override
    public BlockedViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BlockedViewHolder(LayoutInflater.from(context).inflate(R.layout.adapter_item_blocked, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull BlockedViewHolder holder, int position) {
        TalkerContact contact = contacts.get(position);
        holder.setupContact(contact, context);
        holder.setupClickListener(deleteCallback, contact, context);
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }
}
