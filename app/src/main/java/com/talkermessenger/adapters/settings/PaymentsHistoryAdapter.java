package com.talkermessenger.adapters.settings;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.iskar.talkersdk.model.PaymentHistory;
import com.talkermessenger.R;
import com.talkermessenger.adapters.settings.viewHolder.PaymentsHistoryViewHolder;

import java.util.List;

public class PaymentsHistoryAdapter extends RecyclerView.Adapter<PaymentsHistoryViewHolder> {

    private Context context;
    private List<PaymentHistory> paymentHistoryList;

    public PaymentsHistoryAdapter(Context context, List<PaymentHistory> paymentHistoryList) {
        this.context = context;
        this.paymentHistoryList = paymentHistoryList;
    }

    @NonNull
    @Override
    public PaymentsHistoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PaymentsHistoryViewHolder(LayoutInflater.from(context).inflate(R.layout.adapter_item_history_payment, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentsHistoryViewHolder holder, int position) {
        PaymentHistory item = paymentHistoryList.get(position);
        holder.amount.setText(String.valueOf(item.getChars()));
        holder.atChars.setText(String.valueOf(item.getAmount()));
        holder.date.setText(item.getDate());
        holder.method.setText(item.getMethod());
    }

    @Override
    public int getItemCount() {
        return paymentHistoryList.size();
    }
}

