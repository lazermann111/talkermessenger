package com.talkermessenger.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import com.talkermessenger.GlideApp;
import com.talkermessenger.R;

import java.util.List;

/**
 * Created by karasinboots on 25.02.2018.
 */

public class WallpaperChooserAdapter extends RecyclerView.Adapter<WallpaperChooserAdapter.ViewHolder> {

    private List<String> mWallpapers;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context mContext;

    // data is passed into the constructor
    public WallpaperChooserAdapter(Context context, List<String> wallpapers) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.mWallpapers = wallpapers;
    }

    // inflates the cell layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.adapter_item_wallpaper_chooser, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the textview in each cell
    @SuppressLint("StaticFieldLeak")
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;
        GlideApp.with(mContext)
                .load("file://" + mWallpapers.get(position))
                .override(width/3,height/3)
                .fitCenter()
                .into(holder.wallpaper);
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return mWallpapers.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView wallpaper;

        ViewHolder(View itemView) {
            super(itemView);
            wallpaper = itemView.findViewById(R.id.wallpaper_item);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public String getItem(int id) {
        return mWallpapers.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
