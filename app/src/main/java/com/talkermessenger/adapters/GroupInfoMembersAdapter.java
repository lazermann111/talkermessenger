package com.talkermessenger.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.talkermessenger.R;
import com.talkermessenger.activity.VectorMemberDetailsActivity;
import com.talkermessenger.util.VectorUtils;

import org.matrix.androidsdk.MXSession;
import org.matrix.androidsdk.rest.model.RoomMember;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class GroupInfoMembersAdapter extends RecyclerView.Adapter<GroupInfoMembersAdapter.ViewHolder> {
    private ArrayList<RoomMember> mDataset;
    private List<Character> firstLetters;
    private Context mContext;
    private MXSession mSession;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mTextView;
        public View mView;

        @BindView(R.id.contact_avatar)
        CircleImageView vContactAvatar;

        @BindView(R.id.contact_flag_layout)
        FrameLayout frameFlagLayout;

        @BindView(R.id.contact_name)
        TextView vContactName;

        @BindView(R.id.first_big_letter)
        TextView vFirstBigLetter;

        @BindView(R.id.availability)
        ImageView vAvailability;

        @BindView(R.id.contact_view)
        RelativeLayout contactView;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
            vAvailability.setVisibility(View.GONE);
        }
    }

    public GroupInfoMembersAdapter(Context context, MXSession session, ArrayList<RoomMember> dataset) {
        mDataset = dataset;
        mContext = context;
        mSession = session;
        firstLetters = new ArrayList<>();
    }

    @Override
    public GroupInfoMembersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                 int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_item_contact_view_talker, parent, false);
        return new GroupInfoMembersAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(GroupInfoMembersAdapter.ViewHolder holder, int position) {
        RoomMember item = mDataset.get(position);
        if (position >= firstLetters.size() || firstLetters.size() != getItemCount()) {
            sort(alphaComparator);
            updateFirstLetters();
        }
        VectorUtils.loadUserAvatar(mContext, mSession, holder.vContactAvatar, mSession.getDataHandler().getUser(mDataset.get(position).getUserId()));
        holder.vContactName.setText(VectorUtils.getUserDisplayName(mDataset.get(position).getUserId(), mSession, mContext));
        if (firstLetters != null)
            holder.vFirstBigLetter.setText(String.valueOf(firstLetters.get(position)));
        holder.contactView.setOnClickListener(v -> {
            Intent startRoomInfoIntent = new Intent(mContext, VectorMemberDetailsActivity.class);
            startRoomInfoIntent.putExtra(VectorMemberDetailsActivity.EXTRA_MEMBER_ID, item.getUserId());
            startRoomInfoIntent.putExtra(VectorMemberDetailsActivity.EXTRA_MATRIX_ID, mSession.getCredentials().userId);
            mContext.startActivity(startRoomInfoIntent);
        });
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void sort(Comparator<RoomMember> a) {
        Collections.sort(mDataset, a);
    }

    /**
     * Generate list of big letters
     */
    private void updateFirstLetters() {
        List<Character> characters = new LinkedList<>();
        char lastFirstLetter = ' ';
        for (int i = 0; i < getItemCount(); i++) {
            Object abstractObject = mDataset.get(i);
            if (!(abstractObject instanceof RoomMember)) {
                characters.add(' ');
                continue;
            }
            RoomMember item = (RoomMember) abstractObject;
            String currentName = item != null ? VectorUtils.getUserDisplayName(item.getUserId(), mSession, mContext) : "";
            if (currentName.length() > 0 && lastFirstLetter != Character.toUpperCase(currentName.charAt(0))) {
                lastFirstLetter = Character.toUpperCase(currentName.charAt(0));
                if (lastFirstLetter == '@' && currentName.length() > 1)
                    lastFirstLetter = Character.toUpperCase(currentName.charAt(1));
                characters.add(!characters.contains(lastFirstLetter) && lastFirstLetter != '@' ? lastFirstLetter : ' ');
            } else characters.add(' ');
        }
        firstLetters.clear();
        firstLetters.addAll(characters);
    }

    // Comparator to order members alphabetically
    private Comparator<RoomMember> alphaComparator = new Comparator<RoomMember>() {
        @Override
        public int compare(RoomMember member1, RoomMember member2) {
            String lhs = VectorUtils.getUserDisplayName(member1.getUserId(), mSession, mContext);
            String rhs = VectorUtils.getUserDisplayName(member2.getUserId(), mSession, mContext);

            if (lhs == null) {
                return -1;
            } else if (rhs == null) {
                return 1;
            }
            if (lhs.startsWith("@")) {
                lhs = lhs.substring(1);
            }
            if (rhs.startsWith("@")) {
                rhs = rhs.substring(1);
            }
            return String.CASE_INSENSITIVE_ORDER.compare(lhs, rhs);
        }
    };
}
