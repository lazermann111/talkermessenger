/*
 * Copyright 2015 OpenMarket Ltd
 * Copyright 2017 Vector Creations Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.talkermessenger.contacts;

import android.content.Context;

import com.iskar.talkersdk.ContactHandler;
import com.iskar.talkersdk.callback.ContactsInterface;
import com.iskar.talkersdk.model.ContactInfo;
import com.iskar.talkersdk.model.TalkerContact;
import com.talkermessenger.Matrix;

import org.matrix.androidsdk.MXSession;
import org.matrix.androidsdk.rest.model.User;
import org.matrix.androidsdk.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.realm.RealmList;

/**
 * retrieve the contact matrix IDs
 */
public class PIDsRetriever {
    private static final String LOG_TAG = PIDsRetriever.class.getSimpleName();


    public interface PIDsRetrieverListener {
        /**
         * Called when the contacts PIDs are retrieved.
         */
        void onSuccess(String accountId);

        /**
         * Called the PIDs retrieval fails.
         */
        void onFailure(String accountId);
    }

    // current instance
    private static PIDsRetriever mPIDsRetriever = null;

    /**
     * @return the PIDsRetriever instance.
     */
    public static PIDsRetriever getInstance() {
        if (null == mPIDsRetriever) {
            mPIDsRetriever = new PIDsRetriever();
        }

        return mPIDsRetriever;
    }

    // MatrixID <-> medium
    private final HashMap<String, Contact.MXID> mMatrixIdsByMedium = new HashMap<>();

    // listeners list
    private PIDsRetrieverListener mListener = null;

    /**
     * Set the listener.
     *
     * @param listener the listener.
     */
    public void setPIDsRetrieverListener(PIDsRetrieverListener listener) {
        mListener = listener;
    }

    /**
     * Clear the email to matrix id conversion table
     */
    public void onAppBackgrounded() {
        mMatrixIdsByMedium.clear();
    }

    /**
     * reset
     */
    public void reset() {
        mMatrixIdsByMedium.clear();
        mListener = null;
    }

    /**
     * ce (email, phonenumber...)
     *
     * @param item the item to retrieve
     * @return the linked MXID if it exists
     */
    public Contact.MXID getMXID(String item) {
        Contact.MXID mxId = null;

        if ((null != item) && mMatrixIdsByMedium.containsKey(item)) {
            mxId = mMatrixIdsByMedium.get(item);

            // ensure that a valid matrix Id is set
            if (null == mxId.mMatrixId) {
                mxId = null;
            }
        }

        return mxId;
    }

    /**
     * Retrieve the matrix ids for a list of contacts with the local cache.
     *
     * @param contacts the contacts list
     * @return the medium addresses which are not cached.
     */
    private Set<String> retrieveMatrixIds(List<Contact> contacts) {
        Set<String> requestedMediums = new HashSet<>();

        for (Contact contact : contacts) {
            // check if the medium have only been checked
            // i.e. requested their match PID to the identity server.

            for (Contact.PhoneNumber pn : contact.getPhonenumbers()) {
                if (mMatrixIdsByMedium.containsKey(pn.mE164PhoneNumber)) {
                    Contact.MXID mxid = mMatrixIdsByMedium.get(pn.mE164PhoneNumber);
                    if (null != mxid) {
                        contact.put(pn.mE164PhoneNumber, mxid);
                    }
                } else {
                    requestedMediums.add(pn.mE164PhoneNumber);
                }
            }
        }
        // Make sure the set does not contain null value
        requestedMediums.remove(null);

        return requestedMediums;
    }

    /**
     * Retrieve the matrix IDs from the contact fields (only emails are supported by now).
     * Update the contact fields with the found Matrix Ids.
     * The update could require some remote requests : they are done only localUpdateOnly is false.
     *
     * @param context         the context.
     * @param contacts        the contacts list.
     * @param localUpdateOnly true to only support refresh from local information.
     * @return true if the matrix Ids have been retrieved
     */
    public void retrieveMatrixIds(final Context context, final List<Contact> contacts, final boolean localUpdateOnly) {
        Log.d(LOG_TAG, String.format("retrieveMatrixIds starts for %d contacts", contacts == null ? 0 : contacts.size()));
        MXSession mxSession = Matrix.getInstance(context.getApplicationContext()).getDefaultSession();
        if (mxSession != null) {
            ContactHandler contactHandler = new ContactHandler(context, mxSession.getMyUserId(),
                    mxSession.getCredentials().accessToken, new ContactsInterface() {
                @Override
                public void onContactsReceived(List<ContactInfo> registeredList) {
                    final String accountId = mxSession.getCredentials().userId;
                    ArrayList<TalkerContact> talkerContacts = new ArrayList<>();
                    ContactHandler contactHandler1 = new ContactHandler();
                    if (contacts != null)
                        for (Contact contact : contacts) {
                            for (ContactInfo contactInfo : registeredList) {
                                if (contactInfo.getInfo() != null && contactInfo.getInfo().getMatrixId() != null) {
                                    for (Contact.PhoneNumber pn : contact.getPhonenumbers()) {
                                        if (pn.mE164PhoneNumber != null && (pn.mE164PhoneNumber).equals(contactInfo.getPhone())) {
                                            RealmList<String> numbers = new RealmList<>();
                                            contact.put(pn.mE164PhoneNumber, new Contact.MXID(contactInfo.getInfo().getMatrixId(), accountId));
                                            User user = mxSession.getDataHandler().getUser(contactInfo.getInfo().getMatrixId());
                                            if (user != null)
                                                user.setDisplayname(contact.getDisplayName());
                                            contact.setMatrixId(contactInfo.getInfo().getMatrixId());
                                            numbers.add(pn.mE164PhoneNumber);
                                            for (Contact.PhoneNumber pn1 : contact.getPhonenumbers()) {
                                                Log.e(LOG_TAG, "number added to list");
                                                if (!pn1.equals(numbers.get(0)))
                                                    numbers.add(pn1.mE164PhoneNumber);
                                            }
                                            talkerContacts.add(new TalkerContact(
                                                    contact.getContactId(), contactInfo.getInfo().getMatrixId(),
                                                    numbers, contact.getThumbnailUri(),
                                                    contact.getPhotoUri(), contact.getLookupUri().toString(),
                                                    contact.getDisplayName()));
                                        }
                                    }
                                }
                            }
                        }
                    else  mListener.onSuccess(mxSession.getMyUserId());
                    contactHandler1.saveContact(context, talkerContacts);
                    mListener.onSuccess(mxSession.getMyUserId());
                }

                @Override
                public void onContactReceived(ContactInfo contactInfo) {

                }

                @Override
                public void onFailure(String msg) {
                    mListener.onSuccess(mxSession.getMyUserId());
                }
            });
            Set<String> phones = new HashSet<>();
            if (contacts != null)
                for (Contact contact : contacts) {
                    for (Contact.PhoneNumber phoneNumbers : contact.getPhonenumbers())
                        if (phoneNumbers.mE164PhoneNumber != null)
                            phones.add(phoneNumbers.mE164PhoneNumber);
                }
            if (phones.size() > 0)
                contactHandler.uploadContacts(phones);
            else mListener.onSuccess(mxSession.getMyUserId());
        }
    }
}


