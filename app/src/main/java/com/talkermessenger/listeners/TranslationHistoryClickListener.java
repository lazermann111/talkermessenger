package com.talkermessenger.listeners;

import com.iskar.talkersdk.model.TranslationHistoryItem;

public interface TranslationHistoryClickListener {
    void onLongClick(TranslationHistoryItem item, int position);

    void onClick(TranslationHistoryItem item, int position);
}
