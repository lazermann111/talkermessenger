package com.talkermessenger.backup;

/**
 * Created by karasinboots on 25.11.2017.
 */

public interface DropboxAsyncResponse {
    void processFinish(String path);
}
