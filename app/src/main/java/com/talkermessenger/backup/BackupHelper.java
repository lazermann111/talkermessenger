package com.talkermessenger.backup;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.dropbox.core.DbxException;
import com.talkermessenger.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Map;
import java.util.Set;

/**
 * Created by karasinboots on 12.02.2018.
 */

public class BackupHelper implements DropboxAsyncResponse {
    private String mUserId;
    private DropboxAsyncResponse mListener;
    private Context mContext;

    public BackupHelper(String userId, DropboxAsyncResponse listener) {
        this.mUserId = userId;
        this.mListener = listener;
    }

    public void backupAllExisting(Context context) {
        Map<String, ?> map = PreferenceManager.getDefaultSharedPreferences(context).getAll();
        String filePath = writeToFile(map);
        if (filePath != null)
            sendBackupToDropbox(filePath);
    }

    public void restorePreferences(Context context) {
        this.mContext = context;
        Log.e("restore preferences", "activated");
        getBackupFromDropbox();

    }

    private void restoreFromFile() {
        removeCurrentSettings();
        restoreShareds();

    }

    private void removeCurrentSettings() {
        File currentPrefsDir = new File(Environment.getDataDirectory() + "/shared_prefs/");
        traverse(currentPrefsDir);

    }

    private void restoreShareds() {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(mContext).edit();
        Map<String, ?> map = getMapFromFile();
        // Log.e("restored key", String.valueOf(map.size()));
        if (map != null)
            for (Map.Entry<String, ?> e : map.entrySet()) {
                if (e.getValue() instanceof Integer) {
                    editor.putInt(e.getKey(), (Integer) e.getValue());
                } else if (e.getValue() instanceof String) {
                    editor.putString(e.getKey(), (String) e.getValue());
                } else if (e.getValue() instanceof Long) {
                    editor.putLong(e.getKey(), (Long) e.getValue());
                } else if (e.getValue() instanceof Boolean) {
                    editor.putBoolean(e.getKey(), (Boolean) e.getValue());
                } else if (e.getValue() instanceof Float) {
                    editor.putFloat(e.getKey(), (Float) e.getValue());
                } else if (e.getValue() instanceof Set) {
                    editor.putStringSet(e.getKey(), (Set<String>) e.getValue());
                }
                Log.e("restored key", e.getKey() + "   " + e.getValue());
            }
        editor.apply();
        Toast.makeText(mContext, R.string.recover_sucess_beckup, Toast.LENGTH_LONG).show();
    }

    private void traverse(File dir) {
        if (dir.exists()) {
            File[] files = dir.listFiles();
            for (File file : files) {
                if (file.isDirectory()) {
                    traverse(file);
                } else {
                    file.delete();
                }
            }
        }
    }

    private Map<String, ?> getMapFromFile() {
        ObjectInputStream input = null;
        try {
            input = new ObjectInputStream(new FileInputStream(Environment.getExternalStorageDirectory() + "/talkerBackup/" + mUserId + ".txt"));
            return (Map<String, ?>) input.readObject();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                if (input != null) {
                    input.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return null;
    }

    private void getBackupFromDropbox() {
        Log.e("getBackupFromDropbox", "activated");
        Dropbox dropbox = new Dropbox();
        try {
            dropbox.init();
        } catch (DbxException e) {
            e.printStackTrace();
        }
        dropbox.delegate = this;
        dropbox.execute(null, mUserId);
    }

    private void sendBackupToDropbox(String filePath) {
        Dropbox dropbox = new Dropbox();
        try {
            dropbox.init();
        } catch (DbxException e) {
            e.printStackTrace();
        }
        dropbox.delegate = mListener;
        dropbox.execute(filePath, null);
    }

    private String writeToFile(Map<String, ?> map) {
        ObjectOutputStream output = null;
        try {
            File file = new File(
                    Environment.getExternalStorageDirectory() + "/talkerBackup/");
            file.mkdirs();
            String filename = mUserId + ".txt";
            String fullPath = file.getPath() + "/" + filename;
            File fileObj = new File(fullPath);
            output = new ObjectOutputStream(new FileOutputStream(fileObj));
            output.writeObject(map);
            return fullPath;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (output != null) {
                    output.flush();
                    output.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public void processFinish(String path) {
        Log.e("loaded from dropbox", path + "");
        restoreFromFile();
    }
}

