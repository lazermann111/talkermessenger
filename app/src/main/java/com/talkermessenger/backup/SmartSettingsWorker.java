package com.talkermessenger.backup;

import android.content.Context;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.util.Log;

import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.talkermessenger.R;
import com.talkermessenger.util.ThemeUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.Locale;

import androidx.work.Data;
import androidx.work.Worker;

public class SmartSettingsWorker extends Worker {
    private static final String ACCESS_TOKEN = "AgdDrXL1B_QAAAAAAABXYRBsG7LWSVgDDE18c_SeYygsr5iB_YXFBjWzOxQdRMPk";

    @NonNull
    @Override
    public WorkerResult doWork() {
        DbxRequestConfig config = new DbxRequestConfig("talkerBackup", "en_US");
        DbxClientV2 client = new DbxClientV2(config, ACCESS_TOKEN);
        CharSequence[] imageNames = new CharSequence[]{"design_settings", "purchases_settings", "dnd_settings", "media_settings", "profile_settings", "privacy_settings", "notification_settings", "translation_settings", "security_settings"};
        String language = Locale.getDefault().getLanguage();
        if (!language.equals("ru"))
            language = "en";
        for (CharSequence imageName : imageNames) {
            try {
                String filename = imageName + ".png";
                String dropPath = "/talkerSmartSettings/" + language + "/" + ThemeUtils.getApplicationTheme(getApplicationContext()) + "/" + filename;
                File baseFolderFile = new File(getApplicationContext().getFilesDir(), "SmartSettings");
                if (!baseFolderFile.exists())
                    baseFolderFile.mkdirs();
                InputStream is = client.files().download(dropPath).getInputStream();
                String fullPath = baseFolderFile.getPath() + "/" + filename;
                File file = new File(fullPath);
                if (file.exists())
                    file.delete();
                OutputStream output = new FileOutputStream(fullPath, false);
                byte[] buffer = new byte[4 * 1024];
                int read;
                while ((read = is.read(buffer)) != -1) {
                    output.write(buffer, 0, read);
                }
                output.flush();
                output.close();
            } catch (Exception e) {
                e.printStackTrace();
                return WorkerResult.FAILURE;
            }
        }
        return WorkerResult.SUCCESS;
    }
}
