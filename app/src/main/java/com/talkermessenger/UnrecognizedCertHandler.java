/*
 * Copyright 2015 OpenMarket Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.talkermessenger;

import android.app.Activity;

import org.matrix.androidsdk.HomeServerConnectionConfig;
import org.matrix.androidsdk.ssl.Fingerprint;
import org.matrix.androidsdk.util.Log;

import java.util.HashMap;
import java.util.HashSet;

public class UnrecognizedCertHandler {
    private static final String LOG_TAG = UnrecognizedCertHandler.class.getSimpleName();

    private static final HashMap<String, HashSet<Fingerprint>> ignoredFingerprints = new HashMap<>();
    private static final HashSet<String> openDialogIds = new HashSet<>();

    public static void show(final HomeServerConnectionConfig hsConfig, final Fingerprint unrecognizedFingerprint, boolean existing, final Callback callback) {
        final Activity activity = VectorApp.getCurrentActivity();

        if (activity == null) {
            return;
        }

        final String dialogId;
        if (hsConfig.getCredentials() != null) {
            dialogId = hsConfig.getCredentials().userId;
        } else {
            dialogId = hsConfig.getHomeserverUri().toString() + unrecognizedFingerprint.getBytesAsHexString();
        }

        if (openDialogIds.contains(dialogId)) {
            Log.i(LOG_TAG, "Not opening dialog " + dialogId + " as one is already open.");
            return;
        }

        if (hsConfig.getCredentials() != null) {
            HashSet<Fingerprint> f = ignoredFingerprints.get(hsConfig.getCredentials().userId);
            if (f != null && f.contains(unrecognizedFingerprint)) {
                callback.onIgnore();
                return;
            }
        }
    }

    public interface Callback {
        void onAccept();

        void onIgnore();

        void onReject();
    }
}
