package com.talkermessenger.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;

import com.talkermessenger.Matrix;
import com.talkermessenger.R;
import com.talkermessenger.adapters.GroupInfoMembersAdapter;

import org.matrix.androidsdk.MXSession;
import org.matrix.androidsdk.data.Room;
import org.matrix.androidsdk.rest.model.RoomMember;

import java.util.ArrayList;

import static com.talkermessenger.activity.GroupInfoActivity.EXTRA_MATRIX_ID;
import static com.talkermessenger.activity.GroupInfoActivity.EXTRA_ROOM_ID;

public class GroupInfoMembersActivity extends AppCompatActivity {
    private MXSession mSession;
    private Room mRoom;
    private ArrayList<RoomMember> mRoomMembersList;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_info_members);

        toolbar = findViewById(R.id.account_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        MXSession session;
        RecyclerView recyclerView = findViewById(R.id.group_members_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRoomMembersList = new ArrayList<>();

        Intent intent = getIntent();

        String matrixId = intent.getStringExtra(EXTRA_MATRIX_ID);
        if (!TextUtils.isEmpty(matrixId))
            mSession = Matrix.getMXSession(this, matrixId);
        else
            mSession = Matrix.getInstance(this).getDefaultSession();

        String roomId = intent.getStringExtra(EXTRA_ROOM_ID);
        if (!TextUtils.isEmpty(matrixId))
            mRoom = mSession.getDataHandler().getRoom(roomId);

        if (mSession == null || mRoom == null)
            finish();

        for (RoomMember member : mRoom.getState().getMembers()) {
            if (!mSession.getMyUserId().equals(member.getUserId())) {
                mRoomMembersList.add(member);
            }
        }
        GroupInfoMembersAdapter mAdapter = new GroupInfoMembersAdapter(this, mSession, mRoomMembersList);
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
