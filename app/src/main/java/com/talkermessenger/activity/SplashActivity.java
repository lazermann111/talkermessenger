/*
 * Copyright 2014 OpenMarket Ltd
 * Copyright 2017 Vector Creations Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.talkermessenger.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;

import com.iskar.talkersdk.ContactHandler;
import com.iskar.talkersdk.LanguagesHandler;
import com.iskar.talkersdk.callback.LanguagesInterface;
import com.iskar.talkersdk.model.UserLanguages;
import com.talkermessenger.ErrorListener;
import com.talkermessenger.Matrix;
import com.talkermessenger.R;
import com.talkermessenger.VectorApp;
import com.talkermessenger.activity.settings.AccountRegistrationActivity;
import com.talkermessenger.contacts.Contact;
import com.talkermessenger.contacts.ContactsManager;
import com.talkermessenger.gcm.GcmRegistrationManager;
import com.talkermessenger.receiver.VectorUniversalLinkReceiver;
import com.talkermessenger.services.EventStreamService;
import com.talkermessenger.util.PreferencesManager;
import com.talkermessenger.util.RoomUtils;

import org.matrix.androidsdk.MXSession;
import org.matrix.androidsdk.data.Room;
import org.matrix.androidsdk.data.RoomTag;
import org.matrix.androidsdk.listeners.IMXEventListener;
import org.matrix.androidsdk.listeners.MXEventListener;
import org.matrix.androidsdk.rest.callback.ApiCallback;
import org.matrix.androidsdk.rest.model.MatrixError;
import org.matrix.androidsdk.util.Log;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import static android.os.Build.VERSION_CODES.M;
import static com.talkermessenger.activity.CommonActivityUtils.REQUEST_CODE_PERMISSION_CONTACTS_UPDATE;

/**
 * SplashActivity displays a splash while loading and inittializing the client.
 */
public class SplashActivity extends MXCActionBarActivity {
    private static final String LOG_TAG = SplashActivity.class.getSimpleName();

    public static final String EXTRA_MATRIX_ID = "EXTRA_MATRIX_ID";
    public static final String EXTRA_ROOM_ID = "EXTRA_ROOM_ID";

    private HashMap<MXSession, IMXEventListener> mListeners;
    private HashMap<MXSession, IMXEventListener> mDoneListeners;

    private final long mLaunchTime = System.currentTimeMillis();

    /**
     * @return true if a store is corrupted.
     */
    private boolean hasCorruptedStore() {
        boolean hasCorruptedStore = false;
        ArrayList<MXSession> sessions = Matrix.getMXSessions(this);

        for (MXSession session : sessions) {
            if (session.isAlive()) {
                hasCorruptedStore |= session.getDataHandler().getStore().isCorrupted();
            }
        }
        return hasCorruptedStore;
    }

    /**
     * Close the splash screen if the stores are fully loaded.
     */
    private void onInitialized() {
        Log.e(LOG_TAG, "on initialized");
        if (!ContactsManager.getInstance().isContactBookAccessRequested())
            if (android.os.Build.VERSION.SDK_INT >= M)
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest
                        .permission
                        .READ_CONTACTS}, REQUEST_CODE_PERMISSION_CONTACTS_UPDATE);
            else {
                if (!ContactsManager.getInstance().isContactBookAccessRequested()) {
                    AlertDialog.Builder permissionsInfoDialog = new AlertDialog.Builder(this);
                    permissionsInfoDialog.setIcon(android.R.drawable.ic_dialog_info);
                    Resources resource = getResources();
                    if (null != resource) {
                        permissionsInfoDialog.setTitle(resource.getString(R.string.permissions_rationale_popup_title));
                    }
                    permissionsInfoDialog.setMessage(resource.getString(R.string.permissions_msg_contacts_warning_other_androids));
                    // gives the contacts book access
                    permissionsInfoDialog.setPositiveButton(this.getString(R.string.yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ContactsManager.getInstance().setIsContactBookAccessAllowed(true);
                            ActivityCompat.requestPermissions(SplashActivity.this, new String[]{android.Manifest.permission.READ_CONTACTS}, REQUEST_CODE_PERMISSION_CONTACTS_UPDATE);
                        }
                    });
                    permissionsInfoDialog.show();
                } else preloadContacts();
            } else onFinish();
    }

    @Override
    public void onRequestPermissionsResult(int aRequestCode, @NonNull String[] aPermissions, @NonNull int[] aGrantResults) {
        Log.e(LOG_TAG, "permission result");
        if (aRequestCode == REQUEST_CODE_PERMISSION_CONTACTS_UPDATE)
            if (hasAllPermissionsGranted(aGrantResults)) {
                Log.e(LOG_TAG, new ContactHandler().isContactSnapshotAvailable(this) + "");
                preloadContacts();
            } else {
                onFinish();
            }

    }

    private void preloadContacts() {
        if (!new ContactHandler().isContactSnapshotAvailable(this)) {
            ContactsManager.getInstance().addListener(new ContactsManager.ContactsManagerListener() {
                @Override
                public void onRefresh() {
                    Log.e(LOG_TAG, "contacts refreshed");
                }

                @Override
                public void onPIDsUpdate() {
                    Log.e(LOG_TAG, "on pids update");
                    ContactsManager.getInstance().removeListener(this);
                    onFinish();
                }

                @Override
                public void onContactPresenceUpdate(Contact contact, String matrixId) {
                }
            });
            ContactsManager.getInstance().refreshLocalContactsSnapshot();
        } else onFinish();
    }

    private boolean hasAllPermissionsGranted(@NonNull int[] grantResults) {
        for (int grantResult : grantResults) {
            if (grantResult == PackageManager.PERMISSION_DENIED) {
                return false;
            }
        }
        return true;
    }

    /**
     * Close the splash screen if the stores are fully loaded.
     */
    private void onFinish() {
        Log.e(LOG_TAG, "onFinish");
        if (!hasCorruptedStore()) {
            // Go to the home page
//            Intent intent = new Intent(SplashActivity.this, VectorHomeActivity.class);
            if (PreferencesManager.getFavouriteMsgCommonRoom(this) == null) {
                MXSession defaultSession = Matrix.getInstance(this).getDefaultSession();
                if (defaultSession != null) {
                    Room favouriteMsgsRoom = RoomUtils.findFavouriteMsgsRoom(defaultSession);
                    if (favouriteMsgsRoom != null)
                        PreferencesManager.addFavouriteMsgCommonRoom(this, favouriteMsgsRoom.getRoomId());
                    else
                        RoomUtils.createFavouriteMsgRoomWithoutOpening(defaultSession, this);
                }
            }

            Intent intent;
            mSession = Matrix.getInstance(this).getDefaultSession();
            String myUserId = mSession.getMyUserId().substring(1, mSession.getMyUserId().lastIndexOf(':'));
            if (mSession.getMyUser().displayname == null
                    || mSession.getMyUser().displayname.equals(PreferencesManager.getUserNumber(this))
                    || myUserId.equals(mSession.getMyUser().displayname))
                intent = new Intent(SplashActivity.this, AccountRegistrationActivity.class);
            else {
                new LanguagesHandler(this, mSession.getMyUserId(), mSession.getCredentials().accessToken, new LanguagesInterface() {
                    @Override
                    public void onLanguagesReceived(UserLanguages userLanguages) {

                    }

                    @Override
                    public void onMainLanguageReceived(String mainLanguage) {

                    }

                    @Override
                    public void onFailure(String message) {

                    }
                }).getMainUserLanguage();
                intent = new Intent(SplashActivity.this, VectorHomeActivity.class);
            }
            mSession.joinRoom(getString(R.string.test_room_id), new ApiCallback<String>() {
                @Override
                public void onSuccess(String info) {
                    Log.e("joining", "to test IM room succeed");
                    RoomUtils.updateRoomTag(mSession, getString(R.string.test_room_id), null, RoomTag.ROOM_TAG_PIN, new ApiCallback<Void>() {
                        @Override
                        public void onSuccess(Void info) {
                            Log.e("update tag request", "waiting");
                        }

                        @Override
                        public void onNetworkError(Exception e) {
                            Log.e("update tag request", "network error");
                        }

                        @Override
                        public void onMatrixError(MatrixError e) {
                            Log.e("update tag request", "matrix error");
                        }

                        @Override
                        public void onUnexpectedError(Exception e) {
                            Log.e("update tag request", "unexpected");
                        }
                    });
                }

                @Override
                public void onNetworkError(Exception e) {

                }

                @Override
                public void onMatrixError(MatrixError e) {

                }

                @Override
                public void onUnexpectedError(Exception e) {

                }
            });
            Bundle receivedBundle = getIntent().getExtras();

            if (null != receivedBundle) {
                intent.putExtras(receivedBundle);
            }

            // display a spinner while managing the universal link
            if (intent.hasExtra(VectorUniversalLinkReceiver.EXTRA_UNIVERSAL_LINK_URI)) {
                intent.putExtra(VectorHomeActivity.EXTRA_WAITING_VIEW_STATUS, VectorHomeActivity.WAITING_VIEW_START);
            }

            // launch from a shared files menu
            if (getIntent().hasExtra(VectorHomeActivity.EXTRA_SHARED_INTENT_PARAMS)) {
                intent.putExtra(VectorHomeActivity.EXTRA_SHARED_INTENT_PARAMS, (Parcelable) getIntent().getParcelableExtra(VectorHomeActivity.EXTRA_SHARED_INTENT_PARAMS));
                getIntent().removeExtra(VectorHomeActivity.EXTRA_SHARED_INTENT_PARAMS);
            }

            if (getIntent().hasExtra(EXTRA_ROOM_ID) && getIntent().hasExtra(EXTRA_MATRIX_ID)) {
                HashMap<String, Object> params = new HashMap<>();
                params.put(VectorRoomActivity.EXTRA_MATRIX_ID, getIntent().getStringExtra(EXTRA_MATRIX_ID));
                params.put(VectorRoomActivity.EXTRA_ROOM_ID, getIntent().getStringExtra(EXTRA_ROOM_ID));
                intent.putExtra(VectorHomeActivity.EXTRA_JUMP_TO_ROOM_PARAMS, params);
            }
            startActivity(intent);
            SplashActivity.this.finish();
        } else {
            CommonActivityUtils.logout(SplashActivity.this);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(LOG_TAG, "onCreate");

        setContentView(R.layout.vector_activity_splash);

        Collection<MXSession> sessions = Matrix.getInstance(getApplicationContext()).getSessions();

        if (sessions == null) {
            Log.e(LOG_TAG, "onCreate no Sessions");
            finish();
            return;
        }
        mListeners = new HashMap<>();
        mDoneListeners = new HashMap<>();

        ArrayList<String> matrixIds = new ArrayList<>();

        for (final MXSession session : sessions) {
            final MXSession fSession = session;

            final IMXEventListener eventListener = new MXEventListener() {
                private void onReady() {
                    boolean isAlreadyDone;

                    synchronized (LOG_TAG) {
                        isAlreadyDone = mDoneListeners.containsKey(fSession);
                    }

                    if (!isAlreadyDone) {
                        synchronized (LOG_TAG) {
                            boolean noMoreListener;

                            Log.e(LOG_TAG, "Session " + fSession.getCredentials().userId + " is initialized");

                            mDoneListeners.put(fSession, mListeners.get(fSession));
                            // do not remove the listeners here
                            // it crashes the application because of the upper loop
                            //fSession.getDataHandler().removeListener(mListeners.get(fSession));
                            // remove from the pending list

                            mListeners.remove(fSession);
                            noMoreListener = (mListeners.size() == 0);

                            if (noMoreListener) {
                                VectorApp.addSyncingSession(session);
                                onInitialized();
                            }
                        }
                    }
                }

                // should be called if the application was already initialized
                @Override
                public void onLiveEventsChunkProcessed(String fromToken, String toToken) {
                    super.onLiveEventsChunkProcessed(fromToken, toToken);
                    onReady();
                }

                // first application launched
                @Override
                public void onInitialSyncComplete(String toToken) {
                    super.onInitialSyncComplete(toToken);
                    onReady();
                }
            };

            if (!fSession.getDataHandler().isInitialSyncComplete()) {
                session.getDataHandler().getStore().open();

                mListeners.put(fSession, eventListener);
                fSession.getDataHandler().addListener(eventListener);

                // Set the main error listener
                fSession.setFailureCallback(new ErrorListener(fSession, this));

                // session to activate
                matrixIds.add(session.getCredentials().userId);
            }
        }

        // when the events stream has been disconnected by the user
        // they must be awoken even if they are initialized
        if (Matrix.getInstance(this).mHasBeenDisconnected) {
            matrixIds = new ArrayList<>();

            for (MXSession session : sessions) {
                matrixIds.add(session.getCredentials().userId);
            }

            Matrix.getInstance(this).mHasBeenDisconnected = false;
        }

        if (EventStreamService.getInstance() == null) {
            // Start the event stream service
            Intent intent = new Intent(this, EventStreamService.class);
            intent.putExtra(EventStreamService.EXTRA_MATRIX_IDS, matrixIds.toArray(new String[matrixIds.size()]));
            intent.putExtra(EventStreamService.EXTRA_STREAM_ACTION, EventStreamService.StreamAction.START.ordinal());
            startService(intent);
        } else {
            EventStreamService.getInstance().startAccounts(matrixIds);
        }

        // trigger the GCM registration if required
        GcmRegistrationManager gcmRegistrationManager = Matrix.getInstance(getApplicationContext()).getSharedGCMRegistrationManager();

        if (!gcmRegistrationManager.isGCMRegistred()) {
            gcmRegistrationManager.checkRegistrations();
        } else {
            gcmRegistrationManager.forceSessionsRegistration(null);
        }

        boolean noUpdate;

        synchronized (LOG_TAG) {
            noUpdate = (mListeners.size() == 0);
        }

        // nothing to do ?
        // just dismiss the activity
        if (noUpdate) {
            // do not launch an activity if there was nothing new.
            Log.e(LOG_TAG, "nothing to do");
            onInitialized();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Collection<MXSession> sessions = mDoneListeners.keySet();

        for (MXSession session : sessions) {
            if (session.isAlive()) {
                session.getDataHandler().removeListener(mDoneListeners.get(session));
                session.setFailureCallback(null);
            }
        }
    }
}
