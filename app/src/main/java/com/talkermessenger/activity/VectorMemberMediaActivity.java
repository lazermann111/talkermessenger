package com.talkermessenger.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.talkermessenger.Matrix;
import com.talkermessenger.R;
import com.talkermessenger.adapters.AdapterUtils;
import com.talkermessenger.util.MediaMonthSection;
import com.talkermessenger.util.RoomUtils;

import org.matrix.androidsdk.MXSession;
import org.matrix.androidsdk.data.Room;
import org.matrix.androidsdk.rest.model.Event;
import org.matrix.androidsdk.rest.model.Message;
import org.matrix.androidsdk.rest.model.User;
import org.matrix.androidsdk.util.JsonUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter;

public class VectorMemberMediaActivity extends AppCompatActivity {

    private ArrayList<Event> fileEvents = new ArrayList<>();
    private ArrayList<Event> mediaEvents = new ArrayList<>();
    private ArrayList<Event> linkEvents = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private GridLayoutManager mLayoutManager;
    private MXSession mSession;
    //    private String LOG_TAG = getClass().getName();
    private TextView mMemberNameTextView;
    private User mUser;
    private Room mRoom;
    //    private Button mMediaButton;
//    private Button mLinkButton;
//    private Button mFileButton;
    private SectionedRecyclerViewAdapter sectionAdapter;
    private ArrayList<String> previousLinks = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vector_member_media);

        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.member_media_toolbar);
        setSupportActionBar(toolbar);
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.member_details_media_media);
        }
        sectionAdapter = new SectionedRecyclerViewAdapter();

//        toolbar.findViewById(R.id.back_icon).setOnClickListener(view -> onBackPressed());
//        mMemberNameTextView = findViewById(R.id.member_details_name);
//        mMediaButton = findViewById(R.id.media_btn);
//        mLinkButton = findViewById(R.id.link_btn);
//        mFileButton = findViewById(R.id.file_btn);
//        mMediaButton.setTransformationMethod(null);
//        mLinkButton.setTransformationMethod(null);
//        mFileButton.setTransformationMethod(null);

//        mMediaButton.setOnClickListener(view -> {
//            mMediaButton.setTextColor(Color.WHITE);
//            mMediaButton.setBackground(ContextCompat.getDrawable(this, R.drawable.media_tab_button_left_clicked));
//
//            mLinkButton.setTextColor(ContextCompat.getColor(this, R.color.media_tab_button));
//            mLinkButton.setBackground(ContextCompat.getDrawable(this, R.drawable.media_tab_button_center));
//
//            mFileButton.setTextColor(ContextCompat.getColor(this, R.color.media_tab_button));
//            mFileButton.setBackground(ContextCompat.getDrawable(this, R.drawable.media_tab_button_right));
//
//            setAdapterData(mediaEvents, sectionAdapter);
//        });
//
//        mLinkButton.setOnClickListener(view -> {
//            mMediaButton.setTextColor(ContextCompat.getColor(this, R.color.media_tab_button));
//            mMediaButton.setBackground(ContextCompat.getDrawable(this, R.drawable.media_tab_button_left));
//
//            mLinkButton.setTextColor(Color.WHITE);
//            mLinkButton.setBackground(ContextCompat.getDrawable(this, R.drawable.media_tab_button_center_clicked));
//
//            mFileButton.setTextColor(ContextCompat.getColor(this, R.color.media_tab_button));
//            mFileButton.setBackground(ContextCompat.getDrawable(this, R.drawable.media_tab_button_right));
//
//            setAdapterData(linkEvents, sectionAdapter);
//        });
//
//        mFileButton.setOnClickListener(view -> {
//            mMediaButton.setTextColor(ContextCompat.getColor(this, R.color.media_tab_button));
//            mMediaButton.setBackground(ContextCompat.getDrawable(this, R.drawable.media_tab_button_left));
//
//            mLinkButton.setTextColor(ContextCompat.getColor(this, R.color.media_tab_button));
//            mLinkButton.setBackground(ContextCompat.getDrawable(this, R.drawable.media_tab_button_center));
//
//            mFileButton.setTextColor(Color.WHITE);
//            mFileButton.setBackground(ContextCompat.getDrawable(this, R.drawable.media_tab_button_right_clicked));
//
//            setAdapterData(fileEvents, sectionAdapter);
//        });

        Intent intent = getIntent();
        if (intent != null) {
            String userId = intent.getStringExtra("userId");
            String roomId = intent.getStringExtra("roomId");
            mSession = Matrix.getInstance(getApplicationContext()).getDefaultSession();
            if (userId != null) {
//                if (roomId == null) {
                final List<Room> mDirectChats = RoomUtils.getAllRooms(mSession, this);
                for (Room room : mDirectChats) {
//                    if (room.getMember(userId) != null) {
                    getAllRoomMedia(room);
//                    }
                }
//                } else {
//                    mRoom = mSession.getDataHandler().getRoom(roomId);
//                    getAllRoomMedia(mRoom);
//                }
            } else if (roomId != null) {
                mRoom = mSession.getDataHandler().getRoom(roomId);
                getAllRoomMedia(mRoom);
            }

            Collections.sort(mediaEvents, (obj1, obj2) -> Long.valueOf(obj2.getOriginServerTs()).compareTo(obj1.getOriginServerTs()));
//            Collections.sort(fileEvents, (obj1, obj2) -> Long.valueOf(obj2.getOriginServerTs()).compareTo(obj1.getOriginServerTs()));
//            Collections.sort(linkEvents, (obj1, obj2) -> Long.valueOf(obj2.getOriginServerTs()).compareTo(obj1.getOriginServerTs()));

            mRecyclerView = (RecyclerView) findViewById(R.id.media_recycler_view);
            int numberOfColumns = 4;
            mLayoutManager = new GridLayoutManager(this, numberOfColumns);
            mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    switch (sectionAdapter.getSectionItemViewType(position)) {
                        case SectionedRecyclerViewAdapter.VIEW_TYPE_HEADER:
                            return 4;
                        default:
                            return 1;
                    }
                }
            });
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setAdapter(sectionAdapter);
            setAdapterData(mediaEvents, sectionAdapter);

        }

    }

    private void getAllRoomMedia(Room room) {
        List<Event> events = room.getAllRoomMessages();
        if (events!=null&&events.size() > 0)
            for (Event event : events) {
                Message message = JsonUtils.toMessage(event.getContent());
                if ((Message.MSGTYPE_IMAGE.equals(message.msgtype) ||
                        Message.MSGTYPE_VIDEO.equals(message.msgtype) ||
                        Message.MSGTYPE_AUDIO.equals(message.msgtype)) && !event.isHidden()) {
                    mediaEvents.add(event);
                }
            }
    }

    private void setAdapterData(ArrayList<Event> events, SectionedRecyclerViewAdapter sectionAdapter) {
        sectionAdapter.removeAllSections();
        String monthYear = null;
        ArrayList<Event> eventsForMonthYear = new ArrayList<>();

        for (Event event : events) {
            String eventMonthYear = AdapterUtils.convertDateToString(event.getOriginServerTs(), "LLLL yyyy", this);
            if (!eventMonthYear.equals(monthYear)) {
                if (monthYear != null) {
                    sectionAdapter.addSection(new MediaMonthSection(monthYear, (List<Event>) eventsForMonthYear.clone(), this, mSession));
                    eventsForMonthYear.clear();
                }
                monthYear = eventMonthYear;
                eventsForMonthYear.add(event);
            } else {
                eventsForMonthYear.add(event);
            }
        }
        if (eventsForMonthYear.size() > 0) {
            sectionAdapter.addSection(new MediaMonthSection(monthYear, eventsForMonthYear, this, mSession));
        }
        sectionAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
