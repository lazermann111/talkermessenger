package com.talkermessenger.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.iskar.talkersdk.ContactHandler;
import com.iskar.talkersdk.UserPublicFieldsHandler;
import com.iskar.talkersdk.model.TalkerContact;
import com.talkermessenger.Matrix;
import com.talkermessenger.R;
import com.talkermessenger.adapters.PhoneNumbersAdapter;
import com.talkermessenger.util.VectorUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmList;

import static com.talkermessenger.activity.VectorMemberDetailsActivity.EXTRA_MEMBER_ID;

public class VectorMemberPhoneNumberActivity extends AppCompatActivity {
    @BindView(R.id.active_phone_number)
    TextView activeNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vector_member_phone_number);
        ButterKnife.bind(this);

        setSupportActionBar(findViewById(R.id.account_toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Intent intent = getIntent();
        String matrixId = intent.getStringExtra(EXTRA_MEMBER_ID);
        if (matrixId != null) {
            getSupportActionBar().setTitle(VectorUtils.getUserDisplayName(matrixId, Matrix.getInstance(getApplicationContext()).getDefaultSession(), this));
            ContactHandler contactHandler = new ContactHandler();
            TalkerContact contact = contactHandler.getContactFromTalkerContactSnapshot(matrixId, this);
            if (contact != null) {
                RealmList<String> phoneNumbers = contact.getPhoneNumbers();
                setNumber(phoneNumbers.get(0));
                ArrayList<String> additionalPhoneNumbers = new ArrayList<>();
                for (String phoneNumber : phoneNumbers) {
                    if (!TextUtils.equals(phoneNumbers.get(0), phoneNumber)) {
                        additionalPhoneNumbers.add(phoneNumber);
                    }
                }
                if (additionalPhoneNumbers.size() > 0) {
                    TextView additionalNumberTitle = findViewById(R.id.additional_phone_number_section_title);
                    additionalNumberTitle.setVisibility(View.VISIBLE);
                    RecyclerView recyclerView = findViewById(R.id.additional_phone_number_section_list);
                    PhoneNumbersAdapter phoneNumbersAdapter = new PhoneNumbersAdapter(additionalPhoneNumbers);
                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(VectorMemberPhoneNumberActivity.this);
                    recyclerView.setLayoutManager(layoutManager);
                    recyclerView.setAdapter(phoneNumbersAdapter);
                }
            } else {
                String phone = new UserPublicFieldsHandler(Matrix.getInstance(this).getDefaultSession().getCredentials().accessToken, this)
                        .getUserNumber(matrixId, this::setNumber);
                if (phone != null) {
                    setNumber(phone);
                }
            }
        }
    }

    private void setNumber(String phone) {
        try {
            PhoneNumberUtil pnu = PhoneNumberUtil.getInstance();
            Phonenumber.PhoneNumber pn = pnu.parse(phone, null);
            String phoneNumber = PhoneNumberUtil.getInstance().format(pn,
                    PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL);
            activeNumber.setText(phoneNumber);
        } catch (Exception e) {
            e.printStackTrace();
            activeNumber.setText(phone);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
