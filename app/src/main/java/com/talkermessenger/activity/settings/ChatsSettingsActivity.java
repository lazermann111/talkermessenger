package com.talkermessenger.activity.settings;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.talkermessenger.Matrix;
import com.talkermessenger.R;
import com.talkermessenger.activity.CommonActivityUtils;
import com.talkermessenger.backup.BackupHelper;
import com.talkermessenger.backup.DropboxAsyncResponse;
import com.talkermessenger.util.RoomUtils;

import org.matrix.androidsdk.MXSession;
import org.matrix.androidsdk.data.Room;
import org.matrix.androidsdk.data.RoomTag;
import org.matrix.androidsdk.rest.callback.ApiCallback;
import org.matrix.androidsdk.rest.model.Event;
import org.matrix.androidsdk.rest.model.MatrixError;
import org.matrix.androidsdk.rest.model.Message;
import org.matrix.androidsdk.util.JsonUtils;
import org.matrix.androidsdk.util.Log;

import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dev_serhii on 25.01.2018.
 */

public class ChatsSettingsActivity extends AppCompatActivity implements View.OnClickListener, DropboxAsyncResponse {

    private static final String LOG_TAG = "ChatsSettingsActivity";
    private static final int VECTOR_ROOM_ADMIN_LEVEL = 100;

    @BindView(R.id.account_toolbar)
    Toolbar mToolbar;

    @BindView(R.id.media_settings_click)
    RelativeLayout mMediaSettings;

    @BindView(R.id.statistics_settings_click)
    RelativeLayout mStatisticsSettings;

    @BindView(R.id.notification_settings_click)
    RelativeLayout mNotificationSettings;

    @BindView(R.id.translation_settings_click)
    RelativeLayout mTranslationSettings;

    @BindView(R.id.design_settings_click)
    RelativeLayout mDesignSettings;

    @BindView(R.id.archived_chats_settings_click)
    RelativeLayout mArchivedChatsSetting;

    @BindView(R.id.chat_backup_settings_click)
    RelativeLayout mBackUpChatsSetting;

    @BindView(R.id.archive_chats_settings_click)
    RelativeLayout mArchiveAllChatsSetting;

    @BindView(R.id.favorites_chats_settings_click)
    RelativeLayout mFavoritesChatsSetting;

    @BindView(R.id.clean_chats_settings_click)
    RelativeLayout mCleanChatsSettings;

    @BindView(R.id.remove_chats_settings_click)
    RelativeLayout mRemoveChatsSettings;

    @BindView(R.id.chat_restore_settings_click)
    RelativeLayout mRestoreSettingsClick;

    private MXSession mSession;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_settings_chats);

        ButterKnife.bind(this);

        mSession = Matrix.getInstance(ChatsSettingsActivity.this).getDefaultSession();

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        setupOnClickListeners();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.media_settings_click:
                startActivity(new Intent(this, MediaSettingsActivity.class));
                break;
            case R.id.statistics_settings_click:
                startActivity(new Intent(this, StatisticsSettingsActivity.class));
                break;
            case R.id.notification_settings_click:
                startActivity(new Intent(this, NotificationSettingsActivity.class));
                break;
            case R.id.archived_chats_settings_click:
                startActivity(new Intent(this, ArchivedChatsActivity.class));
                break;
            case R.id.favorites_chats_settings_click:
                startActivity(new Intent(this, FavouriteChatsActivity.class));
                break;
            case R.id.chat_backup_settings_click:
                backupSettings();
                break;
            case R.id.chat_restore_settings_click:
                restoreSettings();
                break;
            case R.id.archive_chats_settings_click:
                archiveAllChats(true);
                break;
            case R.id.clean_chats_settings_click:
                cleanAllChats();
                break;
            case R.id.remove_chats_settings_click:
                deleteAllChats();
                break;
            case R.id.design_settings_click:
                startActivity(new Intent(this, ChatDetailDesignActivity.class));
                break;
            case R.id.translation_settings_click:
                startActivity(new Intent(this, TranslationSettingsActivity.class));
                break;
        }
    }

    private void setupOnClickListeners() {
        mArchivedChatsSetting.setOnClickListener(this);
        mFavoritesChatsSetting.setOnClickListener(this);
        mArchiveAllChatsSetting.setOnClickListener(this);
        mBackUpChatsSetting.setOnClickListener(this);
        mCleanChatsSettings.setOnClickListener(this);
        mRemoveChatsSettings.setOnClickListener(this);
        mMediaSettings.setOnClickListener(this);
        mStatisticsSettings.setOnClickListener(this);
        mNotificationSettings.setOnClickListener(this);
        mRestoreSettingsClick.setOnClickListener(this);
        mDesignSettings.setOnClickListener(this);
        mTranslationSettings.setOnClickListener(this);
    }

    private void cleanAllChats() {
        int hiddenCount = 0;
        if ((null == mSession) || (null == mSession.getDataHandler())) {
            Log.e(LOG_TAG, "## archiveAllChats() : null session");
        }

        final List<Room> mDirectChats = getAllRooms();
        for (Room room : mDirectChats) {
            /*room.updateUserPowerLevels(mSession.getMyUserId(), VECTOR_ROOM_ADMIN_LEVEL, new ApiCallback<Void>() {
                @Override
                public void onSuccess(Void info) {
                    Toast.makeText(ChatsSettingsActivity.this,
                            "Admin permission granted!", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onNetworkError(Exception e) {
                    Toast.makeText(ChatsSettingsActivity.this,
                            "Admin permission failed!", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onMatrixError(MatrixError e) {
                }

                @Override
                public void onUnexpectedError(Exception e) {
                }
            });*/
            List<Event> events = room.getAllRoomMessages();
            for (Event event : events) {
                if (!event.isHidden()) {
                    event.hide(true);
                    hiddenCount++;
                }
                try {
                    String eventType = event.getType();
                    if (Event.EVENT_TYPE_MESSAGE.equals(eventType)) {
                        Message message = JsonUtils.toMessage(event.getContent());
                        if (message.msgtype != null)
                            Toast.makeText(this, message.msgtype, Toast.LENGTH_SHORT).show();
                        if (message.msgtype.equals(Message.MSGTYPE_TEXT) ||
                                message.msgtype.equals(Message.MSGTYPE_AUDIO) ||
                                message.msgtype.equals(Message.MSGTYPE_FILE) ||
                                message.msgtype.equals(Message.MSGTYPE_VIDEO)) {
                            room.redact(event.eventId, new ApiCallback<Event>() {
                                @Override
                                public void onSuccess(final Event redactedEvent) {
//                                    if (null != redactedEvent) {
//                                    Toast.makeText(ChatsSettingsActivity.this,
//                                            "success removed: name: " + name + " text: " + message.body, Toast.LENGTH_SHORT).show();
//                                    }
                                }

                                private void onError(String message) {
//                                    event.hide(true);
                                    Toast.makeText(ChatsSettingsActivity.this,
                                            getString(R.string.could_not_redact) + ": " + message + getString(R.string.settings_chat_hidden),
                                            Toast.LENGTH_SHORT).show();

                                }

                                @Override
                                public void onNetworkError(Exception e) {
                                    onError(e.getMessage());
                                }

                                @Override
                                public void onMatrixError(MatrixError e) {
                                    onError(e.getMessage());
                                }

                                @Override
                                public void onUnexpectedError(Exception e) {
                                    onError(e.getMessage());
                                }
                            });
                        }
                    }
                } catch (NullPointerException ignore) {
                }
            }
        }
        Toast.makeText(this, "hidden: " + hiddenCount, Toast.LENGTH_SHORT).show();
    }

    private void backupSettings() {
        CommonActivityUtils.checkPermissions(CommonActivityUtils.REQUEST_CODE_PERMISSION_HOME_ACTIVITY, this);
        BackupHelper backupHelper = new BackupHelper(mSession.getMyUserId(), this);
        backupHelper.backupAllExisting(this);
    }

    private void restoreSettings() {
        CommonActivityUtils.checkPermissions(CommonActivityUtils.REQUEST_CODE_PERMISSION_HOME_ACTIVITY, this);
        BackupHelper backupHelper = new BackupHelper(mSession.getMyUserId(), this);
        backupHelper.restorePreferences(this);
    }

    private void deleteAllChats() {
        if ((null == mSession) || (null == mSession.getDataHandler())) {
            Log.e(LOG_TAG, "## archiveAllChats() : null session");
        }

        final List<Room> mDirectChats = getAllRooms();
        for (Room room : mDirectChats) {
            final Set<String> tags = room.getAccountData().getKeys();
            if ((null == tags) || !tags.contains(RoomTag.ROOM_TAG_LOW_PRIORITY)) {
                final boolean isArchived = tags != null && tags.contains(RoomTag.ROOM_TAG_ARCHIVED);
                if (!isArchived) {
                    RoomUtils.updateRoomTag(mSession, room.getRoomId(), null, RoomTag.ROOM_TAG_DELETED, new ApiCallback<Void>() {
                        @Override
                        public void onSuccess(Void info) {
                            Log.e("deleted from delete all", "ok");
                        }

                        @Override
                        public void onNetworkError(Exception e) {

                        }

                        @Override
                        public void onMatrixError(MatrixError e) {

                        }

                        @Override
                        public void onUnexpectedError(Exception e) {

                        }
                    });
                }
            }
        }
    }

    private void archiveAllChats(boolean action) {
        if ((null == mSession) || (null == mSession.getDataHandler())) {
            Log.e(LOG_TAG, "## archiveAllChats() : null session");
        }

        final List<Room> mDirectChats = getAllRooms();
        for (Room room : mDirectChats) {
            final Set<String> tags = room.getAccountData().getKeys();
            if ((null == tags) || !tags.contains(RoomTag.ROOM_TAG_LOW_PRIORITY)) {
                final boolean isArchived = tags != null && tags.contains(RoomTag.ROOM_TAG_ARCHIVED);
                final boolean isDeleted = tags != null && tags.contains(RoomTag.ROOM_TAG_DELETED);
                if (!isArchived && !isDeleted && action)
                    archiveChat(room, RoomTag.ROOM_TAG_ARCHIVED);
                else if (isArchived && !action)
                    archiveChat(room, null);
            }
        }

        Toast.makeText(this, (action ? getString(R.string.chat_settings_archiving) : getString(R.string.chat_settings_backup)) + getString(R.string.chat_settings_completed), Toast.LENGTH_LONG).show();

    }

    /**
     * Fill the direct chats mAdapter with data
     */
    private List<Room> getAllRooms() {
        return RoomUtils.getAllRooms(mSession, this);
    }

    private void archiveChat(Room room, String newTag) {
        RoomUtils.updateRoomTag(mSession,
                room.getRoomId(), null, newTag, new ApiCallback<Void>() {
                    @Override
                    public void onSuccess(Void info) {
                    }

                    @Override
                    public void onNetworkError(Exception e) {
                        Toast.makeText(ChatsSettingsActivity.this,
                                R.string.chat_settings_network_error, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onMatrixError(MatrixError e) {
                        Toast.makeText(ChatsSettingsActivity.this,
                                R.string.chat_settings_matrix_error, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onUnexpectedError(Exception e) {
                        Toast.makeText(ChatsSettingsActivity.this,
                                R.string.chat_settings_error, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void processFinish(String path) {
        if (path != null) {
            Toast.makeText(this, R.string.chat_settings_upload_success, Toast.LENGTH_LONG).show();
        }
    }
}
