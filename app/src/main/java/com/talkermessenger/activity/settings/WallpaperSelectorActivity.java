package com.talkermessenger.activity.settings;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.talkermessenger.R;
import com.talkermessenger.adapters.WallpaperChooserAdapter;
import com.talkermessenger.util.PreferencesManager;

import org.matrix.androidsdk.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WallpaperSelectorActivity extends AppCompatActivity implements WallpaperChooserAdapter.ItemClickListener {
    private static final int PICK_IMAGE = 1111;
    @BindView(R.id.photo_library_click)
    RelativeLayout photoLibraryClick;

    @BindView(R.id.wallpaper_recycler)
    RecyclerView wallpaperRecycler;

    @BindView(R.id.account_toolbar)
    Toolbar mToolbar;

    WallpaperChooserAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_wallpaper_selector);
        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        copyAssets();

        photoLibraryClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, getString(R.string.select_picture)), PICK_IMAGE);
            }
        });
    }

    private void setupRecycler(List<String> wallpaperList) {
        // set up the RecyclerView
        int numberOfColumns = 3;
        wallpaperRecycler.setLayoutManager(new GridLayoutManager(this, numberOfColumns));
        mAdapter = new WallpaperChooserAdapter(this, wallpaperList);
        mAdapter.setClickListener(this);
        wallpaperRecycler.setAdapter(mAdapter);
    }

    @Override
    public void onItemClick(View view, int position) {
        Log.e("wallpaper clicked", "You clicked number " + mAdapter.getItem(position) + ", which is at cell position " + position);
        Uri pictureUri = Uri.parse(mAdapter.getItem(position));
        if (null != pictureUri) {
            PreferencesManager.setBackgroundChatPicture(this, pictureUri.toString());
            Toast.makeText(this, pictureUri == null ? "smth wrong" : "success", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private List<String> getWallpaperList() {
        List<String> paths = new ArrayList<String>();
        File directory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/im_wallpapers/");
        Log.e("directory path", Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/im_wallpapers");
        File[] files = directory.listFiles();
        if (files != null && files.length > 0)
            for (File file : files) {
                if (file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf('/')).contains("wallpaper")) {
                    paths.add(file.getAbsolutePath());
                    Log.e("wallpaper_Added", file.getAbsolutePath());
                }
            }
        return paths;
    }

    @SuppressLint("StaticFieldLeak")
    private void copyAssets() {
        new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                AssetManager assetManager = getAssets();
                String[] files = null;
                try {
                    File directory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/im_wallpapers/");
                    directory.mkdirs();
                    File noMedia = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/im_wallpapers/.nomedia");
                    try {
                        if (!noMedia.exists())
                            noMedia.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    files = assetManager.list("wallpapers");
                } catch (IOException e) {
                    Log.e("tag", "Failed to get asset file list.", e);
                }
                if (files != null) for (String filename : files) {
                    InputStream in = null;
                    OutputStream out = null;
                    try {
                        in = assetManager.open("wallpapers/" + filename);
                        File outFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/im_wallpapers/", filename);
                        out = new FileOutputStream(outFile);
                        copyFile(in, out);
                    } catch (IOException e) {
                        Log.e("tag", "Failed to copy asset file: " + filename, e);
                    } finally {
                        if (in != null) {
                            try {
                                in.close();
                            } catch (IOException e) {
                                // NOOP
                            }
                        }
                        if (out != null) {
                            try {
                                out.close();
                            } catch (IOException e) {
                                // NOOP
                            }
                        }
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                setupRecycler(getWallpaperList());
            }
        }.execute();
    }

    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PICK_IMAGE:
                    Uri pictureUri = data.getData();
                    Toast.makeText(this, pictureUri == null ? "smth wrong" : "success", Toast.LENGTH_SHORT).show();
                    if (null != pictureUri) {
                        PreferencesManager.setBackgroundChatPicture(this, pictureUri.toString());
                    }
                    finish();
                    break;
            }
        }
    }
}
