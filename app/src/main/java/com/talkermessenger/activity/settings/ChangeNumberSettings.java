package com.talkermessenger.activity.settings;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.iskar.talkersdk.LoginHandler;
import com.iskar.talkersdk.callback.MatrixLoginInterface;
import com.iskar.talkersdk.model.MatrixCredentials;
import com.iskar.talkersdk.model.PhoneBadRequest;
import com.talkermessenger.Matrix;
import com.talkermessenger.R;
import com.talkermessenger.activity.CountryPickerActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Serhii on 12.02.2018.
 */

public class ChangeNumberSettings extends AppCompatActivity implements View.OnClickListener, MatrixLoginInterface {

    private static final int REQUEST_OLD_COUNTRY = 5678;
    private static final int REQUEST_NEW_COUNTRY = 5679;
    @BindView(R.id.account_toolbar)
    Toolbar mToolbar;
    @BindView(R.id.old_phone_number)
    EditText mOldPhoneNumber;
    @BindView(R.id.new_phone_number)
    EditText mNewPhoneNumber;
    @BindView(R.id.new_country_code)
    TextView mNewCountryCode;
    @BindView(R.id.old_country_code)
    TextView mOldCountryCode;
    private LoginHandler handler = new LoginHandler(this);

//    private KeyListener originalKeyListener1;
//    private KeyListener originalKeyListener2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_change_number);

        ButterKnife.bind(this);

        mOldPhoneNumber.requestFocus();

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        setupOnClickListeners();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_next, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.save:
                if (mNewPhoneNumber.getText().toString().length() > 0) {
                    handler.requestCodeForPhone(
                            Matrix.getInstance(this).getDefaultSession().getMyUserId(),
                            Matrix.getInstance(this).getDefaultSession().getCredentials().accessToken,
                            mNewCountryCode.getText().toString() + mNewPhoneNumber.getText().toString());
                } else {
                    showText(getString(R.string.empty_number));
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        String tag = v.getTag().toString();

        switch (v.getTag().toString()) {
            case "remove":
                if (mOldPhoneNumber.hasFocus()) {
                    String oldNum = mOldPhoneNumber.getText().toString();
                    if (!oldNum.isEmpty())
                        mOldPhoneNumber.setText(oldNum.substring(0, oldNum.length() - 1));
                } else {
                    String newNum = mNewPhoneNumber.getText().toString();
                    if (!newNum.isEmpty())
                        mNewPhoneNumber.setText(newNum.substring(0, newNum.length() - 1));
                }
                break;
            default:
                if (mOldPhoneNumber.hasFocus()) {
                    String oldNum = mOldPhoneNumber.getText().toString() + tag;
                    mOldPhoneNumber.setText(oldNum);
                } else {
                    String newNum = mNewPhoneNumber.getText().toString() + tag;
                    mNewPhoneNumber.setText(newNum);
                }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_NEW_COUNTRY:
                    if (data != null && data.hasExtra(CountryPickerActivity.EXTRA_OUT_CALLING_CODE))
                        mNewCountryCode.setText(data.getStringExtra(CountryPickerActivity.EXTRA_OUT_CALLING_CODE));
                    break;
                case REQUEST_OLD_COUNTRY:
                    if (data != null && data.hasExtra(CountryPickerActivity.EXTRA_OUT_CALLING_CODE))
                        mOldCountryCode.setText(data.getStringExtra(CountryPickerActivity.EXTRA_OUT_CALLING_CODE));
                    break;
            }
        }
    }

    private void setupOnClickListeners() {

        mNewCountryCode.setOnClickListener(v -> {
            final Intent intent = CountryPickerActivity.getIntent(ChangeNumberSettings.this, true);
            startActivityForResult(intent, REQUEST_NEW_COUNTRY);
        });
        mOldCountryCode.setOnClickListener(v -> {
            final Intent intent = CountryPickerActivity.getIntent(ChangeNumberSettings.this, true);
            startActivityForResult(intent, REQUEST_OLD_COUNTRY);
        });
    }

    private void showText(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getCredentialsSuccess(MatrixCredentials matrixCredentials) {

    }

    @Override
    public void getCodeSuccess() {
        Intent intent = new Intent(ChangeNumberSettings.this, EnterCodeActivity.class);
        String number = mNewCountryCode.getText().toString() + mNewPhoneNumber.getText().toString();
        intent.putExtra(EnterCodeActivity.PHONE_NUMBER, number);
        startActivity(intent);
        finish();
    }

    @Override
    public void tooManyAttempts(PhoneBadRequest phoneBadRequest) {

    }

    @Override
    public void changePhoneSuccess(String newPhone) {

    }

    @Override
    public void onFailure(String msg) {
        showText(getString(R.string.number_taken));
    }
}
