package com.talkermessenger.activity.settings;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.transition.Transition;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.talkermessenger.BuildConfig;
import com.talkermessenger.R;
import com.talkermessenger.util.PreferencesManager;
import com.talkermessenger.util.ThemeUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SecurityActivity extends AppCompatActivity {

    @BindView(R.id.security_toolbar)
    Toolbar toolbar;

    @BindView(R.id.security_click)
    RelativeLayout securityClick;

    @BindView(R.id.set_password)
    TextView setPassword;

    @BindView(R.id.reset_password)
    TextView resetPassword;

    @BindView(R.id.reset_password_section)
    LinearLayout resetPasswordSection;

    @Nullable
    @BindView(R.id.for_transition)
    ImageView transition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_security);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

      /* this part of code - workable
      setPassword.setOnClickListener(v -> {
            Intent passCodeIntent = new Intent(this, EnterPassCodeActivity.class);
            passCodeIntent.putExtra(EnterPassCodeActivity.EXTRA_IS_SET_MODE, true);
            startActivity(passCodeIntent);
        });*/

        resetPassword.setOnClickListener(v -> {
            PreferencesManager.setPassword(this, "");
            checkPassword();
        });

        checkPassword();

        securityClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.parse("package:" + BuildConfig.APPLICATION_ID)));
            }
        });


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ThemeUtils.setTransitionImage(this, "security_settings", transition);

            if (!getIntent().getBooleanExtra(SmartSettingsActivity.IS_SHARED_ELEMENT_TRANSITION_ENABLED, false))
                findViewById(R.id.hide_transition).setVisibility(View.GONE);

            getWindow().getSharedElementEnterTransition().addListener(new Transition.TransitionListener() {
                @Override
                public void onTransitionStart(Transition transition) {

                }

                @Override
                public void onTransitionEnd(Transition transition) {
                    findViewById(R.id.hide_transition).setVisibility(View.INVISIBLE);
                }

                @Override
                public void onTransitionCancel(Transition transition) {

                }

                @Override
                public void onTransitionPause(Transition transition) {

                }

                @Override
                public void onTransitionResume(Transition transition) {

                }
            });
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        checkPassword();
    }

    @Override
    protected void onStart() {
        super.onStart();
        checkPassword();
    }

    private void checkPassword() {
        if (TextUtils.isEmpty(PreferencesManager.getPassword(this)))
            resetPasswordSection.setVisibility(View.GONE);
        else
            resetPasswordSection.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        if (getIntent().getBooleanExtra(SmartSettingsActivity.IS_SHARED_ELEMENT_TRANSITION_ENABLED, false))
            findViewById(R.id.hide_transition).setVisibility(View.VISIBLE);
        supportFinishAfterTransition();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (getIntent().getBooleanExtra(SmartSettingsActivity.IS_SHARED_ELEMENT_TRANSITION_ENABLED, false))
                    findViewById(R.id.hide_transition).setVisibility(View.VISIBLE);
                supportFinishAfterTransition();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
