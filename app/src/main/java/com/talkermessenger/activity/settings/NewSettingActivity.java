package com.talkermessenger.activity.settings;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.talkermessenger.R;
import com.talkermessenger.activity.CommonActivityUtils;
import com.talkermessenger.util.ThemeUtils;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Nikita on 16.04.2018.
 */

public class NewSettingActivity extends AppCompatActivity {

    @OnClick(R.id.design)
    void design(){
        startActivity(ChatDetailDesignActivity.class);
    }

    @OnClick(R.id.purchases)
    void purchase(){
       // startActivity(PurchasesSettingsActivity.class);
    }

    @OnClick(R.id.dnd)
    void dnd(){
        startActivity(DoNotDisturbSettingsActivity.class);
    }

    @OnClick(R.id.medeia)
    void media(){
        startActivity(MediaSettingsActivity.class);
    }

    @OnClick(R.id.profile)
    void profile(){
        startActivity(AccountActivity.class);
    }

    @OnClick(R.id.privacy)
    void privacy(){
        startActivity(ConfidentialActivity.class);
    }

    @OnClick(R.id.notifications)
    void notifications(){
        startActivity(NotificationSettingsActivity.class);
    }

    @OnClick(R.id.translate)
    void translate(){startActivity(TranslationSettingsActivity.class);}

    @OnClick(R.id.security)
    void security(){startActivity(SecurityActivity.class);}

    @OnClick(R.id.about)
    void about(){
        startActivity(AboutActivity.class);
    }

    @OnClick(R.id.account)
    void account(){
        startActivity(ExtraSettingsActivity.class);
    }

    @OnClick(R.id.blocked)
    void blocked(){
        startActivity(BlockedListSettings.class);
    }

    @BindView(R.id.im1)
    AppCompatImageView im1;
    @BindView(R.id.im2)
    AppCompatImageView im2;
    @BindView(R.id.im3)
    AppCompatImageView im3;
    @BindView(R.id.im4)
    AppCompatImageView im4;
    @BindView(R.id.im5)
    AppCompatImageView im5;
    @BindView(R.id.im6)
    AppCompatImageView im6;
    @BindView(R.id.im7)
    AppCompatImageView im7;
    @BindView(R.id.im8)
    AppCompatImageView im8;
    @BindView(R.id.im9)
    AppCompatImageView im9;
    @BindView(R.id.im0)
    AppCompatImageView im0;
    @BindView(R.id.im11)
    AppCompatImageView im11;
    @BindView(R.id.im12)
    AppCompatImageView im12;
    @BindView(R.id.settings_toolbar)
    Toolbar toolbar;

    @BindColor(R.color.list_divider_color_dark)
    int color;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_settings);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
     //   setImagesTint();
    }

    private void startActivity(Class c){
        startActivity(new Intent(this, c));
    }

    private void setImagesTint(){
        im1.setColorFilter(color);
        im2.setColorFilter(color);
        im3.setColorFilter(color);
        im4.setColorFilter(color);
        im5.setColorFilter(color);
        im6.setColorFilter(color);
        im7.setColorFilter(color);
        im8.setColorFilter(color);
        im9.setColorFilter(color);
        im0.setColorFilter(color);
        im11.setColorFilter(color);
        im12.setColorFilter(color);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.settings, menu);
        CommonActivityUtils.tintMenuIcons(menu, ThemeUtils.getColor(this, R.attr.actionbar_text_color));
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            menu.findItem(R.id.action_open_smart_settings).setVisible(false);
        }
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_open_smart_settings:
                startActivity(new Intent(this, SmartSettingsActivity.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
