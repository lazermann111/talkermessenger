package com.talkermessenger.activity.settings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.talkermessenger.R;
import com.talkermessenger.activity.CommonActivityUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Serhii on 12.02.2018.
 */

public class DeleteNumberSettings extends AppCompatActivity {

    private static final int REQUEST_COUNTRY_CODE = 5680;

    @BindView(R.id.account_toolbar)
    Toolbar mToolbar;

    @OnClick(R.id.delete_account_card)
    void deleteAccount() {
        showDeleteDialog();
    }
//    private KeyListener originalKeyListener1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_delete_number);

        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

    }

    private void showDeleteDialog() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.are_you_sure)
                .setPositiveButton(getString(R.string.yes), (dialog, which) -> {
                    CommonActivityUtils.logout(DeleteNumberSettings.this, false);
                    finish();
                })
                .setNegativeButton(getString(R.string.no), (dialog, which) -> dialog.dismiss())
                .show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void showText(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }
}
