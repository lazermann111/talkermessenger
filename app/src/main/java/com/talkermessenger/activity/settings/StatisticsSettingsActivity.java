package com.talkermessenger.activity.settings;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.talkermessenger.Matrix;
import com.talkermessenger.R;
import com.talkermessenger.util.PreferencesManager;
import com.talkermessenger.util.VectorUtils;

import org.matrix.androidsdk.MXSession;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dev_serhii on 25.01.2018.
 */

public class StatisticsSettingsActivity extends AppCompatActivity {

    @BindView(R.id.account_toolbar)
    Toolbar mToolbar;

    @BindView(R.id.all_messages_incoming_count)
    TextView incomeAll;
    @BindView(R.id.all_messages_outgoing_count)
    TextView outgoingAll;
    @BindView(R.id.text_incoming_count)
    TextView incomeText;
    @BindView(R.id.text_outgoing_count)
    TextView outgoingText;
    @BindView(R.id.incoming_calls_count)
    TextView incomeCall;
    @BindView(R.id.outgoing_calls_count)
    TextView outCall;
    @BindView(R.id.incoming_photos_count)
    TextView incomePhotos;
    @BindView(R.id.outgoing_photos_count)
    TextView outPhotos;
    @BindView(R.id.incoming_videos_count)
    TextView inVideos;
    @BindView(R.id.outgoing_videos_count)
    TextView outVideo;
    @BindView(R.id.incoming_audios_count)
    TextView inAudio;
    @BindView(R.id.outgoing_audios_count)
    TextView outAudio;


    private MXSession session;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_settings_statistics);

        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        session = Matrix.getInstance(this).getDefaultSession();

        Intent intent = getIntent();
        if (intent != null) {
            String userId = intent.getStringExtra("userId");
            String roomId = intent.getStringExtra("roomId");
            if (userId == null && roomId == null) {
                incomeAll.setText(PreferencesManager.getAllIncomeMessages(this) + "");
                outgoingAll.setText(PreferencesManager.getAllOutMessages(this) + "");
                incomeText.setText(PreferencesManager.getIncomeTextMessage(this) + "");
                outgoingText.setText(PreferencesManager.getOutTextMessage(this) + "");
                incomeCall.setText(PreferencesManager.getIncomeCall(this) / 60000L + "");
                outCall.setText(PreferencesManager.getOutCall(this) / 60000L + "");
                incomePhotos.setText(PreferencesManager.getIncomeImage(this) + "");
                outPhotos.setText(PreferencesManager.getOutImage(this) + "");
                inVideos.setText(PreferencesManager.getIncomeVideo(this) + "");
                outVideo.setText(PreferencesManager.getOutVideo(this) + "");
                inAudio.setText(PreferencesManager.getIncomeAudio(this) + "");
                outAudio.setText(PreferencesManager.getOutAudio(this) + "");
            } else if(roomId != null){
                getSupportActionBar().setTitle(VectorUtils.getRoomDisplayName(this, session, session.getDataHandler().getRoom(roomId)));
                incomeAll.setText(PreferencesManager.getAllIncomeMessagesForGroup(this, roomId) + "");
                outgoingAll.setText(PreferencesManager.getAllOutMessagesForGroup(this, roomId) + "");
                incomeText.setText(PreferencesManager.getIncomeTextMessageForGroup(this, roomId) + "");
                outgoingText.setText(PreferencesManager.getOutTextMessageForGroup(this, roomId) + "");
                incomeCall.setText(PreferencesManager.getIncomeCallForGroup(this, roomId) / 60000L + "");
                outCall.setText(PreferencesManager.getOutCallForGroup(this, roomId) / 60000L + "");
                incomePhotos.setText(PreferencesManager.getIncomeImageForGroup(this, roomId) + "");
                outPhotos.setText(PreferencesManager.getOutImageForGroup(this, roomId) + "");
                inVideos.setText(PreferencesManager.getIncomeVideoForGroup(this, roomId) + "");
                outVideo.setText(PreferencesManager.getOutVideoForGroup(this, roomId) + "");
                inAudio.setText(PreferencesManager.getIncomeAudioForGroup(this, roomId) + "");
                outAudio.setText(PreferencesManager.getOutAudioForGroup(this, roomId) + "");
            } else {
                getSupportActionBar().setTitle(VectorUtils.getUserDisplayName(userId,session,this));
                incomeAll.setText(PreferencesManager.getAllIncomeMessagesForUser(this, userId) + "");
                outgoingAll.setText(PreferencesManager.getAllOutMessagesForUser(this, userId) + "");
                incomeText.setText(PreferencesManager.getIncomeTextMessageForUser(this, userId) + "");
                outgoingText.setText(PreferencesManager.getOutTextMessageForUser(this, userId) + "");
                incomeCall.setText(PreferencesManager.getIncomeCallForUser(this, userId) / 60000L + "");
                outCall.setText(PreferencesManager.getOutCallForUser(this, userId) / 60000L + "");
                incomePhotos.setText(PreferencesManager.getIncomeImageForUser(this, userId) + "");
                outPhotos.setText(PreferencesManager.getOutImageForUser(this, userId) + "");
                inVideos.setText(PreferencesManager.getIncomeVideoForUser(this, userId) + "");
                outVideo.setText(PreferencesManager.getOutVideoForUser(this, userId) + "");
                inAudio.setText(PreferencesManager.getIncomeAudioForUser(this, userId) + "");
                outAudio.setText(PreferencesManager.getOutAudioForUser(this, userId) + "");
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showText(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }
}
