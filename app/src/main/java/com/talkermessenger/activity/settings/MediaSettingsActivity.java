package com.talkermessenger.activity.settings;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.transition.Transition;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.talkermessenger.Matrix;
import com.talkermessenger.R;
import com.talkermessenger.util.DownloadMethod;
import com.talkermessenger.util.PreferencesManager;
import com.talkermessenger.util.ThemeUtils;

import org.matrix.androidsdk.MXSession;
import org.matrix.androidsdk.db.MXMediasCache;
import org.matrix.androidsdk.rest.callback.SimpleApiCallback;
import org.matrix.androidsdk.rest.model.MatrixError;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by dev_serhii on 25.01.2018.
 */

public class MediaSettingsActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.account_toolbar)
    Toolbar mToolbar;

    @BindView(R.id.view_all_media_settings_click)
    RelativeLayout mViewAllMediaSettings;

    @BindView(R.id.save_media_mess_switcher)
    SwitchCompat mSaveMediaMessageSwitcher;

    @BindView(R.id.save_media_group_switcher)
    SwitchCompat mSaveMediaGroupSwitcher;

    @BindView(R.id.photos_settings_click)
    RelativeLayout mPhotoMediaSettings;

    @BindView(R.id.audio_settings_click)
    RelativeLayout mAudioMediaSettings;

    @BindView(R.id.video_settings_click)
    RelativeLayout mVideoMediaSettings;

    @BindView(R.id.document_settings_click)
    RelativeLayout mDocMediaSettings;

    @BindView(R.id.reset_download_settings_click)
    RelativeLayout mResetMediaSettings;

    @BindView(R.id.photos_enabled)
    TextView mPhotoEnabled;

    @BindView(R.id.audio_enabled)
    TextView mAudioEnabled;

    @BindView(R.id.video_enabled)
    TextView mVideoEnabled;

    @BindView(R.id.document_enabled)
    TextView mDocEnabled;

    @BindView(R.id.total_cache_text)
    TextView allCache;

    @BindView(R.id.clear_cache)
    LinearLayout clearCache;

    @BindView(R.id.clear_cache_text)
    TextView clearCacheText;
    private MXSession mxSession;

    @Nullable
    @BindView(R.id.for_transition)
    ImageView transition;

    @OnClick(R.id.clear_cache)
    void clearAllCache() {
        mxSession.getDataHandler().getMediasCache().clear(this);
        showText("Cache was cleared!");
        getCache();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_media);

        ButterKnife.bind(this);

        mxSession = Matrix.getInstance(this).getDefaultSession();
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        getCache();
        setupUserData();
        setupOnClickListeners();


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ThemeUtils.setTransitionImage(this, "media_settings", transition);

            if (!getIntent().getBooleanExtra(SmartSettingsActivity.IS_SHARED_ELEMENT_TRANSITION_ENABLED, false))
                findViewById(R.id.hide_transition).setVisibility(View.GONE);

            getWindow().getSharedElementEnterTransition().addListener(new Transition.TransitionListener() {
                @Override
                public void onTransitionStart(Transition transition) {

                }

                @Override
                public void onTransitionEnd(Transition transition) {
                    findViewById(R.id.hide_transition).setVisibility(View.INVISIBLE);
                }

                @Override
                public void onTransitionCancel(Transition transition) {

                }

                @Override
                public void onTransitionPause(Transition transition) {

                }

                @Override
                public void onTransitionResume(Transition transition) {

                }
            });
        }

    }

    @Override
    public void onBackPressed() {
        if (getIntent().getBooleanExtra(SmartSettingsActivity.IS_SHARED_ELEMENT_TRANSITION_ENABLED, false))
            findViewById(R.id.hide_transition).setVisibility(View.VISIBLE);
        supportFinishAfterTransition();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (getIntent().getBooleanExtra(SmartSettingsActivity.IS_SHARED_ELEMENT_TRANSITION_ENABLED, false))
                    findViewById(R.id.hide_transition).setVisibility(View.VISIBLE);
                supportFinishAfterTransition();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.view_all_media_settings_click:
                showText("not implemented");
                break;
            case R.id.photos_settings_click:
                showDownloadMethodChooserAlertDialog(v,
                        PreferencesManager.getPhotoAutoDownloadMethod(this).ordinal());
                break;
            case R.id.audio_settings_click:
                showDownloadMethodChooserAlertDialog(v,
                        PreferencesManager.getAudioAutoDownloadMethod(this).ordinal());
                break;
            case R.id.video_settings_click:
                showDownloadMethodChooserAlertDialog(v,
                        PreferencesManager.getVideoAutoDownloadMethod(this).ordinal());
                break;
            case R.id.document_settings_click:
                showDownloadMethodChooserAlertDialog(v,
                        PreferencesManager.getDocAutoDownloadMethod(this).ordinal());
                break;
            case R.id.reset_download_settings_click:
                mPhotoEnabled.setText(DownloadMethod.WIFI_AND_CELLULAR.getString());
                PreferencesManager.setPhotoAutoDownloadMethod(this, DownloadMethod.WIFI_AND_CELLULAR);
                mAudioEnabled.setText(DownloadMethod.WIFI_AND_CELLULAR.getString());
                PreferencesManager.setAudioAutoDownloadMethod(this, DownloadMethod.WIFI_AND_CELLULAR);
                mVideoEnabled.setText(DownloadMethod.WIFI_AND_CELLULAR.getString());
                PreferencesManager.setVideoAutoDownloadMethod(this, DownloadMethod.WIFI_AND_CELLULAR);
                mDocEnabled.setText(DownloadMethod.WIFI_AND_CELLULAR.getString());
                PreferencesManager.setDocAutoDownloadMethod(this, DownloadMethod.WIFI_AND_CELLULAR);
                break;
        }
    }

    private void showDownloadMethodChooserAlertDialog(View v, int position) {
        final CharSequence[] items = {
                getResources().getString(DownloadMethod.WIFI_AND_CELLULAR.getString()),
                getResources().getString(DownloadMethod.WIFI.getString()),
                getResources().getString(DownloadMethod.OFF.getString())
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.choose_download_method_dialog_title);
        builder.setSingleChoiceItems(items, position, (dialog, which) -> {
            DownloadMethod method = DownloadMethod.values()[which];
            switch (v.getId()) {
                case R.id.photos_settings_click:
                    mPhotoEnabled.setText(method.getString());
                    PreferencesManager.setPhotoAutoDownloadMethod(MediaSettingsActivity.this, method);
                    break;
                case R.id.audio_settings_click:
                    mAudioEnabled.setText(method.getString());
                    PreferencesManager.setAudioAutoDownloadMethod(MediaSettingsActivity.this, method);
                    break;
                case R.id.video_settings_click:
                    mVideoEnabled.setText(method.getString());
                    PreferencesManager.setVideoAutoDownloadMethod(MediaSettingsActivity.this, method);
                    break;
                case R.id.document_settings_click:
                    mDocEnabled.setText(method.getString());
                    PreferencesManager.setDocAutoDownloadMethod(MediaSettingsActivity.this, method);
                    break;
            }
            dialog.dismiss();
        });
        builder.setNegativeButton(getResources().getString(R.string.action_close),
                (dialog, which) -> {
                    // negative button logic
                });

        builder.show();
    }

    private void setupOnClickListeners() {
        mPhotoMediaSettings.setOnClickListener(this);
        mAudioMediaSettings.setOnClickListener(this);
        mVideoMediaSettings.setOnClickListener(this);
        mDocMediaSettings.setOnClickListener(this);
        mResetMediaSettings.setOnClickListener(this);
        mViewAllMediaSettings.setOnClickListener(this);

        mSaveMediaMessageSwitcher.setOnCheckedChangeListener((buttonView, isChecked) ->
                PreferencesManager.setSaveMediaForPersonalChatAllow(MediaSettingsActivity.this, isChecked));
        mSaveMediaGroupSwitcher.setOnCheckedChangeListener((buttonView, isChecked) ->
                PreferencesManager.setSaveMediaForGroupChatAllow(MediaSettingsActivity.this, isChecked));
    }

    private void setupUserData() {
        mSaveMediaMessageSwitcher.setChecked(PreferencesManager.isSaveMediaForPersonalChatAllow(this));
        mSaveMediaGroupSwitcher.setChecked(PreferencesManager.isSaveMediaForGroupChatAllow(this));
        mPhotoEnabled.setText(PreferencesManager.getPhotoAutoDownloadMethod(this).getString());
        mAudioEnabled.setText(PreferencesManager.getAudioAutoDownloadMethod(this).getString());
        mVideoEnabled.setText(PreferencesManager.getVideoAutoDownloadMethod(this).getString());
        mDocEnabled.setText(PreferencesManager.getDocAutoDownloadMethod(this).getString());
    }

    private void getCache() {
        MXMediasCache.getCachesSize(this, new SimpleApiCallback<Long>() {
            @Override
            public void onSuccess(Long info) {
                super.onSuccess(info);
                allCache.setText(humanReadableByteCount(info, true));
                if (info == 0) {
                    clearCache.setClickable(false);
                    clearCacheText.setTextColor(getResources().getColor(android.R.color.darker_gray));
                } else {
                    clearCache.setClickable(true);
                    clearCacheText.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
                }
            }

            @Override
            public void onNetworkError(Exception e) {
                super.onNetworkError(e);
                showError();
            }

            @Override
            public void onMatrixError(MatrixError e) {
                super.onMatrixError(e);
                showError();
            }

            @Override
            public void onUnexpectedError(Exception e) {
                super.onUnexpectedError(e);
                showError();
            }
        });
    }

    private String humanReadableByteCount(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "i");
        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }

    private void showError() {
        showText("Error while clearing cache! Try again!");
    }

    private void showText(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }
}
