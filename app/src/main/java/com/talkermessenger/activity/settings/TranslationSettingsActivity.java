package com.talkermessenger.activity.settings;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.transition.Transition;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.iskar.talkersdk.TranslationHandler;
import com.talkermessenger.R;
import com.talkermessenger.util.PreferencesManager;
import com.talkermessenger.util.ThemeUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dev_serhii on 25.01.2018.
 */

public class TranslationSettingsActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.account_toolbar)
    Toolbar mToolbar;

    @BindView(R.id.auto_trans_switcher)
    SwitchCompat mAutoTransIncomingSwitcher;

    @BindView(R.id.flexible_translation_switch)
    SwitchCompat mFlexibleSwitcher;

    @BindView(R.id.statistics_settings_click)
    RelativeLayout mTransStatisticsSettings;

    @Nullable
    @BindView(R.id.for_transition)
    ImageView transition;

    @BindView(R.id.clear_cache_text)
    TextView clearCache;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_settings_translation);

        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        setupUserData();
        setupOnClickListeners();


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ThemeUtils.setTransitionImage(this, "translation_settings", transition);

            if (!getIntent().getBooleanExtra(SmartSettingsActivity.IS_SHARED_ELEMENT_TRANSITION_ENABLED, false))
                findViewById(R.id.hide_transition).setVisibility(View.GONE);

            getWindow().getSharedElementEnterTransition().addListener(new Transition.TransitionListener() {
                @Override
                public void onTransitionStart(Transition transition) {

                }

                @Override
                public void onTransitionEnd(Transition transition) {
                    findViewById(R.id.hide_transition).setVisibility(View.INVISIBLE);
                }

                @Override
                public void onTransitionCancel(Transition transition) {

                }

                @Override
                public void onTransitionPause(Transition transition) {

                }

                @Override
                public void onTransitionResume(Transition transition) {

                }
            });
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.statistics_settings_click:
                startActivity(new Intent(this, TranslationStatisticsSettingsActivity.class));
                break;
            case R.id.clear_cache_text:
                TranslationHandler translationHandler = new TranslationHandler(TranslationSettingsActivity.this);
                translationHandler.cleanTransaltionCache();
                Toast.makeText(TranslationSettingsActivity.this,getString(R.string.settings_translation_cleared_cache),Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (getIntent().getBooleanExtra(SmartSettingsActivity.IS_SHARED_ELEMENT_TRANSITION_ENABLED, false))
            findViewById(R.id.hide_transition).setVisibility(View.VISIBLE);
        supportFinishAfterTransition();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (getIntent().getBooleanExtra(SmartSettingsActivity.IS_SHARED_ELEMENT_TRANSITION_ENABLED, false))
                    findViewById(R.id.hide_transition).setVisibility(View.VISIBLE);
                supportFinishAfterTransition();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupUserData() {
        mAutoTransIncomingSwitcher.setChecked(PreferencesManager.isAutoTranslationIncomingAllow(this));
        mFlexibleSwitcher.setChecked(PreferencesManager.isFlexibleTranslationEnabled(this));
    }

    private void setupOnClickListeners() {
        mAutoTransIncomingSwitcher.setOnCheckedChangeListener((buttonView, isChecked) -> {
            PreferencesManager.setAutoTranslationIncomingAllow(TranslationSettingsActivity.this, isChecked);
        });
        mFlexibleSwitcher.setOnCheckedChangeListener((buttonView, isChecked) -> {
            PreferencesManager.setFlexibleTranslation(TranslationSettingsActivity.this, isChecked);
        });
        mTransStatisticsSettings.setOnClickListener(this);
        clearCache.setOnClickListener(this);
    }

    private void showSetSecondsAlert() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(R.string.set_translation_secons);

        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);

        final EditText editText = new EditText(this);
        editText.setInputType(InputType.TYPE_CLASS_NUMBER);
        layout.addView(editText);

        alert.setView(layout);

        alert.setPositiveButton(getResources().getString(R.string.save), (dialog, whichButton) -> {
            String youEditTextValue = editText.getText().toString();
            if (youEditTextValue.length() > 0) {
                PreferencesManager.setTranslationTimer(TranslationSettingsActivity.this,
                        Integer.parseInt(youEditTextValue));
            }
        });

        alert.setNegativeButton(getResources().getString(R.string.cancel), (dialog, whichButton) -> {
            // what ever you want to do with No option.
        });

        alert.show();
    }

}
