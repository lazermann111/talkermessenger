package com.talkermessenger.activity.settings;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.transition.ChangeBounds;
import android.transition.Transition;
import android.util.DisplayMetrics;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.talkermessenger.GlideApp;
import com.talkermessenger.R;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SmartSettingsActivity extends AppCompatActivity {

    public static String IS_SHARED_ELEMENT_TRANSITION_ENABLED = "IS_SHARED_ELEMENT_TRANSITION_ENABLED";

    @BindView(R.id.design_settings)
    ImageView design;

    @BindView(R.id.purchases_settings)
    ImageView purchases;

    @BindView(R.id.dnd_settings)
    ImageView dnd;

    @BindView(R.id.media_settings)
    ImageView media;

    @BindView(R.id.profile_settings)
    ImageView profile;

    @BindView(R.id.privacy_settings)
    ImageView privacy;

    @BindView(R.id.notification_settings)
    ImageView notification;

    @BindView(R.id.translation_settings)
    ImageView translation;

    @BindView(R.id.security_settings)
    ImageView security;

//    @BindView(R.id.about_settings)
//    ImageView about;
//
//    @BindView(R.id.account_settings)
//    ImageView account;
//
//    @BindView(R.id.block_settings)
//    ImageView block;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_smart_settings);

        ButterKnife.bind(this);
//        getWindow().setSharedElementEnterTransition(enterTransition());
//        getWindow().setSharedElementReturnTransition(returnTransition());
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        File baseFolderFile = new File(getApplicationContext().getFilesDir(), "SmartSettings");
        if (!baseFolderFile.exists())
            baseFolderFile.mkdirs();
        File designFile = new File(baseFolderFile.getPath() + "/design_settings.png");
        if (designFile.exists())
            GlideApp.with(this).load(designFile).override(metrics.widthPixels / 3, metrics.heightPixels / 3).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(design);
        else
            GlideApp.with(this).load(R.drawable.design_settings).override(metrics.widthPixels / 3, metrics.heightPixels / 3).into(design);

        File purchaseFile = new File(baseFolderFile.getPath() + "/purchases_settings.png");
        if (purchaseFile.exists())
            GlideApp.with(this).load(purchaseFile).override(metrics.widthPixels / 3, metrics.heightPixels / 3).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(purchases);
        else
            GlideApp.with(this).load(R.drawable.purchases_settings).override(metrics.widthPixels / 3, metrics.heightPixels / 3).into(purchases);

        File dndFile = new File(baseFolderFile.getPath() + "/dnd_settings.png");
        if (dndFile.exists())
            GlideApp.with(this).load(dndFile).override(metrics.widthPixels / 3, metrics.heightPixels / 3).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(dnd);
        else
            GlideApp.with(this).load(R.drawable.dnd_settings).override(metrics.widthPixels / 3, metrics.heightPixels / 3).into(dnd);

        File mediaFile = new File(baseFolderFile.getPath() + "/media_settings.png");
        if (mediaFile.exists())
            GlideApp.with(this).load(mediaFile).override(metrics.widthPixels / 3, metrics.heightPixels / 3).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(media);
        else
            GlideApp.with(this).load(R.drawable.media_settings).override(metrics.widthPixels / 3, metrics.heightPixels / 3).into(media);

        File profileFile = new File(baseFolderFile.getPath() + "/profile_settings.png");
        if (profileFile.exists())
            GlideApp.with(this).load(profileFile).override(metrics.widthPixels / 3, metrics.heightPixels / 3).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(profile);
        else
            GlideApp.with(this).load(R.drawable.profile_settings).override(metrics.widthPixels / 3, metrics.heightPixels / 3).into(profile);

        File privacyFile = new File(baseFolderFile.getPath() + "/privacy_settings.png");
        if (privacyFile.exists())
            GlideApp.with(this).load(privacyFile).override(metrics.widthPixels / 3, metrics.heightPixels / 3).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(privacy);
        else
            GlideApp.with(this).load(R.drawable.privacy_settings).override(metrics.widthPixels / 3, metrics.heightPixels / 3).into(privacy);

        File ntfctnFile = new File(baseFolderFile.getPath() + "/notification_settings.png");
        if (ntfctnFile.exists())
            GlideApp.with(this).load(ntfctnFile).override(metrics.widthPixels / 3, metrics.heightPixels / 3).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(notification);
        else
            GlideApp.with(this).load(R.drawable.notification_settings).override(metrics.widthPixels / 3, metrics.heightPixels / 3).into(notification);

        File trnsltnFile = new File(baseFolderFile.getPath() + "/translation_settings.png");
        if (trnsltnFile.exists())
            GlideApp.with(this).load(trnsltnFile).override(metrics.widthPixels / 3, metrics.heightPixels / 3).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(translation);
        else
            GlideApp.with(this).load(R.drawable.translation_settings).override(metrics.widthPixels / 3, metrics.heightPixels / 3).into(translation);

        File securityFile = new File(baseFolderFile.getPath() + "/security_settings.png");
        if (securityFile.exists())
            GlideApp.with(this).load(securityFile).override(metrics.widthPixels / 3, metrics.heightPixels / 3).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(security);
        else
            GlideApp.with(this).load(R.drawable.security_settings).override(metrics.widthPixels / 3, metrics.heightPixels / 3).into(security);
//        GlideApp.with(this).load(R.drawable.about_settings).override(metrics.widthPixels / 3, metrics.heightPixels / 3).into(about);
//        GlideApp.with(this).load(R.drawable.account_settings).override(metrics.widthPixels / 3, metrics.heightPixels / 3).into(account);
//        GlideApp.with(this).load(R.drawable.block_settings).override(metrics.widthPixels / 3, metrics.heightPixels / 3).into(block);
        design.setOnClickListener(v -> {
            Intent intent = new Intent(this, ChatDetailDesignActivity.class);
            ActivityOptionsCompat options = ActivityOptionsCompat.
                    makeSceneTransitionAnimation(this, design, "design_settings");
            intent.putExtra(IS_SHARED_ELEMENT_TRANSITION_ENABLED, true);
            startActivity(intent, options.toBundle());
        });

        purchases.setOnClickListener(v -> {
            Intent intent = new Intent(this, PurchasesSettingsActivity.class);
            ActivityOptionsCompat options = ActivityOptionsCompat.
                    makeSceneTransitionAnimation(this, purchases, "purchases_settings");
            intent.putExtra(IS_SHARED_ELEMENT_TRANSITION_ENABLED, true);
            // startActivity(intent, options.toBundle());
        });

        dnd.setOnClickListener(v -> {
            Intent intent = new Intent(this, DoNotDisturbSettingsActivity.class);
            ActivityOptionsCompat options = ActivityOptionsCompat.
                    makeSceneTransitionAnimation(this, dnd, "dnd_settings");
            intent.putExtra(IS_SHARED_ELEMENT_TRANSITION_ENABLED, true);
            startActivity(intent, options.toBundle());
        });

        media.setOnClickListener(v -> {
            Intent intent = new Intent(this, MediaSettingsActivity.class);
            ActivityOptionsCompat options = ActivityOptionsCompat.
                    makeSceneTransitionAnimation(this, media, "media_settings");
            intent.putExtra(IS_SHARED_ELEMENT_TRANSITION_ENABLED, true);
            startActivity(intent, options.toBundle());
        });

        profile.setOnClickListener(v -> {
            Intent intent = new Intent(this, AccountActivity.class);
            ActivityOptionsCompat options = ActivityOptionsCompat.
                    makeSceneTransitionAnimation(this, profile, "profile_settings");
            intent.putExtra(IS_SHARED_ELEMENT_TRANSITION_ENABLED, true);
            startActivity(intent, options.toBundle());
        });

        privacy.setOnClickListener(v -> {
            Intent intent = new Intent(this, ConfidentialActivity.class);
            ActivityOptionsCompat options = ActivityOptionsCompat.
                    makeSceneTransitionAnimation(this, privacy, "privacy_settings");
            intent.putExtra(IS_SHARED_ELEMENT_TRANSITION_ENABLED, true);
            startActivity(intent, options.toBundle());
        });

        notification.setOnClickListener(v -> {
            Intent intent = new Intent(this, NotificationSettingsActivity.class);
            ActivityOptionsCompat options = ActivityOptionsCompat.
                    makeSceneTransitionAnimation(this, notification, "notification_settings");
            intent.putExtra(IS_SHARED_ELEMENT_TRANSITION_ENABLED, true);
            startActivity(intent, options.toBundle());
        });

        translation.setOnClickListener(v -> {
            Intent intent = new Intent(this, TranslationSettingsActivity.class);
            ActivityOptionsCompat options = ActivityOptionsCompat.
                    makeSceneTransitionAnimation(this, translation, "translation_settings");
            intent.putExtra(IS_SHARED_ELEMENT_TRANSITION_ENABLED, true);
            startActivity(intent, options.toBundle());
        });

        security.setOnClickListener(v -> {
            Intent intent = new Intent(this, SecurityActivity.class);
            ActivityOptionsCompat options = ActivityOptionsCompat.
                    makeSceneTransitionAnimation(this, security, "security_settings");
            intent.putExtra(IS_SHARED_ELEMENT_TRANSITION_ENABLED, true);
            startActivity(intent, options.toBundle());
        });

//        about.setOnClickListener(v -> {
//            Intent intent = new Intent(this, AboutActivity.class);
//            ActivityOptionsCompat options = ActivityOptionsCompat.
//                    makeSceneTransitionAnimation(this, about, "about_settings");
//            intent.putExtra(IS_SHARED_ELEMENT_TRANSITION_ENABLED, true);
//            startActivity(intent, options.toBundle());
//        });
//
//        account.setOnClickListener(v -> {
//            Intent intent = new Intent(this, ExtraSettingsActivity.class);
//            ActivityOptionsCompat options = ActivityOptionsCompat.
//                    makeSceneTransitionAnimation(this, account, "account_settings");
//            intent.putExtra(IS_SHARED_ELEMENT_TRANSITION_ENABLED, true);
//            startActivity(intent, options.toBundle());
//        });
//
//        block.setOnClickListener(v -> {
//            Intent intent = new Intent(this, BlockedListSettings.class);
//            ActivityOptionsCompat options = ActivityOptionsCompat.
//                    makeSceneTransitionAnimation(this, block, "block_settings");
//            intent.putExtra(IS_SHARED_ELEMENT_TRANSITION_ENABLED, true);
//            startActivity(intent, options.toBundle());
//        });

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private Transition enterTransition() {
        ChangeBounds bounds = new ChangeBounds();
        bounds.setDuration(10000);
        return bounds;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private Transition returnTransition() {
        ChangeBounds bounds = new ChangeBounds();
        bounds.setInterpolator(new DecelerateInterpolator());
        bounds.setDuration(10000);
        return bounds;
    }
}
