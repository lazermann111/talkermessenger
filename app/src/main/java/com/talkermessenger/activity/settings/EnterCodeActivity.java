package com.talkermessenger.activity.settings;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.iskar.talkersdk.LoginHandler;
import com.iskar.talkersdk.callback.MatrixLoginInterface;
import com.iskar.talkersdk.model.MatrixCredentials;
import com.iskar.talkersdk.model.PhoneBadRequest;
import com.talkermessenger.Matrix;
import com.talkermessenger.R;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Serhii on 13.02.2018.
 */

public class EnterCodeActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String PHONE_NUMBER = "PHONE_NUMBER";
    @BindView(R.id.account_toolbar)
    Toolbar mToolbar;
    @BindView(R.id.enter_code)
    EditText mEnterCode;
    @BindView(R.id.enter_code_desc)
    TextView mEnterCodeDesc;
    @BindView(R.id.number_to_change)
    TextView numberToChange;
    @BindView(R.id.resend_code)
    TextView resendCode;
    CountDownTimer timer = new CountDownTimer(60000, 1000) {
        @Override
        public void onTick(long millisUntilFinished) {
            resendCode.setText(getResources().getString(R.string.settings_resend_code_in) + TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) + ":" + TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished));
            resendCode.setClickable(false);
        }

        @Override
        public void onFinish() {
            resendCode.setClickable(true);
            resendCode.setText(R.string.settings_resend_code);
        }
    };
    private LoginHandler loginHandler = new LoginHandler(new MatrixLoginInterface() {
        @Override
        public void getCredentialsSuccess(MatrixCredentials matrixCredentials) {

        }

        @Override
        public void getCodeSuccess() {

        }

        @Override
        public void tooManyAttempts(PhoneBadRequest phoneBadRequest) {

        }

        @Override
        public void changePhoneSuccess(String newPhone) {
            showSuccess();
        }

        @Override
        public void onFailure(String msg) {
            showText(getString(R.string.wrong_code));
        }
    });

    @OnClick(R.id.change_number)
    void changeNumber() {
        showNewNumber();
    }

//    private KeyListener originalKeyListener1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_settings_enter_code);

        ButterKnife.bind(this);

        initPhone(getIntent().getStringExtra(PHONE_NUMBER));

        mEnterCode.requestFocus();

//        originalKeyListener1 = mEnterCode.getKeyListener();

//        mEnterCode.setKeyListener(null);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        timer.start();
        resendCode.setOnClickListener(v -> {
            timer.start();
            loginHandler.requestCodeForPhone(
                    Matrix.getInstance(this).getDefaultSession().getMyUserId(),
                    Matrix.getInstance(this).getDefaultSession().getCredentials().accessToken,
                    getIntent().getStringExtra(PHONE_NUMBER)
            );
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_next, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                showCancelDialog();
                return true;
            case R.id.save:
                loginHandler.changeNumber(
                        Matrix.getInstance(this).getDefaultSession().getMyUserId(),
                        Matrix.getInstance(this).getDefaultSession().getCredentials().accessToken,
                        getIntent().getStringExtra(PHONE_NUMBER),
                        mEnterCode.getText().toString());
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        String tag = v.getTag().toString();
        String num = mEnterCode.getText().toString();

        switch (v.getTag().toString()) {
            case "remove":
                if (!num.isEmpty())
                    mEnterCode.setText(num.substring(0, num.length() - 1));

                break;
            default:
                num += tag;
                mEnterCode.setText(num);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer.cancel();
    }

    @Override
    protected void onStop() {
        super.onStop();
        timer.cancel();
    }

    @Override
    public void onBackPressed() {
        showCancelDialog();
    }

    private void showCancelDialog() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.canceling)
                .setMessage(R.string.are_you_cure_to_cancel)
                .setPositiveButton(R.string.accept, (dialog, which) -> finish())
                .setNegativeButton(R.string.cancel, (dialog, which) -> dialog.dismiss())
                .show();
    }

    private void showNewNumber() {
        final EditText input = new EditText(EnterCodeActivity.this);
        input.setText("+");
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setPositiveButton(getString(R.string.accept), null)
                .setNegativeButton(getString(R.string.accept), ((dialog, which) -> dialog.dismiss()))
                .setView(input)
                .setCancelable(false);
        AlertDialog dialog = adb.create();
        dialog.show();
        dialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(v -> {
            if (input.getText().toString().length() < 0 || (input.getText().toString().length() == 1 && input.getText().toString().equals("+"))) {
                showText(getString(R.string.empty_number));
            } else if (input.getText().toString().toCharArray()[0] != '+') {
                showText(getString(R.string.firs_sympol_should_be));
            } else if (!getIntent().getStringExtra(PHONE_NUMBER).equals(input.getText().toString())) {
                loginHandler.requestCodeForPhone(
                        Matrix.getInstance(this).getDefaultSession().getMyUserId(),
                        Matrix.getInstance(this).getDefaultSession().getCredentials().accessToken,
                        input.getText().toString());
                initPhone(input.getText().toString());
                getIntent().putExtra(PHONE_NUMBER, input.getText().toString());
                dialog.dismiss();
            } else {
                showText(getString(R.string.same_numbers_error));
            }
        });
    }

    private void showSuccess() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.number_changed)
                .setPositiveButton(getString(R.string.ok), (dialog, which) -> {
                    dialog.dismiss();
                    finish();
                })
                .show();
    }

    private void initPhone(String phone) {
        String desc = getResources().getString(R.string.we_have_sent_you_an_sms_with_a_code_to_the_number)
                + phone;

        numberToChange.setText(phone);

        mEnterCodeDesc.setText(desc);
    }

    private void showText(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }
}
