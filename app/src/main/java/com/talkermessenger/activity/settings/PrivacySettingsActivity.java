package com.talkermessenger.activity.settings;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.iskar.talkersdk.BlockedUserHandler;
import com.iskar.talkersdk.callback.BlockUserInterface;
import com.iskar.talkersdk.model.Blocked;
import com.talkermessenger.Matrix;
import com.talkermessenger.R;
import com.talkermessenger.util.PreferencesManager;
import com.talkermessenger.util.PrivacyMethod;

import org.jetbrains.annotations.NotNull;
import org.matrix.androidsdk.MXSession;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dev_serhii on 25.01.2018.
 */

public class PrivacySettingsActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.account_toolbar)
    Toolbar mToolbar;

    @BindView(R.id.do_not_disturb_settings_click)
    RelativeLayout mDoNotDisturbSettings;

    @BindView(R.id.profile_photo_settings_click)
    RelativeLayout mProfilePhotoPrivacySettings;
    @BindView(R.id.last_seen_settings_click)
    RelativeLayout mLastSeenPrivacySettings;
    @BindView(R.id.read_receipts_settings_click)
    RelativeLayout mReadReceiptsSettings;
    @BindView(R.id.calls_settings_click)
    RelativeLayout mCallsSettings;

    @BindView(R.id.profile_photo_enabled)
    TextView mProfilePhotoEnabled;
    @BindView(R.id.last_seen_enabled)
    TextView mLastSeenEnabled;
    @BindView(R.id.read_receipts_enabled)
    TextView mReadReceiptsEnabled;
    @BindView(R.id.calls_enabled)
    TextView mCallsEnabled;
    @BindView(R.id.blocked_enabled)
    TextView blockedNumber;

    @BindView(R.id.links_settings_click)
    RelativeLayout mLinksSettings;
    @BindView(R.id.passcode_settings_click)
    RelativeLayout mPasscodeSettings;
    @BindView(R.id.blocked_settings_click)
    RelativeLayout mBlockedSettings;

    @BindView(R.id.security_click)
    RelativeLayout securityClick;
    @BindView(R.id.conf_click)
    RelativeLayout confClick;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_settings_privacy);

        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        setupUserData();
        setupOnClickListeners();
        blockedUser();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void blockedUser(){
        blockedNumber.setText(getResources().getString(R.string.loading));
        MXSession session = Matrix.getInstance(this).getDefaultSession();
        new BlockedUserHandler(session.getMyUserId(), session.getCredentials().accessToken)
                .getBlockedUsers(new BlockUserInterface.GetBlockUserInterface() {
                    @Override
                    public void blockedUsersReceived(@NotNull List<Blocked> blockedList) {
                        blockedNumber.setText(String.valueOf(blockedList.size()));
                    }

                    @Override
                    public void onFailure(@NotNull String message) {

                    }
                });
    }

    private void showDownloadMethodChooserAlertDialog(View v, int position, CharSequence[] items) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.choose_privacy_method_dialog_title);
        builder.setSingleChoiceItems(items, position, (dialog, which) -> {
            PrivacyMethod method = PrivacyMethod.valueOf(items[which].toString());
            switch (v.getId()) {
                case R.id.last_seen_settings_click:
                    mLastSeenEnabled.setText(method.getString(PrivacySettingsActivity.this));
                    PreferencesManager.setLastSeenPrivacyMethod(PrivacySettingsActivity.this, method);
                    break;
                case R.id.profile_photo_settings_click:
                    mProfilePhotoEnabled.setText(method.getString(PrivacySettingsActivity.this));
                    PreferencesManager.setProfilePhotoPrivacyMethod(PrivacySettingsActivity.this, method);
                    break;
                case R.id.read_receipts_settings_click:
                    mProfilePhotoEnabled.setText(method.getString(PrivacySettingsActivity.this));
                    PreferencesManager.setReadReceiptsPrivacyMethod(PrivacySettingsActivity.this, method);
                    break;
                case R.id.calls_settings_click:
                    mProfilePhotoEnabled.setText(method.getString(PrivacySettingsActivity.this));
                    PreferencesManager.setCallsPrivacyMethod(PrivacySettingsActivity.this, method);
                    break;
            }
            dialog.dismiss();
        });
        builder.setNegativeButton(getResources().getString(R.string.action_close),
                (dialog, which) -> {
                    // negative button logic
                });

        builder.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.do_not_disturb_settings_click:
                startActivity(new Intent(this, DoNotDisturbSettingsActivity.class));
                break;
            case R.id.last_seen_settings_click:
                showDownloadMethodChooserAlertDialog(v,
                        PreferencesManager.getLastSeenPrivacyMethod(this).ordinal(), new CharSequence[]{
                                PrivacyMethod.EVERYONE.getString(PrivacySettingsActivity.this),
                                PrivacyMethod.NOBODY.getString(PrivacySettingsActivity.this),
                                PrivacyMethod.MY_CONTACTS.getString(PrivacySettingsActivity.this),
                                PrivacyMethod.FAVOURITES.getString(PrivacySettingsActivity.this),
                                PrivacyMethod.EXCEPT.getString(PrivacySettingsActivity.this),
                                PrivacyMethod.ONLY.getString(PrivacySettingsActivity.this),
                        });
                break;
            case R.id.profile_photo_settings_click:
                showDownloadMethodChooserAlertDialog(v,
                        PreferencesManager.getProfilePhotoPrivacyMethod(this).ordinal(), new CharSequence[]{
                                PrivacyMethod.EVERYONE.getString(PrivacySettingsActivity.this),
                                PrivacyMethod.MY_CONTACTS.getString(PrivacySettingsActivity.this),
                        });
                break;
            case R.id.read_receipts_settings_click:
                showDownloadMethodChooserAlertDialog(v,
                        PreferencesManager.getReadReceiptsPrivacyMethod(this).ordinal(), new CharSequence[]{
                                PrivacyMethod.EVERYONE.getString(PrivacySettingsActivity.this),
                                PrivacyMethod.MY_CONTACTS.getString(PrivacySettingsActivity.this),
                                PrivacyMethod.NOBODY.getString(PrivacySettingsActivity.this),
                        });
                break;
            case R.id.calls_settings_click:
                showDownloadMethodChooserAlertDialog(v,
                        PreferencesManager.getCallsPrivacyMethod(this).ordinal(), new CharSequence[]{
                                PrivacyMethod.EVERYONE.getString(PrivacySettingsActivity.this),
                                PrivacyMethod.MY_CONTACTS.getString(PrivacySettingsActivity.this),
                                PrivacyMethod.NOBODY.getString(PrivacySettingsActivity.this),
                        });
                break;
            case R.id.links_settings_click:
                startActivity(new Intent(this, LinksSettingsActivity.class));
                break;
            case R.id.passcode_settings_click:
                startActivity(new Intent(this, PasscodeSettingsActivity.class));
                break;
            case R.id.blocked_settings_click:
                break;
        }

    }

    private void setupOnClickListeners() {
//        mReadReceiptSwitcher.setOnCheckedChangeListener((compoundButton, b)
//                -> {
//            PreferencesManager.setHideReadReceipts(PrivacySettingsActivity.this, !b);
//            Toast.makeText(PrivacySettingsActivity.this, "hide Read receipt " + !b, Toast.LENGTH_SHORT).show();
//        });
        mDoNotDisturbSettings.setOnClickListener(this);

        mLastSeenPrivacySettings.setOnClickListener(this);
        mProfilePhotoPrivacySettings.setOnClickListener(this);
        mReadReceiptsSettings.setOnClickListener(this);
        mCallsSettings.setOnClickListener(this);

        mLinksSettings.setOnClickListener(this);
        mPasscodeSettings.setOnClickListener(this);
        mBlockedSettings.setOnClickListener(this);
        confClick.setOnClickListener(v -> startActivity(new Intent(PrivacySettingsActivity.this, ConfidentialActivity.class)));
        securityClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo start new activity
            }
        });
    }

    private void setupUserData() {
        mProfilePhotoEnabled.setText(PreferencesManager.getProfilePhotoPrivacyMethod(this)
                .getString(PrivacySettingsActivity.this));
        mLastSeenEnabled.setText(PreferencesManager.getLastSeenPrivacyMethod(this)
                .getString(PrivacySettingsActivity.this));


//        mReadReceiptSwitcher.setChecked(!PreferencesManager.hideReadReceipts(this));
    }

    private void showText(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }
}
