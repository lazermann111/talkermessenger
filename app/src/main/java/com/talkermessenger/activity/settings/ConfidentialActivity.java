package com.talkermessenger.activity.settings;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Transition;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.talkermessenger.R;
import com.talkermessenger.util.PreferencesManager;
import com.talkermessenger.util.PrivacyMethod;
import com.talkermessenger.util.ThemeUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ConfidentialActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.account_toolbar)
    Toolbar mToolbar;
    @BindView(R.id.profile_photo_settings_click)
    RelativeLayout mProfilePhotoPrivacySettings;
    @BindView(R.id.last_seen_settings_click)
    RelativeLayout mLastSeenPrivacySettings;
    @BindView(R.id.read_receipts_settings_click)
    RelativeLayout mReadReceiptsSettings;
    @BindView(R.id.calls_settings_click)
    RelativeLayout mCallsSettings;

    @BindView(R.id.profile_photo_enabled)
    TextView mProfilePhotoEnabled;
    @BindView(R.id.last_seen_enabled)
    TextView mLastSeenEnabled;
    @BindView(R.id.read_receipts_enabled)
    TextView mReadReceiptsEnabled;
    @BindView(R.id.calls_enabled)
    TextView mCallsEnabled;

    @Nullable
    @BindView(R.id.for_transition)
    ImageView transition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confidential);

        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        setupOnClickListeners();
        setupUserData();


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ThemeUtils.setTransitionImage(this, "privacy_settings", transition);

            if (!getIntent().getBooleanExtra(SmartSettingsActivity.IS_SHARED_ELEMENT_TRANSITION_ENABLED, false))
                findViewById(R.id.hide_transition).setVisibility(View.GONE);

            getWindow().getSharedElementEnterTransition().addListener(new Transition.TransitionListener() {
                @Override
                public void onTransitionStart(Transition transition) {

                }

                @Override
                public void onTransitionEnd(Transition transition) {
                    findViewById(R.id.hide_transition).setVisibility(View.INVISIBLE);
                }

                @Override
                public void onTransitionCancel(Transition transition) {

                }

                @Override
                public void onTransitionPause(Transition transition) {

                }

                @Override
                public void onTransitionResume(Transition transition) {

                }
            });
        }

    }

    @Override
    public void onBackPressed() {
        if (getIntent().getBooleanExtra(SmartSettingsActivity.IS_SHARED_ELEMENT_TRANSITION_ENABLED, false))
            findViewById(R.id.hide_transition).setVisibility(View.VISIBLE);
        supportFinishAfterTransition();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (getIntent().getBooleanExtra(SmartSettingsActivity.IS_SHARED_ELEMENT_TRANSITION_ENABLED, false))
                    findViewById(R.id.hide_transition).setVisibility(View.VISIBLE);
                supportFinishAfterTransition();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.do_not_disturb_settings_click:
                startActivity(new Intent(this, DoNotDisturbSettingsActivity.class));
                break;
            case R.id.last_seen_settings_click:
                showDownloadMethodChooserAlertDialog(v,
                        PreferencesManager.getLastSeenPrivacyMethod(this).ordinal(), new CharSequence[]{
                                PrivacyMethod.EVERYONE.getString(this),
                                PrivacyMethod.MY_CONTACTS.getString(this),
                                PrivacyMethod.NOBODY.getString(this),
//                                PrivacyMethod.FAVOURITES.getString(this),
//                                PrivacyMethod.EXCEPT.getString(this),
//                                PrivacyMethod.ONLY.getString(this),
                        });
                break;
            case R.id.profile_photo_settings_click:
                showDownloadMethodChooserAlertDialog(v,
                        PreferencesManager.getProfilePhotoPrivacyMethod(this).ordinal(), new CharSequence[]{
                                PrivacyMethod.EVERYONE.getString(this),
                                PrivacyMethod.MY_CONTACTS.getString(this),
                        });
                break;
            case R.id.read_receipts_settings_click:
                showDownloadMethodChooserAlertDialog(v,
                        PreferencesManager.getReadReceiptsPrivacyMethod(this).ordinal(), new CharSequence[]{
                                PrivacyMethod.EVERYONE.getString(this),
                                PrivacyMethod.MY_CONTACTS.getString(this),
                                PrivacyMethod.NOBODY.getString(this),
                        });
                break;
            case R.id.calls_settings_click:
                showDownloadMethodChooserAlertDialog(v,
                        PreferencesManager.getCallsPrivacyMethod(this).ordinal(), new CharSequence[]{
                                PrivacyMethod.EVERYONE.getString(this),
                                PrivacyMethod.MY_CONTACTS.getString(this),
                                PrivacyMethod.NOBODY.getString(this),
                        });
                break;
            case R.id.links_settings_click:
                startActivity(new Intent(this, LinksSettingsActivity.class));
                break;
            case R.id.passcode_settings_click:
                startActivity(new Intent(this, PasscodeSettingsActivity.class));
                break;
            case R.id.blocked_settings_click:
                break;
        }

    }

    private void showDownloadMethodChooserAlertDialog(View v, int position, CharSequence[] items) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.choose_privacy_method_dialog_title);
        builder.setSingleChoiceItems(items, position, (dialog, which) -> {
//            PrivacyMethod method = PrivacyMethod.valueOf(items[which].toString().toUpperCase().replaceAll(" ", "_"));
            PrivacyMethod method = PrivacyMethod.getPrivacyMethod(items[which].toString(), this);
            switch (v.getId()) {
                case R.id.last_seen_settings_click:
                    mLastSeenEnabled.setText(method.getString(this));
                    PreferencesManager.setLastSeenPrivacyMethod(this, method);
                    break;
                case R.id.profile_photo_settings_click:
                    mProfilePhotoEnabled.setText(method.getString(this));
                    PreferencesManager.setProfilePhotoPrivacyMethod(this, method);
                    break;
                case R.id.read_receipts_settings_click:
                    mReadReceiptsEnabled.setText(method.getString(this));
                    PreferencesManager.setReadReceiptsPrivacyMethod(this, method);
                    break;
                case R.id.calls_settings_click:
                    mCallsEnabled.setText(method.getString(this));
                    PreferencesManager.setCallsPrivacyMethod(this, method);
                    break;
            }
            dialog.dismiss();
        });
        builder.setNegativeButton(getResources().getString(R.string.action_close),
                (dialog, which) -> {
                    // negative button logic
                });

        builder.show();
    }

    private void setupOnClickListeners() {
        mLastSeenPrivacySettings.setOnClickListener(this);
        mProfilePhotoPrivacySettings.setOnClickListener(this);
        mReadReceiptsSettings.setOnClickListener(this);
        mCallsSettings.setOnClickListener(this);

    }

    private void setupUserData() {
        mProfilePhotoEnabled.setText(PreferencesManager.getProfilePhotoPrivacyMethod(this)
                .getString(this));
        mLastSeenEnabled.setText(PreferencesManager.getLastSeenPrivacyMethod(this)
                .getString(this));
        mReadReceiptsEnabled.setText(PreferencesManager.getReadReceiptsPrivacyMethod(this).getString(this));
        mCallsEnabled.setText(PreferencesManager.getCallsPrivacyMethod(this).getString(this));
    }

    private void showText(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }
}
