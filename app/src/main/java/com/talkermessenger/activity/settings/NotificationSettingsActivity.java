package com.talkermessenger.activity.settings;

import android.app.Activity;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.transition.Transition;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.talkermessenger.Matrix;
import com.talkermessenger.R;
import com.talkermessenger.gcm.GcmRegistrationManager;
import com.talkermessenger.util.PreferencesManager;
import com.talkermessenger.util.ThemeUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dev_serhii on 25.01.2018.
 */

public class NotificationSettingsActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int REQUEST_MESSAGE_NOTIFICATION_RINGTONE = 888;
    private static final int REQUEST_GROUP_NOTIFICATION_RINGTONE = 889;

    @BindView(R.id.account_toolbar)
    Toolbar mToolbar;

    @BindView(R.id.show_mess_notification_switcher)
    SwitchCompat mMessNotificationsSwitch;

    @BindView(R.id.show_group_notification_switcher)
    SwitchCompat mGroupNotificationsSwitch;

    @BindView(R.id.show_preview_switcher)
    SwitchCompat mShowPreviewSwitch;

    @BindView(R.id.mess_sound_enabled)
    TextView mMessageNotificationSoundName;

    @BindView(R.id.group_sound_enabled)
    TextView mGroupNotificationSoundName;

    @BindView(R.id.sound_mess_notification_settings_click)
    RelativeLayout mChooseMessageNotificationSound;

    @BindView(R.id.sound_group_notification_settings_click)
    RelativeLayout mChooseGroupNotificationSound;

    @Nullable
    @BindView(R.id.for_transition)
    ImageView transition;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_settings_notifications);

        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        setupUserData();
        setupOnClickListeners();


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ThemeUtils.setTransitionImage(this, "notification_settings", transition);

            if (!getIntent().getBooleanExtra(SmartSettingsActivity.IS_SHARED_ELEMENT_TRANSITION_ENABLED, false))
                findViewById(R.id.hide_transition).setVisibility(View.GONE);

            getWindow().getSharedElementEnterTransition().addListener(new Transition.TransitionListener() {
                @Override
                public void onTransitionStart(Transition transition) {

                }

                @Override
                public void onTransitionEnd(Transition transition) {
                    findViewById(R.id.hide_transition).setVisibility(View.INVISIBLE);
                }

                @Override
                public void onTransitionCancel(Transition transition) {

                }

                @Override
                public void onTransitionPause(Transition transition) {

                }

                @Override
                public void onTransitionResume(Transition transition) {

                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        if (getIntent().getBooleanExtra(SmartSettingsActivity.IS_SHARED_ELEMENT_TRANSITION_ENABLED, false))
            findViewById(R.id.hide_transition).setVisibility(View.VISIBLE);
        supportFinishAfterTransition();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (getIntent().getBooleanExtra(SmartSettingsActivity.IS_SHARED_ELEMENT_TRANSITION_ENABLED, false))
                    findViewById(R.id.hide_transition).setVisibility(View.VISIBLE);
                supportFinishAfterTransition();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_MESSAGE_NOTIFICATION_RINGTONE: {
                    PreferencesManager.setMessageNotificationRingTone(this, data.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI));
                    String soundName = PreferencesManager.getMessageNotificationRingToneName(this);
                    mMessageNotificationSoundName.setText(soundName != null ? soundName : "Default");
                    break;
                }
                case REQUEST_GROUP_NOTIFICATION_RINGTONE: {
                    PreferencesManager.setGroupNotificationRingTone(this, data.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI));
                    String soundName = PreferencesManager.getGroupNotificationRingToneName(this);
                    mGroupNotificationSoundName.setText(soundName != null ? soundName : "Default");
                    break;
                }
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sound_mess_notification_settings_click:
              //  Toast.makeText(NotificationSettingsActivity.this,
              //          "sound_mess_notification_settings_click", Toast.LENGTH_SHORT).show();
                setNewRingTone(REQUEST_MESSAGE_NOTIFICATION_RINGTONE);
                break;
            case R.id.sound_group_notification_settings_click:
              //  Toast.makeText(NotificationSettingsActivity.this,
               //         "sound_group_notification_settings_click", Toast.LENGTH_SHORT).show();
                setNewRingTone(REQUEST_GROUP_NOTIFICATION_RINGTONE);
                break;
        }
    }

    private void setupOnClickListeners() {
        mMessNotificationsSwitch.setOnCheckedChangeListener((compoundButton, b) -> {
            GcmRegistrationManager gcmMgr = Matrix.getInstance(this).getSharedGCMRegistrationManager();
            gcmMgr.setMessageNotificationsAllowed(b);
           // Toast.makeText(NotificationSettingsActivity.this, "Show mess notif: " + b, Toast.LENGTH_SHORT).show();
        });
        mGroupNotificationsSwitch.setOnCheckedChangeListener((compoundButton, b) -> {
            GcmRegistrationManager gcmMgr = Matrix.getInstance(this).getSharedGCMRegistrationManager();
            gcmMgr.setGroupNotificationsAllowed(b);
           // Toast.makeText(NotificationSettingsActivity.this, "Show group notif: " + b, Toast.LENGTH_SHORT).show();
        });
        mShowPreviewSwitch.setOnCheckedChangeListener((buttonView, isChecked) ->
                PreferencesManager.setShowPreviewAllow(NotificationSettingsActivity.this, isChecked));

        mChooseMessageNotificationSound.setOnClickListener(this);
        mChooseGroupNotificationSound.setOnClickListener(this);
    }

    private void setupUserData() {
        GcmRegistrationManager gcmMgr = Matrix.getInstance(this).getSharedGCMRegistrationManager();
//        Toast.makeText(NotificationSettingsActivity.this,
//                "Are device notify? " + gcmMgr.areDeviceNotificationsAllowed(), Toast.LENGTH_SHORT).show();

        mMessNotificationsSwitch.setChecked(gcmMgr.areMessageNotificationsAllowed());
        mGroupNotificationsSwitch.setChecked(gcmMgr.areGroupNotificationsAllowed());
        mShowPreviewSwitch.setChecked(PreferencesManager.isShowPreviewAllow(this));

        String soundName = PreferencesManager.getMessageNotificationRingToneName(this);
        mMessageNotificationSoundName.setText(soundName != null ? soundName : getString(R.string.default_sound));
        soundName = PreferencesManager.getGroupNotificationRingToneName(this);
        mGroupNotificationSoundName.setText(soundName != null ? soundName : getString(R.string.default_sound));
    }

    private void setNewRingTone(int request) {
        Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_NOTIFICATION);

        if (null != PreferencesManager.getMessageNotificationRingTone(this)) {
            if (request == REQUEST_MESSAGE_NOTIFICATION_RINGTONE)
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, PreferencesManager.getMessageNotificationRingTone(this));
            else if (request == REQUEST_GROUP_NOTIFICATION_RINGTONE)
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, PreferencesManager.getGroupNotificationRingTone(this));
        }
        startActivityForResult(intent, request);
    }

}
