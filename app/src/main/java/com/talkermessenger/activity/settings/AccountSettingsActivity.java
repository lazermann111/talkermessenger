package com.talkermessenger.activity.settings;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.talkermessenger.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dev_serhii on 24.01.2018.
 */

public class AccountSettingsActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.account_toolbar)
    Toolbar mToolbar;

    @BindView(R.id.profile_settings_click)
    RelativeLayout profileSettings;

    @BindView(R.id.purchases_settings_click)
    RelativeLayout purchasesSettings;

    @BindView(R.id.notif_settings_click)
    RelativeLayout notifSettings;

    @BindView(R.id.extra_settings_click)
    RelativeLayout extraSettings;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_settings_account);

        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        setupOnClickListeners();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.profile_settings_click:
                Intent intent = new Intent(this, AccountActivity.class);
                startActivity(intent);
                break;
            case R.id.purchases_settings_click:
                startActivity(new Intent(this, PurchasesSettingsActivity.class));
                break;
            case R.id.notif_settings_click:
                startActivity(new Intent(this, NotificationSettingsActivity.class));
                break;
            case R.id.extra_settings_click:
                startActivity(new Intent(this, ExtraSettingsActivity.class));
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupOnClickListeners() {
        profileSettings.setOnClickListener(this);
        purchasesSettings.setOnClickListener(this);
        notifSettings.setOnClickListener(this);
        extraSettings.setOnClickListener(this);
    }

    private void showText(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

}
