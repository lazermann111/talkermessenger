package com.talkermessenger.activity.settings;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.talkermessenger.Matrix;
import com.talkermessenger.R;
import com.talkermessenger.activity.CommonActivityUtils;
import com.talkermessenger.activity.VectorRoomActivity;
import com.talkermessenger.adapters.AbsAdapter;
import com.talkermessenger.adapters.ParticipantAdapterItem;
import com.talkermessenger.adapters.PeopleAdapter;
import com.talkermessenger.services.EventStreamService;
import com.talkermessenger.util.RoomUtils;
import com.talkermessenger.view.EmptyViewItemDecoration;
import com.talkermessenger.view.SimpleDividerItemDecoration;

import org.matrix.androidsdk.MXDataHandler;
import org.matrix.androidsdk.MXSession;
import org.matrix.androidsdk.data.Room;
import org.matrix.androidsdk.data.RoomPreviewData;
import org.matrix.androidsdk.data.RoomSummary;
import org.matrix.androidsdk.data.RoomTag;
import org.matrix.androidsdk.data.store.IMXStore;
import org.matrix.androidsdk.listeners.MXEventListener;
import org.matrix.androidsdk.rest.callback.ApiCallback;
import org.matrix.androidsdk.rest.model.MatrixError;
import org.matrix.androidsdk.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Serhii on 02.02.2018.
 */

public class FavouriteChatsActivity extends AppCompatActivity implements AbsAdapter.InvitationListener, AbsAdapter.MoreRoomActionListener {

    private static final String LOG_TAG = "FavouriteChatsActivity";
    private static final int MAX_KNOWN_CONTACTS_FILTER_COUNT = 50;
    private final List<Room> mDirectChats = new ArrayList<>();
    @BindView(R.id.recyclerview)
    RecyclerView mRecycler;
    private PeopleAdapter mAdapter;
    private MXEventListener mEventsListener;

    private MXSession mSession;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_people);

        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mSession = Matrix.getInstance(FavouriteChatsActivity.this).getDefaultSession();

        initViews();
        initDirectChatsData();
        initDirectChatsViews();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /*
     * *********************************************************************************************
     * UI management
     * *********************************************************************************************
     */

    /**
     * Prepare views
     */
    private void initViews() {
        int margin = (int) getResources().getDimension(R.dimen.item_decoration_left_margin);
        mRecycler.setLayoutManager(new LinearLayoutManager(FavouriteChatsActivity.this, LinearLayoutManager.VERTICAL, false));
        mRecycler.addItemDecoration(new SimpleDividerItemDecoration(FavouriteChatsActivity.this, DividerItemDecoration.VERTICAL, margin));
        mRecycler.addItemDecoration(new EmptyViewItemDecoration(FavouriteChatsActivity.this, DividerItemDecoration.VERTICAL, 40, 16, 14));
        mAdapter = new PeopleAdapter(FavouriteChatsActivity.this, "home", new PeopleAdapter.OnSelectItemListener() {
            @Override
            public void onSelectItem(Room room, int position) {
                openRoom(room);
            }

            @Override
            public void onSelectItem(ParticipantAdapterItem contact, int position) {
                //  onContactSelected(contact);
            }

            @Override
            public void onSelectAvatar(ParticipantAdapterItem item, View view) {

            }
        }, this, this);
        mRecycler.setAdapter(mAdapter);

    }

    /*
     * *********************************************************************************************
     * Data management
     * *********************************************************************************************
     */

    /**
     * Fill the direct chats mAdapter with data
     */
    private void initDirectChatsData() {
        if ((null == mSession) || (null == mSession.getDataHandler())) {
            Log.e(LOG_TAG, "## initDirectChatsData() : null session");
        }

        final List<String> directChatIds = mSession.getDirectChatRoomIdsList();
        final MXDataHandler dataHandler = mSession.getDataHandler();
        final IMXStore store = dataHandler.getStore();

        mDirectChats.clear();
        if (directChatIds != null && !directChatIds.isEmpty()) {
            for (String roomId : directChatIds) {
                Room room = store.getRoom(roomId);

                if ((null != room) && !room.isConferenceUserRoom()) {
                    // it seems that the server syncs some left rooms
                    if (null == room.getMember(mSession.getMyUserId())) {
                        Log.e(LOG_TAG, "## initDirectChatsData(): invalid room " + room.getRoomId() + ", the user is not anymore member of it");
                    } else {
                        final Set<String> tags = room.getAccountData().getKeys();
                        if ((null == tags) || !tags.contains(RoomTag.ROOM_TAG_LOW_PRIORITY)) {
                            final boolean isFavorite = tags != null && tags.contains(RoomTag.ROOM_TAG_FAVOURITE);
                            if (isFavorite)
                                mDirectChats.add(dataHandler.getRoom(roomId));
                        }
                    }
                }
            }
        }

        List<RoomSummary> roomSummaries = new ArrayList<>(store.getSummaries());
        HashSet<String> directChatRoomIds = new HashSet<>(mSession.getDirectChatRoomIdsList());
        HashSet<String> lowPriorityRoomIds = new HashSet<>(mSession.roomIdsWithTag(RoomTag.ROOM_TAG_LOW_PRIORITY));
        for (RoomSummary summary : roomSummaries) {
            // don't display the invitations
            if (!summary.isInvited()) {
                Room room = store.getRoom(summary.getRoomId());

                // test
                if ((null != room) && // if the room still exists
                        !room.isConferenceUserRoom() && // not a VOIP conference room
                        !directChatRoomIds.contains(room.getRoomId()) &&
                        !lowPriorityRoomIds.contains(room.getRoomId())) {
                    final Set<String> tags = room.getAccountData().getKeys();
                    final boolean isFavorite = tags != null && tags.contains(RoomTag.ROOM_TAG_FAVOURITE);
                    if (isFavorite)
                        mDirectChats.add(room);
                }
            }
        }
    }

    private void initDirectChatsViews() {
        mAdapter.setRooms(mDirectChats);
    }


    /**
     * Open the selected room
     *
     * @param room
     */
    void openRoom(final Room room) {
        // sanity checks
        // reported by GA
        if ((null == mSession.getDataHandler()) || (null == mSession.getDataHandler().getStore())) {
            return;
        }

        final String roomId;
        // cannot join a leaving room
        if (room == null || room.isLeaving()) {
            roomId = null;
        } else {
            roomId = room.getRoomId();
        }

        if (roomId != null) {
            final RoomSummary roomSummary = mSession.getDataHandler().getStore().getSummary(roomId);

            if (null != roomSummary) {
                room.sendReadReceipt();
            }

            // Update badge unread count in case device is offline
            CommonActivityUtils.specificUpdateBadgeUnreadCount(mSession, FavouriteChatsActivity.this);

            // Launch corresponding room activity
            HashMap<String, Object> params = new HashMap<>();
            params.put(VectorRoomActivity.EXTRA_MATRIX_ID, mSession.getMyUserId());
            params.put(VectorRoomActivity.EXTRA_ROOM_ID, roomId);

            CommonActivityUtils.goToRoomPage(FavouriteChatsActivity.this, mSession, params);
        }
    }

    @Override
    public void onPreviewRoom(MXSession session, String roomId) {
        String roomAlias = null;

        Room room = session.getDataHandler().getRoom(roomId);
        if ((null != room) && (null != room.getLiveState())) {
            roomAlias = room.getLiveState().getAlias();
        }

        final RoomPreviewData roomPreviewData = new RoomPreviewData(mSession, roomId, null, roomAlias, null);
        CommonActivityUtils.previewRoom(this, roomPreviewData);
    }

    @Override
    public void onRejectInvitation(MXSession session, String roomId) {
        Room room = session.getDataHandler().getRoom(roomId);

        if (null != room) {
//            showWaitingView();

            room.leave(new ApiCallback<Void>() {
                @Override
                public void onSuccess(Void info) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // clear any pending notification for this room
                            EventStreamService.cancelNotificationsForRoomId(mSession.getMyUserId(), roomId);
//                            stopWaitingView();
                        }
                    });
                }

                private void onError(final String message) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            stopWaitingView();
                            Toast.makeText(FavouriteChatsActivity.this, message, Toast.LENGTH_LONG).show();
                        }
                    });
                }

                @Override
                public void onNetworkError(Exception e) {
                    onError(e.getLocalizedMessage());
                }

                @Override
                public void onMatrixError(MatrixError e) {
                    onError(e.getLocalizedMessage());
                }

                @Override
                public void onUnexpectedError(Exception e) {
                    onError(e.getLocalizedMessage());
                }
            });
        }
    }

    @Override
    public void onMoreActionClick(View itemView, Room room) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.unfavorite_this_chat)
                .setPositiveButton(getString(R.string.unfavourite), (dialog, id) -> {
                    RoomUtils.updateRoomTag(mSession,
                            room.getRoomId(), null, null, new ApiCallback<Void>() {
                                @Override
                                public void onSuccess(Void info) {
                                    Toast.makeText(FavouriteChatsActivity.this,
                                            R.string.success, Toast.LENGTH_SHORT).show();
                                    Intent intent = getIntent();
                                    finish();
                                    startActivity(intent);
                                }

                                @Override
                                public void onNetworkError(Exception e) {
                                    Toast.makeText(FavouriteChatsActivity.this,
                                            R.string.network_error, Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onMatrixError(MatrixError e) {
                                    Toast.makeText(FavouriteChatsActivity.this,
                                            R.string.matrix_error, Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onUnexpectedError(Exception e) {
                                    Toast.makeText(FavouriteChatsActivity.this,
                                            R.string.error, Toast.LENGTH_SHORT).show();
                                }
                            });
                })
                .setNegativeButton(R.string.cancel, (dialog, id) -> {
                    // User cancelled the dialog
                });
//        builder.create();
        builder.show();
//        final Set<String> tags = room.getAccountData().getKeys();
//        final boolean isFavorite = tags != null && tags.contains(RoomTag.ROOM_TAG_FAVOURITE);
//        final boolean isLowPriority = tags != null && tags.contains(RoomTag.ROOM_TAG_LOW_PRIORITY);
//        RoomUtils.displayPopupMenu(FavouriteChatsActivity.this, mSession, room, itemView, isFavorite, isLowPriority, FavouriteChatsActivity.this);
    }
}
