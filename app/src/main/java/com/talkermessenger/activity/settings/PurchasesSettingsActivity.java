package com.talkermessenger.activity.settings;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Transition;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.iskar.talkersdk.PaymentsHandler;
import com.iskar.talkersdk.callback.BalanceInterface;
import com.iskar.talkersdk.callback.PaymentInterface;
import com.talkermessenger.GlideApp;
import com.talkermessenger.Matrix;
import com.talkermessenger.R;
import com.talkermessenger.purchase.Purchase;
import com.talkermessenger.util.ThemeUtils;

import org.matrix.androidsdk.MXSession;
import org.matrix.androidsdk.rest.model.login.Credentials;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Serhii on 30.01.2018.
 */

public class PurchasesSettingsActivity extends AppCompatActivity implements View.OnClickListener, BillingProcessor.IBillingHandler, PaymentInterface {

    private static final String TAG = "SYMBOLS_PURCHASE";
    @BindView(R.id.account_toolbar)
    Toolbar mToolbar;

    @BindView(R.id.user_symbols)
    TextView userSymbols;

    @BindView(R.id.history_pay_settings_click)
    RelativeLayout mHistoryPaySettings;

    @BindView(R.id.offer_first_settings_click)
    View symbols20k;

    @BindView(R.id.offer_second_settings_click)
    View symbols40k;

    @BindView(R.id.offer_third_settings_click)
    View symbols100k;

    @BindView(R.id.offer_fourth_settings_click)
    View symbols250k;

    @BindView(R.id.offer_fifth_settings_click)
    View symbols500k;

    @BindView(R.id.offer_first_price)
    TextView symbols20kPrice;

    @BindView(R.id.offer_second_price)
    TextView symbols40kPrice;

    @BindView(R.id.offer_third_price)
    TextView symbols100kPrice;

    @BindView(R.id.offer_fourth_price)
    TextView symbols250kPrice;

    @BindView(R.id.offer_fifth_price)
    TextView symbols500kPrice;

    @BindView(R.id.first_offer_name)
    TextView symbols20kName;

    @BindView(R.id.second_offer_name)
    TextView symbols40kName;

    @BindView(R.id.third_offer_name)
    TextView symbols100kName;

    @BindView(R.id.fourth_offer_name)
    TextView symbols250kName;

    @BindView(R.id.fifth_offer_name)
    TextView symbols500kName;

    @Nullable
    @BindView(R.id.for_transition)
    ImageView transition;

    private BillingProcessor bp;
    private MXSession mxSession;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_purchases);

        ButterKnife.bind(this);
        mxSession = Matrix.getInstance(this).getDefaultSession();
        bp = new BillingProcessor(this, Purchase.APP_KEY, this);
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getMyBalance();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ThemeUtils.setTransitionImage(this, "purchases_settings", transition);

            if (!getIntent().getBooleanExtra(SmartSettingsActivity.IS_SHARED_ELEMENT_TRANSITION_ENABLED, false))
                findViewById(R.id.hide_transition).setVisibility(View.GONE);

            getWindow().getSharedElementEnterTransition().addListener(new Transition.TransitionListener() {
                @Override
                public void onTransitionStart(Transition transition) {

                }

                @Override
                public void onTransitionEnd(Transition transition) {
                    findViewById(R.id.hide_transition).setVisibility(View.INVISIBLE);
                }

                @Override
                public void onTransitionCancel(Transition transition) {

                }

                @Override
                public void onTransitionPause(Transition transition) {

                }

                @Override
                public void onTransitionResume(Transition transition) {

                }
            });
        }

    }

    private void setPrices() {
        try {
            if (isBillingProcessorReady()) {
                symbols20kPrice.setText(bp.getPurchaseListingDetails(Purchase.SYMBOLS_20_000_SKU).priceText);
                symbols40kPrice.setText(bp.getPurchaseListingDetails(Purchase.SYMBOLS_40_000_SKU).priceText);
                symbols100kPrice.setText(bp.getPurchaseListingDetails(Purchase.SYMBOLS_100_000_SKU).priceText);
                symbols250kPrice.setText(bp.getPurchaseListingDetails(Purchase.SYMBOLS_250_000_SKU).priceText);
                symbols500kPrice.setText(bp.getPurchaseListingDetails(Purchase.SYMBOLS_500_000_SKU).priceText);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getMyBalance() {
        new PaymentsHandler().getMySymbols(mxSession.getCredentials().userId, mxSession.getCredentials().accessToken, new BalanceInterface() {
            @Override
            public void onBalanceRecieved(Long balance) {
                if (balance != null)
                    userSymbols.setText(String.valueOf(balance));
            }

            @Override
            public void onFailure(String message) {
                Log.e("get balance failure", message + "");
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.history_pay_settings_click:
                startActivity(new Intent(this, PaymentsHistorySettingsActivity.class));
                break;
            case R.id.offer_first_settings_click:
                buySymbols(Purchase.SYMBOLS_20_000_SKU);
                break;
            case R.id.offer_second_settings_click:
                buySymbols(Purchase.SYMBOLS_40_000_SKU);
                break;
            case R.id.offer_third_settings_click:
                buySymbols(Purchase.SYMBOLS_100_000_SKU);
                break;
            case R.id.offer_fourth_settings_click:
                buySymbols(Purchase.SYMBOLS_250_000_SKU);
                break;
            case R.id.offer_fifth_settings_click:
                buySymbols(Purchase.SYMBOLS_500_000_SKU);
                break;
        }
    }

    private void buySymbols(String sku) {
        Log.d(TAG, sku);
        if (isBillingProcessorReady())
            bp.purchase(this, sku);
    }

    @Override
    public void onBackPressed() {
        if (getIntent().getBooleanExtra(SmartSettingsActivity.IS_SHARED_ELEMENT_TRANSITION_ENABLED, false))
            findViewById(R.id.hide_transition).setVisibility(View.VISIBLE);
        supportFinishAfterTransition();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (getIntent().getBooleanExtra(SmartSettingsActivity.IS_SHARED_ELEMENT_TRANSITION_ENABLED, false))
                    findViewById(R.id.hide_transition).setVisibility(View.VISIBLE);
                supportFinishAfterTransition();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupOnClickListeners() {
        mHistoryPaySettings.setOnClickListener(this);
        symbols20k.setOnClickListener(this);
        symbols40k.setOnClickListener(this);
        symbols100k.setOnClickListener(this);
        symbols250k.setOnClickListener(this);
        symbols500k.setOnClickListener(this);
    }

    @Override
    public void onProductPurchased(@NonNull String productId, @Nullable TransactionDetails details) {
        switch (productId) {
            case Purchase.SYMBOLS_20_000_SKU:
                addSymbolsToAccount(details);
                break;
            case Purchase.SYMBOLS_40_000_SKU:
                addSymbolsToAccount(details);
                break;
            case Purchase.SYMBOLS_100_000_SKU:
                addSymbolsToAccount(details);
                break;
            case Purchase.SYMBOLS_250_000_SKU:
                addSymbolsToAccount(details);
                break;
            case Purchase.SYMBOLS_500_000_SKU:
                addSymbolsToAccount(details);
                break;
        }
        Log.d(TAG, "You bought: " + productId);
    }

    private void addSymbolsToAccount(TransactionDetails details) {
        Credentials credentials = mxSession.getCredentials();
        Log.e("purchaseState", String.valueOf(details.purchaseInfo.purchaseData.purchaseState));
        Log.e("purchaseTime", String.valueOf(details.purchaseInfo.purchaseData.purchaseTime));
        Log.e("packageName", details.purchaseInfo.purchaseData.packageName);
        Log.e("productId", details.purchaseInfo.purchaseData.productId);
        Log.e("orderId", details.purchaseInfo.purchaseData.orderId);
        Log.e("developerPayload", details.purchaseInfo.purchaseData.developerPayload);
        Log.e("purchaseToken", details.purchaseInfo.purchaseData.purchaseToken);
        Log.e("autoRenewing", String.valueOf(details.purchaseInfo.purchaseData.autoRenewing));
        PaymentsHandler paymentsHandler = new PaymentsHandler(this, credentials.userId, credentials.accessToken, new PaymentInterface() {
            @Override
            public void onSuccess(Long message) {
                getMyBalance();
                bp.consumePurchase(details.purchaseInfo.purchaseData.productId);
            }

            @Override
            public void onFailure(String message) {
                bp.consumePurchase(details.purchaseInfo.purchaseData.productId);
            }
        }, details.purchaseInfo.purchaseData);
        paymentsHandler.checkPayment();
    }

    @Override
    public void onPurchaseHistoryRestored() {

    }

    @Override
    public void onBillingError(int errorCode, @Nullable Throwable error) {

    }

    @Override
    public void onBillingInitialized() {
        setupOnClickListeners();
        setPrices();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!bp.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onResume() {
        if (bp != null && !bp.isInitialized()) {
            bp.initialize();
        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        if (bp != null) {
            bp.release();
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (bp != null) {
            bp.release();
        }
        super.onDestroy();
    }

    public boolean isBillingProcessorReady() {
        return bp != null && bp.isInitialized() && bp.isOneTimePurchaseSupported();
    }

    @Override
    public void onSuccess(Long message) {
        Toast.makeText(this, "Pyment success!", Toast.LENGTH_LONG).show();
        if (message != null)
            userSymbols.setText(String.valueOf(message));
    }

    @Override
    public void onFailure(String message) {
        Toast.makeText(this, "Pyment failure!  " + message, Toast.LENGTH_LONG).show();
    }

}
