package com.talkermessenger.activity.settings;

import android.animation.ValueAnimator;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.talkermessenger.R;
import com.talkermessenger.VectorApp;
import com.talkermessenger.listeners.FlipListener;
import com.talkermessenger.util.ThemeUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.talkermessenger.util.ThemeUtils.THEME_BLACK_VALUE;
import static com.talkermessenger.util.ThemeUtils.THEME_DARK_VALUE;
import static com.talkermessenger.util.ThemeUtils.THEME_LIGHT_LIGHT_BLUE_HEADER_VALUE;
import static com.talkermessenger.util.ThemeUtils.THEME_LIGHT_VALUE;

/**
 * Created by djnig on 2/19/2018.
 */

public class AppThemeActivity extends AppCompatActivity implements View.OnClickListener {
    Handler handler = new Handler();

    @BindView(R.id.account_toolbar)
    Toolbar mToolbar;

    @BindView(R.id.dark_theme)
    View darkTheme;

    @BindView(R.id.black_theme)
    View blackTheme;

    @BindView(R.id.light_black_status_theme)
    View lightThemeBlackStatusbar;

    @BindView(R.id.white_blue_theme)
    View whiteBlueTheme;

    @BindView(R.id.dark_theme_icon)
    ImageView darkThemeIcon;

    @BindView(R.id.black_theme_icon)
    ImageView blackThemeIcon;

    @BindView(R.id.dark_theme_icon_selected)
    ImageView darkThemeIconSelected;

    @BindView(R.id.black_theme_icon_selected)
    ImageView blackThemeIconSelected;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_theme);
        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        darkTheme.setOnClickListener(this);
        blackTheme.setOnClickListener(this);
        lightThemeBlackStatusbar.setOnClickListener(this);
        whiteBlueTheme.setOnClickListener(this);

     /*   String theme = ThemeUtils.getApplicationTheme(this);
        switch (theme) {
            case DARK:
                darkThemeIcon.setVisibility(View.GONE);
                darkThemeIconSelected.setVisibility(View.VISIBLE);
                break;
            case BLACK:
                blackThemeIcon.setVisibility(View.GONE);
                blackThemeIconSelected.setVisibility(View.VISIBLE);
                break;
            default:
                lightThemeIcon.setVisibility(View.GONE);
                lightThemeIconSelected.setVisibility(View.VISIBLE);
        }*/

    }

   /* private void configureSelectedIcon(String theme) {

        switch (theme) {
            case THEME_DARK_VALUE:
                flipIcon(darkThemeIcon, darkThemeIconSelected);
                blackThemeIconSelected.setVisibility(View.GONE);
                blackThemeIcon.setVisibility(View.VISIBLE);
                lightThemeIconSelected.setVisibility(View.GONE);
                lightThemeIcon.setVisibility(View.VISIBLE);
                break;
            case THEME_BLACK_VALUE:
                flipIcon(blackThemeIcon, blackThemeIconSelected);
                darkThemeIconSelected.setVisibility(View.GONE);
                darkThemeIcon.setVisibility(View.VISIBLE);
                lightThemeIconSelected.setVisibility(View.GONE);
                lightThemeIcon.setVisibility(View.VISIBLE);
                break;
            case THEME_LIGHT_LIGHT_GREY_STATUSBAR_VALUE:
                flipIcon(lightThemeIcon, lightThemeIconSelected);
                blackThemeIconSelected.setVisibility(View.GONE);
                blackThemeIcon.setVisibility(View.VISIBLE);
                darkThemeIconSelected.setVisibility(View.GONE);
                darkThemeIcon.setVisibility(View.VISIBLE);
                break;
            case THEME_LIGHT_BLACK_STATUSBAR_VALUE:
                flipIcon(lightThemeIcon, lightThemeIconSelected);
                blackThemeIconSelected.setVisibility(View.GONE);
                blackThemeIcon.setVisibility(View.VISIBLE);
                darkThemeIconSelected.setVisibility(View.GONE);
                darkThemeIcon.setVisibility(View.VISIBLE);
                break;
            default:
                flipIcon(lightThemeIcon, lightThemeIconSelected);
                blackThemeIconSelected.setVisibility(View.GONE);
                blackThemeIcon.setVisibility(View.VISIBLE);
                darkThemeIconSelected.setVisibility(View.GONE);
                darkThemeIcon.setVisibility(View.VISIBLE);
        }
    }*/

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dark_theme:
                applyTheme(THEME_DARK_VALUE);
                break;
            case R.id.black_theme:
                applyTheme(THEME_BLACK_VALUE);
                break;
            case R.id.light_black_status_theme:
                applyTheme(THEME_LIGHT_VALUE);
                break;
            case R.id.white_blue_theme:
                applyTheme(THEME_LIGHT_LIGHT_BLUE_HEADER_VALUE);
                break;
        }
    }

    private void applyTheme(String theme) {

        if (theme.equals(ThemeUtils.getApplicationTheme(this)))
            return;

       // configureSelectedIcon(theme);

        handler.postDelayed(() -> {
            VectorApp.updateApplicationTheme(theme);
            startActivity(getIntent());
            finish();
        }, 500);

    }

    private void flipIcon(View frontView, View backView) {
        ValueAnimator mFlipAnimator = ValueAnimator.ofFloat(0f, 1f);
        mFlipAnimator.addUpdateListener(new FlipListener(frontView, backView));
        mFlipAnimator.start();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
