/*
 * Copyright 2014 OpenMarket Ltd
 * Copyright 2017 Vector Creations Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.talkermessenger.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.iskar.talkersdk.BlockedUserHandler;
import com.iskar.talkersdk.ContactHandler;
import com.iskar.talkersdk.LanguageSettingsHandler;
import com.iskar.talkersdk.callback.BlockUserInterface;
import com.iskar.talkersdk.model.Status;
import com.iskar.talkersdk.model.TalkerContact;
import com.iskar.talkersdk.ui.LanguageSettingsView;
import com.talkermessenger.Matrix;
import com.talkermessenger.R;
import com.talkermessenger.activity.settings.StatisticsSettingsActivity;
import com.talkermessenger.util.PreferencesManager;
import com.talkermessenger.util.RoomUtils;
import com.talkermessenger.util.SlidableMediaInfo;
import com.talkermessenger.util.ThemeUtils;
import com.talkermessenger.util.VectorUtils;

import org.jetbrains.annotations.NotNull;
import org.matrix.androidsdk.MXSession;
import org.matrix.androidsdk.call.IMXCall;
import org.matrix.androidsdk.data.Room;
import org.matrix.androidsdk.data.RoomState;
import org.matrix.androidsdk.data.RoomSummary;
import org.matrix.androidsdk.data.store.IMXStore;
import org.matrix.androidsdk.listeners.MXEventListener;
import org.matrix.androidsdk.rest.callback.ApiCallback;
import org.matrix.androidsdk.rest.callback.SimpleApiCallback;
import org.matrix.androidsdk.rest.model.Event;
import org.matrix.androidsdk.rest.model.MatrixError;
import org.matrix.androidsdk.rest.model.Message;
import org.matrix.androidsdk.rest.model.RoomMember;
import org.matrix.androidsdk.rest.model.User;
import org.matrix.androidsdk.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

/**
 * VectorMemberDetailsActivity displays the member information and allows to perform some dedicated actions.
 */
public class VectorMemberDetailsActivity extends MXCActionBarActivity {
    private static final String LOG_TAG = VectorMemberDetailsActivity.class.getSimpleName();

    public static final String EXTRA_ROOM_ID = "EXTRA_ROOM_ID";
    public static final String EXTRA_OPEN_FOR_CALL = "EXTRA_OPEN_FOR_CALL";
    public static final String EXTRA_MEMBER_ID = "EXTRA_MEMBER_ID";
    public static final String EXTRA_MEMBER_DISPLAY_NAME = "EXTRA_MEMBER_DISPLAY_NAME";
    public static final String EXTRA_MEMBER_AVATAR_URL = "EXTRA_MEMBER_AVATAR_URL";
    public static final String EXTRA_LOOKUP_URI = "EXTRA_LOOKUP_URI";
    public static final String EXTRA_CONTACT_ID = "EXTRA_CONTACT_ID";
    public static final String EXTRA_NUMBERS = "EXTRA_NUMBERS";


    private static final int REQUEST_MESSAGE_NOTIFICATION_RINGTONE = 888;
    TextView mMessageNotificationSoundName;


    public static final String EXTRA_STORE_ID = "EXTRA_STORE_ID";

    public static final String RESULT_MENTION_ID = "RESULT_MENTION_ID";

    private static final String AVATAR_FULLSCREEN_MODE = "AVATAR_FULLSCREEN_MODE";

    // list view items associated actions
    private static final int ITEM_ACTION_INVITE = 0;
    private static final int ITEM_ACTION_LEAVE = 1;
    public static final int ITEM_ACTION_KICK = 2;
    private static final int ITEM_ACTION_BAN = 3;
    private static final int ITEM_ACTION_UNBAN = 4;
    private static final int ITEM_ACTION_IGNORE = 5;
    private static final int ITEM_ACTION_UNIGNORE = 6;
    private static final int ITEM_ACTION_SET_DEFAULT_POWER_LEVEL = 7;
    private static final int ITEM_ACTION_SET_MODERATOR = 8;
    private static final int ITEM_ACTION_SET_ADMIN = 9;
    //public static final int ITEM_ACTION_SET_CUSTOM_POWER_LEVEL = 10;
    private static final int ITEM_ACTION_START_CHAT = 11;
    private static final int ITEM_ACTION_START_VOICE_CALL = 12;
    private static final int ITEM_ACTION_START_VIDEO_CALL = 13;
    private static final int ITEM_ACTION_MENTION = 14;
    private static final int ITEM_ACTION_DEVICES = 15;

    private static final int VECTOR_ROOM_MODERATOR_LEVEL = 50;
    private static final int VECTOR_ROOM_ADMIN_LEVEL = 100;

    // internal info
    private Room mRoom;
    private String mMemberId;       // member whose details area displayed (provided in EXTRAS)
    private RoomMember mRoomMember; // room member corresponding to mMemberId
    private MXSession mSession;
    private User mUser;
    //private ArrayList<MemberDetailsAdapter.AdapterMemberActionItems> mActionItemsArrayList;
//    private VectorMemberDetailsAdapter mListViewAdapter;
//    private VectorMemberDetailsDevicesAdapter mDevicesListViewAdapter;

    // UI widgets
    private ImageView mMemberAvatarImageView;
    //    private CircleImageView mMemberAvatarBadgeImageView;
    private TextView mMemberNameTextView;
    private TextView mPresenceTextView;
    private View mProgressBarView;

    // full screen avatar
//    private View mFullMemberAvatarLayout;
//    private ImageView mFullMemberAvatarImageView;

    // listview
//    private ExpandableListView mExpandableListView;
//    private ListView mDevicesListView;
//    private View mDevicesListHeaderView;

    //time picker
    private boolean wasStartTimeClick;
    private TextView mDoNotDisturbCurrentTime;
    private TextView mDoNotDisturbStartTime;
    private TextView mDoNotDisturbFinishTime;
    private TextView mDoNotDisturbSeparator;
    private ImageView mDoNotDisturbToolbarIndicator;
    private ImageView mDoNotDisturbSectionIndicator;


    private ImageView mCallImageView;

    //language section
    private LanguageSettingsView languagesHolder;
    private LanguageSettingsHandler languageSettingsHandler;

    private RelativeLayout mShowAllMediaLayout;
    private RelativeLayout mCallHistoryLayout;
    private RelativeLayout mSendMessageLayout;
    private RelativeLayout mFavouriteLayout;
    private RelativeLayout mSoundLayout;
    private TextView mSoundTextView;


    private TextView mBlockUserTextView;
    private RelativeLayout mPhoneNumberLayout;
    private RelativeLayout mStatistics;
    private TextView mClearChatTextView;
    private TextView mEditProfileTextView;
    private SwitchCompat mSaveMediaMessageSwitcher;


    private ProgressBar progressBar;

    private boolean isCall;
    // direct message
    /**
     * callback for the creation of the direct message room
     **/
    private final ApiCallback<String> mCreateDirectMessageCallBack = new ApiCallback<String>() {
        @Override
        public void onSuccess(String roomId) {
            Log.d(LOG_TAG, "## mCreateDirectMessageCallBack: onSuccess - start goToRoomPage " + roomId);
            openRoomById(roomId, isCall);
        }

        @Override
        public void onMatrixError(MatrixError e) {
            Log.d(LOG_TAG, "## mCreateDirectMessageCallBack: onMatrixError Msg=" + e.getLocalizedMessage());
            mRoomActionsListener.onMatrixError(e);
        }

        @Override
        public void onNetworkError(Exception e) {
            Log.d(LOG_TAG, "## mCreateDirectMessageCallBack: onNetworkError Msg=" + e.getLocalizedMessage());
            mRoomActionsListener.onNetworkError(e);
        }

        @Override
        public void onUnexpectedError(Exception e) {
            Log.d(LOG_TAG, "## mCreateDirectMessageCallBack: onUnexpectedError Msg=" + e.getLocalizedMessage());
            mRoomActionsListener.onUnexpectedError(e);
        }
    };

    // MX event listener
    private final MXEventListener mLiveEventsListener = new MXEventListener() {
        @Override
        public void onLiveEvent(final Event event, RoomState roomState) {
            VectorMemberDetailsActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String eventType = event.getType();

                    // check if the event is received for the current room
                    // check if there is a member update
                    if ((Event.EVENT_TYPE_STATE_ROOM_MEMBER.equals(eventType)) || (Event.EVENT_TYPE_STATE_ROOM_POWER_LEVELS.equals(eventType))) {
                        // update only if it is the current user
                        VectorMemberDetailsActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (checkRoomMemberStatus()) {
                                    updateUi();
                                } else if (null != mRoom) {
                                    // exit from the activity
                                    finish();
                                }
                            }
                        });
                    }
                }
            });
        }

        @Override
        public void onLeaveRoom(String roomId) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // pop to the home activity
                    Intent intent = new Intent(VectorMemberDetailsActivity.this, VectorHomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    VectorMemberDetailsActivity.this.startActivity(intent);
                }
            });
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_MESSAGE_NOTIFICATION_RINGTONE: {
                    Uri uri = data.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
                    PreferencesManager.setMessageNotificationCustomRingTone(this, uri, mUser.user_id);

                    // Log.d("---","setSound for id="+mUser.user_id);
                    String soundName = PreferencesManager.getNotificationRingToneName(this, uri);

                    //Log.d("---",soundName);
                    mSoundTextView.setText(soundName != null ? soundName : getString(R.string.default_sound));
                    //mMessageNotificationSoundName.setText(soundName != null ? soundName : "Default");
                    break;
                }

            }
        }
    }


    private final MXEventListener mPresenceEventsListener = new MXEventListener() {
        @Override
        public void onPresenceUpdate(Event event, final User user) {
            if (mMemberId.equals(user.user_id)) {
                // Someone's presence has changed, reprocess the whole list
                VectorMemberDetailsActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // display an avatar it it was not used
                        updateMemberAvatarUi();
                        // refresh the presence
                        updatePresenceInfoUi();
                    }
                });
            }
        }
    };

    // Room action listeners. Every time an action is detected the UI must be updated.
    private final ApiCallback<Void> mRoomActionsListener = new SimpleApiCallback<Void>(this) {
        @Override
        public void onMatrixError(MatrixError e) {
            Toast.makeText(VectorMemberDetailsActivity.this, e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            updateUi();
        }

        @Override
        public void onSuccess(Void info) {
            updateUi();
        }

        @Override
        public void onNetworkError(Exception e) {
            Toast.makeText(VectorMemberDetailsActivity.this, e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            updateUi();
        }

        @Override
        public void onUnexpectedError(Exception e) {
            Toast.makeText(VectorMemberDetailsActivity.this, e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            updateUi();
        }
    };
    private String roomId;

    /**
     * Start a call in a dedicated room
     *
     * @param isVideo true if the call is a video call
     */
    private void startCall(final boolean isVideo) {
        if (!mSession.isAlive()) {
            Log.e(LOG_TAG, "startCall : the session is not anymore valid");
            return;
        }

        // create the call object
        mSession.mCallsManager.createCallInRoom(roomId, isVideo, new ApiCallback<IMXCall>() {
            @Override
            public void onSuccess(final IMXCall call) {
                Log.d(LOG_TAG, "## startIpCall(): onSuccess");
                VectorMemberDetailsActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        final Intent intent = new Intent(VectorMemberDetailsActivity.this, VectorCallViewActivity.class);

                        intent.putExtra(VectorCallViewActivity.EXTRA_MATRIX_ID, mSession.getCredentials().userId);
                        intent.putExtra(VectorCallViewActivity.EXTRA_CALL_ID, call.getCallId());

                        VectorMemberDetailsActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                VectorMemberDetailsActivity.this.startActivity(intent);
                            }
                        });
                    }
                });
            }

            private void onError(final String errorMessage) {
                VectorMemberDetailsActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Activity activity = VectorMemberDetailsActivity.this;
                        CommonActivityUtils.displayToastOnUiThread(activity, activity.getString(R.string.cannot_start_call) + " (user offline)");
                    }
                });
            }

            @Override
            public void onNetworkError(Exception e) {
                Log.e(LOG_TAG, "## startIpCall(): onNetworkError Msg=" + e.getMessage());
                onError(e.getLocalizedMessage());
            }

            @Override
            public void onMatrixError(MatrixError e) {
                Log.e(LOG_TAG, "## startIpCall(): onMatrixError Msg=" + e.getLocalizedMessage());
                onError(e.getLocalizedMessage());
            }

            @Override
            public void onUnexpectedError(Exception e) {
                Log.e(LOG_TAG, "## startIpCall(): onUnexpectedError Msg=" + e.getLocalizedMessage());
                onError(e.getLocalizedMessage());
            }
        });
    }

    /**
     * Check the permissions to establish an audio/video call.
     * If permissions are already granted, the call is established, otherwise
     * the permissions are checked against the system. Final result is provided in
     * {@link #onRequestPermissionsResult(int, String[], int[])}.
     *
     * @param aIsVideoCall true if video call, false if audio call
     */
    private void startCheckCallPermissions(boolean aIsVideoCall) {
        int requestCode = CommonActivityUtils.REQUEST_CODE_PERMISSION_AUDIO_IP_CALL;

        if (aIsVideoCall) {
            requestCode = CommonActivityUtils.REQUEST_CODE_PERMISSION_VIDEO_IP_CALL;
        }

        if (CommonActivityUtils.checkPermissions(requestCode, this)) {
            startCall(aIsVideoCall);
        }
    }

    @Override
    public void onRequestPermissionsResult(int aRequestCode, @NonNull String[] aPermissions, @NonNull int[] aGrantResults) {
        if (0 == aPermissions.length) {
            Log.e(LOG_TAG, "## onRequestPermissionsResult(): cancelled " + aRequestCode);
        } else if (aRequestCode == CommonActivityUtils.REQUEST_CODE_PERMISSION_AUDIO_IP_CALL) {
            if (CommonActivityUtils.onPermissionResultAudioIpCall(this, aPermissions, aGrantResults)) {
                startCall(false);
            }
        }
    }

//    @Override
//    public void selectRoom(final Room aRoom) {
//        VectorMemberDetailsActivity.this.runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                HashMap<String, Object> params = new HashMap<>();
//                params.put(VectorRoomActivity.EXTRA_MATRIX_ID, mSession.getMyUserId());
//                params.put(VectorRoomActivity.EXTRA_ROOM_ID, aRoom.getRoomId());
//
//                Log.d(LOG_TAG, "## selectRoom(): open the room " + aRoom.getRoomId());
//                CommonActivityUtils.goToRoomPage(VectorMemberDetailsActivity.this, mSession, params);
//            }
//        });
//    }

//    @Override
//    public void performItemAction(final int aActionType) {
//        if (!mSession.isAlive()) {
//            Log.e(LOG_TAG, "performItemAction : the session is not anymore valid");
//            return;
//        }
//
//        final ArrayList<String> idsList = new ArrayList<>();
//
//        switch (aActionType) {
//            case ITEM_ACTION_DEVICES:
//                refreshDevicesListView();
//                break;
//
//            case ITEM_ACTION_START_CHAT:
//                Log.d(LOG_TAG, "## performItemAction(): Start new room - start chat");
//
//                VectorMemberDetailsActivity.this.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        enableProgressBarView(CommonActivityUtils.UTILS_DISPLAY_PROGRESS_BAR);
//                        mSession.createDirectMessageRoom(mMemberId, mCreateDirectMessageCallBack);
//                    }
//                });
//                break;
//
//            case ITEM_ACTION_START_VIDEO_CALL:
//            case ITEM_ACTION_START_VOICE_CALL:
//                Log.d(LOG_TAG, "## performItemAction(): Start call");
//                startCheckCallPermissions(ITEM_ACTION_START_VIDEO_CALL == aActionType);
//                break;
//
//            case ITEM_ACTION_INVITE:
//                Log.d(LOG_TAG, "## performItemAction(): Invite");
//                if (null != mRoom) {
//                    enableProgressBarView(CommonActivityUtils.UTILS_DISPLAY_PROGRESS_BAR);
//                    mRoom.invite(mRoomMember.getUserId(), mRoomActionsListener);
//                }
//                break;
//
//            case ITEM_ACTION_LEAVE:
//                Log.d(LOG_TAG, "## performItemAction(): Leave the room");
//                if (null != mRoom) {
//                    enableProgressBarView(CommonActivityUtils.UTILS_DISPLAY_PROGRESS_BAR);
//                    mRoom.leave(mRoomActionsListener);
//                }
//                break;
//
//            case ITEM_ACTION_SET_ADMIN:
//                if (null != mRoom) {
//                    updateUserPowerLevels(mMemberId, VECTOR_ROOM_ADMIN_LEVEL, mRoomActionsListener);
//                    Log.d(LOG_TAG, "## performItemAction(): Make Admin");
//                }
//                break;
//
//            case ITEM_ACTION_SET_MODERATOR:
//                if (null != mRoom) {
//                    updateUserPowerLevels(mMemberId, VECTOR_ROOM_MODERATOR_LEVEL, mRoomActionsListener);
//                    Log.d(LOG_TAG, "## performItemAction(): Make moderator");
//                }
//                break;
//
//            case ITEM_ACTION_SET_DEFAULT_POWER_LEVEL:
//                if (null != mRoom) {
//                    int defaultPowerLevel = 0;
//                    PowerLevels powerLevels = mRoom.getLiveState().getPowerLevels();
//
//                    if (null != powerLevels) {
//                        defaultPowerLevel = powerLevels.users_default;
//                    }
//
//                    updateUserPowerLevels(mMemberId, defaultPowerLevel, mRoomActionsListener);
//                    Log.d(LOG_TAG, "## performItemAction(): default power level");
//                }
//                break;
//
//            case ITEM_ACTION_BAN:
//                if (null != mRoom) {
//                    enableProgressBarView(CommonActivityUtils.UTILS_DISPLAY_PROGRESS_BAR);
//                    mRoom.ban(mRoomMember.getUserId(), null, mRoomActionsListener);
//                    Log.d(LOG_TAG, "## performItemAction(): Block (Ban)");
//                }
//                break;
//
//            case ITEM_ACTION_UNBAN:
//                if (null != mRoom) {
//                    enableProgressBarView(CommonActivityUtils.UTILS_DISPLAY_PROGRESS_BAR);
//                    mRoom.unban(mRoomMember.getUserId(), mRoomActionsListener);
//                    Log.d(LOG_TAG, "## performItemAction(): Block (unban)");
//                }
//                break;
//
//            case ITEM_ACTION_KICK:
//                if (null != mRoom) {
//                    enableProgressBarView(CommonActivityUtils.UTILS_DISPLAY_PROGRESS_BAR);
//                    mRoom.kick(mRoomMember.getUserId(), mRoomActionsListener);
//                    Log.d(LOG_TAG, "## performItemAction(): Kick");
//                }
//                break;
//
//            case ITEM_ACTION_IGNORE: {
//                ignoreUser();
//                break;
//            }
//
//            case ITEM_ACTION_UNIGNORE: {
//                unignoreUser();
//                break;
//            }
//            case ITEM_ACTION_MENTION:
//                String displayName = TextUtils.isEmpty(mRoomMember.displayname) ? mRoomMember.getUserId() : mRoomMember.displayname;
//
//                // provide the mention name
//                Intent intent = new Intent();
//                intent.putExtra(RESULT_MENTION_ID, displayName);
//                setResult(RESULT_OK, intent);
//                finish();
//
//                break;
//
//            default:
//                // unknown action
//                Log.w(LOG_TAG, "## performItemAction(): unknown action type = " + aActionType);
//                break;
//        }
//    }

    private void unignoreUser() {
        showProgress();
        blockedUserHandler.unblockUser(mUser.user_id, new BlockUserInterface.BlockingUser() {
            @Override
            public void onSuccess() {
                Log.d("Uncloecked", "done");
                isUserIgnored();
            }

            @Override
            public void onFailure(@NotNull String message) {
                closeProgress();
                Toast.makeText(VectorMemberDetailsActivity.this, getString(R.string.something_wrong), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void isUserIgnored() {
        blockedUserHandler.checkIfBlocked(mUser.user_id, new BlockUserInterface.CheckBlockingUser() {
            @Override
            public void statusReceived(@NotNull Status status) {
                Log.d("TAG", status.toString());
                closeProgress();
                if (status.getStatus().equals("true")) {
                    mBlockUserTextView.setText(getResources().getString(R.string.unblock));
                    mBlockUserTextView.setTextColor(getResources().getColor(android.R.color.holo_blue_dark));
                } else {
                    mBlockUserTextView.setText(getResources().getString(R.string.block));
                    mBlockUserTextView.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
                }
            }

            @Override
            public void onFailure(@NotNull String message) {
                closeProgress();
                mBlockUserTextView.setText(getResources().getString(R.string.block));
                mBlockUserTextView.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
            }
        });
    }


    private void ignoreUser() {
        showProgress();
        blockedUserHandler.blockUser(mUser.user_id, new BlockUserInterface.BlockingUser() {
            @Override
            public void onSuccess() {
                Log.d("Locked", "done");
                isUserIgnored();
            }

            @Override
            public void onFailure(@NotNull String message) {
                closeProgress();
                Toast.makeText(VectorMemberDetailsActivity.this, getString(R.string.something_wrong), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void closeProgress() {
        progressBar.setVisibility(View.GONE);
    }

    private BlockedUserHandler blockedUserHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Log.d("---","VectorMemeberDetailsActivity");

        blockedUserHandler = new BlockedUserHandler(
                Matrix.getInstance(this).getDefaultSession().getMyUserId(),
                Matrix.getInstance(this).getDefaultSession().getCredentials().accessToken);

        if (CommonActivityUtils.shouldRestartApp(this)) {
            Log.e(LOG_TAG, "Restart the application");
            CommonActivityUtils.restartApp(this);
            return;
        }

        if (CommonActivityUtils.isGoingToSplash(this)) {
            Log.d(LOG_TAG, "onCreate : Going to splash screen");
            return;
        }

        // retrieve the parameters contained extras and setup other
        // internal state values such as the; session, room..
        if (!initContextStateValues()) {
            // init failed, just return
            Log.e(LOG_TAG, "## onCreate(): Parameters init failure");
            finish();
        } else {
            // check if the user is a member of the room
            checkRoomMemberStatus();

            // setup UI view and bind the widgets
            setContentView(R.layout.activity_member_details_new);

            // use a toolbar instead of the actionbar
            // to be able to display a large header
            android.support.v7.widget.Toolbar toolbar = findViewById(R.id.account_toolbar);
            this.setSupportActionBar(toolbar);


            if (null != getSupportActionBar()) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }

            mMemberAvatarImageView = findViewById(R.id.member_image);
            mMemberAvatarImageView.setOnClickListener(v -> openImage());
//            mMemberAvatarBadgeImageView = findViewById(R.id.member_avatar_badge);
//
//            mFullMemberAvatarImageView = findViewById(R.id.member_details_fullscreen_avatar_image_view);
//            mFullMemberAvatarLayout = findViewById(R.id.member_details_fullscreen_avatar_layout);

            mMemberNameTextView = findViewById(R.id.member_details_name);
            mPresenceTextView = findViewById(R.id.member_details_presence);

            mSendMessageLayout = findViewById(R.id.send_message_section);
            if (getIntent() != null && !TextUtils.isEmpty(getIntent().getStringExtra(EXTRA_ROOM_ID)))
                mSendMessageLayout.setVisibility(View.GONE);
            else
                mSendMessageLayout.setOnClickListener(view -> startRoom(false));

            mFavouriteLayout = findViewById(R.id.favourite_section);
            mFavouriteLayout.setOnClickListener(view -> RoomUtils.openFavoutiteMsgs(this, mSession, RoomUtils.isDirectChatRoomAlreadyExist(mUser.user_id, mSession), mUser.user_id));


            //myCode
            mSoundLayout = findViewById(R.id.sound_section);
            mSoundLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Log.d("---","Click Sound");


                    final Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
                    intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_NOTIFICATION);

                    if (null != PreferencesManager.getMessageNotificationCustomRingTone(getBaseContext(), mUser.user_id)) {
                        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI,
                                PreferencesManager.getMessageNotificationCustomRingTone(getBaseContext(), mUser.user_id));
                    }
                    startActivityForResult(intent, REQUEST_MESSAGE_NOTIFICATION_RINGTONE);
                }
            });

            mSoundTextView = findViewById(R.id.sound_section_default);


            String soundName = PreferencesManager.getNotificationRingToneName(this,
                    PreferencesManager.getMessageNotificationCustomRingTone(this, mUser.user_id));
            mSoundTextView.setText(soundName != null ? soundName : getString(R.string.default_sound));
            //

            mPhoneNumberLayout = findViewById(R.id.phone_number_section);
            mPhoneNumberLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent currentIntent = getIntent();
                    Intent intent = new Intent(VectorMemberDetailsActivity.this, VectorMemberPhoneNumberActivity.class);
                    String matrixId = currentIntent.getStringExtra(EXTRA_MEMBER_ID);
                    if (matrixId != null)
                        intent.putExtra(EXTRA_MEMBER_ID, matrixId);

//                    ArrayList<String> numbers = currentIntent.getStringArrayListExtra(EXTRA_NUMBERS);
//                    if (numbers != null && numbers.size() > 0)
//                        intent.putExtra(VectorMemberDetailsActivity.EXTRA_NUMBERS, numbers);

                    String displayName = currentIntent.getStringExtra(EXTRA_MEMBER_DISPLAY_NAME);
                    if (displayName != null)
                        intent.putExtra(VectorMemberDetailsActivity.EXTRA_MEMBER_DISPLAY_NAME, displayName);

                    startActivity(intent);
                }
            });

            mStatistics = findViewById(R.id.statistics_section);
            mStatistics.setOnClickListener(view -> {
                Intent intent = new Intent(this, StatisticsSettingsActivity.class);
                intent.putExtra("userId", mUser.user_id);
                startActivity(intent);
            });

            mClearChatTextView = findViewById(R.id.clear_chat_text);
            mClearChatTextView.setOnClickListener(view -> {
                String existingRoomId;
                if (null != (existingRoomId = RoomUtils.isDirectChatRoomAlreadyExist(mUser.user_id, mSession))) {
                    RoomUtils.clearChat(mSession.getDataHandler().getRoom(existingRoomId));
                    Toast.makeText(this, "Chat was cleared", Toast.LENGTH_LONG).show();
                } else
                    Toast.makeText(this, "Can't find direct chat with this contact", Toast.LENGTH_LONG).show();
            });

            mEditProfileTextView = findViewById(R.id.edit_profile_text);
            mEditProfileTextView.setOnClickListener(view -> {
                Intent intent = getIntent();
                if (intent != null) {
                    Uri lookupUri = intent.getParcelableExtra(EXTRA_LOOKUP_URI);
                    if (lookupUri != null) {
                        Intent editIntent = new Intent(Intent.ACTION_EDIT);
                        editIntent.setDataAndType(lookupUri, ContactsContract.Contacts.CONTENT_ITEM_TYPE);
                        editIntent.putExtra("finishActivityOnSaveCompleted", true);
                        startActivity(editIntent);
                    }
                }

            });


            mCallImageView = findViewById(R.id.call_icon);
            mCallImageView.setOnClickListener(view -> {

                        Log.d(LOG_TAG, "## performItemAction(): Start new room - start chat");
//                                VectorMemberDetailsActivity.this.runOnUiThread(new Runnable() {
//                                    @Override
//                                    public void run() {
                        startRoom(true);
//                                    }
//                                });
                    }
            );

            //setup time picker
            mDoNotDisturbToolbarIndicator = findViewById(R.id.do_not_disturb_indicator);
            mDoNotDisturbSectionIndicator = findViewById(R.id.do_not_disturb_section_indicator);
            mDoNotDisturbCurrentTime = findViewById(R.id.do_not_disturb_section_curren_time);
            mDoNotDisturbStartTime = findViewById(R.id.do_not_disturb_section_start);
            mDoNotDisturbFinishTime = findViewById(R.id.do_not_disturb_section_finish);
            mDoNotDisturbSeparator = findViewById(R.id.do_not_disturb_section_separator);
            updateDoNotDisturbUi();

            mDoNotDisturbStartTime.setOnClickListener(view -> {
                wasStartTimeClick = true;
                //    showTimePickerDialog(view);
            });
            mDoNotDisturbFinishTime.setOnClickListener(view -> {
                wasStartTimeClick = false;
                //  showTimePickerDialog(view);
            });

            languagesHolder = findViewById(R.id.languages_holder);
            findViewById(R.id.add_language_button).setOnClickListener(view -> {
                CommonActivityUtils.showLanguageSelectionDialog(mSession, this, languageSettingsHandler);
            });

            setupUserLanguagesFragment(mUser.user_id);
            // when clicking on the username
            // switch member name <-> member id
//            mMemberNameTextView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    User user = mSession.getDataHandler().getUser(mMemberId);
//
//                    if (TextUtils.equals(mMemberNameTextView.getText(), mMemberId)) {
//                        if ((null != user) && !TextUtils.isEmpty(user.displayname)) {
//                            mMemberNameTextView.setText(user.displayname);
//                        }
//                    } else {
//                        mMemberNameTextView.setText(mMemberId);
//                    }
//                }
//            });

            mBlockUserTextView = findViewById(R.id.block_user_text);
            checkBlockStatus();
            mBlockUserTextView.setOnClickListener(view -> {
                if (mBlockUserTextView.getText().toString().equals("Block")) {
                    ignoreUser();
                    checkBlockStatus();
                    Log.d(getClass().getName(), "User was ignored");
                } else {
                    unignoreUser();
                    checkBlockStatus();
                    Log.d(getClass().getName(), "User was unignored");
                }
            });

            mShowAllMediaLayout = findViewById(R.id.show_all_media_section);
            mShowAllMediaLayout.setOnClickListener(view -> {
                Intent mediaIntent = new Intent(this, VectorMemberMediaActivity.class);
//                mediaIntent.putExtra("userId", mUser.user_id);
                String roomId = RoomUtils.isDirectChatRoomAlreadyExist(mUser.user_id, mSession);
                if (roomId != null)
                    mediaIntent.putExtra("roomId", roomId);
                startActivity(mediaIntent);
            });

            mCallHistoryLayout = findViewById(R.id.call_history_section);
            mCallHistoryLayout.setOnClickListener(view -> {
                Intent callHistoryIntent = new Intent(this, CallHistoryActivity.class);
                callHistoryIntent.putExtra("userId", mUser.user_id);
                startActivity(callHistoryIntent);
            });

            mSaveMediaMessageSwitcher = findViewById(R.id.save_incoming_section_switcher);
            if (TextUtils.isEmpty(roomId))
                if (mRoom != null)
                    roomId = mRoom.getRoomId();
                else
                    roomId = RoomUtils.isDirectChatRoomAlreadyExist(mUser.user_id, mSession);

            mSaveMediaMessageSwitcher.setOnCheckedChangeListener((buttonView, isChecked) ->
                    PreferencesManager.setSaveMediaForRoomAllow(this, isChecked, roomId)
            );


            progressBar = findViewById(R.id.profile_progress);

            // long tap : copy to the clipboard
            mMemberNameTextView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    VectorUtils.copyToClipboard(VectorMemberDetailsActivity.this, mMemberNameTextView.getText());
                    return true;
                }
            });


            // update the UI
            updateUi();

//            if ((null != savedInstanceState) && savedInstanceState.getBoolean(AVATAR_FULLSCREEN_MODE, false)) {
//                displayFullScreenAvatar();
//            }
        }
    }

    private void checkBlockStatus() {
        isUserIgnored();

    }

    private void startRoom(boolean isCall) {
        this.isCall = isCall;
        String existingRoomId;
//        enableProgressBarView(CommonActivityUtils.UTILS_DISPLAY_PROGRESS_BAR);
        if (null != (existingRoomId = RoomUtils.isDirectChatRoomAlreadyExist(mUser.user_id, mSession))) {
            openRoom(mSession.getDataHandler().getRoom(existingRoomId), isCall);
        } else {
            // direct message flow
            mSession.createDirectMessageRoom(mUser.user_id, mCreateDirectMessageCallBack);
        }
    }

    private int getTextColor() {
        return VectorUtils.getTextColor(this);
    }

    private int getBackgroundColor() {
        return VectorUtils.getBackgroundColor(this);
    }

    private void setupUserLanguagesFragment(String userId) {
        languageSettingsHandler = new LanguageSettingsHandler();
        languageSettingsHandler.setActivity(this);
        languageSettingsHandler.setUserId(userId);
        languageSettingsHandler.setMyUid(mSession.getCredentials().userId);
        languageSettingsHandler.setUserToken(mSession.getCredentials().accessToken);
        languageSettingsHandler.setBackgroundColor(getBackgroundColor());
        languageSettingsHandler.setTextColor(getTextColor());
        languageSettingsHandler.init();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if ((KeyEvent.KEYCODE_BACK == keyCode)) {
//            if (View.VISIBLE == mFullMemberAvatarLayout.getVisibility()) {
//                hideFullScreenAvatar();
//                return true;
//            } else if ((View.VISIBLE == mDevicesListView.getVisibility())) {
//                setScreenDevicesListVisibility(View.GONE);
//                return true;
//            }
//        }

        return super.onKeyDown(keyCode, event);
    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
//        savedInstanceState.putBoolean(AVATAR_FULLSCREEN_MODE, View.VISIBLE == mFullMemberAvatarLayout.getVisibility());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

//        if (savedInstanceState.getBoolean(AVATAR_FULLSCREEN_MODE, false)) {
//            displayFullScreenAvatar();
//        }
    }

    /**
     * Retrieve all the state values required to run the activity.
     * If values are not provided in the intent or are some are
     * null, then the activity can not continue to run and must be finished
     *
     * @return true if init succeed, false otherwise
     */
    private boolean initContextStateValues() {
        Intent intent = getIntent();
        boolean isParamInitSucceed = false;

        if (null != intent) {
            if (null == (mMemberId = intent.getStringExtra(EXTRA_MEMBER_ID))) {
                Log.e(LOG_TAG, "member ID missing in extra");
                return false;
            } else if (null == (mSession = getSession(this, intent))) {
                Log.e(LOG_TAG, "Invalid session");
                return false;
            }

            int storeIndex = intent.getIntExtra(EXTRA_STORE_ID, -1);
            IMXStore store;

            if (storeIndex >= 0) {
                store = Matrix.getInstance(this).getTmpStore(storeIndex);
            } else {
                store = mSession.getDataHandler().getStore();

                if (refreshUser()) {
                    intent.removeExtra(EXTRA_ROOM_ID);
                }
            }

            String roomId = intent.getStringExtra(EXTRA_ROOM_ID);

            if ((null != roomId) && (null == (mRoom = store.getRoom(roomId)))) {
                Log.e(LOG_TAG, "The room is not found");
            } else {
                // Everything is OK
                Log.d(LOG_TAG, "Parameters init succeed");
                isParamInitSucceed = true;
            }
        }

        return isParamInitSucceed;
    }

    /**
     * Search if the member is present in the list of the members of
     * the room
     *
     * @return true if member was found in the room , false otherwise
     */
    private boolean checkRoomMemberStatus() {
        mRoomMember = null;

        if (null != mRoom) {
            // find out the room member
            Collection<RoomMember> members = mRoom.getMembers();
            for (RoomMember member : members) {
                if (member.getUserId().equals(mMemberId)) {
                    mRoomMember = member;
                    break;
                }
            }
        }

        return (null == mRoom) || (null != mRoomMember);
    }

    /**
     * Refresh the user information
     *
     * @return true if the user is not a known one
     */
    private boolean refreshUser() {
        mUser = mSession.getDataHandler().getUser(mMemberId);

        // build a tmp user from the data provided as parameters
        if (null == mUser) {
            mUser = new User();
            mUser.user_id = mMemberId;
            mUser.displayname = getIntent().getStringExtra(EXTRA_MEMBER_DISPLAY_NAME);
            if (TextUtils.isEmpty(mUser.displayname)) {
                mUser.displayname = mMemberId;
            }
            mUser.avatar_url = getIntent().getStringExtra(EXTRA_MEMBER_AVATAR_URL);
            return true;
        }
        return false;
    }

    /**
     * Update the UI
     */
    private void updateUi() {
        if (!mSession.isAlive()) {
            Log.e(LOG_TAG, "updateUi : the session is not anymore valid");
            return;
        }

        if (null != mMemberNameTextView) {
//            if ((null != mRoomMember) && !TextUtils.isEmpty(mRoomMember.displayname)) {
//                mMemberNameTextView.setText(mRoomMember.displayname);
//            } else {
            refreshUser();
            mMemberNameTextView.setText(VectorUtils.getUserDisplayName(mUser.user_id, mSession, this));
//            }

            // do not display the activity name in the action bar
            setTitle("");
        }

        // disable the progress bar
        enableProgressBarView(CommonActivityUtils.UTILS_HIDE_PROGRESS_BAR);

        // UI updates
        updateMemberAvatarUi();
        updatePresenceInfoUi();
        updateDoNotDisturbUi();
        mSaveMediaMessageSwitcher.setChecked(PreferencesManager.isSaveMediaForRoomAllow(this, roomId));


        // Update adapter list view:
        // notify the list view to update the items that could
        // have changed according to the new rights of the member
//        updateAdapterListViewItems();
//        if (null != mListViewAdapter) {
//            mListViewAdapter.notifyDataSetChanged();
//        }
    }

    private void updateDoNotDisturbUi() {
        if (PreferencesManager.isInDNDModeForUser(this, mUser.user_id)) {
            mDoNotDisturbToolbarIndicator.setVisibility(View.VISIBLE);
            mDoNotDisturbSectionIndicator.setVisibility(View.VISIBLE);
        } else {
            mDoNotDisturbStartTime.setText(R.string.member_details_off);
            mDoNotDisturbFinishTime.setText("");
            mDoNotDisturbToolbarIndicator.setVisibility(View.GONE);
            mDoNotDisturbSectionIndicator.setVisibility(View.GONE);
        }
        long startDNDTimeForUser = PreferencesManager.getStartDNDTimeForUser(this, mUser.user_id);
        if (startDNDTimeForUser > 0) {
            int startHour = (int) TimeUnit.MILLISECONDS.toHours(startDNDTimeForUser);
            int startMin = (int) TimeUnit.MILLISECONDS.toMinutes(startDNDTimeForUser - TimeUnit.HOURS.toMillis(startHour));
            mDoNotDisturbStartTime.setText(String.format("%02d:%02d", startHour, startMin));
        }
        long endDNDTimeForUser = PreferencesManager.getEndDNDTimeForUser(this, mUser.user_id);
        if (endDNDTimeForUser > 0) {
            int endHour = (int) TimeUnit.MILLISECONDS.toHours(endDNDTimeForUser);
            int endMin = (int) TimeUnit.MILLISECONDS.toMinutes(endDNDTimeForUser - TimeUnit.HOURS.toMillis(endHour));
            mDoNotDisturbFinishTime.setText(String.format("%02d:%02d", endHour, endMin));
            mDoNotDisturbSeparator.setVisibility(View.VISIBLE);
            Calendar calendar = Calendar.getInstance();
            int currentHour = calendar.get(Calendar.HOUR_OF_DAY);
            int currentMin = calendar.get(Calendar.MINUTE);
            mDoNotDisturbCurrentTime.setText(String.format("%02d:%02d", currentHour, currentMin));
        }
    }

    private void updatePresenceInfoUi() {
        // sanity check
        if (null != mPresenceTextView) {
            mPresenceTextView.setText(VectorUtils.getUserOnlineStatus(this, mSession, mMemberId, new SimpleApiCallback<Void>() {
                @Override
                public void onSuccess(Void info) {
                    mPresenceTextView.setText(VectorUtils.getUserOnlineStatus(VectorMemberDetailsActivity.this, mSession, mMemberId, null));
                }
            }));
            if (mPresenceTextView.getText().toString().equals("")) {
                mPresenceTextView.setVisibility(View.GONE);
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mMemberNameTextView.getLayoutParams();
                lp.addRule(RelativeLayout.CENTER_VERTICAL);
                mMemberNameTextView.setLayoutParams(lp);
            }
        }
    }

    /**
     * update the profile avatar
     */
    private void updateMemberAvatarUi() {
        //  Log.d("---", "updateMemeberAvatar");
        if (null != mMemberAvatarImageView) {
            // use the room member if it exists
            if (null != mRoomMember) {
                String displayname = mRoomMember.displayname;
                String avatarUrl = mRoomMember.getAvatarUrl();

                // if there is no avatar or displayname , try to find one from the known user
                // it is always better than the vector avatar or the matrid id.
                if (TextUtils.isEmpty(avatarUrl) || TextUtils.isEmpty(displayname)) {
                    if (null != mUser) {
                        if (TextUtils.isEmpty(avatarUrl)) {
                            avatarUrl = mUser.avatar_url;
                        }

                        if (TextUtils.isEmpty(displayname)) {
                            displayname = mUser.displayname;
                        }
                    }
                }
                VectorUtils.loadProfileAvatar(this, mSession, mMemberAvatarImageView, avatarUrl, mRoomMember.getUserId());
            } else {
                // use the user if it is defined
                if (null != mUser) {
                    VectorUtils.loadProfileAvatar(this, mSession, mMemberAvatarImageView, mUser.getAvatarUrl(), mUser.user_id);
                } else {
                    // default avatar
                    VectorUtils.loadProfileAvatar(this, mSession, mMemberAvatarImageView, null, null);
                }
            }
        }
    }

    public TimePickerDialog createTimePickerDialog() {
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        // Create a new instance of TimePickerDialog and return it
        TimePickerDialog timePickerDialog = new TimePickerDialog(this, t, hour, minute,
                DateFormat.is24HourFormat(this));
        timePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                mDoNotDisturbStartTime.setText(R.string.member_details_off);
                mDoNotDisturbFinishTime.setText("");
                PreferencesManager.stopDNDModeForUser(VectorMemberDetailsActivity.this, mUser.user_id);
                mDoNotDisturbToolbarIndicator.setVisibility(View.GONE);
                mDoNotDisturbSectionIndicator.setVisibility(View.GONE);
                mDoNotDisturbSeparator.setVisibility(View.GONE);
                mDoNotDisturbCurrentTime.setVisibility(View.GONE);
            }
        });
        return timePickerDialog;
    }

    TimePickerDialog.OnTimeSetListener t = new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            if (wasStartTimeClick) {
                long start = TimeUnit.HOURS.toMillis(hourOfDay) + TimeUnit.MINUTES.toMillis(minute);
                PreferencesManager.saveStartDNDTimeForUser(start, VectorMemberDetailsActivity.this, mUser.user_id);
                if (mDoNotDisturbStartTime.getText().toString().equals(getResources().getString(R.string.member_details_off)))
                    mDoNotDisturbFinishTime.performClick();
            } else {
                long end = TimeUnit.HOURS.toMillis(hourOfDay) + TimeUnit.MINUTES.toMillis(minute);
                PreferencesManager.saveEndDNDTimeForUser(end, VectorMemberDetailsActivity.this, mUser.user_id);
            }
            updateDoNotDisturbUi();
        }
    };

    public void showTimePickerDialog(View v) {
        Dialog timePickerDialog = createTimePickerDialog();
        switch (v.getId()) {
            case R.id.do_not_disturb_section_start:
                timePickerDialog.setTitle(R.string.member_details_start_time);
                timePickerDialog.show();
                break;
            case R.id.do_not_disturb_section_finish:
                timePickerDialog.setTitle(R.string.member_details_end_time);
                timePickerDialog.show();
                break;
        }
    }

    /**
     * Helper method to enable/disable the progress bar view used when a
     * remote server action is on progress.
     *
     * @param aIsProgressBarDisplayed true to show the progress bar screen, false to hide it
     */
    private void enableProgressBarView(boolean aIsProgressBarDisplayed) {
        if (null != mProgressBarView) {
            mProgressBarView.setVisibility(aIsProgressBarDisplayed ? View.VISIBLE : View.GONE);
        }
    }

    //================================================================================
    // Room creation
    //================================================================================

    /**
     * Open the selected room
     *
     * @param room
     */
    void openRoom(final Room room, boolean isCall) {
        // sanity checks
        // reported by GA
        if ((null == mSession.getDataHandler()) || (null == mSession.getDataHandler().getStore())) {
            return;
        }

        final String roomId;
        // cannot join a leaving room
        if (room == null || room.isLeaving()) {
            roomId = null;
        } else {
            roomId = room.getRoomId();
        }

        if (roomId != null) {
            openRoomById(roomId, isCall);
        }
    }

    private void openRoomById(String roomId, boolean isCall) {
        Room room = mSession.getDataHandler().getRoom(roomId);
        this.roomId = roomId;
        final RoomSummary roomSummary = mSession.getDataHandler().getStore().getSummary(roomId);
        if (null != roomSummary) {
            room.sendReadReceipt();
        }
        // Update badge unread count in case device is offline
        CommonActivityUtils.specificUpdateBadgeUnreadCount(mSession, this);
        if (isCall) {
            startCheckCallPermissions(false);
        } else {
            Intent intent = new Intent(this, VectorRoomActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.putExtra(EXTRA_ROOM_ID, roomId);
            startActivity(intent);
        }
        finish();
    }


    @Override
    protected void onPause() {
        super.onPause();

        if (null != mSession) {
            if (null != mRoom) {
                mRoom.removeEventListener(mLiveEventsListener);
            }

            mSession.getDataHandler().removeListener(mPresenceEventsListener);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (null != mSession) {
            if (null != mRoom) {
                mRoom.addEventListener(mLiveEventsListener);
            }
            mSession.getDataHandler().addListener(mPresenceEventsListener);

//            updateAdapterListViewItems();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (null != mSession) {
            if (null != mRoom) {
                mRoom.removeEventListener(mLiveEventsListener);
            }

            mSession.getDataHandler().removeListener(mPresenceEventsListener);
        }
    }

    // ********* IDevicesAdapterListener implementation *********

//    private final ApiCallback<Void> mDevicesVerificationCallback = new ApiCallback<Void>() {
//        @Override
//        public void onSuccess(Void info) {
//            mDevicesListViewAdapter.notifyDataSetChanged();
//        }
//
//        @Override
//        public void onNetworkError(Exception e) {
//            mDevicesListViewAdapter.notifyDataSetChanged();
//        }
//
//        @Override
//        public void onMatrixError(MatrixError e) {
//            mDevicesListViewAdapter.notifyDataSetChanged();
//        }
//
//        @Override
//        public void onUnexpectedError(Exception e) {
//            mDevicesListViewAdapter.notifyDataSetChanged();
//        }
//    };

//    @Override
//    public void OnVerifyDeviceClick(MXDeviceInfo aDeviceInfo) {
//        switch (aDeviceInfo.mVerified) {
//            case MXDeviceInfo.DEVICE_VERIFICATION_VERIFIED:
//                mSession.getCrypto().setDeviceVerification(MXDeviceInfo.DEVICE_VERIFICATION_UNVERIFIED, aDeviceInfo.deviceId, mMemberId, mDevicesVerificationCallback);
//                break;
//
//            case MXDeviceInfo.DEVICE_VERIFICATION_UNVERIFIED:
//            default: // Blocked
//                CommonActivityUtils.displayDeviceVerificationDialog(aDeviceInfo, mMemberId, mSession, this, mDevicesVerificationCallback);
//                break;
//        }
//    }

//    @Override
//    public void OnBlockDeviceClick(MXDeviceInfo aDeviceInfo) {
//        if (aDeviceInfo.mVerified == MXDeviceInfo.DEVICE_VERIFICATION_BLOCKED) {
//            mSession.getCrypto().setDeviceVerification(MXDeviceInfo.DEVICE_VERIFICATION_UNVERIFIED, aDeviceInfo.deviceId, aDeviceInfo.userId, mDevicesVerificationCallback);
//        } else {
//            mSession.getCrypto().setDeviceVerification(MXDeviceInfo.DEVICE_VERIFICATION_BLOCKED, aDeviceInfo.deviceId, aDeviceInfo.userId, mDevicesVerificationCallback);
//        }
//
//        mDevicesListViewAdapter.notifyDataSetChanged();
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // the application is in a weird state
        // GA : mSession is null
        if (CommonActivityUtils.shouldRestartApp(this) || (null == mSession)) {
            return false;
        }
        getMenuInflater().inflate(R.menu.menu_member_details, menu);
        CommonActivityUtils.tintMenuIcons(menu, ThemeUtils.getColor(this, R.attr.icon_tint_on_dark_action_bar_color));
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        Intent intent = getIntent();
        if (intent != null) {
            Uri lookupUri = intent.getParcelableExtra(EXTRA_LOOKUP_URI);
            if (lookupUri == null) {
                ContactHandler contactHandler = new ContactHandler();
                TalkerContact contact = contactHandler.getContactFromTalkerContactSnapshot(mUser.user_id, this);
                if (contact == null || TextUtils.isEmpty(contact.lookupUri))
                    menu.findItem(R.id.ic_action_edit_profile).setVisible(false);
            }
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.ic_action_edit_profile:
                Intent intent = getIntent();
                if (intent != null) {
                    Uri lookupUri = intent.getParcelableExtra(EXTRA_LOOKUP_URI);
                    if (lookupUri != null) {
                        Intent editIntent = new Intent(Intent.ACTION_EDIT);
                        editIntent.setDataAndType(lookupUri, ContactsContract.Contacts.CONTENT_ITEM_TYPE);
                        editIntent.putExtra("finishActivityOnSaveCompleted", true);
                        startActivity(editIntent);
                    } else {
                        ContactHandler contactHandler = new ContactHandler();
                        TalkerContact contact = contactHandler.getContactFromTalkerContactSnapshot(mUser.user_id, this);
                        if (contact != null && !TextUtils.isEmpty(contact.lookupUri)) {
                            Intent editIntent = new Intent(Intent.ACTION_EDIT);
                            editIntent.setDataAndType(Uri.parse(contact.lookupUri), ContactsContract.Contacts.CONTENT_ITEM_TYPE);
                            editIntent.putExtra("finishActivityOnSaveCompleted", true);
                            startActivity(editIntent);
                        }

                    }
                }
                return true;
            case R.id.ic_action_export:
                try {
                    RoomUtils.exportChat(roomId, mSession, this, mUser.user_id);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.ic_action_clear:
                String existingRoomId;
                if (null != (existingRoomId = RoomUtils.isDirectChatRoomAlreadyExist(mUser.user_id, mSession))) {
                    //    RoomUtils.clearChat(mSession.getDataHandler().getRoom(existingRoomId));
                    mSession.getDataHandler().getStore().getSummary(existingRoomId).setDeletionTime(System.currentTimeMillis());
                    Toast.makeText(this, "Chat was cleared", Toast.LENGTH_LONG).show();
                } else
                    Toast.makeText(this, "Can't find direct chat with this contact", Toast.LENGTH_LONG).show();
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void openImage() {
        //   Log.d("---", "openImage");
        ArrayList<SlidableMediaInfo> mediaMessagesList = listSlidableMessages();
        if (mediaMessagesList != null) {
            Intent viewImageIntent = new Intent(this, VectorMediasViewerActivity.class);
            viewImageIntent.putExtra(VectorMediasViewerActivity.EXTRA_MATRIX_ID, mSession.getCredentials().userId);
            viewImageIntent.putExtra(VectorMediasViewerActivity.KEY_INFO_LIST, mediaMessagesList);
            viewImageIntent.putExtra(VectorMediasViewerActivity.KEY_INFO_LIST_INDEX, 0);
            startActivity(viewImageIntent);
        }

    }

    private ArrayList<SlidableMediaInfo> listSlidableMessages() {
        ArrayList<SlidableMediaInfo> res = new ArrayList<>();
        TalkerContact contact = new ContactHandler().getContactFromTalkerContactSnapshot(mUser.user_id, this);
        if (null == contact) {
            Log.d("---", "contact NULL  User = " + mUser.user_id);
            return null;
        }
        SlidableMediaInfo info = new SlidableMediaInfo();
        info.mMessageType = Message.MSGTYPE_IMAGE;
        info.mFileName = contact.getDisplayName();
        if (mSession.getDataHandler().getUser(mUser.user_id) != null && mSession.getDataHandler().getUser(mUser.user_id).getAvatarUrl() != null)
            info.mMediaUrl = mSession.getDataHandler().getUser(mUser.user_id).getAvatarUrl();
        else if (contact.getPhotoUri() != null)
            info.mMediaUrl = contact.getPhotoUri();
        else
            return null;
        res.add(info);
        return res;
    }

}
