package com.talkermessenger.activity;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iskar.talkersdk.FullscreenTranslationHandler;
import com.iskar.talkersdk.TranslationHistoryItemHandler;
import com.iskar.talkersdk.callback.TranslationInterface;
import com.iskar.talkersdk.model.Translation;
import com.iskar.talkersdk.model.TranslationHistoryItem;
import com.iskar.talkersdk.ui.LanguageSelectorDialog;
import com.iskar.talkersdk.ui.languageRecycler.Languages;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.talkermessenger.R;
import com.talkermessenger.util.PreImEditText;
import com.talkermessenger.util.PreferencesManager;
import com.talkermessenger.util.ThemeUtils;
import com.talkermessenger.util.VectorUtils;

import org.matrix.androidsdk.MXSession;
import org.matrix.androidsdk.util.Log;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

public class TranslatorActivity extends AppCompatActivity implements TextView.OnEditorActionListener {

    @BindView(R.id.source_language)
    TextView sourceLanguageView;

    @BindView(R.id.target_language)
    TextView targetLanguageView;

    @BindView(R.id.switch_languages)
    ImageView switchLanguagesView;

    @BindView(R.id.arrow_left)
    ImageView leftArrow;

    @BindView(R.id.arrow_right)
    ImageView rightArrow;

    @BindView(R.id.original_text_language_code)
    TextView originalTextLanguageCodeView;

    @BindView(R.id.original_text)
    TextView originalTextView;

    private static int TRANSLATION_HISTORY = 1999;

    @BindView(R.id.translated_text_voice)
    ImageView translatedTextVoiceView;

    @BindView(R.id.translated_text_language_code)
    TextView translatedTextLanguageCodeView;

    @BindView(R.id.translated_text)
    TextView translatedTextView;

    @BindView(R.id.share)
    ImageView shareView;

    @BindView(R.id.planet)
    ImageView planetView;

    @BindView(R.id.copy)
    ImageView copyView;

    @BindView(R.id.save)
    ImageView saveView;

    @BindView(R.id.cards_switcher)
    ImageView cardSwitcher;

    @BindView(R.id.share_top)
    ImageView shareView_top;

    @BindView(R.id.planet_top)
    ImageView planetView_top;

    @BindView(R.id.copy_top)
    ImageView copyView_top;

    @BindView(R.id.save_top)
    ImageView saveView_top;

    @BindView(R.id.cards_switcher_top)
    ImageView cardSwitcher_top;


    @BindView(R.id.account_toolbar)
    Toolbar mToolbar;

    @BindView(R.id.source_text)
    PreImEditText sourceTextView;

    @BindView(R.id.cards_layout)
    LinearLayout cardsLayout;

    @BindView(R.id.dual_translation_card)
    CardView dualTranslationCard;

    @BindView(R.id.translation_card)
    CardView translationCard;


    @BindView(R.id.input_layout)
    LinearLayout inputLayout;

    FullscreenTranslationHandler fullscreenTranslationHandler;
    private Translation translationResponse;
    private Translation previousTranslationResponse;
    boolean isTwoCardsVisible = true;
    private int softKeyboardHeight;
    private String sourceLanguage = "ru";
    private String targetLanguage = "en";


    @SuppressLint("CheckResult")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_translator);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        PreImEditText.OnKeyPreImeListener onKeyPreImeListener = new PreImEditText.OnKeyPreImeListener() {
            @Override
            public void onBackPressed() {
                TranslatorActivity.this.onBackPressed();
            }
        };
        switch (ThemeUtils.getApplicationTheme(this)) {
            case ThemeUtils.THEME_LIGHT_LIGHT_BLUE_HEADER_VALUE:
            case ThemeUtils.THEME_LIGHT_BLACK_STATUSBAR_VALUE:
                inputLayout.setBackground(getResources().getDrawable(R.drawable.bubble_send_white));
                break;
            case ThemeUtils.THEME_BLACK_VALUE:
                inputLayout.setBackground(getResources().getDrawable(R.drawable.bubble_send_black));
                break;
            case ThemeUtils.THEME_DARK_VALUE:
                inputLayout.setBackground(getResources().getDrawable(R.drawable.bubble_send_dark));
                break;
        }
        sourceTextView.setOnKeyPreImeListener(onKeyPreImeListener);
        sourceTextView.setOnEditorActionListener(this);
        showKeyboard();

        sourceLanguageView.setOnClickListener(v -> showLanguageSelectionDialog(true));
        targetLanguageView.setOnClickListener(v -> showLanguageSelectionDialog(false));
        MXSession mxSession = com.talkermessenger.Matrix.getInstance(this).getDefaultSession();
        fullscreenTranslationHandler =
                new FullscreenTranslationHandler(this, mxSession.getMyUserId(),
                        mxSession.getMyUser().getDisplayname(), mxSession.getCredentials().accessToken, true);
        sourceTextView.setText(PreferencesManager.getTranslatorString(this));

        PreferencesManager.setLanguageCodes(TranslatorActivity.this, sourceLanguage, targetLanguage);
        if (sourceTextView.getText().length() > 0) {
            sourceTextView.setSelection(sourceTextView.getText().length());
            makeTranslation();
        }
        switchLanguagesView.setOnClickListener(v -> {
            String originLanguage = sourceLanguageView.getText().toString();
            String originText = sourceTextView.getText() + "";
            String tempCode = sourceLanguage;
            sourceLanguage = targetLanguage;
            targetLanguage = tempCode;

            sourceLanguageView.setText(targetLanguageView.getText().toString());
            originalTextLanguageCodeView.setText(targetLanguage);
            targetLanguageView.setText(originLanguage);
            translatedTextLanguageCodeView.setText(tempCode);
            originalTextView.setText(translatedTextView.getText().toString());
            sourceTextView.setText(translatedTextView.getText().toString());
            if (sourceTextView.getText().length() > 0)
                sourceTextView.setSelection(sourceTextView.getText().length());
            translatedTextView.setText(originText);
            PreferencesManager.setLanguageCodes(TranslatorActivity.this, sourceLanguage, targetLanguage);
            makeTranslation();
        });
        sourceTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                PreferencesManager.setTranslatorString(TranslatorActivity.this, s.toString());
            }
        });

        RxTextView.textChanges(sourceTextView).debounce(1, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                .subscribe(text -> {
                    if (text.length() > 0) {
                        makeTranslation();
                    }
                });

        shareView.setOnClickListener(v -> {
            if (translatedTextView.getText().length() > 0) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, translatedTextView.getText().toString());
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });

        copyView.setOnClickListener(v -> {
            ClipboardManager clipboard = (ClipboardManager) TranslatorActivity.this.getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("translated text", translatedTextView.getText().toString());
            clipboard.setPrimaryClip(clip);
        });

        saveView.setOnClickListener(v -> {
            if (originalTextView.getText().length() > 0 && translatedTextView.getText().length() > 0) {
                TranslationHistoryItemHandler handler = new TranslationHistoryItemHandler(TranslatorActivity.this);
                handler.addTranslationHistoryItem(new TranslationHistoryItem(originalTextView.getText().toString(),
                        originalTextLanguageCodeView.getText().toString(), translatedTextView.getText().toString(),
                        translatedTextLanguageCodeView.getText().toString(), sourceLanguageView.getText().toString(),
                        targetLanguageView.getText().toString()));
            }
        });

        planetView.setOnClickListener(v -> {
            String query = "";
            if (translatedTextView.getText().length() > 0) {
                try {
                    query = URLEncoder.encode(translatedTextView.getText().toString(), "utf-8");
                    String url = "http://www.google.com/search?q=" + query;
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(url));
                    startActivity(intent);

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        });


        shareView_top.setOnClickListener(v -> {
            if (translatedTextView.getText().length() > 0) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, translatedTextView.getText().toString());
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });

        copyView_top.setOnClickListener(v -> {
            ClipboardManager clipboard = (ClipboardManager) TranslatorActivity.this.getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("translated text", translatedTextView.getText().toString());
            clipboard.setPrimaryClip(clip);
        });

        saveView_top.setOnClickListener(v -> {
            if (originalTextView.getText().length() > 0 && translatedTextView.getText().length() > 0) {
                TranslationHistoryItemHandler handler = new TranslationHistoryItemHandler(TranslatorActivity.this);
                handler.addTranslationHistoryItem(new TranslationHistoryItem(originalTextView.getText().toString(),
                        originalTextLanguageCodeView.getText().toString(), translatedTextView.getText().toString(),
                        translatedTextLanguageCodeView.getText().toString(), sourceLanguageView.getText().toString(),
                        targetLanguageView.getText().toString()));
            }
        });

        planetView_top.setOnClickListener(v -> {
            String query = "";
            if (translatedTextView.getText().length() > 0) {
                try {
                    query = URLEncoder.encode(translatedTextView.getText().toString(), "utf-8");
                    String url = "http://www.google.com/search?q=" + query;
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(url));
                    startActivity(intent);

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        });
        cardSwitcher.setOnClickListener(v -> {
            if (isTwoCardsVisible) {
                translationCard.setVisibility(GONE);
                findViewById(R.id.extras_layout_top).setVisibility(VISIBLE);
                cardSwitcher.setImageResource(R.drawable.two_cards);
                cardSwitcher_top.setImageResource(R.drawable.two_cards);
                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) dualTranslationCard.getLayoutParams();
                layoutParams.height = LinearLayout.LayoutParams.WRAP_CONTENT;
                layoutParams.weight = 0;
                dualTranslationCard.setLayoutParams(layoutParams);
                originalTextLanguageCodeView.setVisibility(VISIBLE);
                isTwoCardsVisible = false;
            } else {
                translationCard.setVisibility(View.VISIBLE);
                findViewById(R.id.extras_layout_top).setVisibility(INVISIBLE);
                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) dualTranslationCard.getLayoutParams();
                layoutParams.height = LinearLayout.LayoutParams.MATCH_PARENT;
                layoutParams.weight = 1;
                dualTranslationCard.setLayoutParams(layoutParams);
                originalTextLanguageCodeView.setVisibility(GONE);
                cardSwitcher.setImageResource(R.drawable.one_card);
                cardSwitcher_top.setImageResource(R.drawable.one_card);
                isTwoCardsVisible = true;
            }
        });
        cardSwitcher_top.setOnClickListener(v -> {
            if (isTwoCardsVisible) {
                translationCard.setVisibility(GONE);
                findViewById(R.id.extras_layout_top).setVisibility(VISIBLE);
                cardSwitcher.setImageResource(R.drawable.two_cards);
                cardSwitcher_top.setImageResource(R.drawable.two_cards);
                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) dualTranslationCard.getLayoutParams();
                layoutParams.height = LinearLayout.LayoutParams.WRAP_CONTENT;
                layoutParams.weight = 0;
                dualTranslationCard.setLayoutParams(layoutParams);
                originalTextLanguageCodeView.setVisibility(VISIBLE);
                isTwoCardsVisible = false;
            } else {
                translationCard.setVisibility(View.VISIBLE);
                findViewById(R.id.extras_layout_top).setVisibility(INVISIBLE);
                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) dualTranslationCard.getLayoutParams();
                layoutParams.height = LinearLayout.LayoutParams.MATCH_PARENT;
                layoutParams.weight = 1;
                dualTranslationCard.setLayoutParams(layoutParams);
                originalTextLanguageCodeView.setVisibility(GONE);
                cardSwitcher.setImageResource(R.drawable.one_card);
                cardSwitcher_top.setImageResource(R.drawable.one_card);
                isTwoCardsVisible = true;
            }
        });
        translatedTextView.setMovementMethod(new ScrollingMovementMethod());
        originalTextView.setMovementMethod(new ScrollingMovementMethod());
        setOnclickListeners();
    }

    @Override
    protected void onResume() {
        super.onResume();
        showKeyboard();
    }

    private void makeTranslation() {
        CharSequence text = sourceTextView.getText();
        if (text.length() > 0) {
            Log.e("languages_translate", sourceLanguage + " " + targetLanguage);
            fullscreenTranslationHandler.makeTranslation(sourceLanguage, targetLanguage, text.toString(), new TranslationInterface() {
                @Override
                public void onTranslate(Translation translationResponse) {
                    TranslatorActivity.this.translationResponse = translationResponse;
                    updateViews();
                }

                @Override
                public void onFailure(String msg) {
                    if (msg.equals("payment")) {
                        try {
                            PreferencesManager.setPaymentTimeout(TranslatorActivity.this, System.currentTimeMillis());
                          /*  new AlertDialog.Builder(TranslatorActivity.this)
                                    .setMessage(R.string.payment_required)
                                    .setPositiveButton(getString(R.string.yes), (dialog, which) ->
                                            startActivity(new Intent(TranslatorActivity.this, PurchasesSettingsActivity.class)))
                                    .setNegativeButton(getString(R.string.no), (dialog, which) -> dialog.dismiss())
                                    .create().show();*/
                        } catch (Exception e) {
                        }
                    }
                }
            });
            originalTextView.setMovementMethod(new ScrollingMovementMethod());
            translatedTextView.setMovementMethod(new ScrollingMovementMethod());
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void showLanguageSelectionDialog(boolean isOriginalLanguageChange) {
        LanguageSelectorDialog languageSelectorDialog = new LanguageSelectorDialog(this,
                getTextColor(), getBackgroundColor(), getLanguages(), true);
        languageSelectorDialog.show();
        languageSelectorDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (languageSelectorDialog.chosenLanguageCode != null)
                    if (isOriginalLanguageChange) {
                        sourceLanguageView.setText(languageSelectorDialog.chosenLanguage);
                        originalTextLanguageCodeView.setText(languageSelectorDialog.chosenLanguageCode);
                        sourceLanguage = languageSelectorDialog.chosenLanguageCode;
                        //   makeTranslation();
                    } else {
                        targetLanguageView.setText(languageSelectorDialog.chosenLanguage);
                        translatedTextLanguageCodeView.setText(languageSelectorDialog.chosenLanguageCode);
                        targetLanguage = languageSelectorDialog.chosenLanguageCode;
                        //   makeTranslation();
                    }
                PreferencesManager.setLanguageCodes(TranslatorActivity.this, sourceLanguage, targetLanguage);
            }
        });
    }

    private ArrayList<Languages> getLanguages() {
        //  return VectorUtils.getLanguages(Objects.requireNonNull(this));
        Log.d("---", "Locale: " + Locale.getDefault().getLanguage());
        //return VectorUtils.getLanguages(Objects.requireNonNull(this), Locale.getDefault().toString());
        return VectorUtils.getLanguages(Objects.requireNonNull(this), Locale.getDefault().getLanguage());
    }

    private int getTextColor() {
        return VectorUtils.getTextColor(Objects.requireNonNull(this));
    }

    private int getBackgroundColor() {
        return VectorUtils.getBackgroundColor(Objects.requireNonNull(this));
    }

    private boolean isDualTranslationCorrect() {
        return translationResponse.getDualTranslation().get(0) != null &&
                translationResponse.getDualTranslation().get(0).toLowerCase().replaceAll("\\s+", "")
                        .equals(sourceTextView.getText().toString().toLowerCase().replaceAll("\\s+", ""));
    }

    private void updateViews() {
        if (translationResponse != null) {
            originalTextView.setText(translationResponse.getDualTranslation().get(0));
            originalTextLanguageCodeView.setText(translationResponse.getSourceLang());
            translatedText = false;
            translatedTextView.setText(translationResponse.getTranslation().get(0));
            translatedTextLanguageCodeView.setText(translationResponse.getTargetLangs().get(0).getLanguage());
            if (!isDualTranslationCorrect()) {
                rightArrow.setVisibility(View.VISIBLE);
            } else rightArrow.setVisibility(View.GONE);
        }
    }

    private boolean translatedText = false;

    private void setOnclickListeners() {
        TextView translationTextWindow = findViewById(R.id.original_text);
        ImageView leftArrow = findViewById(R.id.arrow_left);
        ImageView rightArrow = findViewById(R.id.arrow_right);
        leftArrow.setOnClickListener(v -> {
            if (previousTranslationResponse != null) {
                translationResponse = previousTranslationResponse;
                translationTextWindow.setText(translationResponse.getDualTranslation().get(0));
                sourceTextView.setText(translationResponse.getSourceText());
                leftArrow.setVisibility(GONE);
            }
            previousTranslationResponse = null;
        });

        rightArrow.setOnClickListener(v -> {
            if (translationResponse != null) {
                previousTranslationResponse = translationResponse;
                leftArrow.setVisibility(View.VISIBLE);
                rightArrow.setVisibility(View.INVISIBLE);
                sourceTextView.setText(translationResponse.getDualTranslation().get(0));
            }
        });

        originalTextLanguageCodeView.setOnClickListener(v -> {
            if (translationResponse != null)
                if (!translatedText) {
                    translationTextWindow.setText(translationResponse.getTranslation().get(0));
                    leftArrow.setVisibility(GONE);
                    rightArrow.setVisibility(GONE);
                    originalTextLanguageCodeView.setText(translationResponse.getTargetLangs().get(0).getLanguage());
                    translatedText = true;
                } else {
                    translationTextWindow.setText(translationResponse.getDualTranslation().get(0));
                    originalTextLanguageCodeView.setText(translationResponse.getSourceLang());
                    if (previousTranslationResponse != null)
                        leftArrow.setVisibility(VISIBLE);
                    if (!isDualTranslationCorrect()) {
                        rightArrow.setVisibility(View.VISIBLE);
                    }
                    translatedText = false;
                }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == TRANSLATION_HISTORY && data != null) {
            originalTextLanguageCodeView.setText(data.getStringExtra("sourceLang"));
            originalTextView.setText(data.getStringExtra("sourceText"));
            translatedTextLanguageCodeView.setText(data.getStringExtra("translatedLang"));
            translatedTextView.setText(data.getStringExtra("translatedText"));
            sourceLanguageView.setText(data.getStringExtra("sourceLangName"));
            targetLanguageView.setText(data.getStringExtra("translatedLangName"));
            sourceLanguage = data.getStringExtra("sourceLang");
            targetLanguage = data.getStringExtra("translatedLang");
            Log.e("languages", sourceLanguage + " " + targetLanguage);
            PreferencesManager.setLanguageCodes(this, sourceLanguage, targetLanguage);
        }
    }

    private void showKeyboard() {
        if (sourceTextView != null) {
            sourceTextView.requestFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(sourceTextView, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.ic_action_history:
                startActivityForResult(new Intent(TranslatorActivity.this, TranslationHistoryActivity.class), TRANSLATION_HISTORY);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_translation, menu);
        CommonActivityUtils.tintMenuIcons(menu, ThemeUtils.getColor(this, R.attr.actionbar_text_color));
        return true;
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        makeTranslation();
        return true;
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        showKeyboard();
    }
}
