/*
 * Copyright 2014 OpenMarket Ltd
 * Copyright 2017 Vector Creations Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.talkermessenger.activity;

import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.talkermessenger.Matrix;
import com.talkermessenger.MyPresenceManager;
import com.talkermessenger.R;
import com.talkermessenger.VectorApp;
import com.talkermessenger.activity.settings.NewSettingActivity;
import com.talkermessenger.fragments.AbsHomeFragment;
import com.talkermessenger.fragments.CallLogFragment;
import com.talkermessenger.fragments.FavouritesFragment;
import com.talkermessenger.fragments.HomeFragment;
import com.talkermessenger.fragments.PeopleFragment;
import com.talkermessenger.receiver.VectorUniversalLinkReceiver;
import com.talkermessenger.services.EventStreamService;
import com.talkermessenger.util.CallsManager;
import com.talkermessenger.util.PreferencesManager;
import com.talkermessenger.util.RoomUtils;
import com.talkermessenger.util.ThemeUtils;
import com.talkermessenger.view.UnreadCounterBadgeView;
import com.talkermessenger.view.VectorPendingCallView;

import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.UpdateManager;

import org.matrix.androidsdk.MXDataHandler;
import org.matrix.androidsdk.MXSession;
import org.matrix.androidsdk.call.IMXCall;
import org.matrix.androidsdk.crypto.data.MXDeviceInfo;
import org.matrix.androidsdk.crypto.data.MXUsersDevicesMap;
import org.matrix.androidsdk.data.MyUser;
import org.matrix.androidsdk.data.Room;
import org.matrix.androidsdk.data.RoomPreviewData;
import org.matrix.androidsdk.data.RoomState;
import org.matrix.androidsdk.data.RoomSummary;
import org.matrix.androidsdk.data.RoomTag;
import org.matrix.androidsdk.data.store.IMXStore;
import org.matrix.androidsdk.listeners.MXEventListener;
import org.matrix.androidsdk.rest.callback.ApiCallback;
import org.matrix.androidsdk.rest.callback.SimpleApiCallback;
import org.matrix.androidsdk.rest.model.Event;
import org.matrix.androidsdk.rest.model.MatrixError;
import org.matrix.androidsdk.util.Log;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Displays the main screen of the app, with rooms the user has joined and the ability to create
 * new rooms.
 */
public class VectorHomeActivity extends RiotAppCompatActivity implements SearchView.OnQueryTextListener {

    public static final String EXTRA_JUMP_TO_ROOM_PARAMS = "VectorHomeActivity.EXTRA_JUMP_TO_ROOM_PARAMS";
    // jump to a member details sheet
    public static final String EXTRA_MEMBER_ID = "VectorHomeActivity.EXTRA_MEMBER_ID";
    // there are two ways to open an external link
    // 1- EXTRA_UNIVERSAL_LINK_URI : the link is opened as soon there is an event check processed (application is launched when clicking on the URI link)
    // 2- EXTRA_JUMP_TO_UNIVERSAL_LINK : do not wait that an event chunk is processed.
    public static final String EXTRA_JUMP_TO_UNIVERSAL_LINK = "VectorHomeActivity.EXTRA_JUMP_TO_UNIVERSAL_LINK";
    public static final String EXTRA_WAITING_VIEW_STATUS = "VectorHomeActivity.EXTRA_WAITING_VIEW_STATUS";
    // call management
    // the home activity is launched to start a call.
    public static final String EXTRA_CALL_SESSION_ID = "VectorHomeActivity.EXTRA_CALL_SESSION_ID";
    public static final String EXTRA_CALL_ID = "VectorHomeActivity.EXTRA_CALL_ID";
    public static final String EXTRA_CALL_UNKNOWN_DEVICES = "VectorHomeActivity.EXTRA_CALL_UNKNOWN_DEVICES";
    // the home activity is launched in shared files mode
    // i.e the user tries to send several files with VECTOR
    public static final String EXTRA_SHARED_INTENT_PARAMS = "VectorHomeActivity.EXTRA_SHARED_INTENT_PARAMS";
    public static final boolean WAITING_VIEW_START = true;
    public static final String BROADCAST_ACTION_STOP_WAITING_VIEW = "im.vector.activity.ACTION_STOP_WAITING_VIEW";
    private static final String LOG_TAG = VectorHomeActivity.class.getSimpleName();
    private static final boolean WAITING_VIEW_STOP = false;
    private static final String TAG_FRAGMENT_HOME = "TAG_FRAGMENT_HOME";
    private static final String TAG_FRAGMENT_FAVOURITES = "TAG_FRAGMENT_FAVOURITES";
    private static final String TAG_FRAGMENT_PEOPLE = "TAG_FRAGMENT_PEOPLE";
    private static final String TAG_FRAGMENT_CALL_LOG = "TAG_FRAGMENT_CALL_LOG";
    private static final String TAG_FRAGMENT_TRANSLATOR = "TAG_FRAGMENT_TRANSLATOR";

    // Key used to restore the proper fragment after orientation change
    private static final String CURRENT_MENU_ID = "CURRENT_MENU_ID";
    //for viewPager pages
    private static final int CHATS = 0;
    private static final int CONTACTS = 1;
    private static final int CALLS = 2;
    private static final String NO_DEVICE_ID_WARNING_KEY = "NO_DEVICE_ID_WARNING_KEY";
    // shared instance
    // only one instance of VectorHomeActivity should be used.
    private static VectorHomeActivity sharedInstance = null;
    // Badge view <-> menu entry id
    private final Map<Integer, UnreadCounterBadgeView> mBadgeViewByIndex = new HashMap<>();
    @BindView(R.id.listView_spinner_views)
    View mWaitingView;
    private final BroadcastReceiver mBrdRcvStopWaitingView = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            stopWaitingView();
        }
    };
    @BindView(R.id.floating_action_button)
    FloatingActionButton mFloatingActionButton;
    @BindView(R.id.home_toolbar)
    Toolbar mToolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    // calls
    @BindView(R.id.listView_pending_callview)
    VectorPendingCallView mVectorPendingCallView;
    @BindView(R.id.home_recents_sync_in_progress)
    ProgressBar mSyncInProgressView;
    @BindView(R.id.view_pager_main)
    ViewPager viewPager;
    @BindView(R.id.tabs)
    TabLayout tabs;
    @BindView(R.id.translator_button)
    ImageView translatorButton;
    //  @BindView(R.id.bottom_navigation)
    //  BottomNavigationView mBottomNavigationView;
    Adapter adapter;
    // switch to a room activity
    private Map<String, Object> mAutomaticallyOpenedRoomParams = null;
    private Uri mUniversalLinkToOpen = null;
    private String mMemberIdToOpen = null;
    // mFloatingActionButton is hidden for 1s when there is scroll
    private Timer mFloatingActionButtonTimer;
    private MXEventListener mEventsListener;
    // sliding menu management
    private int mSlidingMenuIndex = -1;
    private MXSession mSession;
    // events listener to track required refresh
    private final MXEventListener mBadgeEventsListener = new MXEventListener() {
        private boolean mRefreshBadgeOnChunkEnd = false;

        @Override
        public void onLiveEventsChunkProcessed(String fromToken, String toToken) {
            if (mRefreshBadgeOnChunkEnd) {
                //  refreshUnreadBadges();
                mRefreshBadgeOnChunkEnd = false;
            }
        }

        @Override
        public void onLiveEvent(final Event event, final RoomState roomState) {
            String eventType = event.getType();

            countUnreadEvents();
            // refresh the UI at the end of the next events chunk
            mRefreshBadgeOnChunkEnd |= ((event.roomId != null) && RoomSummary.isSupportedEvent(event)) ||
                    Event.EVENT_TYPE_STATE_ROOM_MEMBER.equals(eventType) ||
                    Event.EVENT_TYPE_REDACTION.equals(eventType) ||
                    Event.EVENT_TYPE_TAGS.equals(eventType)
                    || Event.EVENT_TYPE_STATE_ROOM_THIRD_PARTY_INVITE.equals(eventType);

        }

        @Override
        public void onReceiptEvent(String roomId, List<String> senderIds) {
            // refresh only if the current user read some messages (to update the unread messages counters)
            mRefreshBadgeOnChunkEnd |= (senderIds.indexOf(mSession.getCredentials().userId) >= 0);
        }

        @Override
        public void onLeaveRoom(final String roomId) {
            mRefreshBadgeOnChunkEnd = true;
        }

        @Override
        public void onNewRoom(String roomId) {
            mRefreshBadgeOnChunkEnd = true;
        }

        @Override
        public void onJoinRoom(String roomId) {
            mRefreshBadgeOnChunkEnd = true;
        }

        @Override
        public void onDirectMessageChatRoomsListUpdate() {
            mRefreshBadgeOnChunkEnd = true;
        }

        @Override
        public void onRoomTagEvent(String roomId) {
            mRefreshBadgeOnChunkEnd = true;
        }

    };
    //    @BindView(R.id.search_view)
    private SearchView mSearchView;
    private MenuItem mSearchMenuItem;
    private boolean mStorePermissionCheck = false;
    // a shared files intent is waiting the store init
    private Intent mSharedFilesIntent = null;
    private FragmentManager mFragmentManager;
    // The current item selected (bottom navigation)
    private int mCurrentMenuId;

    /*
     * *********************************************************************************************
     * Static methods
     * *********************************************************************************************
     */
    // the current displayed fragment
    private String mCurrentFragmentTag;

    /*
     * *********************************************************************************************
     * Activity lifecycle
     * *********************************************************************************************
     */
    private List<Room> mDirectChatInvitations;
    private List<Room> mRoomInvitations;
    // floating action bar dialog
    private AlertDialog mFabDialog;

    /**
     * @return the current instance
     */
    public static VectorHomeActivity getInstance() {
        return sharedInstance;
    }
    private void checkForCrashes() {
        CrashManager.register(this);
    }

    private void checkForUpdates() {
        // Remove this for store builds!
        UpdateManager.register(this);
    }

    private void unregisterManagers() {
        UpdateManager.unregister();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        //checkForUpdates();
        //  makeCrash();
        mFragmentManager = getSupportFragmentManager();
        if (mCurrentFragmentTag == null) {
            mCurrentFragmentTag = TAG_FRAGMENT_HOME;
            mCurrentMenuId = 0;
        }
        mToolbar.setTitle("Im");
        if (CommonActivityUtils.shouldRestartApp(this)) {
            Log.e(LOG_TAG, "Restart the application.");
            CommonActivityUtils.restartApp(this);
            return;
        }
        findViewById(R.id.toolbar_title).setOnClickListener(v -> {
            final Intent settingsIntent = new Intent(this, NewSettingActivity.class);
            settingsIntent.putExtra(MXCActionBarActivity.EXTRA_MATRIX_ID, mSession.getMyUserId());
            startActivity(settingsIntent);
        });
        // mFloatingActionButton.setBackgroundTintList(ColorStateList.valueOf(getResources().getAttribute(R.attr.colorControlActivated)));

        // mFloatingActionButton.setBackgroundTintList(ColorStateList.valueOf(R.attr.colorControlActivated));
        // mFloatingActionButton.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
        //   mFloatingActionButton.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));

        if (ThemeUtils.getApplicationTheme(getBaseContext()).equals(ThemeUtils.THEME_BLACK_VALUE) ||
                ThemeUtils.getApplicationTheme(getBaseContext()).equals(ThemeUtils.THEME_DARK_VALUE)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mFloatingActionButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_add_black, getBaseContext().getTheme()));
            } else {
                mFloatingActionButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_add_black));
            }
        }

        if (CommonActivityUtils.isGoingToSplash(this)) {
            Log.d(LOG_TAG, "onCreate : Going to splash screen");
            return;
        }

        sharedInstance = this;

        mSession = Matrix.getInstance(this).getDefaultSession();
        callLogFragment = new CallLogFragment();
        callLogFragment.setmSession(mSession);

        // track if the application update
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        int version = preferences.getInt("VERSION_BUILD", 0);

        if (version != VectorApp.VERSION_BUILD) {
            Log.d(LOG_TAG, "The application has been updated from version " + version + " to version " + VectorApp.VERSION_BUILD);

            // TODO add some dedicated actions here

            SharedPreferences.Editor editor = preferences.edit();
            editor.putInt("VERSION_BUILD", VectorApp.VERSION_BUILD);
            editor.commit();
        }

        // process intent parameters
        final Intent intent = getIntent();

        if (null != savedInstanceState) {
            // fix issue #1276
            // if there is a saved instance, it means that onSaveInstanceState has been called.
            // theses parameters must only be used once.
            // The activity might have been created after being killed by android while the application is in background
            intent.removeExtra(EXTRA_SHARED_INTENT_PARAMS);
            intent.removeExtra(EXTRA_CALL_SESSION_ID);
            intent.removeExtra(EXTRA_CALL_ID);
            intent.removeExtra(EXTRA_CALL_UNKNOWN_DEVICES);
            intent.removeExtra(EXTRA_WAITING_VIEW_STATUS);
            intent.removeExtra(EXTRA_JUMP_TO_UNIVERSAL_LINK);
            intent.removeExtra(EXTRA_JUMP_TO_ROOM_PARAMS);
            intent.removeExtra(EXTRA_MEMBER_ID);
            intent.removeExtra(VectorUniversalLinkReceiver.EXTRA_UNIVERSAL_LINK_URI);
        } else {

            if (intent.hasExtra(EXTRA_CALL_SESSION_ID) && intent.hasExtra(EXTRA_CALL_ID)) {
                startCall(intent.getStringExtra(EXTRA_CALL_SESSION_ID), intent.getStringExtra(EXTRA_CALL_ID), (MXUsersDevicesMap<MXDeviceInfo>) intent.getSerializableExtra(EXTRA_CALL_UNKNOWN_DEVICES));
                intent.removeExtra(EXTRA_CALL_SESSION_ID);
                intent.removeExtra(EXTRA_CALL_ID);
                intent.removeExtra(EXTRA_CALL_UNKNOWN_DEVICES);
            }

            // the activity could be started with a spinner
            // because there is a pending action (like universalLink processing)
            if (intent.getBooleanExtra(EXTRA_WAITING_VIEW_STATUS, WAITING_VIEW_STOP)) {
                showWaitingView();
            } else {
                stopWaitingView();
            }
            intent.removeExtra(EXTRA_WAITING_VIEW_STATUS);

            mAutomaticallyOpenedRoomParams = (Map<String, Object>) intent.getSerializableExtra(EXTRA_JUMP_TO_ROOM_PARAMS);
            intent.removeExtra(EXTRA_JUMP_TO_ROOM_PARAMS);

            mUniversalLinkToOpen = intent.getParcelableExtra(EXTRA_JUMP_TO_UNIVERSAL_LINK);
            intent.removeExtra(EXTRA_JUMP_TO_UNIVERSAL_LINK);

            mMemberIdToOpen = intent.getStringExtra(EXTRA_MEMBER_ID);
            intent.removeExtra(EXTRA_MEMBER_ID);

            mSearchMenuItem = null;

            findViewById(R.id.translator_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(VectorHomeActivity.this, TranslatorActivity.class));
                }
            });

            // the home activity has been launched with an universal link
            if (intent.hasExtra(VectorUniversalLinkReceiver.EXTRA_UNIVERSAL_LINK_URI)) {
                Log.d(LOG_TAG, "Has an universal link");

                final Uri uri = intent.getParcelableExtra(VectorUniversalLinkReceiver.EXTRA_UNIVERSAL_LINK_URI);
                intent.removeExtra(VectorUniversalLinkReceiver.EXTRA_UNIVERSAL_LINK_URI);

                // detect the room could be opened without waiting the next sync
                HashMap<String, String> params = VectorUniversalLinkReceiver.parseUniversalLink(uri);

                if ((null != params) && params.containsKey(VectorUniversalLinkReceiver.ULINK_ROOM_ID_OR_ALIAS_KEY)) {
                    Log.d(LOG_TAG, "Has a valid universal link");

                    final String roomIdOrAlias = params.get(VectorUniversalLinkReceiver.ULINK_ROOM_ID_OR_ALIAS_KEY);

                    // it is a room ID ?
                    if (MXSession.isRoomId(roomIdOrAlias)) {
                        Log.d(LOG_TAG, "Has a valid universal link to the room ID " + roomIdOrAlias);
                        Room room = mSession.getDataHandler().getRoom(roomIdOrAlias, false);

                        if (null != room) {
                            Log.d(LOG_TAG, "Has a valid universal link to a known room");
                            // open the room asap
                            mUniversalLinkToOpen = uri;
                        } else {
                            Log.d(LOG_TAG, "Has a valid universal link but the room is not yet known");
                            // wait the next sync
                            intent.putExtra(VectorUniversalLinkReceiver.EXTRA_UNIVERSAL_LINK_URI, uri);
                        }
                    } else if (MXSession.isRoomAlias(roomIdOrAlias)) {
                        Log.d(LOG_TAG, "Has a valid universal link of the room Alias " + roomIdOrAlias);

                        // it is a room alias
                        // convert the room alias to room Id
                        mSession.getDataHandler().roomIdByAlias(roomIdOrAlias, new SimpleApiCallback<String>() {
                            @Override
                            public void onSuccess(String roomId) {
                                Log.d(LOG_TAG, "Retrieve the room ID " + roomId);

                                getIntent().putExtra(VectorUniversalLinkReceiver.EXTRA_UNIVERSAL_LINK_URI, uri);

                                // the room exists, opens it
                                if (null != mSession.getDataHandler().getRoom(roomId, false)) {
                                    Log.d(LOG_TAG, "Find the room from room ID : process it");
                                    processIntentUniversalLink();
                                } else {
                                    Log.d(LOG_TAG, "Don't know the room");
                                }
                            }
                        });
                    }
                }
            } else {
                Log.d(LOG_TAG, "create with no universal link");
            }

            if (intent.hasExtra(EXTRA_SHARED_INTENT_PARAMS)) {
                final Intent sharedFilesIntent = intent.getParcelableExtra(EXTRA_SHARED_INTENT_PARAMS);
                Log.d(LOG_TAG, "Has shared intent");

                if (mSession.getDataHandler().getStore().isReady()) {
                    this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.d(LOG_TAG, "shared intent : The store is ready -> display sendFilesTo");
                            CommonActivityUtils.sendFilesTo(VectorHomeActivity.this, sharedFilesIntent);
                        }
                    });
                } else {
                    Log.d(LOG_TAG, "shared intent : Wait that the store is ready");
                    mSharedFilesIntent = sharedFilesIntent;
                }

                // ensure that it should be called once
                intent.removeExtra(EXTRA_SHARED_INTENT_PARAMS);
            }
        }

        // check if  there is some valid session
        // the home activity could be relaunched after an application crash
        // so, reload the sessions before displaying the history
        Collection<MXSession> sessions = Matrix.getMXSessions(VectorHomeActivity.this);
        if (sessions.size() == 0) {
            Log.e(LOG_TAG, "Weird : onCreate : no session");

            if (null != Matrix.getInstance(this).getDefaultSession()) {
                Log.e(LOG_TAG, "No loaded session : reload them");
                // start splash activity and stop here
                startActivity(new Intent(VectorHomeActivity.this, SplashActivity.class));
                VectorHomeActivity.this.finish();
                return;
            }
        }

      /*  final View selectedMenu;
        if (savedInstanceState != null) {
            selectedMenu = tabs.findViewById(savedInstanceState.getInt(CURRENT_MENU_ID, R.id.bottom_action_home));
        } else {
            selectedMenu = tabs.findViewById(R.id.bottom_action_home);
        }
        if (selectedMenu != null) {
            selectedMenu.performClick();
        }*/

        // initialize the public rooms list
        //  PublicRoomsManager.getInstance().setSession(mSession);
        //  PublicRoomsManager.getInstance().refreshPublicRoomsCount(null);
        mSession.joinRoom("!JYzZeuHSegcXtaEYqN:matrix.talkermessenger.com", new ApiCallback<String>() {
            @Override
            public void onSuccess(String info) {
                Log.e("joining", "to test IM room succeed");
            }

            @Override
            public void onNetworkError(Exception e) {

            }

            @Override
            public void onMatrixError(MatrixError e) {

            }

            @Override
            public void onUnexpectedError(Exception e) {

            }
        });
        initViews();
        setupNavigation();
    }

    private void makeCrash() {
        throw new RuntimeException("Test crash");
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyPresenceManager.createPresenceManager(this, Matrix.getInstance(this).getSessions());
        MyPresenceManager.advertiseAllOnline();
        checkForCrashes();
        checkForUpdates();
        // Broadcast receiver to stop waiting screen
        registerReceiver(mBrdRcvStopWaitingView, new IntentFilter(BROADCAST_ACTION_STOP_WAITING_VIEW));

        Intent intent = getIntent();

        // jump to an external link
        if (null != mUniversalLinkToOpen) {
            intent.putExtra(VectorUniversalLinkReceiver.EXTRA_UNIVERSAL_LINK_URI, mUniversalLinkToOpen);
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    processIntentUniversalLink();
                    mUniversalLinkToOpen = null;
                }
            });
        }

        if (mSession.isAlive()) {
            addEventsListener();
        }

        if (null != mFloatingActionButton) {
            if (mCurrentMenuId == R.id.bottom_action_favourites) {
                mFloatingActionButton.setVisibility(View.GONE);
            } else {
                mFloatingActionButton.show();
            }
        }

        mVectorPendingCallView.checkPendingCall();

        if (null != mMemberIdToOpen) {
            Intent startRoomInfoIntent = new Intent(VectorHomeActivity.this, VectorMemberDetailsActivity.class);
            startRoomInfoIntent.putExtra(VectorMemberDetailsActivity.EXTRA_MEMBER_ID, mMemberIdToOpen);
            startRoomInfoIntent.putExtra(VectorMemberDetailsActivity.EXTRA_MATRIX_ID, mSession.getCredentials().userId);
            startActivity(startRoomInfoIntent);
            mMemberIdToOpen = null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // the application is in a weird state
        if (CommonActivityUtils.shouldRestartApp(this)) {
            return false;
        }
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.vector_home, menu);
        CommonActivityUtils.tintMenuIcons(menu, ThemeUtils.getColor(this, R.attr.actionbar_text_color));
        // Retrieve the SearchView and plug it into SearchManager
        mSearchMenuItem = menu.findItem(R.id.action_search);
        mSearchMenuItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                tabs.setVisibility(View.GONE);
                translatorButton.setVisibility(View.GONE);
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                tabs.setVisibility(View.VISIBLE);
                translatorButton.setVisibility(View.VISIBLE);
                return true;
            }
        });

        mSearchView = (SearchView) MenuItemCompat.getActionView(mSearchMenuItem);
        // init the search view
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        // Remove unwanted left margin
        LinearLayout searchEditFrame = mSearchView.findViewById(R.id.search_edit_frame);
        if (searchEditFrame != null) {
            ViewGroup.MarginLayoutParams searchEditFrameParams = (ViewGroup.MarginLayoutParams) searchEditFrame.getLayoutParams();
            searchEditFrameParams.leftMargin = 0;
            if (ThemeUtils.getApplicationTheme(getBaseContext()).equals(ThemeUtils.THEME_LIGHT_LIGHT_BLUE_HEADER_VALUE)) {
                EditText et = searchEditFrame.findViewById(R.id.search_src_text);
                et.setTextColor(ThemeUtils.getColor(getApplicationContext(), R.attr.actionbar_text_color));
                try {
                    Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
                    f.setAccessible(true);
                    f.set(et, R.drawable.custom_cursor);
                } catch (Exception ignored) {
                }
            }
            searchEditFrame.setLayoutParams(searchEditFrameParams);
        }
        ImageView searchIcon = mSearchView.findViewById(R.id.search_mag_icon);
        if (searchIcon != null) {
            ViewGroup.MarginLayoutParams searchIconParams = (ViewGroup.MarginLayoutParams) searchIcon.getLayoutParams();
            searchIconParams.leftMargin = 0;
            searchIcon.setLayoutParams(searchIconParams);
            searchIcon.setColorFilter(ThemeUtils.getColor(this, R.attr.actionbar_text_color));
        }

        // mToolbar.setContentInsetStartWithNavigation(0);

        mSearchView.setMaxWidth(Integer.MAX_VALUE);
        mSearchView.setSubmitButtonEnabled(false);
        mSearchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        View v = mSearchView.findViewById(android.support.v7.appcompat.R.id.search_plate);
        v.setBackgroundColor(getResources().getColor(android.R.color.transparent));
        mSearchView.setIconifiedByDefault(false);
        mSearchView.setOnQueryTextListener(this);
        mSearchView.findViewById(R.id.search_src_text).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        CommonActivityUtils.tintMenuIcons(menu, ThemeUtils.getColor(this, R.attr.icon_tint_on_dark_action_bar_color));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean retCode = true;

        switch (item.getItemId()) {
            // search in rooms content
//            case R.id.ic_action_global_search:
//                final Intent searchIntent = new Intent(this, VectorUnifiedSearchActivity.class);
//
//                if (R.id.bottom_action_people == mCurrentMenuId) {
//                    searchIntent.putExtra(VectorUnifiedSearchActivity.EXTRA_TAB_INDEX, VectorUnifiedSearchActivity.SEARCH_PEOPLE_TAB_POSITION);
//                }
//
//                startActivity(searchIntent);
//                break;
//
//            // search in rooms content
//            case R.id.ic_action_historical:
//                startActivity(new Intent(this, HistoricalRoomsActivity.class));
//                break;
//            case R.id.ic_action_mark_all_as_read:
//                // Will be handle by fragments
//                retCode = false;
//                break;
//            case R.id.ic_action_settings:
//                final Intent settingsIntent = new Intent(VectorHomeActivity.this, SettingsActivity.class);
//                settingsIntent.putExtra(MXCActionBarActivity.EXTRA_MATRIX_ID, mSession.getMyUserId());
//                VectorHomeActivity.this.startActivity(settingsIntent);
//                break;
//            case R.id.ic_action_old_settings:
//                final Intent oldSettingsIntent = new Intent(VectorHomeActivity.this, VectorSettingsActivity.class);
//                oldSettingsIntent.putExtra(MXCActionBarActivity.EXTRA_MATRIX_ID, mSession.getMyUserId());
//                VectorHomeActivity.this.startActivity(oldSettingsIntent);
//                break;
            default:
                // not handled item, return the super class implementation value
                retCode = super.onOptionsItemSelected(item);
                break;
        }
        return retCode;
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerVisible(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
            return;
        }

        if (!TextUtils.isEmpty(mSearchView.getQuery().toString())) {
            mSearchView.setQuery("", true);
            return;
        }

        // Clear backstack
        mFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

        super.onBackPressed();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(CURRENT_MENU_ID, mCurrentMenuId);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterManagers();
        // Unregister Broadcast receiver
        stopWaitingView();
        try {
            unregisterReceiver(mBrdRcvStopWaitingView);
        } catch (Exception e) {
            Log.e(LOG_TAG, "## onPause() : unregisterReceiver fails " + e.getMessage());
        }

        if (mSession.isAlive()) {
            removeEventsListener();
        }

        synchronized (this) {
            if (null != mFloatingActionButtonTimer) {
                mFloatingActionButtonTimer.cancel();
                mFloatingActionButtonTimer = null;
            }
        }

        if (mFabDialog != null) {
            // Prevent leak after orientation changed
            mFabDialog.dismiss();
            mFabDialog = null;
        }

        // removeBadgeEventsListener();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterManagers();
        // release the static instance if it is the current implementation
        if (sharedInstance == this) {
            sharedInstance = null;
        }
    }

    /*
     * *********************************************************************************************
     * UI management
     * *********************************************************************************************
     */

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        CommonActivityUtils.onLowMemory(this);
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        CommonActivityUtils.onTrimMemory(this, level);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        mAutomaticallyOpenedRoomParams = (Map<String, Object>) intent.getSerializableExtra(EXTRA_JUMP_TO_ROOM_PARAMS);
        intent.removeExtra(EXTRA_JUMP_TO_ROOM_PARAMS);

        mUniversalLinkToOpen = intent.getParcelableExtra(EXTRA_JUMP_TO_UNIVERSAL_LINK);
        intent.removeExtra(EXTRA_JUMP_TO_UNIVERSAL_LINK);

        mMemberIdToOpen = intent.getStringExtra(EXTRA_MEMBER_ID);
        intent.removeExtra(EXTRA_MEMBER_ID);


        // start waiting view
        if (intent.getBooleanExtra(EXTRA_WAITING_VIEW_STATUS, VectorHomeActivity.WAITING_VIEW_STOP)) {
            showWaitingView();
        } else {
            stopWaitingView();
        }
        intent.removeExtra(EXTRA_WAITING_VIEW_STATUS);

    }

    @Override
    public void onRequestPermissionsResult(int aRequestCode, @NonNull String[] aPermissions, @NonNull int[] aGrantResults) {
        super.onRequestPermissionsResult(aRequestCode, aPermissions, aGrantResults);
        if (0 == aPermissions.length) {
            Log.e(LOG_TAG, "## onRequestPermissionsResult(): cancelled " + aRequestCode);
        } else if (aRequestCode == CommonActivityUtils.REQUEST_CODE_PERMISSION_HOME_ACTIVITY) {
            Log.w(LOG_TAG, "## onRequestPermissionsResult(): REQUEST_CODE_PERMISSION_HOME_ACTIVITY");
        } else {
            Log.e(LOG_TAG, "## onRequestPermissionsResult(): unknown RequestCode = " + aRequestCode);
        }
    }

    /**
     * Setup navigation components of the screen (toolbar, etc.)
     */
    private void setupNavigation() {
        TypedValue vectorTabBarColor = new TypedValue();
        this.getTheme().resolveAttribute(R.attr.tab_bar_background_color, vectorTabBarColor, true);
        TypedValue vectorTabBarTextColor = new TypedValue();
        this.getTheme().resolveAttribute(R.attr.tab_bar_text_color, vectorTabBarTextColor, true);
        tabs.setTabTextColors(getResources().getColor(vectorTabBarTextColor.resourceId),
                getResources().getColor(vectorTabBarTextColor.resourceId));
        tabs.setBackgroundColor(getResources().getColor(vectorTabBarColor.resourceId));
        // tabs.setSelectedTabIndicatorColor(getResources().getColor(vectorTabBarTextColor.resourceId));
        // Toolbar
        setSupportActionBar(mToolbar);
        setupViewPager(viewPager);
        tabs.setupWithViewPager(viewPager);
        TabLayout.Tab chatTab = tabs.getTabAt(0).setCustomView(R.layout.tab_with_counter);
        TextView chatTitle = chatTab.getCustomView().findViewById(R.id.tab_title);
        chatTitle.setText(getResources().getString(R.string.chats));
        TabLayout.Tab contactsTab = tabs.getTabAt(1).setCustomView(R.layout.tab_with_counter);
        contactsTab.getCustomView().findViewById(R.id.rooms_unread_count).setVisibility(View.GONE);
        TextView contactsTitle = contactsTab.getCustomView().findViewById(R.id.tab_title);
        contactsTitle.setText(getResources().getString(R.string.contacts));
        TabLayout.Tab callsTab = tabs.getTabAt(2).setCustomView(R.layout.tab_with_counter);
        callsTab.getCustomView().findViewById(R.id.rooms_unread_count).setVisibility(View.GONE);
        TextView callsTitle = callsTab.getCustomView().findViewById(R.id.tab_title);
        callsTitle.setText(getResources().getString(R.string.calls_upper));
        tabs.getTabAt(1).getCustomView().setAlpha(.7f);
        tabs.getTabAt(2).getCustomView().setAlpha(.7f);
        tabs.setSelectedTabIndicatorColor(Color.parseColor("#eeeeee"));
        Log.e(LOG_TAG, "tabs child counter" + tabs.getChildCount());
        findViewById(R.id.divider0).setVisibility(View.VISIBLE);
        countUnreadEvents();
    }

    /**
     * Count unread events
     */
    private void countUnreadEvents() {
        int unreadEventsCount = 0;
        if ((null == mSession) || (null == mSession.getDataHandler())) {
            Log.e(LOG_TAG, "## initDirectChatsData() : null session");
        }
        final List<String> directChatIds = mSession.getDirectChatRoomIdsList();
        final MXDataHandler dataHandler = mSession.getDataHandler();
        final IMXStore store = dataHandler.getStore();
        if (directChatIds != null && !directChatIds.isEmpty()) {
            for (String roomId : directChatIds) {
                Room room = store.getRoom(roomId);

                if ((null != room) && !room.isConferenceUserRoom()) {
                    // it seems that the server syncs some left rooms
                    if (null == room.getMember(mSession.getMyUserId())) {
                        Log.e(LOG_TAG, "## initDirectChatsData(): invalid room " + room.getRoomId() + ", the user is not anymore member of it");
                    } else {
                        final Set<String> tags = room.getAccountData().getKeys();
                        if ((null == tags) || !tags.contains(RoomTag.ROOM_TAG_LOW_PRIORITY)) {
                            final boolean isArchived = tags != null && tags.contains(RoomTag.ROOM_TAG_ARCHIVED);
                            final boolean isDeleted = tags != null && tags.contains(RoomTag.ROOM_TAG_DELETED);
                            final boolean isFavourite = roomId.equals(PreferencesManager.getFavouriteMsgCommonRoom(this));
                            if (!isArchived && !isDeleted && !isFavourite) {
//                                unreadEventsCount += store.getSummary(room.getRoomId()).getHighlightCount();
                                List<Event> unreadEvents = store.unreadEventsWithoutCalls(room.getRoomId(), Arrays.asList(Event.EVENT_TYPE_MESSAGE));
                                if (unreadEvents != null)
                                    unreadEventsCount += unreadEvents.size();
                            }
                        }
                    }
                }
            }
        }
        List<RoomSummary> roomSummaries = new ArrayList<>(store.getSummaries());
        HashSet<String> directChatRoomIds = new HashSet<>(mSession.getDirectChatRoomIdsList());
        HashSet<String> lowPriorityRoomIds = new HashSet<>(mSession.roomIdsWithTag(RoomTag.ROOM_TAG_LOW_PRIORITY));
        HashSet<String> deletedRoomIds = new HashSet<>(mSession.roomIdsWithTag(RoomTag.ROOM_TAG_DELETED));
        for (RoomSummary summary : roomSummaries) {
            // don't display the invitations
            if (!summary.isInvited()) {
                Room room = store.getRoom(summary.getRoomId());
                // test
                if ((null != room) && // if the room still exists
                        !room.isConferenceUserRoom() && // not a VOIP conference room
                        !directChatRoomIds.contains(room.getRoomId()) &&
                        !lowPriorityRoomIds.contains(room.getRoomId()) &&
                        !deletedRoomIds.contains(room.getRoomId())) {
                    final Set<String> tags = room.getAccountData().getKeys();
                    final boolean isArchived = tags != null && tags.contains(RoomTag.ROOM_TAG_ARCHIVED);
                    final boolean isDeleted = tags != null && tags.contains(RoomTag.ROOM_TAG_DELETED);
                    final boolean isFavourite = room.getRoomId().equals(PreferencesManager.getFavouriteMsgCommonRoom(this));

                    if (!isArchived && !isDeleted && !isFavourite) {
//                        unreadEventsCount += store.getSummary(room.getRoomId()).getHighlightCount();
//                        List<Event> unreadEvents = store.unreadEvents(room.getRoomId(), Arrays.asList(Event.EVENT_TYPE_MESSAGE));
                        List<Event> unreadEvents = store.unreadEventsWithoutCalls(room.getRoomId(), Arrays.asList(Event.EVENT_TYPE_MESSAGE));

                        if (unreadEvents != null)
                            unreadEventsCount += unreadEvents.size();
                    }
                }
            }
        }

        TextView unreadCount = findViewById(R.id.rooms_unread_count);
        if (unreadEventsCount > 0) {
            unreadCount.setVisibility(View.VISIBLE);
            unreadCount.setText(RoomUtils.formatUnreadMessagesCounter(unreadEventsCount));
        } else {
            unreadCount.setVisibility(View.GONE);
        }
    }

    /**
     * Update the displayed fragment according to the selected menu
     *
     * @param item menu item selected by the user
     */
    private void updateSelectedFragment(final MenuItem item) {
        if (mCurrentMenuId == item.getItemId()) {
            return;
        }

        Fragment fragment = null;

        switch (item.getItemId()) {
            case R.id.bottom_action_home:
                Log.d(LOG_TAG, "onNavigationItemSelected HOME");
                fragment = mFragmentManager.findFragmentByTag(TAG_FRAGMENT_HOME);
                if (fragment == null) {
                    fragment = HomeFragment.newInstance();
                }
                mCurrentFragmentTag = TAG_FRAGMENT_HOME;
//                mSearchView.setQueryHint(getString(R.string.home_filter_placeholder_home));
                break;
            case R.id.bottom_action_favourites:
                Log.d(LOG_TAG, "onNavigationItemSelected FAVOURITES");
                fragment = mFragmentManager.findFragmentByTag(TAG_FRAGMENT_FAVOURITES);
                if (fragment == null) {
                    fragment = FavouritesFragment.newInstance();
                }
                mCurrentFragmentTag = TAG_FRAGMENT_FAVOURITES;

//                mSearchView.setQueryHint(getString(R.string.home_filter_placeholder_favorites));

                break;
            case R.id.bottom_action_people:
                Log.d(LOG_TAG, "onNavigationItemSelected PEOPLE");
                fragment = mFragmentManager.findFragmentByTag(TAG_FRAGMENT_PEOPLE);
                if (fragment == null) {
                    fragment = PeopleFragment.newInstance();
                }
                mCurrentFragmentTag = TAG_FRAGMENT_PEOPLE;

//                mSearchView.setQueryHint(getString(R.string.home_filter_placeholder_people));

                break;
            case R.id.bottom_action_rooms:
                Log.d(LOG_TAG, "onNavigationItemSelected CALL_LOG");
                fragment = mFragmentManager.findFragmentByTag(TAG_FRAGMENT_CALL_LOG);
                if (fragment == null) {
                    fragment = callLogFragment;
                }
                mCurrentFragmentTag = TAG_FRAGMENT_CALL_LOG;

//              mSearchView.setQueryHint(getString(R.string.home_filter_placeholder_rooms));

                break;
        }

        synchronized (this) {
            if (null != mFloatingActionButtonTimer) {
                mFloatingActionButtonTimer.cancel();
                mFloatingActionButtonTimer = null;
            }
            mFloatingActionButton.show();
        }

        // clear waiting view
        stopWaitingView();

        // don't display the fab for the favorites tab
        mFloatingActionButton.setVisibility((item.getItemId() != R.id.bottom_action_favourites) ? View.VISIBLE : View.GONE);

        mCurrentMenuId = item.getItemId();

        if (fragment != null) {
            resetFilter();
            try {
                adapter.notifyDataSetChanged();
                /*mFragmentManager.beginTransaction()
                        .replace(R.id.fragment_container, fragment, mCurrentFragmentTag)
                        .addToBackStack(mCurrentFragmentTag)
                        .commit();*/
            } catch (Exception e) {
                Log.e(LOG_TAG, "## updateSelectedFragment() failed : " + e.getMessage());
            }
        }
    }

    /**
     * Update UI colors to match the selected tab
     *
     * @param primaryColor
     * @param secondaryColor
     */
    public void updateTabStyle(final int primaryColor, final int secondaryColor) {
        TypedValue vectorActionBarColor = new TypedValue();
        this.getTheme().resolveAttribute(R.attr.riot_primary_background_color, vectorActionBarColor, true);
        mToolbar.setBackgroundResource(R.color.fab_tint);

        mVectorPendingCallView.updateBackgroundColor(primaryColor);
      /*  mSyncInProgressView.setBackgroundColor(primaryColor);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mSyncInProgressView.setIndeterminateTintList(ColorStateList.valueOf(secondaryColor));
        } else {
            mSyncInProgressView.getIndeterminateDrawable().setColorFilter(
                    secondaryColor, android.graphics.PorterDuff.Mode.SRC_IN);
        }*/
        //mFloatingActionButton.setRippleColor(secondaryColor);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(primaryColor);
        }

        // Set color of toolbar search view
    /*    if (mSearchView != null) {
            EditText edit = mSearchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
            edit.setTextColor(ThemeUtils.getColor(this, R.attr.primary_text_color));
            edit.setHintTextColor(ThemeUtils.getColor(this, R.attr.primary_hint_text_color));
        }*/

    }

    private int getCurrentPagerPosition() {
        return viewPager.getCurrentItem();
    }

    /**
     * Init views
     */
    private void initViews() {
        mVectorPendingCallView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IMXCall call = CallsManager.getSharedInstance().getActiveCall();
                if (null != call) {
                    final Intent intent = new Intent(VectorHomeActivity.this, VectorCallViewActivity.class);
                    intent.putExtra(VectorCallViewActivity.EXTRA_MATRIX_ID, call.getSession().getCredentials().userId);
                    intent.putExtra(VectorCallViewActivity.EXTRA_CALL_ID, call.getCallId());

                    VectorHomeActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            VectorHomeActivity.this.startActivity(intent);
                        }
                    });
                }
            }
        });
        if (null != mFloatingActionButton) {
            mFloatingActionButton.setOnClickListener(v -> onFloatingButtonClick());
        }
    }

    /**
     * Reset the filter
     */
    private void resetFilter() {
        if (mSearchView != null) {
            mSearchView.setQuery("", false);
            mSearchView.clearFocus();
        }

        hideKeyboard();
    }

    /**
     * SHow teh waiting view
     */
    public void showWaitingView() {
        if (null != mWaitingView) {
            mWaitingView.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Hide the waiting view
     */
    public void stopWaitingView() {
        if (null != mWaitingView) {
            mWaitingView.setVisibility(View.GONE);
        }
    }

    /**
     * Tells if the waiting view is currently displayed
     *
     * @return true if the waiting view is displayed
     */
    public boolean isWaitingViewVisible() {
        return (null != mWaitingView) && (View.VISIBLE == mWaitingView.getVisibility());
    }

    /**
     * Hide the keyboard
     */
    private void hideKeyboard() {
        final View view = getCurrentFocus();
        if (view != null) {
            final InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /*
     * *********************************************************************************************
     * User action management
     * *********************************************************************************************
     */
    @Override
    public boolean onQueryTextChange(String newText) {
        // compute an unique pattern
        final String filter = newText + "-" + mCurrentMenuId;

        // wait before really triggering the search
        // else a search is triggered for each new character
        // eg "matt" triggers
        // 1 - search for m
        // 2 - search for ma
        // 3 - search for mat
        // 4 - search for matt
        // whereas only one search should have been triggered
        // else it might trigger some lags evenif the search is done in a background thread
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                String queryText = mSearchView.getQuery().toString();
                String currentFilter = queryText + "-" + mCurrentMenuId;
                // display if the pattern matched
                if (TextUtils.equals(currentFilter, filter)) {
                    Log.e("filter", queryText);
                    applyFilter(queryText);
                }
            }
        }, 500);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }

    /*
     * *********************************************************************************************
     * Floating button management
     * *********************************************************************************************
     */

    /**
     * Provides the selected fragment.
     *
     * @return the displayed fragment
     */
    private Fragment getSelectedFragment() {
        Fragment fragment = null;
        try {
            fragment = adapter.getItem(mCurrentMenuId);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return fragment;
    }

    /**
     * Communicate the search pattern to the currently displayed fragment
     * Note: fragments will handle the search using @{@link android.widget.Filter} which means
     * asynchronous filtering operations
     *
     * @param pattern
     */
    private void applyFilter(final String pattern) {
        Fragment fragment = getSelectedFragment();

        if (fragment instanceof AbsHomeFragment) {
            ((AbsHomeFragment) fragment).applyFilter(pattern.trim());
        }

        //TODO add listener to know when filtering is done and dismiss the keyboard
    }

    /**
     * Process the content of the current intent to detect universal link data.
     * If data present, it means that the app was started through an URL link, but due
     * to the App was not initialized properly, it has been required to re start the App.
     * <p>
     * To indicate the App has finished its Login/Splash/Home flow, a resume action
     * is sent to the receiver.
     */
    private void processIntentUniversalLink() {
        Intent intent;
        Uri uri;

        if (null != (intent = getIntent())) {
            if (intent.hasExtra(VectorUniversalLinkReceiver.EXTRA_UNIVERSAL_LINK_URI)) {
                Log.d(LOG_TAG, "## processIntentUniversalLink(): EXTRA_UNIVERSAL_LINK_URI present1");
                uri = intent.getParcelableExtra(VectorUniversalLinkReceiver.EXTRA_UNIVERSAL_LINK_URI);

                if (null != uri) {
                    // since android O
                    // set the class to avoid having "Background execution not allowed"
                    Intent myBroadcastIntent = new Intent(VectorApp.getInstance(), VectorUniversalLinkReceiver.class);
                    myBroadcastIntent.setAction(VectorUniversalLinkReceiver.BROADCAST_ACTION_UNIVERSAL_LINK_RESUME);
                    myBroadcastIntent.putExtras(getIntent().getExtras());
                    myBroadcastIntent.putExtra(VectorUniversalLinkReceiver.EXTRA_UNIVERSAL_LINK_SENDER_ID, VectorUniversalLinkReceiver.HOME_SENDER_ID);
                    sendBroadcast(myBroadcastIntent);

                    showWaitingView();

                    // use only once, remove since it has been used
                    intent.removeExtra(VectorUniversalLinkReceiver.EXTRA_UNIVERSAL_LINK_URI);
                    Log.d(LOG_TAG, "## processIntentUniversalLink(): Broadcast BROADCAST_ACTION_UNIVERSAL_LINK_RESUME sent");
                }
            }
        }
    }

    private void onFloatingButtonClick() {
        if (!isWaitingViewVisible())
            switch (getCurrentPagerPosition()) {
                case CHATS:
                    invitePeopleToNewRoom(false);
                    break;
                case CONTACTS:
                    // Creates a new Intent to insert a contact
                    Intent addNewContactIntent = new Intent(ContactsContract.Intents.Insert.ACTION);
                    // Sets the MIME type to match the Contacts Provider
                    addNewContactIntent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
                    startActivity(addNewContactIntent);
                    break;
                case CALLS:
                    invitePeopleToNewRoom(true);
                    break;
                default:
                    throw new IllegalArgumentException("Unknown position");
            }
    }

    /**
     * Hide the floating action button for 1 second
     *
     * @param fragmentTag the calling fragment tag
     */
    public void hideFloatingActionButton(String fragmentTag) {
        synchronized (this) {
            // check if the calling fragment is the current one
            // during the fragment switch, the unplugged one might call this method
            // before the new one is plugged.
            // for example, if the switch is performed while the current list is scrolling.
            if (TextUtils.equals(mCurrentFragmentTag, fragmentTag)) {
                if (null != mFloatingActionButtonTimer) {
                    mFloatingActionButtonTimer.cancel();
                }

                if (null != mFloatingActionButton) {
                    mFloatingActionButton.hide();

                    try {
                        mFloatingActionButtonTimer = new Timer();
                        mFloatingActionButtonTimer.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                synchronized (this) {
                                    if (null != mFloatingActionButtonTimer) {
                                        mFloatingActionButtonTimer.cancel();
                                        mFloatingActionButtonTimer = null;
                                    }
                                }
                                VectorHomeActivity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (null != mFloatingActionButton) {
                                            mFloatingActionButton.show();
                                        }
                                    }
                                });
                            }
                        }, 1000);
                    } catch (Throwable throwable) {
                        Log.e(LOG_TAG, "failed to init mFloatingActionButtonTimer " + throwable.getMessage());

                        if (null != mFloatingActionButtonTimer) {
                            mFloatingActionButtonTimer.cancel();
                            mFloatingActionButtonTimer = null;
                        }

                        VectorHomeActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (null != mFloatingActionButton) {
                                    mFloatingActionButton.show();
                                }
                            }
                        });

                    }
                }
            }
        }
    }

    /*
     * *********************************************************************************************
     * Room invitation management
     * *********************************************************************************************
     */

    /**
     * Getter for the floating action button
     *
     * @return fab view
     */
    public FloatingActionButton getFloatingActionButton() {
        return mFloatingActionButton;
    }

    /**
     * Open the room creation with inviting people.
     */
    public void invitePeopleToNewRoom(boolean isCall) {
        final Intent settingsIntent = new Intent(VectorHomeActivity.this, VectorRoomCreationActivity.class);
        settingsIntent.putExtra(MXCActionBarActivity.EXTRA_MATRIX_ID, mSession.getMyUserId());
        if (isCall) {
            settingsIntent.putExtra("isCall", isCall);
            settingsIntent.putExtra("activity", VectorBaseSearchActivity.EXTRA_CALL);
        } else {
            settingsIntent.putExtra("activity", VectorBaseSearchActivity.EXTRA_CHAT);
        }
        startActivity(settingsIntent);
    }

    /**
     * Create a room and open the dedicated activity
     */
    public void createRoom() {
        showWaitingView();
        mSession.createRoom(new SimpleApiCallback<String>(VectorHomeActivity.this) {
            @Override
            public void onSuccess(final String roomId) {
                mWaitingView.post(new Runnable() {
                    @Override
                    public void run() {
                        stopWaitingView();

                        HashMap<String, Object> params = new HashMap<>();
                        params.put(VectorRoomActivity.EXTRA_MATRIX_ID, mSession.getMyUserId());
                        params.put(VectorRoomActivity.EXTRA_ROOM_ID, roomId);
                        params.put(VectorRoomActivity.EXTRA_EXPAND_ROOM_HEADER, true);
                        CommonActivityUtils.goToRoomPage(VectorHomeActivity.this, mSession, params);
                    }
                });
            }

            private void onError(final String message) {
                mWaitingView.post(new Runnable() {
                    @Override
                    public void run() {
                        if (null != message) {
                            Toast.makeText(VectorHomeActivity.this, message, Toast.LENGTH_LONG).show();
                        }
                        stopWaitingView();
                    }
                });
            }

            @Override
            public void onNetworkError(Exception e) {
                onError(e.getLocalizedMessage());
            }

            @Override
            public void onMatrixError(final MatrixError e) {
                onError(e.getLocalizedMessage());
            }

            @Override
            public void onUnexpectedError(final Exception e) {
                onError(e.getLocalizedMessage());
            }
        });
    }

    /*
     * *********************************************************************************************
     * Sliding menu management
     * *********************************************************************************************
     */

    /**
     * Offer to join a room by alias or Id
     */
    private void joinARoom() {
        LayoutInflater inflater = LayoutInflater.from(this);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        View dialogView = inflater.inflate(R.layout.dialog_join_room_by_id, null);
        alertDialogBuilder.setView(dialogView);

        final EditText textInput = dialogView.findViewById(R.id.join_room_edit_text);
        textInput.setTextColor(ThemeUtils.getColor(this, R.attr.riot_primary_text_color));

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton(R.string.join,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                showWaitingView();

                                String text = textInput.getText().toString().trim();

                                mSession.joinRoom(text, new ApiCallback<String>() {
                                    @Override
                                    public void onSuccess(String roomId) {
                                        stopWaitingView();

                                        HashMap<String, Object> params = new HashMap<>();
                                        params.put(VectorRoomActivity.EXTRA_MATRIX_ID, mSession.getMyUserId());
                                        params.put(VectorRoomActivity.EXTRA_ROOM_ID, roomId);
                                        CommonActivityUtils.goToRoomPage(VectorHomeActivity.this, mSession, params);
                                    }

                                    private void onError(final String message) {
                                        mWaitingView.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                if (null != message) {
                                                    Toast.makeText(VectorHomeActivity.this, message, Toast.LENGTH_LONG).show();
                                                }
                                                stopWaitingView();
                                            }
                                        });
                                    }

                                    @Override
                                    public void onNetworkError(Exception e) {
                                        onError(e.getLocalizedMessage());
                                    }

                                    @Override
                                    public void onMatrixError(final MatrixError e) {
                                        onError(e.getLocalizedMessage());
                                    }

                                    @Override
                                    public void onUnexpectedError(final Exception e) {
                                        onError(e.getLocalizedMessage());
                                    }
                                });
                            }
                        })
                .setNegativeButton(R.string.cancel,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

        final Button joinButton = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);

        if (null != joinButton) {
            joinButton.setEnabled(false);
            textInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    String text = textInput.getText().toString().trim();
                    joinButton.setEnabled(MXSession.isRoomId(text) || MXSession.isRoomAlias(text));
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });
        }
    }

    public List<Room> getRoomInvitations() {
        if (mRoomInvitations == null) {
            mRoomInvitations = new ArrayList<>();
        } else {
            mRoomInvitations.clear();
        }
        if (mDirectChatInvitations == null) {
            mDirectChatInvitations = new ArrayList<>();
        } else {
            mDirectChatInvitations.clear();
        }

        if (null == mSession.getDataHandler().getStore()) {
            Log.e(LOG_TAG, "## getRoomInvitations() : null store");
            return new ArrayList<>();
        }

        Collection<RoomSummary> roomSummaries = mSession.getDataHandler().getStore().getSummaries();
        for (RoomSummary roomSummary : roomSummaries) {
            // reported by rageshake
            // i don't see how it is possible to have a null roomSummary
            if (null != roomSummary) {
                String roomSummaryId = roomSummary.getRoomId();
                Room room = mSession.getDataHandler().getStore().getRoom(roomSummaryId);

                // check if the room exists
                // the user conference rooms are not displayed.
                if (room != null && !room.isConferenceUserRoom() && room.isInvited()) {
                    if (room.isDirectChatInvitation()) {
                        mDirectChatInvitations.add(room);
                    } else {
                        mRoomInvitations.add(room);
                    }
                }
            }
        }

        // the invitations are sorted from the oldest to the more recent one
        Comparator<Room> invitationComparator = RoomUtils.getRoomsDateComparator(mSession, true);
        Collections.sort(mDirectChatInvitations, invitationComparator);
        Collections.sort(mRoomInvitations, invitationComparator);

        List<Room> roomInvites = new ArrayList<>();
        switch (mCurrentMenuId) {
            case 1:
                roomInvites.addAll(mDirectChatInvitations);
                break;
            default:
                roomInvites.addAll(mDirectChatInvitations);
                roomInvites.addAll(mRoomInvitations);
                Collections.sort(roomInvites, invitationComparator);
                break;
        }

        return roomInvites;
    }

    //==============================================================================================================
    // VOIP call management
    //==============================================================================================================

    public void onPreviewRoom(MXSession session, String roomId) {
        String roomAlias = null;

        Room room = session.getDataHandler().getRoom(roomId);
        if ((null != room) && (null != room.getLiveState())) {
            roomAlias = room.getLiveState().getAlias();
        }

        final RoomPreviewData roomPreviewData = new RoomPreviewData(mSession, roomId, null, roomAlias, null);
        CommonActivityUtils.previewRoom(this, roomPreviewData);
    }

    //==============================================================================================================
    // Unread counter badges
    //==============================================================================================================

    public void onRejectInvitation(final MXSession session, final String roomId) {
        Room room = session.getDataHandler().getRoom(roomId);

        if (null != room) {
            showWaitingView();

            room.leave(new ApiCallback<Void>() {
                @Override
                public void onSuccess(Void info) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // clear any pending notification for this room
                            EventStreamService.cancelNotificationsForRoomId(mSession.getMyUserId(), roomId);
                            stopWaitingView();
                        }
                    });
                }

                private void onError(final String message) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            stopWaitingView();
                            Toast.makeText(VectorHomeActivity.this, message, Toast.LENGTH_LONG).show();
                        }
                    });
                }

                @Override
                public void onNetworkError(Exception e) {
                    onError(e.getLocalizedMessage());
                }

                @Override
                public void onMatrixError(MatrixError e) {
                    onError(e.getLocalizedMessage());
                }

                @Override
                public void onUnexpectedError(Exception e) {
                    onError(e.getLocalizedMessage());
                }
            });
        }
    }

    /**
     * Start a call with a session Id and a call Id
     *
     * @param sessionId      the session Id
     * @param callId         the call Id
     * @param unknownDevices the unknown e2e devices
     */
    public void startCall(String sessionId, String callId, MXUsersDevicesMap<MXDeviceInfo> unknownDevices) {
        // sanity checks
        if ((null != sessionId) && (null != callId)) {
            final Intent intent = new Intent(VectorHomeActivity.this, VectorCallViewActivity.class);

            intent.putExtra(VectorCallViewActivity.EXTRA_MATRIX_ID, sessionId);
            intent.putExtra(VectorCallViewActivity.EXTRA_CALL_ID, callId);

            if (null != unknownDevices) {
                intent.putExtra(VectorCallViewActivity.EXTRA_UNKNOWN_DEVICES, unknownDevices);
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    VectorHomeActivity.this.startActivity(intent);
                }
            });
        }
    }

    /**
     * Warn the displayed fragment about summary updates.
     */
    public void dispatchOnSummariesUpdate() {
        Fragment fragment = getSelectedFragment();

        if ((null != fragment) && (fragment instanceof AbsHomeFragment)) {
            ((AbsHomeFragment) fragment).onSummariesUpdate();
        }
    }

    /**
     * Add a MXEventListener to the session listeners.
     */
    private void addEventsListener() {
        mEventsListener = new MXEventListener() {
            // set to true when a refresh must be triggered
            private boolean mRefreshOnChunkEnd = false;

            private void onForceRefresh() {
                if (View.VISIBLE != mSyncInProgressView.getVisibility()) {
                    dispatchOnSummariesUpdate();
                }
                countUnreadEvents();
            }

            @Override
            public void onAccountInfoUpdate(MyUser myUser) {
                //  refreshSlidingMenu();
            }

            @Override
            public void onInitialSyncComplete(String toToken) {
                Log.d(LOG_TAG, "## onInitialSyncComplete()");
                Log.e("onInitialSync", "complete");
                PreferencesManager.setInitialSyncComplete(VectorHomeActivity.this);
                dispatchOnSummariesUpdate();
                countUnreadEvents();
            }

            @Override
            public void onLiveEventsChunkProcessed(String fromToken, String toToken) {
                if ((VectorApp.getCurrentActivity() == VectorHomeActivity.this) && mRefreshOnChunkEnd) {
                    dispatchOnSummariesUpdate();
                }

                mRefreshOnChunkEnd = false;
                mSyncInProgressView.setVisibility(View.GONE);
                Log.e("onliveevent", "chunk" + "");
                // if (event.isCallEvent())
                callLogFragment.refreshCalls();
                // treat any pending URL link workflow, that was started previously
                processIntentUniversalLink();
            }

            @Override
            public void onLiveEvent(final Event event, final RoomState roomState) {
                String eventType = event.getType();
                // refresh the UI at the end of the next events chunk
                mRefreshOnChunkEnd |= ((event.roomId != null) && RoomSummary.isSupportedEvent(event)) ||
                        event.isCallEvent() ||
                        Event.EVENT_TYPE_STATE_ROOM_MEMBER.equals(eventType) ||
                        Event.EVENT_TYPE_TAGS.equals(eventType) ||
                        Event.EVENT_TYPE_REDACTION.equals(eventType) ||
                        Event.EVENT_TYPE_RECEIPT.equals(eventType) ||
                        Event.EVENT_TYPE_STATE_ROOM_AVATAR.equals(eventType) ||
                        Event.EVENT_TYPE_STATE_ROOM_THIRD_PARTY_INVITE.equals(eventType);

                countUnreadEvents();
            }

            @Override
            public void onReceiptEvent(String roomId, List<String> senderIds) {
                // refresh only if the current user read some messages (to update the unread messages counters)
                mRefreshOnChunkEnd |= (senderIds.indexOf(mSession.getCredentials().userId) >= 0);

                countUnreadEvents();

            }

            @Override
            public void onRoomTagEvent(String roomId) {
                mRefreshOnChunkEnd = true;
                countUnreadEvents();
            }

            @Override
            public void onStoreReady() {
                onForceRefresh();

                if (null != mSharedFilesIntent) {
                    Log.d(LOG_TAG, "shared intent : the store is now ready, display sendFilesTo");
                    CommonActivityUtils.sendFilesTo(VectorHomeActivity.this, mSharedFilesIntent);
                    mSharedFilesIntent = null;
                }
                countUnreadEvents();
            }

            @Override
            public void onLeaveRoom(final String roomId) {
                // clear any pending notification for this room
                EventStreamService.cancelNotificationsForRoomId(mSession.getMyUserId(), roomId);
                onForceRefresh();
                countUnreadEvents();
            }

            @Override
            public void onNewRoom(String roomId) {
                onForceRefresh();
                countUnreadEvents();
            }

            @Override
            public void onJoinRoom(String roomId) {
                onForceRefresh();
                countUnreadEvents();

            }

            @Override
            public void onDirectMessageChatRoomsListUpdate() {
                mRefreshOnChunkEnd = true;
                countUnreadEvents();
            }

            @Override
            public void onEventDecrypted(Event event) {
                RoomSummary summary = mSession.getDataHandler().getStore().getSummary(event.roomId);

                if (null != summary) {
                    // test if the latest event is refreshed
                    Event latestReceivedEvent = summary.getLatestReceivedEvent();
                    if ((null != latestReceivedEvent) && TextUtils.equals(latestReceivedEvent.eventId, event.eventId)) {
                        dispatchOnSummariesUpdate();
                    }
                }
            }
        };

        mSession.getDataHandler().addListener(mEventsListener);
    }

    /**
     * Remove the MXEventListener to the session listeners.
     */
    private void removeEventsListener() {
        if (mSession.isAlive()) {
            mSession.getDataHandler().removeListener(mEventsListener);
        }
    }

    CallLogFragment callLogFragment;

    private void setupViewPager(ViewPager viewPager) {
        adapter = new Adapter(getSupportFragmentManager());
        adapter.addFragment(HomeFragment.newInstance(), "CHATS");
        adapter.addFragment(PeopleFragment.newInstance(), "CONTACTS");
        adapter.addFragment(callLogFragment, "CALLS");
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(3);
        mCurrentFragmentTag = TAG_FRAGMENT_HOME;
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
          /*      switch (position) {
                    case 0:
                        findViewById(R.id.divider0).setVisibility(View.VISIBLE);
                        findViewById(R.id.divider1).setVisibility(View.INVISIBLE);
                        findViewById(R.id.divider2).setVisibility(View.INVISIBLE);
                        break;
                    case 1:
                        findViewById(R.id.divider0).setVisibility(View.INVISIBLE);
                        findViewById(R.id.divider1).setVisibility(View.VISIBLE);
                        findViewById(R.id.divider2).setVisibility(View.INVISIBLE);
                        break;
                    case 2:
                        findViewById(R.id.divider0).setVisibility(View.INVISIBLE);
                        findViewById(R.id.divider1).setVisibility(View.INVISIBLE);
                        findViewById(R.id.divider2).setVisibility(View.VISIBLE);
                        break;
                }*/
            }

            @Override
            public void onPageSelected(int position) {
                mCurrentMenuId = position;
                switch (position) {
                    case 0:
                        mCurrentFragmentTag = TAG_FRAGMENT_HOME;
                        tabs.getTabAt(0).getCustomView().setAlpha(1);
                        tabs.getTabAt(1).getCustomView().setAlpha(.7f);
                        tabs.getTabAt(2).getCustomView().setAlpha(.7f);
                        findViewById(R.id.divider0).setVisibility(View.VISIBLE);
                        findViewById(R.id.divider1).setVisibility(View.INVISIBLE);
                        findViewById(R.id.divider2).setVisibility(View.INVISIBLE);
                        break;
                    case 1:
                        mCurrentFragmentTag = TAG_FRAGMENT_PEOPLE;
                        tabs.getTabAt(0).getCustomView().setAlpha(.7f);
                        tabs.getTabAt(1).getCustomView().setAlpha(1);
                        tabs.getTabAt(2).getCustomView().setAlpha(.7f);
                        findViewById(R.id.divider0).setVisibility(View.INVISIBLE);
                        findViewById(R.id.divider1).setVisibility(View.VISIBLE);
                        findViewById(R.id.divider2).setVisibility(View.INVISIBLE);
                        break;
                    case 2:
                        mCurrentFragmentTag = TAG_FRAGMENT_CALL_LOG;
                        tabs.getTabAt(0).getCustomView().setAlpha(.7f);
                        tabs.getTabAt(1).getCustomView().setAlpha(.7f);
                        tabs.getTabAt(2).getCustomView().setAlpha(1);
                        findViewById(R.id.divider0).setVisibility(View.INVISIBLE);
                        findViewById(R.id.divider1).setVisibility(View.INVISIBLE);
                        findViewById(R.id.divider2).setVisibility(View.VISIBLE);
                        break;
                }
                tabs.setVisibility(View.VISIBLE);
                translatorButton.setVisibility(View.VISIBLE);
                if (getCurrentFocus() != null) {
                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
