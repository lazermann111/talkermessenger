package com.talkermessenger.activity;

import android.app.Activity;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.talkermessenger.Matrix;
import com.talkermessenger.R;
import com.talkermessenger.activity.settings.StatisticsSettingsActivity;
import com.talkermessenger.adapters.ParticipantAdapterItem;
import com.talkermessenger.util.PreferencesManager;
import com.talkermessenger.util.RoomUtils;
import com.talkermessenger.util.ThemeUtils;
import com.talkermessenger.util.VectorUtils;

import org.matrix.androidsdk.MXSession;
import org.matrix.androidsdk.data.Room;
import org.matrix.androidsdk.rest.callback.ApiCallback;
import org.matrix.androidsdk.rest.callback.SimpleApiCallback;
import org.matrix.androidsdk.rest.model.MatrixError;
import org.matrix.androidsdk.rest.model.RoomMember;
import org.matrix.androidsdk.util.Log;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GroupInfoActivity extends AppCompatActivity {

    public static final String EXTRA_ROOM_ID = "EXTRA_ROOM_ID";
    public static final String EXTRA_MATRIX_ID = "EXTRA_MATRIX_ID";
    private static final int INVITE_USER_REQUEST_CODE = 456;

    private static final int REQUEST_MESSAGE_NOTIFICATION_RINGTONE = 888;

    private MXSession mSession;
    private Room mRoom;

    @BindView(R.id.member_image)
    ImageView groupImage;

    @BindView(R.id.name_section_text)
    TextView roomName;

    @BindView(R.id.admin_section_text)
    TextView adminName;

    @BindView(R.id.group_section)
    RelativeLayout mGroupLayout;

    @BindView(R.id.favourite_section)
    RelativeLayout mFavouriteLayout;

    @BindView(R.id.show_all_media_section)
    RelativeLayout mShowAllMediaLayout;

    @BindView(R.id.statistics_section)
    RelativeLayout mStatisticsLayout;

    @BindView(R.id.leave_group_section)
    RelativeLayout mLeaveGroupLayout;

    @BindView(R.id.sound_section)
    RelativeLayout mSoundLayout;

    @BindView(R.id.sound_section_default)
    TextView mSoundTextView;


    @BindView(R.id.admin_section_icon)
    ImageView adminIcon;

    @BindView(R.id.save_incoming_section_switcher)
    SwitchCompat mSaveMediaMessageSwitcher;

    @BindView(R.id.add_members_section)
    RelativeLayout addMember;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_info);
        ButterKnife.bind(this);

        Intent intent = getIntent();

        String matrixId = intent.getStringExtra(EXTRA_MATRIX_ID);
        if (!TextUtils.isEmpty(matrixId))
            mSession = Matrix.getMXSession(this, matrixId);
        else
            mSession = Matrix.getInstance(this).getDefaultSession();

        String roomId = intent.getStringExtra(EXTRA_ROOM_ID);
        if (!TextUtils.isEmpty(matrixId))
            mRoom = mSession.getDataHandler().getRoom(roomId);

        if (mSession == null || mRoom == null)
            finish();

        setSupportActionBar(findViewById(R.id.account_toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        VectorUtils.loadRoomAvatar(this, mSession, groupImage, mRoom);
        String name = VectorUtils.getRoomDisplayName(this, mSession, mRoom);
        if (name != null && name.equals("test_group_im"))
            name = this.getResources().getString(R.string.im_group_name);
        roomName.setText(name);

        Collection<RoomMember> members = mRoom.getState().getMembers();
        for (RoomMember member : members) {
            if (CommonActivityUtils.isUserAdmin(mRoom, mSession, member.getUserId())) {
                if (mSession.getMyUserId().equals(member.getUserId())) {
                    adminName.setText(R.string.group_info_you);
                    adminIcon.setVisibility(View.VISIBLE);
                } else
                    adminName.setText(VectorUtils.getUserDisplayName(member.getUserId(), mSession, this));
                break;
            }
        }

        mSoundLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Log.d("---","Click group sound");
                final Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_NOTIFICATION);

                if (null != PreferencesManager.getGroupNotificationCustomRingTone(getBaseContext(), mRoom.getRoomId())) {
                    intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI,
                            PreferencesManager.getGroupNotificationCustomRingTone(getBaseContext(), mRoom.getRoomId()));
                }
                startActivityForResult(intent, REQUEST_MESSAGE_NOTIFICATION_RINGTONE);
            }

        });

        String soundName = PreferencesManager.getGroupNotificationCustomRingToneName(this, mRoom.getRoomId());

        mSoundTextView.setText(soundName != null ? soundName : getString(R.string.default_sound));

        mGroupLayout.setOnClickListener(v -> {
            Intent groupInfoIntent = new Intent(GroupInfoActivity.this, GroupInfoMembersActivity.class);
            groupInfoIntent.putExtra(GroupInfoActivity.EXTRA_ROOM_ID, roomId);
            groupInfoIntent.putExtra(GroupInfoActivity.EXTRA_MATRIX_ID, matrixId);
            startActivity(groupInfoIntent);
        });

//        mFavouriteLayout.setOnClickListener(view -> {
//            Intent favouriteMsgsIntent = new Intent(this, FavouriteMsgsActivity.class);
//            favouriteMsgsIntent.putExtra("roomId", mRoom.getRoomId());
//            startActivity(favouriteMsgsIntent);
//        });

        mFavouriteLayout.setOnClickListener(view -> {
//            Intent favouriteMsgsIntent = new Intent(this, VectorRoomActivity.class);
//            favouriteMsgsIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//            favouriteMsgsIntent.putExtra("isFavouriteMode", true);
//            favouriteMsgsIntent.putExtra(EXTRA_ROOM_ID, roomId);
//            favouriteMsgsIntent.putExtra("roomId", roomId);
//            startActivity(favouriteMsgsIntent);

            RoomUtils.openFavoutiteMsgs(this, mSession, roomId, null);
        });

        mShowAllMediaLayout.setOnClickListener(v -> {
            Intent mediaIntent = new Intent(this, VectorMemberMediaActivity.class);
//            mediaIntent.putExtra("userId", "Group");
            mediaIntent.putExtra("roomId", mRoom.getRoomId());
            startActivity(mediaIntent);
        });

        mStatisticsLayout.setOnClickListener(view -> {
            Intent statisticsIntent = new Intent(this, StatisticsSettingsActivity.class);
            statisticsIntent.putExtra("roomId", mRoom.getRoomId());
            startActivity(statisticsIntent);
        });

        mSaveMediaMessageSwitcher.setChecked(PreferencesManager.isSaveMediaForRoomAllow(this, mRoom.getRoomId()));
        mSaveMediaMessageSwitcher.setOnCheckedChangeListener((buttonView, isChecked) ->
                PreferencesManager.setSaveMediaForRoomAllow(this, isChecked, mRoom.getRoomId())
        );

        final ApiCallback<Void> mRoomActionsListener = new SimpleApiCallback<Void>(this) {
            @Override
            public void onMatrixError(MatrixError e) {
                Toast.makeText(GroupInfoActivity.this, e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onSuccess(Void info) {
                Intent homeIntent = new Intent(GroupInfoActivity.this, VectorHomeActivity.class);
                startActivity(homeIntent);
            }

            @Override
            public void onNetworkError(Exception e) {
                Toast.makeText(GroupInfoActivity.this, e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onUnexpectedError(Exception e) {
                Toast.makeText(GroupInfoActivity.this, e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        };

        mLeaveGroupLayout.setOnClickListener(v -> {
            mRoom.leave(mRoomActionsListener);
            Toast.makeText(this, R.string.group_was_left, Toast.LENGTH_LONG).show();
        });
        addMember.setOnClickListener(v -> launchSearchActivity());
    }

    /***
     * Launch the people search activity
     */
    private void launchSearchActivity() {
        Intent intent = new Intent(this, VectorRoomInviteGroupMembersActivity.class);
        intent.putExtra(VectorRoomInviteGroupMembersActivity.EXTRA_MATRIX_ID, mSession.getMyUserId());
        //  intent.putExtra(VectorRoomInviteGroupMembersActivity.EXTRA_HIDDEN_PARTICIPANT_ITEMS, null);
        this.startActivityForResult(intent, INVITE_USER_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_MESSAGE_NOTIFICATION_RINGTONE: {
                    Uri uri = data.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
                    PreferencesManager.setGroupNotificationCustomRingTone(this, uri, mRoom.getRoomId());
                    String soundName = PreferencesManager.getGroupNotificationCustomRingToneName(this, mRoom.getRoomId());
                    mSoundTextView.setText(soundName != null ? soundName : getString(R.string.default_sound));
                    break;
                }

                case INVITE_USER_REQUEST_CODE: {
                    if (null != mRoom) {
                        List<ParticipantAdapterItem> items = (List<ParticipantAdapterItem>) data.getSerializableExtra(VectorRoomInviteMembersActivity.EXTRA_OUT_SELECTED_PARTICIPANT_ITEMS);
                        mRoom.invite(items.get(0).mUserId, new ApiCallback<Void>() {
                            @Override
                            public void onSuccess(Void info) {
                                Log.e("add_member sucsex", items.get(0).mUserId);
                            }

                            @Override
                            public void onNetworkError(Exception e) {

                            }

                            @Override
                            public void onMatrixError(MatrixError e) {

                            }

                            @Override
                            public void onUnexpectedError(Exception e) {

                            }
                        });
                    }
                }
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_group_info, menu);
        CommonActivityUtils.tintMenuIcons(menu, ThemeUtils.getColor(this, R.attr.icon_tint_on_dark_action_bar_color));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.ic_action_export:
                try {
                    RoomUtils.exportChat(mRoom.getRoomId(), mSession, this, "");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.ic_action_clear:
                String existingRoomId;
                RoomUtils.clearChat(mRoom);
                Toast.makeText(this, getResources().getString(R.string.chat_was_cleared), Toast.LENGTH_LONG).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
