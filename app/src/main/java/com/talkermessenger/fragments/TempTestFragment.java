package com.talkermessenger.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.talkermessenger.R;

/**
 * Created by dev_serhii on 18.01.2018.
 */

public class TempTestFragment extends android.support.v4.app.Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_temp_test, container, false);
    }

    public static TempTestFragment newInstance() {
        return new TempTestFragment();
    }
}
