/*
 * Copyright 2017 Vector Creations Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.talkermessenger.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;

import com.talkermessenger.R;
import com.talkermessenger.activity.MXCActionBarActivity;
import com.talkermessenger.activity.VectorRoomCreationActivity;
import com.talkermessenger.activity.settings.ArchivedChatsActivity;
import com.talkermessenger.activity.settings.NewSettingActivity;
import com.talkermessenger.adapters.ParticipantAdapterItem;
import com.talkermessenger.adapters.PeopleAdapter;
import com.talkermessenger.contacts.Contact;
import com.talkermessenger.contacts.ContactsManager;
import com.talkermessenger.util.PreferencesManager;
import com.talkermessenger.util.RoomUtils;
import com.talkermessenger.util.ThemeUtils;
import com.talkermessenger.util.VectorUtils;
import com.talkermessenger.view.EmptyViewItemDecoration;
import com.talkermessenger.view.SimpleDividerItemDecoration;

import org.matrix.androidsdk.MXDataHandler;
import org.matrix.androidsdk.data.Room;
import org.matrix.androidsdk.data.RoomSummary;
import org.matrix.androidsdk.data.RoomTag;
import org.matrix.androidsdk.data.store.IMXStore;
import org.matrix.androidsdk.listeners.MXEventListener;
import org.matrix.androidsdk.rest.callback.ApiCallback;
import org.matrix.androidsdk.rest.model.Event;
import org.matrix.androidsdk.rest.model.MatrixError;
import org.matrix.androidsdk.rest.model.Search.SearchUsersResponse;
import org.matrix.androidsdk.rest.model.User;
import org.matrix.androidsdk.util.Log;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;

public class HomeFragment extends AbsHomeFragment implements AbsHomeFragment.OnRoomChangedListener, ContactsManager.ContactsManagerListener {

    private static final String LOG_TAG = HomeFragment.class.getSimpleName();

    private static final int MAX_KNOWN_CONTACTS_FILTER_COUNT = 50;

    @BindView(R.id.recyclerview)
    RecyclerView mRecycler;


    private PeopleAdapter mAdapter;

    private final List<Room> mDirectChats = new ArrayList<>();

    private MXEventListener mEventsListener;

    /*
     * *********************************************************************************************
     * Static methods
     * *********************************************************************************************
     */

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    /*
     * *********************************************************************************************
     * Fragment lifecycle
     * *********************************************************************************************
     */


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_people, container, false);
    }

    @Override
    public void onActivityCreated(final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mEventsListener = new MXEventListener() {

            @Override
            public void onPresenceUpdate(final Event event, final User user) {
                mAdapter.updateKnownContact(user);
            }
        };
        updateTag(getString(R.string.test_room_id), null, RoomTag.ROOM_TAG_PIN);
        initViews();

        mOnRoomChangedListener = this;

        mAdapter.onFilterDone(mCurrentFilter);

    }

    @Override
    public void onResume() {
        super.onResume();
        mSession.getDataHandler().addListener(mEventsListener);
        ContactsManager.getInstance().addListener(this);
        // Direct chats
        initDirectChatsData();
        initDirectChatsViews();

        mAdapter.setInvitation(mActivity.getRoomInvitations());

        mRecycler.addOnScrollListener(mScrollListener);
    }

    @Override
    public void onPause() {
        super.onPause();

        if (mSession.isAlive()) {
            mSession.getDataHandler().removeListener(mEventsListener);
        }
        //  ContactsManager.getInstance().removeListener(this);
        mRecycler.removeOnScrollListener(mScrollListener);
        // cancel any search
        mSession.cancelUsersSearch();
    }



    /*
     * *********************************************************************************************
     * Abstract methods implementation
     * *********************************************************************************************
     */

    @Override
    protected List<Room> getRooms() {
        return new ArrayList<>(mDirectChats);
    }

    @Override
    protected void onFilter(final String pattern, final OnFilterListener listener) {
        mAdapter.getFilter().filter(pattern, new Filter.FilterListener() {
            @Override
            public void onFilterComplete(int count) {
                boolean newSearch = TextUtils.isEmpty(mCurrentFilter) && !TextUtils.isEmpty(pattern);

                Log.i(LOG_TAG, "onFilterComplete " + count);
                if (listener != null) {
                    listener.onFilterDone(count);
                }

                startRemoteKnownContactsSearch(newSearch);
            }
        });
    }

    @Override
    protected void onResetFilter() {
        mAdapter.getFilter().filter("", new Filter.FilterListener() {
            @Override
            public void onFilterComplete(int count) {
                Log.i(LOG_TAG, "onResetFilter " + count);
            }
        });
    }

    /*
     * *********************************************************************************************
     * UI management
     * *********************************************************************************************
     */

    /**
     * Prepare views
     */
    private void initViews() {
        int margin = (int) getResources().getDimension(R.dimen.item_decoration_left_margin);
        mRecycler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mRecycler.addItemDecoration(new SimpleDividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL, margin));
        mRecycler.addItemDecoration(new EmptyViewItemDecoration(getActivity(), DividerItemDecoration.VERTICAL, 40, 16, 14));
        mAdapter = new PeopleAdapter(getActivity(), "home", new PeopleAdapter.OnSelectItemListener() {
            @Override
            public void onSelectItem(Room room, int position) {
                openRoom(room);
            }

            @Override
            public void onSelectItem(ParticipantAdapterItem contact, int position) {
                //  onContactSelected(contact);
            }

            @Override
            public void onSelectAvatar(ParticipantAdapterItem item, View view) {

            }
        }, this, this);
        mRecycler.setAdapter(mAdapter);

    }

    /*
     * *********************************************************************************************
     * Data management
     * *********************************************************************************************
     */

    /**
     * Change the tag of the given room with the provided one
     *
     * @param roomId
     * @param newTagOrder
     * @param newTag
     */
    private void updateTag(final String roomId, Double newTagOrder, final String newTag) {
        Log.e("update tag request", "waiting");
        RoomUtils.updateRoomTag(mSession, roomId, newTagOrder, newTag, new ApiCallback<Void>() {
            @Override
            public void onSuccess(Void info) {
                Log.e("update tag request", "waiting");
                initViews();
            }

            @Override
            public void onNetworkError(Exception e) {
                Log.e("update tag request", "network error");
            }

            @Override
            public void onMatrixError(MatrixError e) {
                Log.e("update tag request", "matrix error");
            }

            @Override
            public void onUnexpectedError(Exception e) {
                Log.e("update tag request", "unexpected");
            }
        });
    }

    /**
     * Fill the direct chats adapter with data
     */
    private void initDirectChatsData() {
        if ((null == mSession) || (null == mSession.getDataHandler())) {
            Log.e(LOG_TAG, "## initDirectChatsData() : null session");
        }
        final List<String> directChatIds = mSession.getDirectChatRoomIdsList();
        final MXDataHandler dataHandler = mSession.getDataHandler();
        final IMXStore store = dataHandler.getStore();
        final List<Room> mDirectNotPinChats = new ArrayList<>();
        mDirectChats.clear();
        if (directChatIds != null && !directChatIds.isEmpty()) {
            for (String roomId : directChatIds) {
                Room room = store.getRoom(roomId);
                if (null != room && !room.isConferenceUserRoom()) {
                    // it seems that the server syncs some left rooms
                    if (null == room.getMember(mSession.getMyUserId())) {
                        Log.e(LOG_TAG, "## initDirectChatsData(): invalid room " + room.getRoomId() + ", the user is not anymore member of it");
                    } else {
                        final Set<String> tags = room.getAccountData().getKeys();
                        if ((null == tags) || !tags.contains(RoomTag.ROOM_TAG_LOW_PRIORITY)) {
                            final boolean isArchived = tags != null && tags.contains(RoomTag.ROOM_TAG_ARCHIVED);
                            final boolean isDeleted = tags != null && tags.contains(RoomTag.ROOM_TAG_DELETED);
                            final boolean isPin = tags != null && tags.contains(RoomTag.ROOM_TAG_PIN);
                            final boolean isFavourite = roomId.equals(PreferencesManager.getFavouriteMsgCommonRoom(getActivity()));
                            final boolean isNameEmptyChat = VectorUtils.getRoomDisplayName(getContext(), mSession, room).equals(getContext().getString(R.string.room_displayname_no_title));
                            IMXStore imxStore = mSession.getDataHandler().getStore(roomId);
                            Collection<Event> allEvents = imxStore.getRoomMessages(roomId);
                            ArrayList<Event> roomMessages = new ArrayList<Event>();
                            if (allEvents != null && allEvents.size() > 0)
                                for (Event event : allEvents) {
                                    if (event.getType().equals(Event.EVENT_TYPE_MESSAGE))
                                        roomMessages.add(event);
                                }
                            Event latestEvent = imxStore.getLatestEvent(roomId);
                            long originServerTs;
                            if (latestEvent != null)
                                originServerTs = latestEvent.getOriginServerTs();
                            else
                                originServerTs = 0;
                            boolean isEmpty = roomMessages.size() == 0 && System.currentTimeMillis() - originServerTs > 12 * 60 * 60 * 1000;

                            if (!isArchived && !isDeleted && !isFavourite && !isNameEmptyChat && !isEmpty) {
                                if (isPin)
                                    mDirectChats.add(dataHandler.getRoom(roomId));
                                else mDirectNotPinChats.add(dataHandler.getRoom(roomId));
                            }
                        }
                    }
                }
            }
        }
        List<RoomSummary> roomSummaries = new ArrayList<>(store.getSummaries());
        HashSet<String> directChatRoomIds = new HashSet<>(mSession.getDirectChatRoomIdsList());
        HashSet<String> lowPriorityRoomIds = new HashSet<>(mSession.roomIdsWithTag(RoomTag.ROOM_TAG_LOW_PRIORITY));
        HashSet<String> deletedRoomIds = new HashSet<>(mSession.roomIdsWithTag(RoomTag.ROOM_TAG_DELETED));
        for (RoomSummary summary : roomSummaries) {
            // don't display the invitations
            if (!summary.isInvited()) {
                Room room = store.getRoom(summary.getRoomId());
                // test
                if ((null != room) && // if the room still exists
                        !room.isConferenceUserRoom() && // not a VOIP conference room
                        !directChatRoomIds.contains(room.getRoomId()) &&
                        !lowPriorityRoomIds.contains(room.getRoomId()) &&
                        !deletedRoomIds.contains(room.getRoomId())) {
                    final Set<String> tags = room.getAccountData().getKeys();
                    final boolean isArchived = tags != null && tags.contains(RoomTag.ROOM_TAG_ARCHIVED);
                    final boolean isPin = tags != null && tags.contains(RoomTag.ROOM_TAG_PIN);
                    final boolean isDeleted = tags != null && tags.contains(RoomTag.ROOM_TAG_DELETED);
                    final boolean isFavourite = room.getRoomId().equals(PreferencesManager.getFavouriteMsgCommonRoom(getActivity()));
                    final boolean isNameEmptyChat = VectorUtils.getRoomDisplayName(getContext(), mSession, room).equals(getContext().getString(R.string.room_displayname_no_title));
                    IMXStore imxStore = mSession.getDataHandler().getStore(summary.getRoomId());
                    Collection<Event> allEvents = imxStore.getRoomMessages(summary.getRoomId());
                    ArrayList<Event> roomMessages = new ArrayList<Event>();
                    for (Event event : allEvents) {
                        if (event.getType().equals(Event.EVENT_TYPE_MESSAGE))
                            roomMessages.add(event);
                    }
                    Event latestEvent = imxStore.getLatestEvent(summary.getRoomId());
                    long originServerTs = latestEvent.getOriginServerTs();
                    boolean isEmpty = roomMessages.size() == 0 && System.currentTimeMillis() - originServerTs > 12 * 60 * 60 * 1000;

                    if (!isArchived && !isDeleted && !isFavourite && !isNameEmptyChat && !isEmpty) {
                        if (isPin)
                            mDirectChats.add(room);
                        else mDirectNotPinChats.add(room);
                    }
                }
            }
        }
        mDirectChats.addAll(mDirectNotPinChats);
    }

    /**
     * Display the public rooms loading view
     */
    private void showKnownContactLoadingView() {
        mAdapter.getSectionViewForSectionIndex(mAdapter.getSectionsCount() - 1).showLoadingView();
    }

    /**
     * Hide the public rooms loading view
     */
    private void hideKnownContactLoadingView() {
        mAdapter.getSectionViewForSectionIndex(mAdapter.getSectionsCount() - 1).hideLoadingView();
    }

    /**
     * Trigger a request to search known contacts.
     *
     * @param isNewSearch true if the search is a new one
     */
    private void startRemoteKnownContactsSearch(boolean isNewSearch) {
        if (!TextUtils.isEmpty(mCurrentFilter)) {

            // display the known contacts section
            if (isNewSearch) {
                mAdapter.setFilteredKnownContacts(new ArrayList<ParticipantAdapterItem>(), mCurrentFilter);
                showKnownContactLoadingView();
            }
            final String fPattern = mCurrentFilter;
            mSession.searchUsers(mCurrentFilter, MAX_KNOWN_CONTACTS_FILTER_COUNT, new HashSet<String>(), new ApiCallback<SearchUsersResponse>() {
                @Override
                public void onSuccess(SearchUsersResponse searchUsersResponse) {
                    if (TextUtils.equals(fPattern, mCurrentFilter)) {
                        hideKnownContactLoadingView();

                        List<ParticipantAdapterItem> list = new ArrayList<>();

                        if (null != searchUsersResponse.results) {
                            for (User user : searchUsersResponse.results) {
                                list.add(new ParticipantAdapterItem(user));
                            }
                        }

                        mAdapter.setKnownContactsExtraTitle(null);
                        mAdapter.setKnownContactsLimited((null != searchUsersResponse.limited) ? searchUsersResponse.limited : false);
                        mAdapter.setFilteredKnownContacts(list, mCurrentFilter);
                    }
                }

                private void onError(String errorMessage) {
                    Log.e(LOG_TAG, "## startRemoteKnownContactsSearch() : failed " + errorMessage);
                    //
                    if (TextUtils.equals(fPattern, mCurrentFilter)) {
                        hideKnownContactLoadingView();
                        mAdapter.setKnownContactsExtraTitle(HomeFragment.this.getContext().getString(R.string.offline));
                        mAdapter.filterAccountKnownContacts(mCurrentFilter);
                    }
                }

                @Override
                public void onNetworkError(Exception e) {
                    onError(e.getMessage());
                }

                @Override
                public void onMatrixError(MatrixError e) {
                    onError(e.getMessage());
                }

                @Override
                public void onUnexpectedError(Exception e) {
                    onError(e.getMessage());
                }
            });
        }
    }


    /**
     * Init direct chats view with data and update its display
     */
    private void initDirectChatsViews() {
        mAdapter.setRooms(mDirectChats);
    }


    /*
     * *********************************************************************************************
     * Listeners
     * *********************************************************************************************
     */

    @Override
    public void onSummariesUpdate() {
        super.onSummariesUpdate();

        if (isResumed()) {
            mAdapter.setInvitation(mActivity.getRoomInvitations());

            initDirectChatsData();
            initDirectChatsViews();
        }
    }

    @Override
    public void onToggleDirectChat(String roomId, boolean isDirectChat) {
        if (!isDirectChat) {
            mAdapter.removeDirectChat(roomId);
        }
    }

    @Override
    public void onRoomLeft(String roomId) {
        mAdapter.removeDirectChat(roomId);
    }

    @Override
    public void onRoomDeleted(String roomId) {
        mAdapter.removeDirectChat(roomId);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_home_fragment, menu);
        MenuItem mSearchMenuItem = menu.findItem(R.id.action_search);
        if (ThemeUtils.getApplicationTheme(getContext()).equals(ThemeUtils.THEME_LIGHT_LIGHT_BLUE_HEADER_VALUE)) {
            mSearchMenuItem.setIcon(getResources().getDrawable(R.drawable.ic_material_search_white));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean retCode = true;

        switch (item.getItemId()) {

            case R.id.ic_action_favourite:

//                String roomId = PreferencesManager.getFavouriteMsgCommonRoom(getActivity());
//                if (roomId != null)
//                    RoomUtils.openFavouriteMsgRoomById(roomId, mSession, getActivity());
//                else {
//                    Room favouriteMsgsRoom = RoomUtils.findFavouriteMsgsRoom(mSession);
//                    if (favouriteMsgsRoom != null)
//                        RoomUtils.openFavouriteMsgRoomById(favouriteMsgsRoom.getRoomId(), mSession, getActivity());
//                    else
//                        RoomUtils.createFavouriteMsgRoom(mSession, getActivity());
//                }

                RoomUtils.openFavoutiteMsgs(getActivity(), mSession, null, null);
                break;

//            case R.id.ic_action_new_group:
//                invitePeopleToNewRoom(false);
//                break;
//
//            case R.id.ic_action_edit:
//                break;
//
//            case R.id.ic_action_selected_messages:
//                break;

            case R.id.ic_action_archieved_chats:
                startActivity(new Intent(getContext(), ArchivedChatsActivity.class));
                break;

//            case R.id.ic_action_settings:
//                final Intent settingsIntent = new Intent(getContext(), SettingsActivity.class);
//                settingsIntent.putExtra(MXCActionBarActivity.EXTRA_MATRIX_ID, mSession.getMyUserId());
//                getContext().startActivity(settingsIntent);
//                break;

            case R.id.ic_action_settings:
                final Intent settingsIntent = new Intent(getContext(), NewSettingActivity.class);
                settingsIntent.putExtra(MXCActionBarActivity.EXTRA_MATRIX_ID, mSession.getMyUserId());
                getContext().startActivity(settingsIntent);
                break;

            default:
                // not handled item, return the super class implementation value
                retCode = super.onOptionsItemSelected(item);
                break;
        }
        return retCode;
    }

    /**
     * Open the room creation with inviting people.
     */
    public void invitePeopleToNewRoom(boolean isCall) {
        final Intent settingsIntent = new Intent(mActivity, VectorRoomCreationActivity.class);
        settingsIntent.putExtra(MXCActionBarActivity.EXTRA_MATRIX_ID, mSession.getMyUserId());
        if (isCall)
            settingsIntent.putExtra("isCall", isCall);
        startActivity(settingsIntent);
    }

    @Override
    public void onRefresh() {

    }

    @Override
    public void onPIDsUpdate() {
       // initDirectChatsData();
       // initDirectChatsViews();
    }

    @Override
    public void onContactPresenceUpdate(Contact contact, String matrixId) {

    }
}
