/*
 * Copyright 2017 Vector Creations Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.talkermessenger.fragments;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.iskar.talkersdk.ContactHandler;
import com.iskar.talkersdk.model.TalkerContact;
import com.talkermessenger.R;
import com.talkermessenger.activity.CommonActivityUtils;
import com.talkermessenger.activity.MXCActionBarActivity;
import com.talkermessenger.activity.VectorMemberDetailsActivity;
import com.talkermessenger.activity.settings.AccountActivity;
import com.talkermessenger.activity.settings.NewSettingActivity;
import com.talkermessenger.adapters.ParticipantAdapterItem;
import com.talkermessenger.adapters.PeopleAdapter;
import com.talkermessenger.contacts.Contact;
import com.talkermessenger.contacts.ContactsManager;
import com.talkermessenger.util.ThemeUtils;
import com.talkermessenger.util.VectorUtils;
import com.talkermessenger.view.EmptyViewItemDecoration;
import com.talkermessenger.view.SimpleDividerItemDecoration;

import org.matrix.androidsdk.MXDataHandler;
import org.matrix.androidsdk.data.MyUser;
import org.matrix.androidsdk.data.Room;
import org.matrix.androidsdk.data.RoomSummary;
import org.matrix.androidsdk.data.RoomTag;
import org.matrix.androidsdk.data.store.IMXStore;
import org.matrix.androidsdk.listeners.MXEventListener;
import org.matrix.androidsdk.rest.callback.ApiCallback;
import org.matrix.androidsdk.rest.model.Event;
import org.matrix.androidsdk.rest.model.MatrixError;
import org.matrix.androidsdk.rest.model.Search.SearchUsersResponse;
import org.matrix.androidsdk.rest.model.User;
import org.matrix.androidsdk.util.Log;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class PeopleFragment extends AbsHomeFragment implements ContactsManager.ContactsManagerListener, AbsHomeFragment.OnRoomChangedListener {

    private static final String LOG_TAG = PeopleFragment.class.getSimpleName();

//    private static final String MATRIX_USER_ONLY_PREF_KEY = "MATRIX_USER_ONLY_PREF_KEY";

    private static final int MAX_KNOWN_CONTACTS_FILTER_COUNT = 50;

    @BindView(R.id.recyclerview)
    RecyclerView mRecycler;

    @BindView(R.id.contact_avatar)
    CircleImageView mContactAvatar;

    @BindView(R.id.contact_name)
    TextView mContactName;

    @BindView(R.id.current_user_info)
    RelativeLayout mCurrentUserInfo;

    private MyUser mUser;

//    private CheckBox mMatrixUserOnlyCheckbox;

    private PeopleAdapter mAdapter;

    private final List<Room> mDirectChats = new ArrayList<>();
    private final List<ParticipantAdapterItem> mLocalContacts = new ArrayList<>();
    // the known contacts are not sorted
    private final List<ParticipantAdapterItem> mKnownContacts = new ArrayList<>();

    // way to detect that the contacts list has been updated
    private int mContactsSnapshotSession = -1;
    private MXEventListener mEventsListener;

    /*
     * *********************************************************************************************
     * Static methods
     * *********************************************************************************************
     */

    public static PeopleFragment newInstance() {
        return new PeopleFragment();
    }

    /*
     * *********************************************************************************************
     * Fragment lifecycle
     * *********************************************************************************************
     */

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_people_talker, container, false);
    }

    @Override
    public void onActivityCreated(final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mEventsListener = new MXEventListener() {

            @Override
            public void onPresenceUpdate(final Event event, final User user) {
                mAdapter.updateKnownContact(user);
            }
        };

        initViews();

        mOnRoomChangedListener = this;

//        mMatrixUserOnlyCheckbox.setChecked(PreferenceManager.getDefaultSharedPreferences(getActivity()).getBoolean(MATRIX_USER_ONLY_PREF_KEY, false));
//        mMatrixUserOnlyCheckbox.setChecked(false);

        mAdapter.onFilterDone(mCurrentFilter);

        if (!ContactsManager.getInstance().isContactBookAccessRequested()) {
            CommonActivityUtils.checkPermissions(CommonActivityUtils.REQUEST_CODE_PERMISSION_MEMBERS_SEARCH, this);
        }
    }

    private void setupUserData() {
        if (mSession != null) {
            mUser = mSession.getMyUser();
            if (null != mUser) {
                VectorUtils.loadUserAvatar(getActivity(), mSession, mContactAvatar, mUser);
                mContactName.setText(mUser.displayname);
                mCurrentUserInfo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getActivity(), AccountActivity.class);
                        startActivity(intent);
                    }
                });
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mSession.getDataHandler().addListener(mEventsListener);
        ContactsManager.getInstance().addListener(this);
        // Direct chats
        //initDirectChatsData();
        // initDirectChatsViews();

        // Local address book
        initContactsData();
        initContactsViews();

        mAdapter.setInvitation(mActivity.getRoomInvitations());

        mRecycler.addOnScrollListener(mScrollListener);

        setupUserData();
       // initKnownContacts();
    }

    @Override
    public void onPause() {
        super.onPause();

        if (mSession.isAlive()) {
            mSession.getDataHandler().removeListener(mEventsListener);
        }
        ContactsManager.getInstance().removeListener(this);

        mRecycler.removeOnScrollListener(mScrollListener);

        // cancel any search
        mSession.cancelUsersSearch();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == CommonActivityUtils.REQUEST_CODE_PERMISSION_MEMBERS_SEARCH) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && ContactsManager.getInstance().getLocalContactsSnapshotSession() == -1) {
                if (new ContactHandler().isContactSnapshotAvailable(getContext())) {
                    Log.e(LOG_TAG, "initialize contacts from snapshot");
                    initContactsData();
                    initContactsViews();
                } else {
                    Log.e(LOG_TAG, "refresh contacts, then init");
                    ContactsManager.getInstance().refreshLocalContactsSnapshot();
                    initContactsData();
                    initContactsViews();
                }
            }
        }
    }

    /*
     * *********************************************************************************************
     * Abstract methods implementation
     * *********************************************************************************************
     */

    @Override
    protected List<Room> getRooms() {
        return new ArrayList<>(mDirectChats);
    }

    @Override
    protected void onFilter(final String pattern, final OnFilterListener listener) {
        mAdapter.getFilter().filter(pattern, new Filter.FilterListener() {
            @Override
            public void onFilterComplete(int count) {
                boolean newSearch = TextUtils.isEmpty(mCurrentFilter) && !TextUtils.isEmpty(pattern);

                Log.i(LOG_TAG, "onFilterComplete " + count);
                if (listener != null) {
                    listener.onFilterDone(count);
                }

                startRemoteKnownContactsSearch(newSearch);
            }
        });
    }

    @Override
    protected void onResetFilter() {
        mAdapter.getFilter().filter("", new Filter.FilterListener() {
            @Override
            public void onFilterComplete(int count) {
                Log.i(LOG_TAG, "onResetFilter " + count);
            }
        });
    }

    /*
     * *********************************************************************************************
     * UI management
     * *********************************************************************************************
     */

    /**
     * Prepare views
     */

    private void initViews() {
        int margin = (int) getResources().getDimension(R.dimen.item_decoration_left_margin);
        mRecycler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mRecycler.addItemDecoration(new SimpleDividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL, margin));
        mRecycler.addItemDecoration(new EmptyViewItemDecoration(getActivity(), DividerItemDecoration.VERTICAL, 40, 16, 14));
        mAdapter = new PeopleAdapter(getActivity(), "peoples", new PeopleAdapter.OnSelectItemListener() {
            @Override
            public void onSelectItem(Room room, int position) {
                openRoom(room);
            }

            @Override
            public void onSelectItem(ParticipantAdapterItem contact, int position) {
                if (!contact.mUserId.equals(mSession.getMyUserId()))
                    onContactSelected(contact);
                else
                    Toast.makeText(getContext(), R.string.its_your_profile, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSelectAvatar(ParticipantAdapterItem contact, View view) {
                if (!contact.mUserId.equals(mSession.getMyUserId()))
                    onAvatarSelected(contact, view);
            }
        }, this, this);
        mRecycler.setAdapter(mAdapter);

//        View checkBox = mAdapter.findSectionSubViewById(R.id.matrix_only_filter_checkbox);
//        if (checkBox != null && checkBox instanceof CheckBox) {
//            mMatrixUserOnlyCheckbox = (CheckBox) checkBox;
//            mMatrixUserOnlyCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
//                    SharedPreferences.Editor editor = preferences.edit();
//                    editor.putBoolean(MATRIX_USER_ONLY_PREF_KEY, mMatrixUserOnlyCheckbox.isChecked());
//                    editor.commit();
//
//                    initContactsViews();
//                }
//            });
//        }
    }

    /*
     * *********************************************************************************************
     * Data management
     * *********************************************************************************************
     */

    /**
     * Fill the direct chats adapter with data
     */
    private void initDirectChatsData() {
        if ((null == mSession) || (null == mSession.getDataHandler())) {
            Log.e(LOG_TAG, "## initDirectChatsData() : null session");
        }

        final List<String> directChatIds = mSession.getDirectChatRoomIdsList();
        final MXDataHandler dataHandler = mSession.getDataHandler();
        final IMXStore store = dataHandler.getStore();

        mDirectChats.clear();
        if (directChatIds != null && !directChatIds.isEmpty()) {
            for (String roomId : directChatIds) {
                Room room = store.getRoom(roomId);

                if ((null != room) && !room.isConferenceUserRoom()) {
                    // it seems that the server syncs some left rooms
                    if (null == room.getMember(mSession.getMyUserId())) {
                        Log.e(LOG_TAG, "## initDirectChatsData(): invalid room " + room.getRoomId() + ", the user is not anymore member of it");
                    } else {
                        final Set<String> tags = room.getAccountData().getKeys();
                        if ((null == tags) || !tags.contains(RoomTag.ROOM_TAG_LOW_PRIORITY)) {
                            mDirectChats.add(dataHandler.getRoom(roomId));
                        }
                    }
                }
            }
        }
        List<RoomSummary> roomSummaries = new ArrayList<>(store.getSummaries());
        HashSet<String> directChatRoomIds = new HashSet<>(mSession.getDirectChatRoomIdsList());
        HashSet<String> lowPriorityRoomIds = new HashSet<>(mSession.roomIdsWithTag(RoomTag.ROOM_TAG_LOW_PRIORITY));
        for (RoomSummary summary : roomSummaries) {
            // don't display the invitations
            if (!summary.isInvited()) {
                Room room = store.getRoom(summary.getRoomId());

                // test
                if ((null != room) && // if the room still exists
                        !room.isConferenceUserRoom() && // not a VOIP conference room
                        !directChatRoomIds.contains(room.getRoomId()) &&
                        !lowPriorityRoomIds.contains(room.getRoomId())) {
                    mDirectChats.add(room);
                }
            }
        }
    }

    /**
     * Fill the local address book and known contacts adapters with data
     */
    private void initContactsData() {
        mLocalContacts.clear();
        List<ParticipantAdapterItem> participants = new ArrayList<>(getTalkerContacts());
        if (getTalkerContacts() != null && getTalkerContacts().size() > 0) {
            Log.e("getTalkerContacts", getTalkerContacts().size() + "");
            for (ParticipantAdapterItem item : participants) {
                if (item.mContact != null) {
                    mLocalContacts.add(item);
                }
            }
        }
    }

    /**
     * Get the known contacts list, sort it by presence and give it to adapter
     */
    private void initKnownContacts() {
        @SuppressLint("StaticFieldLeak") final AsyncTask<Void, Void, Void> asyncTask = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                // do not sort anymore the full known participants list
                // as they are not displayed unfiltered
                // it saves a lot of times
                // eg with about 17000 items
                // sort requires about 2 seconds
                // sort a 1000 items subset during a search requires about 75ms
                mKnownContacts.clear();
                mKnownContacts.addAll(new ArrayList<>(VectorUtils.listKnownParticipants(mSession).values()));
                return null;
            }

            @Override
            protected void onPostExecute(Void args) {
                mAdapter.setKnownContacts(mKnownContacts);
            }
        };

        try {
            asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } catch (final Exception e) {
            Log.e(LOG_TAG, "## initKnownContacts() failed " + e.getMessage());
            asyncTask.cancel(true);

            (new android.os.Handler(Looper.getMainLooper())).postDelayed(new Runnable() {
                @Override
                public void run() {
                    initKnownContacts();
                }
            }, 1000);
        }
    }

    /**
     * Display the public rooms loading view
     */
    private void showKnownContactLoadingView() {
        mAdapter.getSectionViewForSectionIndex(mAdapter.getSectionsCount() - 1).showLoadingView();
    }

    /**
     * Hide the public rooms loading view
     */
    private void hideKnownContactLoadingView() {
        mAdapter.getSectionViewForSectionIndex(mAdapter.getSectionsCount() - 1).hideLoadingView();
    }

    /**
     * Trigger a request to search known contacts.
     *
     * @param isNewSearch true if the search is a new one
     */
    private void startRemoteKnownContactsSearch(boolean isNewSearch) {
        if (!TextUtils.isEmpty(mCurrentFilter)) {

            // display the known contacts section
            if (isNewSearch) {
                mAdapter.setFilteredKnownContacts(new ArrayList<ParticipantAdapterItem>(), mCurrentFilter);
                showKnownContactLoadingView();
            }

            final String fPattern = mCurrentFilter;

            mSession.searchUsers(mCurrentFilter, MAX_KNOWN_CONTACTS_FILTER_COUNT, new HashSet<String>(), new ApiCallback<SearchUsersResponse>() {
                @Override
                public void onSuccess(SearchUsersResponse searchUsersResponse) {
                    if (TextUtils.equals(fPattern, mCurrentFilter)) {
                        hideKnownContactLoadingView();

                        List<ParticipantAdapterItem> list = new ArrayList<>();

                        if (null != searchUsersResponse.results) {
                            for (User user : searchUsersResponse.results) {
                                list.add(new ParticipantAdapterItem(user));
                            }
                        }

                        mAdapter.setKnownContactsExtraTitle(null);
                        mAdapter.setKnownContactsLimited((null != searchUsersResponse.limited) ? searchUsersResponse.limited : false);
                        mAdapter.setFilteredKnownContacts(list, mCurrentFilter);
                    }
                }

                private void onError(String errorMessage) {
                    Log.e(LOG_TAG, "## startRemoteKnownContactsSearch() : failed " + errorMessage);
                    //
                    if (TextUtils.equals(fPattern, mCurrentFilter)) {
                        hideKnownContactLoadingView();
                        mAdapter.setKnownContactsExtraTitle(PeopleFragment.this.getContext().getString(R.string.offline));
                        mAdapter.filterAccountKnownContacts(mCurrentFilter);
                    }
                }

                @Override
                public void onNetworkError(Exception e) {
                    onError(e.getMessage());
                }

                @Override
                public void onMatrixError(MatrixError e) {
                    onError(e.getMessage());
                }

                @Override
                public void onUnexpectedError(Exception e) {
                    onError(e.getMessage());
                }
            });
        }
    }

    /*
     * *********************************************************************************************
     * User action management
     * *********************************************************************************************
     */

    /**
     * Handle the click on a local or known contact
     *
     * @param item
     */
    private void onContactSelected(final ParticipantAdapterItem item) {
        if (item.mIsValid) {
            Intent startRoomInfoIntent = new Intent(getActivity(), VectorMemberDetailsActivity.class);
            startRoomInfoIntent.putExtra(VectorMemberDetailsActivity.EXTRA_MEMBER_ID, item.mUserId);

            if (!TextUtils.isEmpty(item.mAvatarUrl)) {
                startRoomInfoIntent.putExtra(VectorMemberDetailsActivity.EXTRA_MEMBER_AVATAR_URL, item.mAvatarUrl);
            }

            if (!TextUtils.isEmpty(item.mDisplayName)) {
                startRoomInfoIntent.putExtra(VectorMemberDetailsActivity.EXTRA_MEMBER_DISPLAY_NAME, item.mDisplayName);
            }

            if (!TextUtils.isEmpty(item.mAvatarUrl)) {
                startRoomInfoIntent.putExtra(VectorMemberDetailsActivity.EXTRA_MEMBER_AVATAR_URL, item.mAvatarUrl);
            }

            startRoomInfoIntent.putExtra(VectorMemberDetailsActivity.EXTRA_MATRIX_ID, mSession.getCredentials().userId);
            if (item.mContact != null) {

                if (item.mContact.getLookupUri() != null) {
                    startRoomInfoIntent.putExtra(VectorMemberDetailsActivity.EXTRA_LOOKUP_URI, item.mContact.getLookupUri());
                }

                if (item.mContact.getContactId() != null) {
                    startRoomInfoIntent.putExtra(VectorMemberDetailsActivity.EXTRA_CONTACT_ID, item.mContact.getContactId());
                }
            }
            startActivity(startRoomInfoIntent);
        }
    }

    private void onAvatarSelected(ParticipantAdapterItem item, View view) {
        if (item.mIsValid) {

            Intent intent = new Intent(getActivity(), OnAvatarSelectedDialogActivity.class);
            intent.putExtra("id", item.mUserId);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                ActivityOptions options = null;
                options = ActivityOptions
                        .makeSceneTransitionAnimation(getActivity(), view, "avatar");
                startActivity(intent, options.toBundle());
            } else {
                startActivity(intent);
            }
        }
    }

    /*
     * *********************************************************************************************
     * Utils
     * *********************************************************************************************
     */
    private List<ParticipantAdapterItem> getTalkerContacts() {
        ContactHandler contactHandler = new ContactHandler();
        List<TalkerContact> contacts = contactHandler.getTalkerContactSnapshot(getContext());
        List<ParticipantAdapterItem> participants = new ArrayList<>();
        if (contactHandler.isContactSnapshotAvailable(getContext())) {
            if (null != contacts) {
                for (TalkerContact contact : contacts) {
                    Contact dummyContact = new Contact(contact.getPhoneNumbers().get(0));
                    dummyContact.setDisplayName(contact.getDisplayName());
                    dummyContact.setThumbnailUri(contact.getThumbnailUri());
                    dummyContact.setLookupUri(Uri.parse(contact.getLookupUri()));
                    ArrayList<Contact.PhoneNumber> numbersList = new ArrayList<>();
                    for (String phone : contact.getPhoneNumbers()) {
                        numbersList.add(new Contact.PhoneNumber(phone, phone));
                    }
                    dummyContact.addPhonenumbers(numbersList);
                    ParticipantAdapterItem participant = new ParticipantAdapterItem(dummyContact);
                    participant.mUserId = contact.getMatrixId();
                    participants.add(participant);
                }
            }
        }
        return participants;
    }

    /**
     * Retrieve the contacts
     *
     * @return
     */
    private List<ParticipantAdapterItem> getContacts() {
        List<ParticipantAdapterItem> participants = new ArrayList<>();
        Collection<Contact> contacts = ContactsManager.getInstance().getLocalContactsSnapshot();
        mContactsSnapshotSession = ContactsManager.getInstance().getLocalContactsSnapshotSession();

        if (null != contacts) {
            for (Contact contact : contacts) {
                for (Contact.PhoneNumber pn : contact.getPhonenumbers()) {
                    if (contact.getMXID("" + pn.mE164PhoneNumber) != null
                            && contact.getMXID("" + pn.mE164PhoneNumber).mMatrixId != null) {
                        Contact dummyContact = new Contact("" + pn.mE164PhoneNumber);
                        dummyContact.setDisplayName(contact.getDisplayName());
                        dummyContact.setThumbnailUri(contact.getThumbnailUri());
                        dummyContact.setLookupUri(contact.getLookupUri());
                        dummyContact.addPhonenumbers((ArrayList<Contact.PhoneNumber>) contact.getPhonenumbers());
                        ParticipantAdapterItem participant = new ParticipantAdapterItem(dummyContact);
                        participant.mUserId = contact.getMXID("" + pn.mE164PhoneNumber).mMatrixId;
                        participants.add(participant);
                    }
                }
            }
        }

        return participants;
    }

    private List<ParticipantAdapterItem> getMatrixUsers() {
        List<ParticipantAdapterItem> matrixUsers = new ArrayList<>();
        for (ParticipantAdapterItem item : mLocalContacts) {
            if (!item.mContact.getMatrixIdMediums().isEmpty()) {
                matrixUsers.add(item);
            }
        }
        return matrixUsers;
    }

    /**
     * Init direct chats view with data and update its display
     */
    private void initDirectChatsViews() {
        mAdapter.setRooms(mDirectChats);
    }

    /**
     * Init contacts views with data and update their display
     */
    private void initContactsViews() {
//        mAdapter.setLocalContacts(mMatrixUserOnlyCheckbox != null && mMatrixUserOnlyCheckbox.isChecked()
//                ? getMatrixUsers()
//                : mLocalContacts);

        mAdapter.setLocalContacts(mLocalContacts);
    }

    /*
     * *********************************************************************************************
     * Listeners
     * *********************************************************************************************
     */

    @Override
    public void onSummariesUpdate() {
        super.onSummariesUpdate();

        if (isResumed()) {
            mAdapter.setInvitation(mActivity.getRoomInvitations());

            //initDirectChatsData();
            //initDirectChatsViews();
        }
    }

    @Override
    public void onRefresh() {
        initContactsData();
        initContactsViews();
    }

    @Override
    public void onPIDsUpdate() {
        final List<ParticipantAdapterItem> newContactList = getTalkerContacts();
        if (!mLocalContacts.containsAll(newContactList)) {
            mLocalContacts.clear();
            mLocalContacts.addAll(newContactList);
            initContactsViews();
        }
    }

    @Override
    public void onContactPresenceUpdate(Contact contact, String matrixId) {
        //TODO
        initContactsData();
        mAdapter.updateContactPresenceDot(contact);
        mAdapter.updateRoomsPresenceDot(contact);
    }

    @Override
    public void onToggleDirectChat(String roomId, boolean isDirectChat) {
        if (!isDirectChat) {
            mAdapter.removeDirectChat(roomId);
        }
    }

    @Override
    public void onRoomLeft(String roomId) {
        mAdapter.removeDirectChat(roomId);
    }

    @Override
    public void onRoomDeleted(String roomId) {
        mAdapter.removeDirectChat(roomId);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_contact_list, menu);
        MenuItem mSearchMenuItem = menu.findItem(R.id.action_search);
        if (ThemeUtils.getApplicationTheme(getContext()).equals(ThemeUtils.THEME_LIGHT_LIGHT_BLUE_HEADER_VALUE)) {
            mSearchMenuItem.setIcon(getResources().getDrawable(R.drawable.ic_material_search_white));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean retCode = true;

        switch (item.getItemId()) {
            case R.id.ic_action_invite:
              //  VectorHomeActivity.getInstance().invitePeopleToNewRoom(false);
                String shareBody = "https://play.google.com/store/apps/details?id=com.talkermessenger";
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.try_im));
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent,  getString(R.string.share_im)));

                break;

            case R.id.ic_action_settings:
                final Intent settingsIntent = new Intent(getContext(), NewSettingActivity.class);
                settingsIntent.putExtra(MXCActionBarActivity.EXTRA_MATRIX_ID, mSession.getMyUserId());
                getContext().startActivity(settingsIntent);
                break;

            default:
                // not handled item, return the super class implementation value
                retCode = super.onOptionsItemSelected(item);
                break;
        }
        return retCode;
    }
}
