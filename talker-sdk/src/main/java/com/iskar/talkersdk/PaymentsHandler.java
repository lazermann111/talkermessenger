package com.iskar.talkersdk;

import android.content.Context;

import com.anjlab.android.iab.v3.PurchaseData;
import com.iskar.talkersdk.callback.BalanceInterface;
import com.iskar.talkersdk.callback.HistoryPaymentInterface;
import com.iskar.talkersdk.callback.PaymentInterface;
import com.iskar.talkersdk.model.Balance;
import com.iskar.talkersdk.model.MobilePayment;
import com.iskar.talkersdk.model.PaymentHistory;
import com.iskar.talkersdk.model.SymbolsAmount;
import com.iskar.talkersdk.rest.client.CharactersClient;
import com.iskar.talkersdk.rest.client.PaymentsClient;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by karasinboots on 25.03.2018.
 */

public class PaymentsHandler {
    private String userId;
    private String token;
    private PaymentInterface paymentInterface = null;
    private PurchaseData purchaseData = null;
    private Context context;

    public PaymentsHandler(Context context, String userId, String token, PaymentInterface paymentInterface, PurchaseData purchaseData) {
        this.context = context;
        this.userId = userId;
        this.token = token;
        this.paymentInterface = paymentInterface;
        this.purchaseData = purchaseData;
    }

    public PaymentsHandler() {
    }

    public PaymentsHandler(Context context) {
        this.context = context;
    }

    public void checkPayment() {
        String sharedSecret = "b46bbb0d405e498b99892d76a7cae0a5";
        String device = "android";
        PaymentsClient.getPaymentsApi().checkMobilePaymant(userId, getCredentials(token), new MobilePayment(purchaseData, sharedSecret, device)).enqueue(new Callback<Balance>() {
            @Override
            public void onResponse(Call<Balance> call, Response<Balance> response) {
                if (response.code() == 200 && response.body() != null)
                    if (response.body().getError() == null)
                        paymentInterface.onSuccess(response.body().getBalance());
                    else paymentInterface.onFailure(response.body().getRawError());
            }

            @Override
            public void onFailure(Call<Balance> call, Throwable t) {
                paymentInterface.onFailure(t.getMessage());
            }
        });
    }

    public void getMySymbols(String userId, String token, BalanceInterface balanceInterface) {
        configureRealm();
        Realm realm = Realm.getDefaultInstance();
        if (realm.where(SymbolsAmount.class).findFirst() != null) {
            balanceInterface.onBalanceRecieved(realm.where(SymbolsAmount.class).findFirst().getAmount());
        }
        CharactersClient.getCharctersApi().getBalance(userId, getCredentials(token)).enqueue(new Callback<SymbolsAmount>() {
            @Override
            public void onResponse(Call<SymbolsAmount> call, Response<SymbolsAmount> response) {
                if (response.code() == 200 && response.body() != null) {
                    Realm realm = Realm.getDefaultInstance();
                    SymbolsAmount symbolsAmount = response.body();
                    symbolsAmount.userId = userId;
                    realm.beginTransaction();
                    realm.copyToRealmOrUpdate(symbolsAmount);
                    realm.commitTransaction();
                    balanceInterface.onBalanceRecieved(response.body().getAmount());
                }
            }

            @Override
            public void onFailure(Call<SymbolsAmount> call, Throwable t) {
                balanceInterface.onFailure(t.getMessage());
            }
        });
    }

    public void getPaymentHistory(String userId, String token, HistoryPaymentInterface historyPaymentInterface) {
        this.token = token;
        configureRealm();
        Realm realm = Realm.getDefaultInstance();
        if (realm.where(PaymentHistory.class).findAll() != null) {
            historyPaymentInterface.onSuccess(realm.where(PaymentHistory.class).findAll());
        }
        PaymentsClient.getPaymentsHistoryApi().getPaymentHistory(userId, getCredentials(token)).enqueue(new Callback<List<PaymentHistory>>() {
            @Override
            public void onResponse(Call<List<PaymentHistory>> call, Response<List<PaymentHistory>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    Realm realm = Realm.getDefaultInstance();
                    realm.beginTransaction();
                    for (PaymentHistory paymentHistory : response.body()) {
                        realm.copyToRealmOrUpdate(paymentHistory);
                    }
                    realm.commitTransaction();
                    historyPaymentInterface.onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<PaymentHistory>> call, Throwable t) {
                historyPaymentInterface.onFailure(t.getMessage());
            }
        });
    }

    private void configureRealm() {
        Realm.init(context);
        RealmConfiguration config = new RealmConfiguration
                .Builder().deleteRealmIfMigrationNeeded().name("libraryRealm")
                .modules(new com.iskar.talkersdk.realm.RealmModule())
                .build();
        Realm.setDefaultConfiguration(config);
    }

    private Map<String, String> getCredentials(String userToken) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("token", userToken);
        return params;
    }

}
