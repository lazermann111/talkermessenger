package com.iskar.talkersdk.model;

import com.anjlab.android.iab.v3.PurchaseData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MobilePayment implements Serializable {

    @SerializedName("receipt")
    @Expose
    private PurchaseData receipt;
    @SerializedName("secret")
    @Expose
    private String secret;
    @SerializedName("device")
    @Expose
    private String device;

    public MobilePayment(PurchaseData receipt, String secret, String device) {
        this.receipt = receipt;
        this.secret = secret;
        this.device = device;
    }

    public MobilePayment() {
    }

    public PurchaseData getReceipt() {
        return receipt;
    }

    public void setReceipt(PurchaseData receipt) {
        this.receipt = receipt;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

}