package com.iskar.talkersdk.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PhoneBadRequest {
    @SerializedName("error")
    @Expose
    private String error = null;

    @SerializedName("time_remaining")
    @Expose
    private int timeRemaining;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public int getTimeRemaining() {
        return timeRemaining;
    }

    public void setTimeRemaining(int timeRemaining) {
        this.timeRemaining = timeRemaining;
    }
}
