package com.iskar.talkersdk.model;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class UserPhone extends RealmObject implements Serializable {
    public UserPhone(String userId, String phone) {
        this.phone = phone;
        this.userId = userId;
    }

    public UserPhone() {
    }

    private String phone;
    @PrimaryKey
    private String userId;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
