package com.iskar.talkersdk.model;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaypalIds implements Serializable
{

    @SerializedName("payer_id")
    @Expose
    private String payerId;
    @SerializedName("payment_id")
    @Expose
    private String paymentId;

    public String getPayerId() {
        return payerId;
    }

    public void setPayerId(String payerId) {
        this.payerId = payerId;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

}