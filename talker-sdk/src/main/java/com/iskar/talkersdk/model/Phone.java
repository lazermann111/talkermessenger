package com.iskar.talkersdk.model;

/**
 * Created by karasinboots on 26.02.2018.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Phone implements Serializable {
    public Phone(String phone) {
        this.phone = phone;
    }

    public Phone() {
    }

    @SerializedName("phone")
    @Expose
    private String phone;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

}