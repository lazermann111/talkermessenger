package com.iskar.talkersdk.model;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class ContactInfo extends RealmObject implements Serializable
{

    @SerializedName("info")
    @Expose
    private Info info;
    @SerializedName("is_registered")
    @Expose
    private Boolean isRegistered;
    @PrimaryKey
    @SerializedName("phone")
    @Expose
    private String phone;

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

    public Boolean getIsRegistered() {
        return isRegistered;
    }

    public void setIsRegistered(Boolean isRegistered) {
        this.isRegistered = isRegistered;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

}