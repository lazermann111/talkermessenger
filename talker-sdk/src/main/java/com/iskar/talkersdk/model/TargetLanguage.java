package com.iskar.talkersdk.model;

import io.realm.RealmObject;
import io.realm.annotations.RealmClass;

/**
 * Created by karasinboots on 06.01.2018.
 */
@RealmClass
public class TargetLanguage extends RealmObject {
    private String language;

    public TargetLanguage(String language) {
        this.language = language;
    }

    public TargetLanguage() {
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
