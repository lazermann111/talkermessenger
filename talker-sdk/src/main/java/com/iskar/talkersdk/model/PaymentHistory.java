package com.iskar.talkersdk.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class PaymentHistory extends RealmObject implements Serializable
{

    @SerializedName("amount")
    @Expose
    private Long amount;
    @SerializedName("chars")
    @Expose
    private Long chars;
    @SerializedName("method")
    @Expose
    private String method;
    @SerializedName("transaction")
    @Expose
    private String transaction;
    @PrimaryKey
    @SerializedName("date")
    @Expose
    private String date;

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Long getChars() {
        return chars;
    }

    public void setChars(Long chars) {
        this.chars = chars;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getTransaction() {
        return transaction;
    }

    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}