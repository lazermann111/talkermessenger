package com.iskar.talkersdk.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Langs implements Serializable {

    @SerializedName("langs")
    @Expose
    private List<String> lang = null;

    public List<String> getLangs() {
        return lang;
    }

    public void setLang(List<String> lang) {
        this.lang = lang;
    }

    public Langs() {
    }

    public Langs(List<String> lang) {
        this.lang = lang;
    }
}