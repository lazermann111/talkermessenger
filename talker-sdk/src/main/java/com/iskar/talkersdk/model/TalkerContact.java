package com.iskar.talkersdk.model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class TalkerContact extends RealmObject {
    @PrimaryKey
    private String contactId;
    private String matrixId;
    private RealmList<String> phoneNumbers;
    private String thumbnailUri;
    private String photoUri;
    private String displayName;
    public String lookupUri;

    public TalkerContact(String contactId, String matrixId, RealmList<String> phoneNumbers, String thumbnailUri, String photoUri, String lookupUri, String displayName) {
        this.contactId = contactId;
        this.matrixId = matrixId;
        this.phoneNumbers = phoneNumbers;
        this.thumbnailUri = thumbnailUri;
        this.photoUri = photoUri;
        this.lookupUri = lookupUri;
        this.displayName = displayName;
    }

    public TalkerContact() {
    }

    public RealmList<String> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(RealmList<String> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public String getLookupUri() {
        return lookupUri;
    }

    public void setLookupUri(String lookupUri) {
        this.lookupUri = lookupUri;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getMatrixId() {
        return matrixId;
    }

    public void setMatrixId(String matrixId) {
        this.matrixId = matrixId;
    }

    public String getThumbnailUri() {
        return thumbnailUri;
    }

    public void setThumbnailUri(String thumbnailUri) {
        this.thumbnailUri = thumbnailUri;
    }

    public String getPhotoUri() {
        return photoUri;
    }

    public void setPhotoUri(String photoUri) {
        this.photoUri = photoUri;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
