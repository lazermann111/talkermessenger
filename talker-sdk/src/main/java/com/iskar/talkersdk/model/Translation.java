package com.iskar.talkersdk.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class Translation extends RealmObject implements Parcelable {
    private UserInfo userInfo;
    @SerializedName("double_translation")
    @Expose
    private Boolean doubleTranslation;
    @SerializedName("preferred_translator")
    @Expose
    private Integer preferredTranslator;
    @SerializedName("source_lang")
    @Expose
    private String sourceLang;
    @PrimaryKey
    @SerializedName("source_text")
    @Expose
    private String sourceText;
    @SerializedName("target_langs")
    @Expose
    private RealmList<TargetLanguage> targetLangs = null;
    @SerializedName("dual_translation")
    @Expose
    private RealmList<String> dualTranslation = null;
    @SerializedName("translation")
    @Expose
    private RealmList<String> translation = null;
    @SerializedName("cached")
    @Expose
    private RealmList<Boolean> cached = null;
    public final static Creator<Translation> CREATOR = new Creator<Translation>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Translation createFromParcel(Parcel in) {
            return new Translation(in);
        }

        public Translation[] newArray(int size) {
            return (new Translation[size]);
        }

    };

    protected Translation(Parcel in) {
        this.userInfo = ((UserInfo) in.readValue((UserInfo.class.getClassLoader())));
        this.doubleTranslation = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.preferredTranslator = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.sourceLang = ((String) in.readValue((String.class.getClassLoader())));
        this.sourceText = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.targetLangs, (String.class.getClassLoader()));
        in.readList(this.dualTranslation, (String.class.getClassLoader()));
        in.readList(this.translation, (String.class.getClassLoader()));
        in.readList(this.cached, (Boolean.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     */
    public Translation() {
    }

    /**
     * @param cached
     * @param targetLangs
     * @param translation
     * @param dualTranslation
     * @param doubleTranslation
     * @param sourceText
     * @param sourceLang
     * @param preferredTranslator
     * @param userInfo
     */
    public Translation(UserInfo userInfo, Boolean doubleTranslation, Integer preferredTranslator, String sourceLang, String sourceText, RealmList<TargetLanguage> targetLangs, RealmList<String> dualTranslation, RealmList<String> translation, RealmList<Boolean> cached) {
        super();
        this.userInfo = userInfo;
        this.doubleTranslation = doubleTranslation;
        this.preferredTranslator = preferredTranslator;
        this.sourceLang = sourceLang;
        this.sourceText = sourceText;
        this.targetLangs = targetLangs;
        this.dualTranslation = dualTranslation;
        this.translation = translation;
        this.cached = cached;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public Translation withUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
        return this;
    }

    public Boolean getDoubleTranslation() {
        return doubleTranslation;
    }

    public void setDoubleTranslation(Boolean doubleTranslation) {
        this.doubleTranslation = doubleTranslation;
    }

    public Translation withDoubleTranslation(Boolean doubleTranslation) {
        this.doubleTranslation = doubleTranslation;
        return this;
    }

    public Integer getPreferredTranslator() {
        return preferredTranslator;
    }

    public void setPreferredTranslator(Integer preferredTranslator) {
        this.preferredTranslator = preferredTranslator;
    }

    public Translation withPreferredTranslator(Integer preferredTranslator) {
        this.preferredTranslator = preferredTranslator;
        return this;
    }

    public String getSourceLang() {
        return sourceLang;
    }

    public void setSourceLang(String sourceLang) {
        this.sourceLang = sourceLang;
    }

    public Translation withSourceLang(String sourceLang) {
        this.sourceLang = sourceLang;
        return this;
    }

    public String getSourceText() {
        return sourceText;
    }

    public void setSourceText(String sourceText) {
        this.sourceText = sourceText;
    }

    public Translation withSourceText(String sourceText) {
        this.sourceText = sourceText;
        return this;
    }

    public RealmList<TargetLanguage> getTargetLangs() {
        return targetLangs;
    }

    public void setTargetLangs(RealmList<TargetLanguage> targetLangs) {
        this.targetLangs = targetLangs;
    }

    public Translation withTargetLangs(RealmList<TargetLanguage> targetLangs) {
        this.targetLangs = targetLangs;
        return this;
    }

    public RealmList<String> getDualTranslation() {
        return dualTranslation;
    }

    public void setDualTranslation(RealmList<String> dualTranslation) {
        this.dualTranslation = dualTranslation;
    }

    public Translation withDualTranslation(RealmList<String> dualTranslation) {
        this.dualTranslation = dualTranslation;
        return this;
    }

    public RealmList<String> getTranslation() {
        return translation;
    }

    public void setTranslation(RealmList<String> translation) {
        this.translation = translation;
    }

    public Translation withTranslation(RealmList<String> translation) {
        this.translation = translation;
        return this;
    }

    public RealmList<Boolean> getCached() {
        return cached;
    }

    public void setCached(RealmList<Boolean> cached) {
        this.cached = cached;
    }

    public Translation withCached(RealmList<Boolean> cached) {
        this.cached = cached;
        return this;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(userInfo);
        dest.writeValue(doubleTranslation);
        dest.writeValue(preferredTranslator);
        dest.writeValue(sourceLang);
        dest.writeValue(sourceText);
        dest.writeList(targetLangs);
        dest.writeList(dualTranslation);
        dest.writeList(translation);
        dest.writeList(cached);
    }

    public int describeContents() {
        return 0;
    }

}