package com.iskar.talkersdk.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class UserData extends RealmObject implements Serializable {

    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("key")
    @Expose
    private String key;
    @PrimaryKey
    @SerializedName("matrix_id")
    @Expose
    private String matrixId;
    @SerializedName("value")
    @Expose
    private String value;

    public UserData() {
    }

    public UserData(Long id, String key, String matrixId, String value) {
        this.id = id;
        this.key = key;
        this.matrixId = matrixId;
        this.value = value;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getMatrixId() {
        return matrixId;
    }

    public void setMatrixId(String matrixId) {
        this.matrixId = matrixId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}