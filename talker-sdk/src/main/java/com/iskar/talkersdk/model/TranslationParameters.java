package com.iskar.talkersdk.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class TranslationParameters extends RealmObject implements Serializable {

    @SerializedName("user_info")
    @Expose
    private UserInfo userInfo;
    @SerializedName("double_translation")
    @Expose
    private Boolean doubleTranslation;
    @SerializedName("preferred_translator")
    @Expose
    private int preferredTranslator;
    @SerializedName("source_lang")
    @Expose
    private String sourceLang;
    @PrimaryKey
    @SerializedName("source_text")
    @Expose
    private String sourceText;
    @SerializedName("target_langs")
    @Expose
    private RealmList<String> targetLangs = null;
    @SerializedName("html")
    @Expose
    private boolean html;

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public Boolean getDoubleTranslation() {
        return doubleTranslation;
    }

    public void setDoubleTranslation(Boolean doubleTranslation) {
        this.doubleTranslation = doubleTranslation;
    }

    public int getPreferredTranslator() {
        return preferredTranslator;
    }

    public void setPreferredTranslator(int preferredTranslator) {
        this.preferredTranslator = preferredTranslator;
    }

    public String getSourceLang() {
        return sourceLang;
    }

    public void setSourceLang(String sourceLang) {
        this.sourceLang = sourceLang;
    }

    public String getSourceText() {
        return sourceText;
    }

    public void setSourceText(String sourceText) {
        this.sourceText = sourceText;
    }

    public RealmList<String> getTargetLangs() {
        return targetLangs;
    }

    public void setTargetLangs(RealmList<String> targetLangs) {
        this.targetLangs = targetLangs;
    }

    public boolean getHtml() {
        return html;
    }

    public void setHtml(boolean html) {
        this.html = html;
    }

    public TranslationParameters() {
    }

    public TranslationParameters(UserInfo userInfo, Boolean doubleTranslation, int preferredTranslator, String sourceLang, String sourceText, RealmList<String> targetLangs, boolean html) {

        this.userInfo = userInfo;
        this.doubleTranslation = doubleTranslation;
        this.preferredTranslator = preferredTranslator;
        this.sourceLang = sourceLang;
        this.sourceText = sourceText;
        this.targetLangs = targetLangs;
        this.html = html;
    }
}