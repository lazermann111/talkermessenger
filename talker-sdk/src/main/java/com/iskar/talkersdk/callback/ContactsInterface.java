package com.iskar.talkersdk.callback;

import com.iskar.talkersdk.model.ContactInfo;

import java.util.List;

/**
 * Created by karasinboots on 16.03.2018.
 */

public interface ContactsInterface {
    void onContactsReceived(List<ContactInfo> contactInfo);

    void onContactReceived(ContactInfo contactInfo);

    void onFailure(String msg);
}
