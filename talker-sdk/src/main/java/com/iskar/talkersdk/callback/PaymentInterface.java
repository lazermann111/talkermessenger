package com.iskar.talkersdk.callback;

/**
 * Created by karasinboots on 25.03.2018.
 */

public interface PaymentInterface {

    void onSuccess(Long message);

    void onFailure(String message);
}
