package com.iskar.talkersdk.callback;

import com.iskar.talkersdk.model.UserLanguages;

public interface LanguagesInterface {
    void onLanguagesReceived(UserLanguages userLanguages);
    void onMainLanguageReceived(String mainLanguage);
    void onFailure(String message);
}
