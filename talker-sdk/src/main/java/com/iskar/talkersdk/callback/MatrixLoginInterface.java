package com.iskar.talkersdk.callback;

import com.iskar.talkersdk.model.MatrixCredentials;
import com.iskar.talkersdk.model.PhoneBadRequest;

/**
 * Created by karasinboots on 26.02.2018.
 */

public interface MatrixLoginInterface {
    void getCredentialsSuccess(MatrixCredentials matrixCredentials);

    void getCodeSuccess();

    void tooManyAttempts(PhoneBadRequest phoneBadRequest);

    void changePhoneSuccess(String newPhone);

    void onFailure(String msg);
}
