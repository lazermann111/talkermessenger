package com.iskar.talkersdk.realm.wrappers;


import io.realm.RealmObject;

/**
 * Created by karasinboots on 27.12.2017.
 */

public class RealmBoolean extends RealmObject {
    public boolean value;
}