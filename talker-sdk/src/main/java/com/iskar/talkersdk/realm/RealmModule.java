package com.iskar.talkersdk.realm;

import com.iskar.talkersdk.model.ContactInfo;
import com.iskar.talkersdk.model.Info;
import com.iskar.talkersdk.model.PaymentHistory;
import com.iskar.talkersdk.model.SymbolsAmount;
import com.iskar.talkersdk.model.TalkerContact;
import com.iskar.talkersdk.model.TargetLanguage;
import com.iskar.talkersdk.model.Translation;
import com.iskar.talkersdk.model.TranslationHistoryItem;
import com.iskar.talkersdk.model.TranslationParameters;
import com.iskar.talkersdk.model.UserData;
import com.iskar.talkersdk.model.UserInfo;
import com.iskar.talkersdk.model.UserLanguages;
import com.iskar.talkersdk.model.UserPhone;

/**
 * Created by karasinboots on 20.01.2018.
 */

@io.realm.annotations.RealmModule(library = true, classes = {Translation.class,
        TranslationParameters.class, UserInfo.class, UserLanguages.class, TargetLanguage.class,
        ContactInfo.class, Info.class, UserData.class, ContactInfo.class, TranslationHistoryItem.class,
        TalkerContact.class, UserPhone.class, PaymentHistory.class, SymbolsAmount.class})
public class RealmModule {
}