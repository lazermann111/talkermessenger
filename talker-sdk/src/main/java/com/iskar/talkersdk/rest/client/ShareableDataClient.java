package com.iskar.talkersdk.rest.client;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.iskar.talkersdk.BuildConfig;
import com.iskar.talkersdk.model.TargetLanguage;
import com.iskar.talkersdk.realm.wrappers.RealmLanguagesListTypeAdapter;
import com.iskar.talkersdk.realm.wrappers.RealmStringListTypeAdapter;
import com.iskar.talkersdk.rest.api.ShareableDataApi;

import java.util.concurrent.TimeUnit;

import io.realm.RealmList;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.iskar.talkersdk.constants.ApiConsts.CONTENT_TYPE;
import static com.iskar.talkersdk.constants.ApiConsts.HEADER_FIELD;

/**
 * Created by karasinboots on 14.03.2018.
 */

public class ShareableDataClient {
    public static OkHttpClient shareableDataHttpClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return new OkHttpClient.Builder().addInterceptor(chain -> {
            Request newRequest = chain.request().newBuilder().addHeader(HEADER_FIELD, CONTENT_TYPE)
                    .build();
            return chain.proceed(newRequest);
        }).addInterceptor(logging).connectTimeout(2000, TimeUnit.MILLISECONDS)
                .readTimeout(2000, TimeUnit.MILLISECONDS).build();
    }

    public static ShareableDataApi getShareableDataApi() {
        Gson gson = new GsonBuilder().registerTypeAdapter(new TypeToken<RealmList<String>>() {
        }.getType(), RealmStringListTypeAdapter.INSTANCE)
                .registerTypeAdapter(new TypeToken<RealmList<TargetLanguage>>() {
                }.getType(), RealmLanguagesListTypeAdapter.INSTANCE)
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .client(shareableDataHttpClient())
                .baseUrl(BuildConfig.PUBLIC_DATA_API_BASE_URL)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .callFactory(shareableDataHttpClient())
                .build();
        return retrofit.create(ShareableDataApi.class);
    }
}
