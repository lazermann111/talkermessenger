package com.iskar.talkersdk.rest.api;

import com.iskar.talkersdk.model.Fields;
import com.iskar.talkersdk.model.Phone;
import com.iskar.talkersdk.model.UserData;
import com.iskar.talkersdk.model.UserDataFields;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

/**
 * Created by karasinboots on 14.03.2018.
 */

public interface PublicDataApi {
    @GET("/user/{userId}/public")
    Call<UserData> getPublicData(@Path("userId") String userId);

    @POST("/user/{userId}/public")
    Call<Void> setPublicFields(@Path("userId") String userId, @QueryMap Map<String, String> params, @Body UserDataFields fields);

    @HTTP(method = "DELETE", path = "/user/{userId}/public", hasBody = true)
    Call<Void> deletePublicFields(@Path("userId") String userId, @QueryMap Map<String, String> params, @Body Fields fields);

    @GET("/user/{userId}/phone")
    Call<Phone> getPhone(@Path("userId") String userId, @QueryMap Map<String, String> params);
}
