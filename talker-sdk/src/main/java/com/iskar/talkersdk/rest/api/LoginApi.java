package com.iskar.talkersdk.rest.api;

import com.iskar.talkersdk.model.Credentials;
import com.iskar.talkersdk.model.MatrixCredentials;
import com.iskar.talkersdk.model.Phone;
import com.iskar.talkersdk.model.PhoneBadRequest;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

/**
 * Created by karasinboots on 14.03.2018.
 */

public interface LoginApi {
    @POST("/login/request_code")
    Call<Void> requestCode(@Body Phone phone);

    @PATCH("/user/{userId}/phone/request_code")
    Call<Void> requestCodeForPhone(@Path("userId") String userId, @QueryMap Map<String, String> params, @Body Phone phone);

    @POST("/login")
    Call<MatrixCredentials> login(@Body Credentials credentials);

    @PATCH("/user/{userId}/phone")
    Call<Void> changePhone(@Path("userId") String userId, @QueryMap Map<String, String> params, @Body Credentials credentials);
}
