package com.iskar.talkersdk.rest.api;

import com.iskar.talkersdk.model.Balance;
import com.iskar.talkersdk.model.MobilePayment;
import com.iskar.talkersdk.model.PaymentHistory;
import com.iskar.talkersdk.model.PaypalIds;
import com.iskar.talkersdk.model.PaypalLink;
import com.iskar.talkersdk.model.SymbolsAmount;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

/**
 * Created by karasinboots on 15.03.2018.
 */

public interface PaymentsApi {
    @POST("/payments/{userId}/mobile")
    Call<Balance> checkMobilePaymant(@Path("userId") String userId, @QueryMap Map<String, String> params, @Body MobilePayment mobilePayment);

    @POST("/payments/{userId}/paypal")
    Call<PaypalLink> getPaypalLink(@Path("userId") String userId, @QueryMap Map<String, String> params, @Body SymbolsAmount symbolsAmount);

    @POST("/payments/{userId}/paypal/finish_payment")
    Call<SymbolsAmount> finishPaypalPay(@Path("userId") String userId, @QueryMap Map<String, String> params, @Body PaypalIds paypalIds);

    @GET("/payments/{userId}/history")
    Call<List<PaymentHistory>> getPaymentHistory(@Path("userId") String userId, @QueryMap Map<String, String> params);


}
