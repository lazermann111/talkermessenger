package com.iskar.talkersdk.rest.client;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.iskar.talkersdk.BuildConfig;
import com.iskar.talkersdk.model.TargetLanguage;
import com.iskar.talkersdk.realm.wrappers.RealmLanguagesListTypeAdapter;
import com.iskar.talkersdk.realm.wrappers.RealmStringListTypeAdapter;
import com.iskar.talkersdk.rest.api.CharactersApi;

import java.util.concurrent.TimeUnit;

import io.realm.RealmList;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by karasinboots on 14.03.2018.
 */

public class CharactersClient {
    public static OkHttpClient charactersHttpClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return new OkHttpClient.Builder().addInterceptor(chain -> {
            Request newRequest = chain.request().newBuilder()
                    .build();
            return chain.proceed(newRequest);
        }).addInterceptor(logging).connectTimeout(2000, TimeUnit.MILLISECONDS)
                .readTimeout(2000, TimeUnit.MILLISECONDS).build();
    }

    public static CharactersApi getCharctersApi() {
        Gson gson = new GsonBuilder().registerTypeAdapter(new TypeToken<RealmList<String>>() {
        }.getType(), RealmStringListTypeAdapter.INSTANCE)
                .registerTypeAdapter(new TypeToken<RealmList<TargetLanguage>>() {
                }.getType(), RealmLanguagesListTypeAdapter.INSTANCE)
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .client(charactersHttpClient())
                .baseUrl(BuildConfig.CHARACTERS_API_BASE_URL)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .callFactory(charactersHttpClient())
                .build();
        return retrofit.create(CharactersApi.class);
    }
}
