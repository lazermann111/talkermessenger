package com.iskar.talkersdk.rest.api;

import com.iskar.talkersdk.model.SymbolsAmount;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

/**
 * Created by karasinboots on 14.03.2018.
 */

public interface CharactersApi {
    @GET("/user/{userId}/characters")
    Call<SymbolsAmount> getBalance(@Path("userId") String userId, @QueryMap Map<String, String> params);
}
