package com.iskar.talkersdk.rest.api;

import com.iskar.talkersdk.model.Lang;
import com.iskar.talkersdk.model.Langs;
import com.iskar.talkersdk.model.UserLanguages;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

/**
 * Created by karasinboots on 14.03.2018.
 */

public interface LanguageApi {
    @PUT("/user/{userId}/language/main")
    Call<Void> changeUserMainLanguage(@Path("userId") String userId, @QueryMap Map<String, String> params, @Body Lang lang);

    @PUT("/user/{userId}/language/add")
    Call<Void> addUserLanguage(@Path("userId") String userId, @QueryMap Map<String, String> params, @Body Lang lang);

    @HTTP(method = "DELETE", path = "/user/{userId}/language/delete", hasBody = true)
    Call<Void> deleteUserLanguage(@Path("userId") String userId, @QueryMap Map<String, String> params, @Body Lang lang);

    @GET("/user/{userId}/languages")
    Call<UserLanguages> getUserLanguages(@Path("userId") String userId, @QueryMap Map<String, String> params);

    @PUT("/user/{userId}/languages")
    Call<Void> editLanguages(@Path("userId") String userId, @QueryMap Map<String, String> params, @Body Langs langs);
}
