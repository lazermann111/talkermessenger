package com.iskar.talkersdk;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.iskar.talkersdk.callback.MatrixLoginInterface;
import com.iskar.talkersdk.model.Credentials;
import com.iskar.talkersdk.model.MatrixCredentials;
import com.iskar.talkersdk.model.Phone;
import com.iskar.talkersdk.model.PhoneBadRequest;
import com.iskar.talkersdk.rest.client.LoginClient;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by karasinboots on 26.02.2018.
 */

public class LoginHandler {
    private String mPhoneNumber;
    private MatrixLoginInterface mMatrixCredentials = null;

    public LoginHandler(MatrixLoginInterface matrixCredentials) {
        mMatrixCredentials = matrixCredentials;
    }

    public void requestCode(String phoneNumber) {
        mPhoneNumber = phoneNumber;
        LoginClient.getLoginApi().requestCode(new Phone(phoneNumber)).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    mMatrixCredentials.getCodeSuccess();
                } else {
                    Gson gson = new GsonBuilder().create();
                    try {
                        PhoneBadRequest phoneBadRequest = gson.fromJson(response.errorBody().string(), PhoneBadRequest.class);
                        mMatrixCredentials.tooManyAttempts(phoneBadRequest);
                    } catch (IOException e) {
                    }
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                t.printStackTrace();
                mMatrixCredentials.onFailure(t.getMessage());
            }
        });
    }

    public void login(String code) {
        LoginClient.getLoginApi().login(new Credentials(mPhoneNumber, code)).enqueue(new Callback<MatrixCredentials>() {
            @Override
            public void onResponse(Call<MatrixCredentials> call, Response<MatrixCredentials> response) {
                if (response.isSuccessful() && response.code() == 200 && response.body() != null)
                    mMatrixCredentials.getCredentialsSuccess(response.body());
                else
                    mMatrixCredentials.onFailure(response.message());
            }

            @Override
            public void onFailure(Call<MatrixCredentials> call, Throwable t) {
                t.printStackTrace();
                mMatrixCredentials.onFailure(t.getMessage());
            }
        });
    }

    public void login(String phone, String code) {
        LoginClient.getLoginApi().login(new Credentials(phone, code)).enqueue(new Callback<MatrixCredentials>() {
            @Override
            public void onResponse(Call<MatrixCredentials> call, Response<MatrixCredentials> response) {
                if (response.isSuccessful() && response.code() == 200 && response.body() != null)
                    mMatrixCredentials.getCredentialsSuccess(response.body());
                else
                    mMatrixCredentials.onFailure(response.message());
            }

            @Override
            public void onFailure(Call<MatrixCredentials> call, Throwable t) {
                t.printStackTrace();
                mMatrixCredentials.onFailure(t.getMessage());
            }
        });
    }

    public void changeNumber(String userId, String token, String phoneNumber, String code) {
        LoginClient.getLoginApi().changePhone(userId, getCredentials(token), new Credentials(phoneNumber, code)).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    mMatrixCredentials.changePhoneSuccess(phoneNumber);
                } else {
                    mMatrixCredentials.onFailure(response.message());
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                mMatrixCredentials.onFailure(t.getMessage());
            }
        });
    }

    public void requestCodeForPhone(String userId, String token, String phoneNumber) {
        LoginClient.getLoginApi().requestCodeForPhone(userId, getCredentials(token), new Phone(phoneNumber)).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {

                if (response.isSuccessful() && response.code() == 200) {
                    mMatrixCredentials.getCodeSuccess();
                } else {
                    mMatrixCredentials.onFailure(response.message());
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                mMatrixCredentials.onFailure(t.getMessage());
            }
        });
    }

    private Map<String, String> getCredentials(String userToken) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("token", userToken);
        return params;
    }
}
