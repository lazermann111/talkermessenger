package com.iskar.talkersdk;

import android.content.Context;

import com.iskar.talkersdk.callback.ContactsInterface;
import com.iskar.talkersdk.model.ContactInfo;
import com.iskar.talkersdk.model.Phones;
import com.iskar.talkersdk.model.TalkerContact;
import com.iskar.talkersdk.rest.client.ContactsClient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by karasinboots on 16.03.2018.
 */

public class ContactHandler {
    private String mUserId;
    private String mToken;
    private ContactsInterface mContactsInterface = null;
    private Context mContext;

    public ContactHandler(Context context, String mUserId, String mToken, ContactsInterface mContactsInterface) {
        this.mContext = context;
        this.mUserId = mUserId;
        this.mToken = mToken;
        this.mContactsInterface = mContactsInterface;
    }

    public ContactHandler() {
    }

    public void saveContact(Context context, ArrayList<TalkerContact> contact) {
        configureRealm(context);
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        for (TalkerContact talkerContact : contact)
            realm.copyToRealmOrUpdate(talkerContact);
        realm.commitTransaction();
    }

    public void clearSnapshot(Context context) {
        configureRealm(context);
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.where(TalkerContact.class).findAll().deleteAllFromRealm();
        realm.commitTransaction();
    }

    public boolean isContactSnapshotAvailable(Context context) {
        configureRealm(context);
        Realm realm = Realm.getDefaultInstance();
        return realm.where(TalkerContact.class).findFirst() != null;
    }

    public TalkerContact getContactFromTalkerContactSnapshot(String matrixId, Context context) {
        configureRealm(context);
        Realm realm = Realm.getDefaultInstance();
        TalkerContact talkerContact = realm.where(TalkerContact.class).equalTo("matrixId", matrixId).findFirst();
        if (talkerContact != null)
            return realm.copyFromRealm(talkerContact);
        else return null;
    }

    public List<TalkerContact> getTalkerContactSnapshot(Context context) {
        configureRealm(context);
        Realm realm = Realm.getDefaultInstance();
        return realm.copyFromRealm(realm.where(TalkerContact.class).findAll());
    }

    public void uploadContacts(Set<String> phoneList) {
        configureRealm(mContext);
        List<ContactInfo> contactInfoList = new ArrayList<>();
        Set<String> notRegistered = new HashSet<>();
        Realm realm = Realm.getDefaultInstance();
        for (String phoneNumber : phoneList) {
            if (realm.where(ContactInfo.class).equalTo("phone", phoneNumber).findFirst() != null) {
                contactInfoList.add(realm.where(ContactInfo.class).equalTo("phone", phoneNumber).findFirst());
            } else {
                notRegistered.add(phoneNumber);
            }
        }

        if (contactInfoList.size() > 0)
            mContactsInterface.onContactsReceived(contactInfoList);
        if (notRegistered.size() > 0)
            ContactsClient.getContactsApi().postContactList(mUserId, getCredentials(mToken),
                    new Phones(new ArrayList<>(notRegistered))).enqueue(new Callback<List<ContactInfo>>() {
                @Override
                public void onResponse(Call<List<ContactInfo>> call, Response<List<ContactInfo>> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        Realm realm = Realm.getDefaultInstance();
                        for (ContactInfo phoneRegistered : response.body()) {
                            realm.beginTransaction();
                            realm.copyToRealmOrUpdate(phoneRegistered);
                            realm.commitTransaction();
                        }
                        mContactsInterface.onContactsReceived(response.body());
                    }
                }

                @Override
                public void onFailure(Call<List<ContactInfo>> call, Throwable t) {
                    mContactsInterface.onFailure(t.getMessage());
                }
            });
    }

    private Map<String, String> getCredentials(String userToken) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("token", userToken);
        return params;
    }

    private void configureRealm(Context context) {
        Realm.init(context);
        RealmConfiguration config = new RealmConfiguration
                .Builder().deleteRealmIfMigrationNeeded().name("libraryRealm")
                .modules(new com.iskar.talkersdk.realm.RealmModule())
                .build();
        Realm.setDefaultConfiguration(config);
    }

    public void getContactInfo(String phoneNumber) {
        Realm realm = Realm.getDefaultInstance();
        List<String> phoneList = new ArrayList<>();
        phoneList.add(phoneNumber);
        if (realm.where(ContactInfo.class).equalTo("phone", phoneNumber).findFirst() != null) {
            mContactsInterface.onContactReceived(realm.where(ContactInfo.class).equalTo("phone", phoneNumber).findFirst());
        } else ContactsClient.getContactsApi().postContactList(mUserId, getCredentials(mToken),
                new Phones(phoneList)).enqueue(new Callback<List<ContactInfo>>() {
            @Override
            public void onResponse(Call<List<ContactInfo>> call, Response<List<ContactInfo>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    Realm realm = Realm.getDefaultInstance();
                    for (ContactInfo phoneRegistered : response.body()) {
                        realm.beginTransaction();
                        realm.copyToRealmOrUpdate(phoneRegistered);
                        realm.commitTransaction();
                        // Log.e("added to db phone", phoneRegistered.getPhone() + " " + phoneRegistered.getIsRegistered());
                    }
                    mContactsInterface.onContactsReceived(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<ContactInfo>> call, Throwable t) {
                mContactsInterface.onFailure(t.getMessage());
            }
        });
    }

    public void updateContact(String number) {
        configureRealm(mContext);
        List<String> phoneList = new ArrayList<>();
        phoneList.add(number);
        if (phoneList.size() > 0)
            ContactsClient.getContactsApi().postContactList(mUserId, getCredentials(mToken),
                    new Phones(phoneList)).enqueue(new Callback<List<ContactInfo>>() {
                @Override
                public void onResponse(Call<List<ContactInfo>> call, Response<List<ContactInfo>> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        Realm realm = Realm.getDefaultInstance();
                        for (ContactInfo phoneRegistered : response.body()) {
                            realm.beginTransaction();
                            realm.copyToRealmOrUpdate(phoneRegistered);
                            realm.commitTransaction();
                            //  Log.e("added to db phone", phoneRegistered.getPhone() + " " + phoneRegistered.getIsRegistered());
                        }
                        mContactsInterface.onContactsReceived(response.body());
                    }
                }

                @Override
                public void onFailure(Call<List<ContactInfo>> call, Throwable t) {
                    mContactsInterface.onFailure(t.getMessage());
                }
            });
    }

    public void updateAllContacts(List<String> phoneList) {
        configureRealm(mContext);
        if (phoneList.size() > 0)
            ContactsClient.getContactsApi().postContactList(mUserId, getCredentials(mToken),
                    new Phones(phoneList)).enqueue(new Callback<List<ContactInfo>>() {
                @Override
                public void onResponse(Call<List<ContactInfo>> call, Response<List<ContactInfo>> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        Realm realm = Realm.getDefaultInstance();
                        for (ContactInfo phoneRegistered : response.body()) {
                            realm.beginTransaction();
                            realm.copyToRealmOrUpdate(phoneRegistered);
                            realm.commitTransaction();
                            //   Log.e("added to db phone", phoneRegistered.getPhone() + " " + phoneRegistered.getIsRegistered());
                        }
                        mContactsInterface.onContactsReceived(response.body());
                    }
                }

                @Override
                public void onFailure(Call<List<ContactInfo>> call, Throwable t) {
                    mContactsInterface.onFailure(t.getMessage());
                }
            });
    }
}
