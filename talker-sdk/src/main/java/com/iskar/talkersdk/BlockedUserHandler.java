package com.iskar.talkersdk;

import android.content.Context;

import com.iskar.talkersdk.callback.BlockUserInterface;
import com.iskar.talkersdk.model.BlockUserId;
import com.iskar.talkersdk.model.Blocked;
import com.iskar.talkersdk.model.Status;
import com.iskar.talkersdk.model.TalkerContact;
import com.iskar.talkersdk.rest.client.UserBlockClient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BlockedUserHandler {
    private String userId;
    private String token;

    public BlockedUserHandler(String userId, String token) {
        this.userId = userId;
        this.token = token;
    }

    public void blockUser(String blockId, BlockUserInterface.BlockingUser blockUserInterface) {
        UserBlockClient.getBlockUsersApi().blockUser(userId, getCredentials(), new BlockUserId(blockId)).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful() && response.code() == 200)
                    blockUserInterface.onSuccess();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                blockUserInterface.onFailure(t.getMessage());
            }
        });
    }

    public void unblockUser(String blockId, BlockUserInterface.BlockingUser blockUserInterface) {
        UserBlockClient.getBlockUsersApi().unblockUser(userId, getCredentials(), new BlockUserId(blockId)).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful() && response.code() == 200)
                    blockUserInterface.onSuccess();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                blockUserInterface.onFailure(t.getMessage());
            }
        });
    }

    public void getBlockedUsers(BlockUserInterface.GetBlockUserInterface blockUserInterface) {
        UserBlockClient.getBlockUsersApi().getBlockedUsers(userId, getCredentials()).enqueue(new Callback<List<Blocked>>() {
            @Override
            public void onResponse(Call<List<Blocked>> call, Response<List<Blocked>> response) {
                if (response.isSuccessful() && response.body() != null)
                    blockUserInterface.blockedUsersReceived(response.body());
            }

            @Override
            public void onFailure(Call<List<Blocked>> call, Throwable t) {

            }
        });
    }

    public List<TalkerContact> getBlockedUsersData(Context context, List<Blocked> blockedList) {
        configureRealm(context);
        List<TalkerContact> blockedData = new ArrayList<>();
        Realm realm = Realm.getDefaultInstance();
        for (Blocked blocked : blockedList)
            if (realm.where(TalkerContact.class).equalTo("matrixId", blocked.getUserId()).findFirst() != null)
                blockedData.add(realm.where(TalkerContact.class).equalTo("matrixId", blocked.getUserId()).findFirst());
        return blockedData;
    }

    public void checkIfBlocked(String blockId, BlockUserInterface.CheckBlockingUser blockUserInterface) {
        UserBlockClient.getBlockUsersApi().checkIfBlocked(userId, blockId, getCredentials()).enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {
                if (response.isSuccessful() && response.body() != null)
                    blockUserInterface.statusReceived(response.body());
                else
                    blockUserInterface.onFailure(response.message());
            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {
                blockUserInterface.onFailure(t.getMessage());
            }
        });
    }

    private void configureRealm(Context context) {
        Realm.init(context);
        RealmConfiguration config = new RealmConfiguration
                .Builder().deleteRealmIfMigrationNeeded().name("libraryRealm")
                .modules(new com.iskar.talkersdk.realm.RealmModule())
                .build();
        Realm.setDefaultConfiguration(config);
    }

    private Map<String, String> getCredentials() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("token", token);
        return params;
    }
}
