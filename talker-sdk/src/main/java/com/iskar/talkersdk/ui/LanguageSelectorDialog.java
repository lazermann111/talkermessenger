package com.iskar.talkersdk.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iskar.talkersdk.R;
import com.iskar.talkersdk.model.Lang;
import com.iskar.talkersdk.model.Langs;
import com.iskar.talkersdk.model.UserLanguages;
import com.iskar.talkersdk.rest.client.LanguageClient;
import com.iskar.talkersdk.ui.languageRecycler.LanguageSelectorAdapter;
import com.iskar.talkersdk.ui.languageRecycler.Languages;
import com.iskar.talkersdk.ui.languageRecycler.RecyclerOnClick;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by karasinboots on 03.01.2018.
 */

public class LanguageSelectorDialog extends Dialog implements RecyclerOnClick, SearchView.OnQueryTextListener {
    private Context context;
    private RecyclerView languageRecycler;
    private RecyclerView favoritesLanguagesRecycler;
    private String userToken;
    private String userId;
    private int textColor;
    private int backgroundColor;
    private ArrayList<Languages> languages;
    private boolean changeMain;
    private boolean justGetLanguageCode = false;
    public String chosenLanguageCode;
    public String chosenLanguage;
    private Set<String> favoritesSet = new HashSet<>();
    private View divider1;
    private View divider2;
    private TextView favoritesHeader;
    private TextView languagesHeader;
    private boolean fromTranslator;
    private boolean inMyProfile;
    private ArrayList<Languages> filtered = new ArrayList<>();
    private LanguageSelectorAdapter languageSelectorAdapter;
    private static final String FAVOURITES_SHARED = "favoriteLanguages";

    public void setInMyProfile(boolean inMyProfile) {
        this.inMyProfile = inMyProfile;
    }

    public void setTalkerLanguageList(Langs talkerLanguageList) {
        this.talkerLanguageList = talkerLanguageList;
    }

    private Langs talkerLanguageList;

    public LanguageSelectorDialog(Activity activity, String userToken, String userId, int textColor, int backgroundColor, ArrayList<Languages> languages, boolean changeMain) {
        super(activity, android.R.style.Theme_DeviceDefault_Light_NoActionBar);
        this.userToken = userToken;
        this.userId = userId;
        this.textColor = textColor;
        this.backgroundColor = backgroundColor;
        this.languages = languages;
        this.changeMain = changeMain;
        this.fromTranslator = false;
    }

    public LanguageSelectorDialog(Activity activity, int textColor, int backgroundColor, ArrayList<Languages> languages, boolean justGetLanguageCode) {
        super(activity, android.R.style.Theme_DeviceDefault_Light_NoActionBar);
        this.textColor = textColor;
        this.backgroundColor = backgroundColor;
        this.languages = languages;
        this.justGetLanguageCode = justGetLanguageCode;
        this.fromTranslator = true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.language_selection_layout);
        context = getContext();
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        favoritesHeader = findViewById(R.id.textView2);
        languagesHeader = findViewById(R.id.textView3);
        divider1 = findViewById(R.id.view);
        divider2 = findViewById(R.id.view2);
        sharedPreferences = getContext().getSharedPreferences(FAVOURITES_SHARED, Context.MODE_PRIVATE);
        favoritesSet = getSetOfFavorites();
        Toolbar toolbar = findViewById(R.id.account_toolbar);
        toolbar.setNavigationIcon(getContext().getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        int actionBarColor = ((TextView) toolbar.getChildAt(0)).getCurrentTextColor();
        toolbar.getNavigationIcon().setColorFilter(actionBarColor, PorterDuff.Mode.DST_IN);
        ((ImageView) toolbar.getChildAt(1)).setColorFilter(actionBarColor);
        toolbar.inflateMenu(R.menu.language_selector);

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(((ColorDrawable) toolbar.getBackground()).getColor());
        }
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(toolbar.getMenu().findItem(R.id.action_search));
        searchView.setOnCloseListener(() -> {
            languageSelectorAdapter.setLanguages(languages);
            languageSelectorAdapter.notifyDataSetChanged();
            return false;
        });
        ImageView searchIcon = searchView.findViewById(R.id.search_button);
        searchIcon.setColorFilter(actionBarColor);
        searchView.setOnQueryTextListener(this);
        searchView.setBackgroundColor(getContext().getResources().getColor(android.R.color.transparent));
        ((ImageView) toolbar.findViewById(R.id.search_mag_icon)).setColorFilter(actionBarColor);
        ((ImageView) toolbar.findViewById(R.id.search_close_btn)).setColorFilter(actionBarColor);
        LinearLayout searchEditFrame = searchView.findViewById(R.id.search_edit_frame);
        if (searchEditFrame != null) {
            ViewGroup.MarginLayoutParams searchEditFrameParams = (ViewGroup.MarginLayoutParams) searchEditFrame.getLayoutParams();
            searchEditFrameParams.leftMargin = 0;
            EditText et = searchEditFrame.findViewById(R.id.search_src_text);
            et.setTextColor(actionBarColor);
            searchEditFrame.setLayoutParams(searchEditFrameParams);
            et.setTextColor(actionBarColor);
            try {
                Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
                f.setAccessible(true);
                f.set(et, R.drawable.custom_cursor);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                Drawable drawable = context.getResources().getDrawable(R.drawable.ic_search_black_24dp);
                drawable.mutate().setColorFilter(actionBarColor, PorterDuff.Mode.MULTIPLY);
                et.setCompoundDrawables(null, null, null, null);
                int searchiconId = context.getResources().getIdentifier("android:id/search_button", null, null);
                ImageView imgView = (ImageView) findViewById(searchiconId);
                imgView.setImageDrawable(drawable);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        toolbar.setNavigationOnClickListener(v -> dismiss());
        if (fromTranslator) {
            languagesHeader.setVisibility(View.VISIBLE);
            divider2.setVisibility(View.VISIBLE);
            divider1.setVisibility(View.VISIBLE);
            favoritesLanguagesRecycler = findViewById(R.id.favoritesLanguagesList);
            favoritesLanguagesRecycler.setVisibility(View.VISIBLE);
            ArrayList<Languages> languages = getLanguages(context, Locale.getDefault().getLanguage());
            ArrayList<Languages> favoritesList = new ArrayList<>();
            if (favoritesSet != null)
                for (String lang : favoritesSet) {
                    for (Languages language : languages)
                        if (lang.equals(language.getLanguageCode())) {
                            favoritesList.add(new Languages(language.getLanguageName(), language.getLanguageCode(), language.getFlagCode()));
                        }
                }
            if (favoritesList.size() > 0) favoritesHeader.setVisibility(View.VISIBLE);
            else favoritesHeader.setVisibility(View.GONE);
            LinearLayoutManager llmF = new LinearLayoutManager(context);
            favoritesLanguagesRecycler.setLayoutManager(llmF);
            LanguageSelectorAdapter favoritesLanguageSelectorAdapter = new LanguageSelectorAdapter(favoritesList, this, textColor, false, true, true);
            favoritesLanguagesRecycler.setAdapter(favoritesLanguageSelectorAdapter);
            favoritesLanguagesRecycler.setBackgroundColor(backgroundColor);
            favoritesLanguagesRecycler.addItemDecoration(new DividerItemDecoration(context, R.drawable.divider, 0));
        }
        if (favoritesSet != null)
            Log.e("favoritesSet", favoritesSet.toString());
        languageRecycler = findViewById(R.id.languagesList);
        LinearLayoutManager llm = new LinearLayoutManager(context);
        languageRecycler.setLayoutManager(llm);
        languageSelectorAdapter = new LanguageSelectorAdapter(languages, this, textColor, inMyProfile, false, fromTranslator);
        languageSelectorAdapter.setFavorites(getSetOfFavorites());
        languageRecycler.setAdapter(languageSelectorAdapter);
        languageRecycler.setBackgroundColor(backgroundColor);
        languageRecycler.addItemDecoration(new DividerItemDecoration(context, R.drawable.divider, 0));
    }

    public MenuInflater getMenuInflater() {
        return new MenuInflater(getContext());
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        filtered.clear();
        for (Languages language : languages) {
            if (language.getLanguageName().toLowerCase().contains(query.toLowerCase())) {
                filtered.add(language);
            }
        }
        languageSelectorAdapter.setLanguages(filtered);
        languageSelectorAdapter.notifyDataSetChanged();
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        filtered.clear();
        for (Languages language : languages) {
            if (language.getLanguageName().toLowerCase().contains(newText.toLowerCase())) {
                filtered.add(language);
            }
        }
        languageSelectorAdapter.setLanguages(filtered);
        languageSelectorAdapter.notifyDataSetChanged();
        return false;
    }


    private void addUserLanguage(String languageCode) {
        LanguageClient.getLanguageApi().addUserLanguage(userId, getCredentials(), new Lang(languageCode)).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response.isSuccessful()) {
                    Realm realm = Realm.getDefaultInstance();
                    UserLanguages userLanguages = realm.where(UserLanguages.class).findFirst();
                    realm.beginTransaction();
                    realm.copyToRealmOrUpdate(userLanguages);
                    realm.commitTransaction();
                    dismiss();
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                dismiss();
            }
        });
    }

    private Map<String, String> getCredentials() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("token", userToken);
        return params;
    }


    @Override
    public void onClick(Languages languages, int position) {
        Log.e("onClick", "inDialog");
        if (justGetLanguageCode) {
            chosenLanguage = languages.getLanguageName();
            chosenLanguageCode = languages.getLanguageCode();
            dismiss();
        } else {
            findViewById(R.id.wait_view).setVisibility(View.VISIBLE);
            if (changeMain) changeUserMainLanguage(talkerLanguageList, languages.getLanguageCode());
            else
                addUserLanguage(languages.getLanguageCode());
        }
    }

    @Override
    public void onFavoriteClick(String code, boolean isFavorite) {
        Log.e("onFavoriteClick", code + isFavorite + "");
        if (!isFavorite)
            writeToFavorites(code);
        else removeFromFavorites(code);
        favoritesSet = getSetOfFavorites();
        ArrayList<Languages> favoritesList = new ArrayList<>();
        if (favoritesSet != null)
            for (String lang : favoritesSet) {
                for (Languages language : languages)
                    if (lang.equals(language.getLanguageCode())) {
                        favoritesList.add(new Languages(language.getLanguageName(), language.getLanguageCode(), language.getFlagCode()));
                    }
            }
        if (favoritesLanguagesRecycler.getAdapter() != null)
            ((LanguageSelectorAdapter) favoritesLanguagesRecycler.getAdapter()).setLanguages(favoritesList);
        if (favoritesList.size() > 0) {
            favoritesHeader.setVisibility(View.VISIBLE);
        } else favoritesHeader.setVisibility(View.GONE);
        languageSelectorAdapter.setFavorites(favoritesSet);
        favoritesLanguagesRecycler.getAdapter().notifyDataSetChanged();
        languageRecycler.getAdapter().notifyDataSetChanged();
    }

    private SharedPreferences sharedPreferences;

    private Set<String> getSetOfFavorites() {
        return sharedPreferences.getStringSet(FAVOURITES_SHARED, null);
    }

    private void writeToFavorites(String languageCode) {
        Set<String> tempList = sharedPreferences.getStringSet(FAVOURITES_SHARED, null);
        if (tempList != null)
            tempList.add(languageCode);
        else {
            tempList = new HashSet<>();
            tempList.add(languageCode);
        }
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(FAVOURITES_SHARED);
        editor.commit();
        editor.putStringSet(FAVOURITES_SHARED, tempList);
        editor.commit();
        Log.e("stringset", sharedPreferences.getStringSet(FAVOURITES_SHARED, null).toString());
    }

    private void removeFromFavorites(String languageCode) {
        Set<String> tempList = sharedPreferences.getStringSet(FAVOURITES_SHARED, null);
        if (tempList != null)
            tempList.remove(languageCode);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(FAVOURITES_SHARED);
        editor.commit();
        editor.putStringSet(FAVOURITES_SHARED, tempList);
        editor.commit();
        Log.e("stringset", sharedPreferences.getStringSet(FAVOURITES_SHARED, null).toString());
    }

    @Override
    public void onDeleteClick(Languages languages, int position) {
        Log.e("onClick", "inDialog on delete");
    }

    private void changeUserMainLanguage(Langs languages, String newMainCode) {
        List<Integer> indexesForDeleting = new ArrayList<>();
        for (int i = 0; i < languages.getLangs().size(); i++) {
            if (languages.getLangs().get(i).equals(newMainCode)) {
                indexesForDeleting.add(i);
            }
        }
        for (int i : indexesForDeleting) {
            languages.getLangs().remove(i);
        }
        languages.getLangs().add(0, newMainCode);
        LanguageClient.getLanguageApi().editLanguages(userId, getCredentials(), languages).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                dismiss();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                dismiss();
            }
        });
    }

    private ArrayList<Languages> getLanguages(Context context, String locale) {
        ArrayList<Languages> lList = new ArrayList<>();
        String filename = locale + ".xml";
        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setValidating(false);
            XmlPullParser myxml = factory.newPullParser();
            InputStream raw = context.getAssets().open(filename);
            myxml.setInput(raw, null);
            int eventType = myxml.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {

                String name = "";
                String code = "";

                if (eventType == XmlPullParser.START_TAG
                        && myxml.getName().equals("key"))
                //        && myxml.getText().equals("name")
                {
                    eventType = myxml.next();
                    // Log.d("---", " 1 ="+myxml.getName());
                    eventType = myxml.next();
                    // Log.d("---", " 2 ="+myxml.getName());
                    eventType = myxml.next();
                    // Log.d("---", " 3 ="+myxml.getName());
                    eventType = myxml.next();
                    // Log.d("---", " 4 ="+myxml.getName());
                    eventType = myxml.next();
                    // Log.d("---", " 5 ="+myxml.getName());
                    name = myxml.getText();
                    // Log.d("---", "name ="+name);
                    eventType = myxml.next();
                    //Log.d("---", " 6 ="+myxml.getName());
                    eventType = myxml.next();
                    // Log.d("---", " 7 ="+myxml.getName());
                    eventType = myxml.next();
                    // Log.d("---", " 8 ="+myxml.getName());
                    eventType = myxml.next();
                    //  Log.d("---", " 9 ="+myxml.getName());
                    eventType = myxml.next();
                    //  Log.d("---", " 10 ="+myxml.getName());
                    eventType = myxml.next();
                    // Log.d("---", " 11 ="+myxml.getName());
                    eventType = myxml.next();
                    // Log.d("---", " 12 ="+myxml.getName());
                    eventType = myxml.next();
                    //  Log.d("---", " 13 ="+myxml.getName());
                    code = myxml.getText();
                    // Log.d("---", " code= "+code);
                    lList.add(new Languages(name, code));
                }
                eventType = myxml.next();
            }
        } catch (Exception e) {
            Log.d("---", "Excepetion : " + e.getMessage());
        }
        Collections.sort(lList, new Comparator<Languages>() {
            @Override
            public int compare(Languages o1, Languages o2) {
                return o1.getLanguageName().compareTo(o2.getLanguageName());
            }
        });
        return lList;
    }
}
