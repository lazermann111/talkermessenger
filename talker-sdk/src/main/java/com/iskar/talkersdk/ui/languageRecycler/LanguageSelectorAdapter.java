package com.iskar.talkersdk.ui.languageRecycler;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.iskar.talkersdk.R;

import java.util.List;
import java.util.Set;

/**
 * Created by karasinboots on 03.01.2018.
 */

public class LanguageSelectorAdapter extends RecyclerView.Adapter<LanguageSelectorViewHolder> {

    private List<Languages> languages;
    private Set<String> favorites;
    private RecyclerOnClick recyclerOnClick = null;
    private int textColor;
    private boolean inMyProfile;
    private boolean isFavorites;
    private boolean fromTranslator;
    public LanguageSelectorAdapter(List<Languages> languagesList, RecyclerOnClick recyclerOnClick, int textColor, boolean inMyProfie, boolean isFavorites, boolean fromTranslator) {
        this.textColor = textColor;
        this.languages = languagesList;
        this.recyclerOnClick = recyclerOnClick;
        this.inMyProfile = inMyProfie;
        this.isFavorites = isFavorites;
        this.fromTranslator = fromTranslator;
    }
    public List<Languages> getLanguages() {
        return languages;
    }

    public void setLanguages(List<Languages> languages) {
        this.languages = languages;
    }

    public void setFavorites(Set<String> favorites) {
        this.favorites = favorites;
    }

    @Override
    public LanguageSelectorViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.language_item, viewGroup, false);
        return new LanguageSelectorViewHolder(v);
    }

    @Override
    public void onBindViewHolder(LanguageSelectorViewHolder viewHolder, int i) {
        Languages item = languages.get(i);
        Context context = viewHolder.flag.getContext();
        viewHolder.language.setText(item.getLanguageName());
        viewHolder.language.setTextColor(textColor);
        viewHolder.flag.setImageDrawable(context.getResources().getDrawable(R.drawable.world));
        if (inMyProfile || fromTranslator)
            viewHolder.language.setOnClickListener(v -> recyclerOnClick.onClick(item, i));
        if (inMyProfile) {
            viewHolder.language.setOnLongClickListener(v -> {
                recyclerOnClick.onDeleteClick(item, i);
                return true;
            });
        }

        if (fromTranslator) {
            if (isFavorites) {
                viewHolder.favorite.setImageDrawable(context.getResources().getDrawable(R.drawable.star_filled));
                viewHolder.favorite.setVisibility(View.VISIBLE);
                viewHolder.favorite.setTag("active");
            } else {
                viewHolder.favorite.setImageDrawable(context.getResources().getDrawable(R.drawable.star_outlined));
                viewHolder.favorite.setVisibility(View.VISIBLE);
                viewHolder.favorite.setTag("inactive");
                if (favorites != null && !favorites.isEmpty())
                    for (String code : favorites)
                        if (item.getLanguageCode().equals(code)) {
                            viewHolder.favorite.setImageDrawable(context.getResources().getDrawable(R.drawable.star_filled));
                            viewHolder.favorite.setVisibility(View.VISIBLE);
                            viewHolder.favorite.setTag("active");
                        }
            }
            final boolean active = viewHolder.favorite.getTag().equals("active");
            viewHolder.favorite.setOnClickListener(v -> recyclerOnClick.onFavoriteClick(item.getLanguageCode(), active));
        } else viewHolder.favorite.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return languages.size();
    }


}

