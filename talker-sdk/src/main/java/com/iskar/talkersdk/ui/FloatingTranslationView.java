package com.iskar.talkersdk.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

import com.iskar.talkersdk.R;

/**
 * Created by karasinboots on 31.12.2017.
 */

class FloatingTranslationView extends RelativeLayout {

    public FloatingTranslationView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView();
    }

    public FloatingTranslationView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    private void initView() {
        View view = inflate(getContext(), R.layout.floating_translation_layout, null);
        addView(view);
    }
}
