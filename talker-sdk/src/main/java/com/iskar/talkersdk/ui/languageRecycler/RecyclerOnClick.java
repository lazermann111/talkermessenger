package com.iskar.talkersdk.ui.languageRecycler;

/**
 * Created by karasinboots on 09.01.2018.
 */

public interface RecyclerOnClick {
    void onClick(Languages languages, int position);

    void onFavoriteClick(String code, boolean isFavorite);

    void onDeleteClick(Languages languages, int position);
}
