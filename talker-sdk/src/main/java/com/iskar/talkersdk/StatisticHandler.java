package com.iskar.talkersdk;

import com.iskar.talkersdk.callback.StatisticInterface;
import com.iskar.talkersdk.model.Statistics;
import com.iskar.talkersdk.model.StatisticsPost;
import com.iskar.talkersdk.rest.client.TranslationClient;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.iskar.talkersdk.constants.ApiConsts.STATISTICS_URL;

/**
 * Created by karasinboots on 18.03.2018.
 */

public class StatisticHandler {
    private String mUserId;
    private String mToken;
    private StatisticInterface mStatisticInterface;

    public StatisticHandler(String mUserId, String mToken, StatisticInterface statisticInterface) {
        this.mUserId = mUserId;
        this.mToken = mToken;
        this.mStatisticInterface = statisticInterface;
    }

    public void getStatistics(String dateStart, String dateEnd) {
        TranslationClient.getTranslationApi().getStatistics(STATISTICS_URL + mUserId, getCredentials(mToken), new StatisticsPost(mUserId, dateStart, dateEnd))
                .enqueue(new Callback<Statistics>() {
                             @Override
                             public void onResponse(Call<Statistics> call, Response<Statistics> response) {
                                 if (response.isSuccessful() && response.body() != null) {
                                     mStatisticInterface.onStatisticReceived(response.body());
                                 }
                             }

                             @Override
                             public void onFailure(Call<Statistics> call, Throwable t) {
                                 mStatisticInterface.onFailure(t.getMessage());
                             }
                         }
                );
    }

    private Map<String, String> getCredentials(String userToken) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("token", userToken);
        return params;
    }
}
