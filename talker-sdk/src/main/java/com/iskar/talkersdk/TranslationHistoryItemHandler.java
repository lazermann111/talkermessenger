package com.iskar.talkersdk;

import android.content.Context;

import com.iskar.talkersdk.model.TranslationHistoryItem;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

public class TranslationHistoryItemHandler {

    private Context mContext;

    public TranslationHistoryItemHandler(Context mContext) {
        this.mContext = mContext;
    }

    private void configureRealm() {
        Realm.init(mContext);
        RealmConfiguration config = new RealmConfiguration
                .Builder().deleteRealmIfMigrationNeeded().name("libraryRealm")
                .modules(new com.iskar.talkersdk.realm.RealmModule())
                .build();
        Realm.setDefaultConfiguration(config);
    }

    public void addTranslationHistoryItem(TranslationHistoryItem item) {
        configureRealm();
        Realm realm = Realm.getDefaultInstance();
        if (realm.where(TranslationHistoryItem.class)
                .equalTo("originalText", item.getOriginalText())
                .equalTo("originalLanguageCode", item.getOriginalLanguageCode())
                .equalTo("translatedLanguageCode", item.getTranslatedLanguageCode())
                .equalTo("translatedText", item.getTranslatedText()).findFirst() == null) {
            realm.beginTransaction();
            realm.copyToRealm(item);
            realm.commitTransaction();
        }
        realm.close();
    }

    public List<TranslationHistoryItem> getAllTranslationHistoryItems() {
        configureRealm();
        Realm realm = Realm.getDefaultInstance();
        RealmResults<TranslationHistoryItem> items = realm.where(TranslationHistoryItem.class).findAll();
        List<TranslationHistoryItem> translationHistoryItems = realm.copyFromRealm(items);
        realm.close();
        return translationHistoryItems;
    }

    public void deleteItem(TranslationHistoryItem item) {
        configureRealm();
        Realm realm = Realm.getDefaultInstance();
        TranslationHistoryItem realmItem = realm.where(TranslationHistoryItem.class)
                .equalTo("originalText", item.getOriginalText())
                .equalTo("originalLanguageCode", item.getOriginalLanguageCode())
                .equalTo("translatedLanguageCode", item.getTranslatedLanguageCode())
                .equalTo("translatedText", item.getTranslatedText()).findFirst();
        realm.beginTransaction();
        if (realmItem != null)
            realmItem.deleteFromRealm();
        realm.commitTransaction();
        realm.close();
    }

    public void deleteAllItems() {
        configureRealm();
        Realm realm = Realm.getDefaultInstance();
        RealmResults<TranslationHistoryItem> items = realm.where(TranslationHistoryItem.class).findAll();
        realm.beginTransaction();
        items.deleteAllFromRealm();
        realm.commitTransaction();
        realm.close();
    }
}
